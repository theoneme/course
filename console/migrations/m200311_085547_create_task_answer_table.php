<?php

use yii\db\Migration;

/**
 * Handles the creation of table `task_answer`.
 */
class m200311_085547_create_task_answer_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('task_answer', [
            'id' => $this->primaryKey(),
            'task_id' => $this->integer()->notNull(),
            'correct' => $this->boolean()
        ]);

        $this->addForeignKey('fk_task_answer_task_id', 'task_answer', 'task_id', 'task', 'id', 'CASCADE');
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('task_answer');
    }
}
