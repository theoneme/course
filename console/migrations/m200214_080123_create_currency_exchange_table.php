<?php

use yii\db\Migration;

/**
 * Handles the creation of table `currency_exchange`.
 */
class m200214_080123_create_currency_exchange_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('currency_exchange', [
            'code_from' => $this->string(3),
            'code_to' => $this->string(3),
            'ratio' => $this->decimal(8, 4)
        ]);

        $this->addPrimaryKey('pk_code_from_code_to', 'currency_exchange', ['code_from', 'code_to']);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('currency_exchange');
    }
}
