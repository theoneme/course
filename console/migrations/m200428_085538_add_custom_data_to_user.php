<?php

use yii\db\Migration;

/**
 * Class m200428_085538_add_custom_data_to_user
 */
class m200428_085538_add_custom_data_to_user extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addColumn('user', 'custom_data', $this->binary());
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropColumn('user', 'custom_data');
    }
}
