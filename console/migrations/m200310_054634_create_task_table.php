<?php

use yii\db\Migration;

/**
 * Handles the creation of table `task`.
 */
class m200310_054634_create_task_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('task', [
            'id' => $this->primaryKey(),
            'category_id' => $this->integer()->notNull(),
            'type' => $this->smallInteger(),
        ]);

        $this->addForeignKey('fk_task_category_id', 'task', 'category_id', 'category', 'id', 'CASCADE');

        $this->createIndex('index_task_type', 'task', 'type');
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('task');
    }
}
