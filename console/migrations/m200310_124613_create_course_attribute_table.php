<?php

use yii\db\Migration;

/**
 * Handles the creation of table `course_attribute`.
 */
class m200310_124613_create_course_attribute_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('course_attribute', [
            'id' => $this->primaryKey(),
            'attribute_id' => $this->integer()->notNull(),
            'course_id' => $this->integer()->notNull(),
            'value' => $this->integer()->notNull(),
            'entity_alias' => $this->string(35)->notNull(),
            'value_alias' => $this->string(55)->notNull(),
            'value_number' => $this->float(),
        ]);
        $this->addForeignKey('fk_course_attribute_attribute_id', 'course_attribute', 'attribute_id', 'attribute', 'id', 'CASCADE');
        $this->addForeignKey('fk_course_attribute_course_id', 'course_attribute', 'course_id', 'course', 'id', 'CASCADE');
        $this->addForeignKey('fk_course_attribute_value', 'course_attribute', 'value', 'attribute_value', 'id', 'CASCADE');

        $this->createIndex('index_combined_course_attribute', 'course_attribute', ['course_id', 'entity_alias', 'value_alias']);
        $this->createIndex('index_combined_course_attribute_two', 'course_attribute', ['course_id', 'entity_alias', 'value_number']);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('course_attribute');
    }
}
