<?php

use yii\db\Migration;

/**
 * Handles the creation of table `attribute`.
 */
class m200214_072002_create_attribute_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('attribute', [
            'id' => $this->primaryKey(),
            'alias' => $this->string(65)->notNull()->unique(),
            'type' => $this->smallInteger()->notNull(),
            'filter_type' => $this->smallInteger()->notNull(),
        ]);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('attribute');
    }
}
