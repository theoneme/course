<?php

use yii\db\Migration;

/**
 * Handles the creation of table `attribute_group`.
 */
class m200214_074827_create_attribute_group_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('attribute_group', [
            'id' => $this->primaryKey(),
            'entity' => $this->integer()->notNull(),
            'alias' => $this->string(35),
        ]);

        $this->createIndex('index_attribute_group_alias_entity_type', 'attribute_group', ['alias', 'entity'], true);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('attribute_group');
    }
}
