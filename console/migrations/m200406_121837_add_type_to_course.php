<?php

use yii\db\Migration;

/**
 * Class m200406_121837_add_type_to_course
 */
class m200406_121837_add_type_to_course extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addColumn('course', 'type', $this->smallInteger()->defaultValue(0));

        $this->createIndex('index_course_type', 'course', 'type');
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropColumn('course', 'type');
    }
}
