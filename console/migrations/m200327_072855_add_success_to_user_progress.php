<?php

use yii\db\Migration;

/**
 * Class m200327_072855_add_complete_to_user_progress
 */
class m200327_072855_add_success_to_user_progress extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addColumn('user_progress', 'success', $this->boolean()->defaultValue(true)->after('entity_id'));
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropColumn('user_progress', 'success');
    }
}
