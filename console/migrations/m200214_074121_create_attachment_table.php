<?php

use yii\db\Migration;

/**
 * Handles the creation of table `attachment`.
 */
class m200214_074121_create_attachment_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('attachment', [
            'id' => $this->primaryKey(),
            'entity' => $this->integer()->notNull(),
            'entity_id' => $this->integer()->notNull(),
            'content' => $this->string(255)->notNull(),
        ]);
        $this->createIndex('index_attachment_entity_entity_id', 'attachment', ['entity', 'entity_id']);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('attachment');
    }
}
