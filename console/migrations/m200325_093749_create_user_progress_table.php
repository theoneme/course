<?php

use yii\db\Migration;

/**
 * Handles the creation of table `user_progress`.
 */
class m200325_093749_create_user_progress_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('user_progress', [
            'id' => $this->primaryKey(),
            'user_id' => $this->integer()->notNull(),
            'entity' => $this->integer()->notNull(),
            'entity_id' => $this->integer()->notNull(),
            'custom_data' =>  $this->binary(),
        ]);

        $this->createIndex('index_user_progress_entity_entity_id', 'user_progress', ['user_id', 'entity_id', 'entity']);

        $this->addForeignKey('fk_user_id_user_progress', 'user_progress', 'user_id', 'user', 'id', 'CASCADE');
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('user_progress');
    }
}
