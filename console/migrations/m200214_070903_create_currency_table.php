<?php

use yii\db\Migration;

/**
 * Handles the creation of table `currency`.
 */
class m200214_070903_create_currency_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('currency', [
            'code' => $this->string(3)->notNull()->unique(),
            'title' => $this->string(32)->notNull(),
            'symbol_left' => $this->string(12)->null(),
            'symbol_right' => $this->string(12)->null(),
        ]);
        $this->addPrimaryKey('pk_currency_code', 'currency', 'code');
        $this->insert('currency', [
            'title' => 'Ruble',
            'code' => 'RUB',
            'symbol_left' => '',
            'symbol_right' => 'р.',
        ]);
        $this->insert('currency', [
            'title' => 'Dollar',
            'code' => 'USD',
            'symbol_left' => '$',
            'symbol_right' => '',
        ]);
        $this->insert('currency', [
            'title' => 'Euro',
            'code' => 'EUR',
            'symbol_left' => '€',
            'symbol_right' => '',
        ]);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('currency');
    }
}
