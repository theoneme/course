<?php

use common\models\Lesson;
use common\models\LessonCourse;
use yii\db\Migration;

/**
 * Class m200413_120054_remove_course_id_from_lesson
 */
class m200413_120054_remove_course_id_from_lesson extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $lessons = Lesson::find()->all();
        foreach ($lessons as $lesson) {
            (new LessonCourse(['lesson_id' => $lesson['id'], 'course_id' => $lesson['course_id']]))->save();
        }
        $this->dropForeignKey('fk_lesson_course_id', 'lesson');
        $this->dropColumn('lesson', 'course_id');
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->addColumn('lesson', 'course_id', $this->integer()->notNull());
        $lessons = LessonCourse::find()->select(['min(course_id) as course_id', 'lesson_id'])->groupBy('lesson_id')->all();
        foreach ($lessons as $lesson) {
            Lesson::updateAll(['course_id' => $lesson['course_id']], ['id' => $lesson['lesson_id']]);
        }
        $this->addForeignKey('fk_lesson_course_id', 'lesson', 'course_id', 'course', 'id', 'CASCADE');
    }
}
