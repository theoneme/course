<?php

use yii\db\Migration;

/**
 * Handles the creation of table `category`.
 */
class m200214_074860_create_category_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('{{%category}}', [
            'id' => $this->primaryKey(),
            'lft' => $this->integer()->notNull(),
            'rgt' => $this->integer()->notNull(),
            'lvl' => $this->integer()->notNull(),
            'root' => $this->integer()->notNull()->defaultValue(0),
            'attribute_group_alias' => $this->string(35)->null(),
        ]);
        $this->createIndex('index_category_lft', 'category', 'lft');
        $this->createIndex('index_category_rgt', 'category', 'rgt');
        $this->createIndex('index_category_lvl', 'category', 'lvl');
        $this->createIndex('index_category_root', 'category', 'root');
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('category');
    }
}
