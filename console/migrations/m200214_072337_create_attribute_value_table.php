<?php

use yii\db\Migration;

/**
 * Handles the creation of table `attribute_value`.
 */
class m200214_072337_create_attribute_value_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('attribute_value', [
            'id' => $this->primaryKey(),
            'alias' => $this->string(65)->notNull(),
            'status' => $this->smallInteger()->notNull(),
            'attribute_id' => $this->integer()->notNull()
        ]);
        $this->addForeignKey('fk_attribute_value_attribute_id', 'attribute_value', 'attribute_id', 'attribute', 'id', 'CASCADE');
        $this->createIndex('index_attribute_value_alias', 'attribute_value', 'alias');
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('attribute_value');
    }
}
