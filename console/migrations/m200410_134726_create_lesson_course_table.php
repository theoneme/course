<?php

use yii\db\Migration;

/**
 * Handles the creation of table `lesson_to_course`.
 */
class m200410_134726_create_lesson_course_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('lesson_course', [
            'course_id' => $this->integer()->notNull(),
            'lesson_id' => $this->integer()->notNull(),
        ]);

        $this->addForeignKey('fk_lesson_course_course_id', 'lesson_course', 'course_id', 'course', 'id', 'CASCADE');
        $this->addForeignKey('fk_lesson_course_lesson_id', 'lesson_course', 'lesson_id', 'lesson', 'id', 'CASCADE');

        $this->addPrimaryKey('pk_lesson_id_course_id', 'lesson_course', ['lesson_id', 'course_id']);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('lesson_course');
    }
}
