<?php

use yii\db\Migration;

/**
 * Handles the creation of table `lesson`.
 */
class m200310_054027_create_lesson_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('lesson', [
            'id' => $this->primaryKey(),
            'course_id' => $this->integer()->notNull(),
            'category_id' => $this->integer()->notNull(),
            'status' => $this->smallInteger(),
            'type' => $this->smallInteger(),
            'sort' => $this->smallInteger()
        ]);

        $this->addForeignKey('fk_lesson_category_id', 'lesson', 'category_id', 'category', 'id', 'CASCADE');
        $this->addForeignKey('fk_lesson_course_id', 'lesson', 'course_id', 'course', 'id', 'CASCADE');

        $this->createIndex('index_lesson_status', 'lesson', 'status');
        $this->createIndex('index_lesson_type', 'lesson', 'type');
        $this->createIndex('index_lesson_sort', 'lesson', 'sort');
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('lesson');
    }
}
