<?php

use yii\db\Migration;

/**
 * Handles the creation of table `course`.
 */
class m200310_053729_create_course_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('course', [
            'id' => $this->primaryKey(),
            'category_id' => $this->integer()->notNull(),
            'status' => $this->smallInteger()
        ]);

        $this->addForeignKey('fk_course_category_id', 'course', 'category_id', 'category', 'id', 'CASCADE');

        $this->createIndex('index_course_status', 'course', 'status');
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('course');
    }
}
