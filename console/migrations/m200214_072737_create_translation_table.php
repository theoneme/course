<?php

use yii\db\Migration;

/**
 * Handles the creation of table `translation`.
 */
class m200214_072737_create_translation_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('translation', [
            'id' => $this->primaryKey(),
            'entity' => $this->integer()->notNull(),
            'entity_id' => $this->integer()->notNull(),
            'key' => $this->string(55)->notNull(),
            'locale' => $this->string(5)->notNull(),
            'value' => $this->text()->notNull()
        ]);

        $this->createIndex('index_translation_entity_entity_id', 'translation', ['entity_id', 'entity']);
        $this->createIndex('index_translation_entity_locale_entity_id', 'translation', ['entity_id', 'locale', 'key', 'entity']);
        $this->execute("ALTER TABLE translation ADD FULLTEXT INDEX index_translation_value_fulltext (value)");
        $this->execute("ALTER TABLE `translation` ADD INDEX `index_key_value` (`key`, `value`(100));");
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('translation');
    }
}
