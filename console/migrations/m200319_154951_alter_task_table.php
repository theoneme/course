<?php

use yii\db\Migration;

/**
 * Class m200319_154951_alter_task_table
 */
class m200319_154951_alter_task_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addColumn('task', 'points', $this->smallInteger());
        $this->addColumn('task', 'minutes', $this->smallInteger());
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropColumn('task', 'points');
        $this->dropColumn('task', 'minutes');
    }
}
