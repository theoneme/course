<?php

use yii\db\Migration;

/**
 * Handles the creation of table `user_course`.
 */
class m200310_112950_create_user_course_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('user_course', [
            'user_id' => $this->integer()->notNull(),
            'course_id' => $this->integer()->notNull(),
            'token' => $this->string(32)
        ]);

        $this->addPrimaryKey('pk_user_id_course_id', 'user_course', ['user_id', 'course_id']);

        $this->addForeignKey('fk_user_course_course_id', 'user_course', 'course_id', 'course', 'id', 'CASCADE');
        $this->addForeignKey('fk_user_course_user_id', 'user_course', 'user_id', 'user', 'id', 'CASCADE');
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('user_course');
    }
}
