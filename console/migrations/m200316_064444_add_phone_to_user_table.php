<?php

use yii\db\Migration;

/**
 * Class m200316_064444_add_phone_to_user_table
 */
class m200316_064444_add_phone_to_user_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addColumn('user', 'phone', $this->string()->null()->unique()->after('email'));
        $this->alterColumn('user', 'email', $this->string()->null());
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropColumn('user', 'phone');
        $this->alterColumn('user', 'email', $this->string()->notNull());
    }
}
