<?php

use yii\db\Migration;

/**
 * Handles the creation of table `user_social`.
 */
class m200424_135902_create_user_social_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('user_social', [
            'id' => $this->primaryKey(),
            'user_id' => $this->integer()->notNull(),
            'source' => $this->string()->notNull(),
            'source_id' => $this->string()->notNull(),
            'data' => $this->binary()
        ]);

        $this->addForeignKey('fk_user_social_user_id', 'user_social', 'user_id', 'user', 'id', 'CASCADE');
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropForeignKey('fk_user_social_user_id', 'user_social');
        $this->dropTable('user_social');
    }
}
