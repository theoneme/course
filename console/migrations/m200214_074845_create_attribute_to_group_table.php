<?php

use yii\db\Migration;

/**
 * Handles the creation of table `attribute_to_group`.
 */
class m200214_074845_create_attribute_to_group_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('attribute_to_group', [
            'attribute_group_id' => $this->integer()->notNull(),
            'attribute_id' => $this->integer()->notNull(),
            'custom_data' =>  $this->binary(),
        ]);
        $this->createIndex('attribute_to_group_link', 'attribute_to_group', ['attribute_group_id', 'attribute_id'], true);
        $this->addForeignKey('fk_attribute_to_group_attribute_group', 'attribute_to_group', 'attribute_group_id', 'attribute_group', 'id', 'CASCADE');
        $this->addForeignKey('fk_attribute_to_group_attribute', 'attribute_to_group', 'attribute_id', 'attribute', 'id', 'CASCADE');
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('attribute_to_group');
    }
}
