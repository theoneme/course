<?php

use yii\db\Migration;

/**
 * Handles the creation of table `flag`.
 */
class m200217_064756_create_flag_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('flag', [
            'id' => $this->primaryKey(),
            'entity' => $this->string(15),
            'entity_id' => $this->integer(),
            'type' => $this->string(20),
            'value' => $this->string(40)
        ]);

        $this->createIndex('index_flag_entity_id_entity', 'flag', ['entity_id', 'entity']);
        $this->createIndex('index_flag_type', 'flag', 'type');
        $this->createIndex('index_flag_value', 'flag', 'value');
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('flag');
    }
}
