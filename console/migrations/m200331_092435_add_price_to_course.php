<?php

use yii\db\Migration;

/**
 * Class m200331_092435_add_price_to_course
 */
class m200331_092435_add_price_to_course extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addColumn('course', 'price', $this->integer()->defaultValue(0));
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropColumn('course', 'price');
    }
}
