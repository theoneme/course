<?php

use yii\db\Migration;

/**
 * Class m200325_093601_add_xp_to_lesson
 */
class m200325_093601_add_xp_to_lesson extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addColumn('lesson', 'xp', $this->smallInteger());
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropColumn('lesson', 'xp');
    }
}
