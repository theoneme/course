<?php

use yii\db\Migration;

/**
 * Class m200331_124056_alter_payment_method_table
 */
class m200331_124056_alter_payment_method_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->alterColumn('payment_method', 'alias', $this->string(50)->notNull());
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->alterColumn('payment_method', 'alias', $this->integer()->notNull());
    }
}
