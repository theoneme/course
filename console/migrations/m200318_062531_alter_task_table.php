<?php

use yii\db\Migration;

/**
 * Class m200318_062531_alter_lesson_table
 */
class m200318_062531_alter_task_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addColumn('task', 'lesson_id', $this->integer()->notNull());
        $this->addForeignKey('fk_task_lesson_id', 'task', 'lesson_id', 'lesson', 'id', 'CASCADE');
        $this->dropForeignKey('fk_task_category_id', 'task');
        $this->dropColumn('task', 'category_id');
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->addColumn('task', 'category_id', $this->integer()->notNull());
        $this->addForeignKey('fk_task_category_id', 'task', 'category_id', 'category', 'id', 'CASCADE');
        $this->dropForeignKey('fk_task_lesson_id', 'task');
        $this->dropColumn('task', 'lesson_id');
    }
}