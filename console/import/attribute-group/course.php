<?php

use common\models\AttributeGroup;

return [
    AttributeGroup::ENTITY_COURSE_CATEGORY => [
        'root' => [
            'common' => [
                'type' => [
                    'type' => 'textbox',
                    'group' => 'basic',
                    'note' => [
                        'title' => 'Type',
                        'body' => 'Specify type'
                    ],
                    'rules' => [
                        'string'
                    ]
                ],
            ],
        ],
//        'asd' => [
//            'common' => [
//                'stead_area' => [
//                    'type' => 'numberbox',
//                    'group' => 'stead',
//                    'note' => [
//                        'title' => 'Specify the area of stead',
//                        'body' => '<p>Enter the area of the stead in square meters.</p><p><b>For Example:</b> 800 </p>'
//                    ],
//                    'rules' => [
//                        'number'
//                    ]
//                ],
//            ],
//            'parent' => 'flats'
//        ]
    ]
];