<?php

namespace console\controllers;

use common\models\Attribute;
use common\models\Category;
use common\models\Translation;
use common\models\User;
use common\services\entities\AttributeService;
use common\services\entities\CategoryService;
use common\services\entities\UserService;
use Yii;
use yii\console\Controller;

/**
 * Class CommandController
 * @package console\controllers
 */
class DataController extends Controller
{
    /**
     * @throws \Throwable
     * @throws \yii\db\StaleObjectException
     */
    public function actionInsertCategories()
    {
        /** @var CategoryService $categoryService */
        $categoryService = Yii::$container->get(CategoryService::class);
        $categories = [
            'root' => [
                'title' => 'Root',
//                'children' => [
//                    'wine' => [
//                        'title' => 'Вино',
//                    ]
//                ]
            ]
        ];

        $this->processCategories($categoryService, $categories);
    }

    /**
     * @param CategoryService $categoryService
     * @param $categories
     * @param null $parentId
     */
    private function processCategories($categoryService, $categories, $parentId = null){
        foreach ($categories as $slug => $item) {
            $insertId = $categoryService->create([
                'MetaForm' => [
                    'ru-RU' => [
                        Translation::KEY_TITLE => $item['title'],
                        Translation::KEY_SLUG => $slug,
                    ]
                ],
                'attribute_group_alias' => $slug,
                'parent_id' => $parentId
            ]);

            if ($insertId && !empty($item['children'])) {
                $this->processCategories($categoryService, $item['children'], $insertId);
            }
        }
    }

    /**
     *
     */
    public function actionDropCategories()
    {
        Category::deleteAll();
        Translation::deleteAll(['entity' => Translation::ENTITY_CATEGORY]);
    }


    /**
     * @throws \Throwable
     * @throws \yii\db\StaleObjectException
     */
    public function actionInsertAttributes()
    {
        /** @var AttributeService $attributeService */
        $attributeService = Yii::$container->get(AttributeService::class);
        $attributes = [
            'type' =>  [
                'title' => 'Тип',
                'type' => Attribute::TYPE_STRING,
                'filter_type' => Attribute::FILTER_TYPE_DROPDOWN,
            ],
        ];

        foreach ($attributes as $alias => $item) {
            $attributeService->create([
                'MetaForm' => [
                    'ru-RU' => [
                        Translation::KEY_TITLE => $item['title'],
                    ]
                ],
                'alias' => $alias,
                'type' => $item['type'],
                'filter_type' => $item['filter_type']
            ]);
        }
    }

    /**
     *
     */
    public function actionDropAttributes()
    {
        Attribute::deleteAll();
        Translation::deleteAll(['entity' => Translation::ENTITY_ATTRIBUTE]);
    }

    /**
     * @throws \Throwable
     * @throws \yii\db\StaleObjectException
     */
    public function actionInsertAdminUser()
    {
        /** @var UserService $userService */
        $userService = Yii::$container->get(UserService::class);

            $userService->create([
                'email' => "admin@course.com",
                'username' => 'admin',
                'status' => User::STATUS_ACTIVE,
                'role' => User::ROLE_ADMIN,
                'password_hash' => Yii::$app->security->generatePasswordHash("qwe123asd")
            ]);
    }

    /**
     *
     */
    public function actionDropAdminUser()
    {
        User::deleteAll(['email' => "admin@course.com"]);
    }
}