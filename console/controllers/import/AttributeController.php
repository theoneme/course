<?php

namespace console\controllers\import;

use common\models\AttributeGroup;
use common\repositories\sql\AttributeRepository;
use common\repositories\sql\CategoryRepository;
use common\services\ApiClientService;
use Yii;
use yii\base\InvalidConfigException;
use yii\console\Controller;
use yii\helpers\ArrayHelper;

/**
 * Class AttributeController
 * @package console\controllers\import
 */
class AttributeController extends Controller
{
    /**
     * @var ApiClientService
     */
    private $_categoryRepository;
    /**
     * @var AttributeRepository
     */
    private $_attributeRepository;

    /**
     * AttributeController constructor.
     * @param $id
     * @param $module
     * @param CategoryRepository $categoryRepository
     * @param AttributeRepository $attributeRepository
     * @param array $config
     */
    public function __construct($id, $module,
                                CategoryRepository $categoryRepository,
                                AttributeRepository $attributeRepository,
                                array $config = [])
    {
        parent::__construct($id, $module, $config);
        $this->_categoryRepository = $categoryRepository;
        $this->_attributeRepository = $attributeRepository;
    }

    /**
     * @return bool
     * @throws InvalidConfigException
     * @throws \common\exceptions\RepositoryException
     * @throws \yii\db\Exception
     * @throws \yii\di\NotInstantiableException
     */
    public function actionAttributeGroups()
    {
        $transaction = Yii::$app->db->beginTransaction();
        $result = true;

        $dataFilePath = Yii::getAlias('@console') . '/import/attribute-group/attribute-groups.php';
        $data = file_exists($dataFilePath) ? include($dataFilePath) : [];

        AttributeGroup::deleteAll();

        foreach ($data as $entityKey => $entityData) {
            switch ($entityKey) {
                case AttributeGroup::ENTITY_COURSE_CATEGORY:
                    foreach ($entityData as $groupAlias => $groupData) {
                        $groupData = $this->extractGroupData($entityData, $groupAlias);
                        $this->createGroup($groupData, $groupAlias, $entityKey);
                    }

                    break;
            }
        }

        if ($result === true) {
            $transaction->commit();

        } else {
            $transaction->rollBack();
        }

        return $result;
    }

    /**
     * @param $entityData
     * @param $categoryKey
     * @return array|mixed|null
     */
    private function extractGroupData($entityData, $categoryKey)
    {
        $categoryData = $entityData[$categoryKey];
        $parent = ArrayHelper::remove($categoryData, 'parent');
        $result = ArrayHelper::remove($categoryData, 'common', []);
        if ($parent && !empty($entityData[$parent])) {
            $parentData = $this->extractGroupData($entityData, $parent);
            $result = ArrayHelper::merge($parentData, $result);
        }

        return $result;
    }

    /**
     * @param $data
     * @param $alias
     * @param $entity
     * @return bool
     */
    private function createGroup($data, $alias, $entity)
    {
        $group = new AttributeGroup(['alias' => $alias, 'entity' => $entity]);

        foreach ($data as $attributeKey => $attributeData) {

            $attributeObject = $this->_attributeRepository->findOneByCriteria(['alias' => $attributeKey]);
            if ($attributeObject !== null) {
                $a = $group->bind('groupAttributes');
                $a->attributes = [
                    'attribute_id' => $attributeObject->id,
                    'customDataArray' => $attributeData
                ];
            }
        }

        return $group->save();
    }
}