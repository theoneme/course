<?php

namespace console\forms\ar;

use common\forms\ar\UserForm as BaseUserForm;
use common\models\User;

/**
 * Class UserForm
 * @package common\forms\ar
 */
class UserForm extends BaseUserForm
{
    /**
     * @var string
     */
    public $password_hash;
    /**
     * @var string
     */
    public $status;
    /**
     * @var string
     */
    public $role;

    /**
     * @return array
     */
    public function rules()
    {
        return array_merge(parent::rules(), [
            ['password_hash', 'string'],
            ['status', 'default', 'value' => User::STATUS_ACTIVE],
            ['status', 'in', 'range' => [User::STATUS_ACTIVE, User::STATUS_DELETED, User::STATUS_REQUIRES_MODIFICATION, User::STATUS_REQUIRES_MODERATION]],
            ['role', 'default', 'value' => User::ROLE_USER],
            ['role', 'in', 'range' => [User::ROLE_USER, User::ROLE_MODERATOR, User::ROLE_ADMIN]],
        ]);
    }

    /**
     * @param bool $withValidation
     * @return bool
     */
    public function save($withValidation = true)
    {
        if(!empty($this->password_hash)) {
            $this->_user->password_hash = $this->password_hash;
        }
        if(!empty($this->status)) {
            $this->_user->status = $this->status;
        }
        if(!empty($this->role)) {
            $this->_user->role = $this->role;
        }

        return $this->_user->save($withValidation);
    }
}