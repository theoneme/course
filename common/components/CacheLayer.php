<?php

namespace common\components;

use common\mappers\TranslationsMapper;
use common\models\Category;
use common\models\Translation;
use common\repositories\sql\CategoryRepository;
use Yii;
use yii\base\Component;
use yii\base\InvalidConfigException;

/**
 * Class CacheLayer
 * @package common\components
 */
class CacheLayer extends Component
{
    /**
     * @var CategoryRepository
     */
    private $_categoryRepository;

    /**
     * CacheLayer constructor.
     * @param CategoryRepository $categoryRepository
     * @param array $config
     */
    public function __construct(CategoryRepository $categoryRepository, array $config = [])
    {
        parent::__construct($config);
        $this->_categoryRepository = $categoryRepository;
    }

    /**
     * @throws InvalidConfigException
     * @throws \common\exceptions\RepositoryException
     * @throws \yii\di\NotInstantiableException
     */
    public function prepareCache()
    {
        $this->prepareCategoryAliasCache();
    }

    /**
     * @param null $localeParam
     * @return array
     * @throws InvalidConfigException
     * @throws \common\exceptions\RepositoryException
     * @throws \yii\di\NotInstantiableException
     */
    protected function prepareCategoryAliasCache($localeParam = null)
    {
        $cache = Yii::$app->cache;
        $supportedLocales = Yii::$app->params['supportedLocales'];
        if ($localeParam !== null && (!array_key_exists($localeParam, $supportedLocales))) {
            throw new InvalidConfigException('Данная локаль недоступна');
        }
        $locales = $localeParam === null ? $supportedLocales : [$localeParam];
        foreach ($locales as $locale) {
            $result = [];
            $exists = $cache->exists("categoryAliases_{$locale}");
            if ($exists === false) {
                /** @var Category[] $categories */
                $categories = $this->_categoryRepository->with(['translations'])->groupBy('category.id')->findManyByCriteria();
                foreach ($categories as $category) {
                    $translations = TranslationsMapper::getMappedData($category->translations, TranslationsMapper::MODE_FULL);
                    foreach ($supportedLocales as $locale2 => $code2) {
                        if ($locale !== $locale2 && isset($translations[$locale][Translation::KEY_SLUG], $translations[$locale2][Translation::KEY_SLUG])) {
                            $result[$translations[$locale][Translation::KEY_SLUG]][$locale2] = $translations[$locale2][Translation::KEY_SLUG];
                        }
                    }
                }

                $cache->set("categoryAliases_{$locale}", $result, 60 * 60 * 24 * 10);
                if ($localeParam === $locale) {
                    return $result;
                }
            }
        }

        return [];
    }

    /**
     * @param $locale
     * @return array|bool|mixed
     * @throws InvalidConfigException
     * @throws \common\exceptions\RepositoryException
     * @throws \yii\di\NotInstantiableException
     */
    public function getCategoryAliasCache($locale)
    {
        $data = Yii::$app->cache->get("categoryAliases_{$locale}");

        if ($data === false) {
            $data = $this->prepareCategoryAliasCache($locale);
        }

        return $data;
    }
}
