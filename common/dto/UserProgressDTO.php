<?php

namespace common\dto;

use common\interfaces\DTOInterface;
use common\mappers\TranslationsMapper;
use common\models\UserProgress;
use Yii;

/**
 * Class UserProgressDTO
 * @package common\dto
 */
class UserProgressDTO implements DTOInterface
{
    /**
     * @var UserProgress
     */
    private $_userProgress;

    /**
     * UserProgressDTO constructor.
     * @param UserProgress $userProgress
     */
    public function __construct($userProgress)
    {
        $this->_userProgress = $userProgress;
    }

    /**
     * @param int $mode
     * @return array
     */
    public function getData($mode = self::MODE_FULL)
    {
        if ($mode === self::MODE_SHORT) {
            return $this->getShortData();
        }

        if ($mode === self::MODE_FULL) {
            return $this->getFullData();
        }

        return [];
    }

    /**
     * @return array
     */
    public function getFullData()
    {
        return [
            'id' => $this->_userProgress->id,
            'entity' => $this->_userProgress->entity,
            'entity_id' => $this->_userProgress->entity_id,
            'user_id' => $this->_userProgress->entity_id,
            'success' => $this->_userProgress->success,
            'customData' => $this->_userProgress->customDataArray,
        ];
    }

    /**
     * @return array
     */
    public function getShortData()
    {
        return [
            'id' => $this->_userProgress->id,
            'entity' => $this->_userProgress->entity,
            'entity_id' => $this->_userProgress->entity_id,
            'user_id' => $this->_userProgress->entity_id,
            'success' => $this->_userProgress->success,
            'customData' => $this->_userProgress->customDataArray,
        ];
    }
}