<?php

namespace common\dto;

use common\helpers\UtilityHelper;
use common\interfaces\DTOInterface;
use common\mappers\TranslationsMapper;
use common\models\PaymentMethod;
use common\models\Translation;
use yii\helpers\ArrayHelper;

/**
 * Class PaymentMethodDTO
 * @package common\dto
 */
class PaymentMethodDTO implements DTOInterface
{
    /**
     * @var PaymentMethod
     */
    private $_paymentMethod;

    /**
     * PaymentMethodDTO constructor.
     * @param PaymentMethod $paymentMethod
     */
    public function __construct(PaymentMethod $paymentMethod)
    {
        $this->_paymentMethod = $paymentMethod;
    }

    /**
     * @param $mode
     * @return array
     */
    public function getData($mode = self::MODE_FULL)
    {
        if ($mode === self::MODE_SHORT) {
            return $this->getShortData();
        }

        if ($mode === self::MODE_FULL) {
            return $this->getFullData();
        }

        return [];
    }

    /**
     * @return array
     */
    public function getFullData()
    {
        $translations = TranslationsMapper::getMappedData($this->_paymentMethod['translations'], TranslationsMapper::MODE_FULL);
        $currentTranslation = TranslationsMapper::getMappedData($this->_paymentMethod['translations']);

        return [
            'id' => $this->_paymentMethod->id,
            'alias' => $this->_paymentMethod->alias,
            'logo' => $this->_paymentMethod->logo,
            'sort' => $this->_paymentMethod->sort,
            'enabled' => $this->_paymentMethod->enabled,
            'title' => UtilityHelper::upperFirstLetter(ArrayHelper::remove($currentTranslation, Translation::KEY_TITLE)),
            'translations' => $translations,
        ];
    }

    /**
     * @return array
     */
    public function getShortData()
    {
        $currentTranslation = TranslationsMapper::getMappedData($this->_paymentMethod['translations']);

        return [
            'id' => $this->_paymentMethod->id,
            'alias' => $this->_paymentMethod->alias,
            'logo' => $this->_paymentMethod->logo,
            'sort' => $this->_paymentMethod->sort,
            'enabled' => $this->_paymentMethod->enabled,
            'title' => UtilityHelper::upperFirstLetter(ArrayHelper::remove($currentTranslation, Translation::KEY_TITLE)),
            'translations' => $currentTranslation,
        ];
    }
}