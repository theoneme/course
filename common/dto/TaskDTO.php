<?php

namespace common\dto;

use common\helpers\UtilityHelper;
use common\interfaces\DTOInterface;
use common\mappers\AttachmentsMapper;
use common\mappers\TaskAnswerMapper;
use common\mappers\TranslationsMapper;
use common\models\Task;
use common\models\Translation;
use Yii;
use yii\helpers\ArrayHelper;

/**
 * Class TaskDTO
 * @package common\dto
 */
class TaskDTO implements DTOInterface
{
    /**
     * @var Task
     */
    private $_task;

    /**
     * TaskDTO constructor.
     * @param Task $task
     */
    public function __construct($task)
    {
        $this->_task = $task;
    }

    /**
     * @param int $mode
     * @return array
     */
    public function getData($mode = self::MODE_FULL)
    {
        if ($mode === self::MODE_SHORT) {
            return $this->getShortData();
        }

        if ($mode === self::MODE_FULL) {
            return $this->getFullData();
        }

        return [];
    }

    /**
     * @return array
     */
    public function getFullData()
    {
        $translations = TranslationsMapper::getMappedData($this->_task['translations'], TranslationsMapper::MODE_FULL);
        $currentTranslation = TranslationsMapper::getMappedData($this->_task['translations']);
        $attachments = AttachmentsMapper::getMappedData($this->_task['attachments']);
        $answerAttachments = AttachmentsMapper::getMappedData($this->_task['answerAttachments']);
        $lessonTranslations = TranslationsMapper::getMappedData($this->_task['lesson']['translations']);
        $answers = TaskAnswerMapper::getMappedData($this->_task['answers'], TaskAnswerMapper::MODE_FULL);

        return [
            'id' => $this->_task['id'],
            'lesson_id' => $this->_task['lesson_id'],
            'type' => $this->_task['type'],
            'points' => $this->_task['points'],
            'minutes' => $this->_task['minutes'],
            'title' => UtilityHelper::upperFirstLetter(ArrayHelper::remove($currentTranslation, Translation::KEY_TITLE)),
            'description' => UtilityHelper::upperFirstLetter(ArrayHelper::remove($currentTranslation, Translation::KEY_DESCRIPTION)),
            'solution' => UtilityHelper::upperFirstLetter(ArrayHelper::remove($currentTranslation, Translation::KEY_SOLUTION)),
            'attachment' => $attachments[0] ?? null,
            'answerAttachment' => $answerAttachments[0] ?? null,
            'attachments' => $attachments,
            'answerAttachments' => $answerAttachments,
            'translations' => $translations,
            'lesson' => $lessonTranslations,
            'answers' => $answers,
        ];
    }

    /**
     * @return array
     */
    public function getShortData()
    {
        $currentTranslation = TranslationsMapper::getMappedData($this->_task['translations']);
        $attachments = AttachmentsMapper::getMappedData($this->_task['attachments']);
        $answerAttachments = AttachmentsMapper::getMappedData($this->_task['answerAttachments']);
        $lessonTranslations = TranslationsMapper::getMappedData($this->_task['lesson']['translations']);
        $answers = TaskAnswerMapper::getMappedData($this->_task['answers'], TaskAnswerMapper::MODE_SHORT);

        return [
            'id' => $this->_task['id'],
            'lesson_id' => $this->_task['lesson_id'],
            'type' => $this->_task['type'],
            'points' => $this->_task['points'],
            'minutes' => $this->_task['minutes'],
            'title' => UtilityHelper::upperFirstLetter(ArrayHelper::remove($currentTranslation, 'title')),
            'attachment' => $attachments[0] ?? null,
            'answerAttachment' => $answerAttachments[0] ?? null,
            'attachments' => $attachments,
            'answerAttachments' => $answerAttachments,
            'translations' => $currentTranslation,
            'lesson' => $lessonTranslations,
            'answers' => $answers,
        ];
    }
}