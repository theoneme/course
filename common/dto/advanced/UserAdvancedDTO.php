<?php

namespace common\dto\advanced;

use common\interfaces\DTOInterface;
use common\mappers\TranslationsMapper;
use common\models\User;
use Yii;

/**
 * Class UserAdvancedDTO
 * @package common\dto\advanced
 */
class UserAdvancedDTO implements DTOInterface
{
    /**
     * @var User
     */
    private $_user;

    /**
     * UserDTO constructor.
     * @param User $user
     */
    public function __construct(User $user)
    {
        $this->_user = $user;
    }

    /**
     * @param int $mode
     * @return array
     */
    public function getData($mode = self::MODE_FULL)
    {
        if ($mode === self::MODE_SHORT) {
            return $this->getShortData();
        }

        if ($mode === self::MODE_FULL) {
            return $this->getFullData();
        }

        return [];
    }

    /**
     * @return array
     */
    public function getFullData()
    {
        $translations = TranslationsMapper::getMappedData($this->_user->translations);
        $avatar = !empty($this->_user->avatar) ? $this->_user->avatar : '/images/agent-no-image.png';
        return [
            'id' => $this->_user->id,
            'username' => $this->_user->username,
            'email' => $this->_user->email,
            'avatar' => Yii::$app->mediaLayer->getThumb($avatar),
            'thumb' => Yii::$app->mediaLayer->getThumb($avatar, 'catalog'),
            'translations' => $translations,
            'created_at' => $this->_user->created_at,
            'status' => $this->_user['status'],
        ];
    }

    /**
     * @return array
     */
    public function getShortData()
    {
        $translations = TranslationsMapper::getMappedData($this->_user->translations);
        $avatar = !empty($this->_user->avatar) ? $this->_user->avatar : '/images/agent-no-image.png';
        return [
            'id' => $this->_user->id,
            'username' => $this->_user->username,
            'email' => $this->_user->email,
            'avatar' => Yii::$app->mediaLayer->getThumb($avatar, 'catalog'),
            'translations' => $translations,
            'created_at' => $this->_user->created_at,
            'status' => $this->_user['status'],
        ];
    }
}