<?php

namespace common\dto;

use common\helpers\UtilityHelper;
use common\interfaces\DTOInterface;
use common\mappers\TranslationsMapper;
use common\models\TaskAnswer;
use Yii;
use yii\helpers\ArrayHelper;

/**
 * Class TaskAnswerDTO
 * @package common\dto
 */
class TaskAnswerDTO implements DTOInterface
{
    /**
     * @var TaskAnswer
     */
    private $_taskAnswer;

    /**
     * TaskAnswerDTO constructor.
     * @param TaskAnswer $taskAnswer
     */
    public function __construct($taskAnswer)
    {
        $this->_taskAnswer = $taskAnswer;
    }

    /**
     * @param int $mode
     * @return array
     */
    public function getData($mode = self::MODE_FULL)
    {
        if ($mode === self::MODE_SHORT) {
            return $this->getShortData();
        }

        if ($mode === self::MODE_FULL) {
            return $this->getFullData();
        }

        return [];
    }

    /**
     * @return array
     */
    public function getFullData()
    {
        $translations = TranslationsMapper::getMappedData($this->_taskAnswer['translations'], TranslationsMapper::MODE_FULL);
        $currentTranslation = TranslationsMapper::getMappedData($this->_taskAnswer['translations']);

        return [
            'id' => $this->_taskAnswer['id'],
            'task_id' => $this->_taskAnswer['task_id'],
            'correct' => $this->_taskAnswer['correct'],
            'title' => UtilityHelper::upperFirstLetter(ArrayHelper::remove($currentTranslation, 'title')),
            'translations' => $translations,
        ];
    }

    /**
     * @return array
     */
    public function getShortData()
    {
        $currentTranslation = TranslationsMapper::getMappedData($this->_taskAnswer['translations']);

        return [
            'id' => $this->_taskAnswer['id'],
            'task_id' => $this->_taskAnswer['task_id'],
            'correct' => $this->_taskAnswer['correct'],
            'title' => UtilityHelper::upperFirstLetter(ArrayHelper::remove($currentTranslation, 'title')),
            'translations' => $currentTranslation,
        ];
    }
}