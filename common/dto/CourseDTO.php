<?php

namespace common\dto;

use common\components\CurrencyHelper;
use common\helpers\UtilityHelper;
use common\interfaces\DTOInterface;
use common\mappers\AttachmentsMapper;
use common\mappers\CourseAttributesMapper;
use common\mappers\LessonMapper;
use common\mappers\TranslationsMapper;
use common\models\Course;
use Yii;
use yii\helpers\ArrayHelper;

/**
 * Class CourseDTO
 * @package common\dto
 */
class CourseDTO implements DTOInterface
{
    /**
     * @var Course
     */
    private $_course;
    /**
     * @var array
     */
    private $_attributesData;
    /**
     * @var array
     */
    private $_attributeValuesData;

    /**
     * CourseDTO constructor.
     * @param Course $course
     */
    public function __construct($course, array $attributesData = [], array $attributeValuesData = [])
    {
        $this->_course = $course;
        $this->_attributesData = $attributesData;
        $this->_attributeValuesData = $attributeValuesData;
    }

    /**
     * @param int $mode
     * @return array
     */
    public function getData($mode = self::MODE_FULL)
    {
        if ($mode === self::MODE_SHORT) {
            return $this->getShortData();
        }

        if ($mode === self::MODE_FULL) {
            return $this->getFullData();
        }

        return [];
    }

    /**
     * @return array
     */
    public function getFullData()
    {
        $translations = TranslationsMapper::getMappedData($this->_course['translations'], TranslationsMapper::MODE_FULL);
        $currentTranslation = TranslationsMapper::getMappedData($this->_course['translations']);
        $categoryTranslations = TranslationsMapper::getMappedData($this->_course['category']['translations']);
        $images = AttachmentsMapper::getMappedData($this->_course['attachments']);
        $lessonIds = ArrayHelper::getColumn($this->_course['courseLessons'], 'lesson_id');
        $attributes = CourseAttributesMapper::getMappedData([
            'attributes' => $this->_attributesData,
            'attributeValues' => $this->_attributeValuesData
        ]);

        return [
            'id' => $this->_course['id'],
            'category_id' => $this->_course['category_id'],
            'status' => $this->_course['status'],
            'type' => $this->_course['type'],
            'price' => $this->_course['price'],
            'priceFormatted' => CurrencyHelper::convertAndFormat('RUB', Yii::$app->params['app_currency'], $this->_course['price']),
            'title' => UtilityHelper::upperFirstLetter(ArrayHelper::remove($currentTranslation, 'title')),
            'description' => UtilityHelper::upperFirstLetter(ArrayHelper::remove($currentTranslation, 'description')),
            'image' => $images[0] ?? Yii::$app->mediaLayer->getThumb(null),
            'images' => $images,
            'translations' => $translations,
            'category' => $categoryTranslations,
            'attributes' => $attributes,
            'lessonIds' => $lessonIds,
        ];
    }

    /**
     * @return array
     */
    public function getShortData()
    {
        $currentTranslation = TranslationsMapper::getMappedData($this->_course['translations']);
        $categoryTranslations = TranslationsMapper::getMappedData($this->_course['category']['translations']);
        $images = AttachmentsMapper::getMappedData($this->_course['attachments']);
        $thumbnails = AttachmentsMapper::getMappedData($this->_course['attachments'], AttachmentsMapper::MODE_THUMB);
        $lessons = LessonMapper::getMappedData($this->_course['lessons']);
        $attributes = CourseAttributesMapper::getMappedData([
            'attributes' => $this->_attributesData,
            'attributeValues' => $this->_attributeValuesData
        ]);

        return [
            'id' => $this->_course['id'],
            'status' => $this->_course['status'],
            'type' => $this->_course['type'],
            'price' => $this->_course['price'],
            'priceFormatted' => CurrencyHelper::convertAndFormat('RUB', Yii::$app->params['app_currency'], $this->_course['price']),
            'category_id' => $this->_course['category_id'],
            'title' => UtilityHelper::upperFirstLetter(ArrayHelper::remove($currentTranslation, 'title')),
            'description' => UtilityHelper::upperFirstLetter(ArrayHelper::remove($currentTranslation, 'description')),
            'image' => $thumbnails[0] ?? Yii::$app->mediaLayer->getThumb(null),
            'images' => $images,
            'category' => $categoryTranslations,
            'attributes' => $attributes,
            'lessons' => $lessons,
        ];
    }
}