<?php

namespace common\dto;

use common\helpers\UtilityHelper;
use common\interfaces\DTOInterface;
use common\mappers\AttachmentsMapper;
use common\mappers\TranslationsMapper;
use common\models\Lesson;
use Yii;
use yii\helpers\ArrayHelper;

/**
 * Class LessonDTO
 * @package common\dto
 */
class LessonDTO implements DTOInterface
{
    /**
     * @var Lesson
     */
    private $_lesson;

    /**
     * LessonDTO constructor.
     * @param Lesson $lesson
     */
    public function __construct($lesson)
    {
        $this->_lesson = $lesson;
    }

    /**
     * @param int $mode
     * @return array
     */
    public function getData($mode = self::MODE_FULL)
    {
        if ($mode === self::MODE_SHORT) {
            return $this->getShortData();
        }

        if ($mode === self::MODE_FULL) {
            return $this->getFullData();
        }

        return [];
    }

    /**
     * @return array
     */
    public function getFullData()
    {
        $translations = TranslationsMapper::getMappedData($this->_lesson['translations'], TranslationsMapper::MODE_FULL);
        $currentTranslation = TranslationsMapper::getMappedData($this->_lesson['translations']);
        $categoryTranslations = TranslationsMapper::getMappedData($this->_lesson['category']['translations']);
        $attachments = array_map(function ($value) {
            return $value['content'];
        }, $this->_lesson['attachments']);

        return [
            'id' => $this->_lesson['id'],
            'category_id' => $this->_lesson['category_id'],
            'status' => $this->_lesson['status'],
            'type' => $this->_lesson['type'],
            'sort' => $this->_lesson['sort'],
            'xp' => (int)$this->_lesson['xp'],
            'title' => UtilityHelper::upperFirstLetter(ArrayHelper::remove($currentTranslation, 'title')),
            'description' => UtilityHelper::upperFirstLetter(ArrayHelper::remove($currentTranslation, 'description')),
            'attachment' => $attachments[0] ?? Yii::$app->mediaLayer->getThumb(null),
            'attachments' => $attachments,
            'translations' => $translations,
            'category' => $categoryTranslations,
        ];
    }

    /**
     * @return array
     */
    public function getShortData()
    {
        $currentTranslation = TranslationsMapper::getMappedData($this->_lesson['translations']);
        $categoryTranslations = TranslationsMapper::getMappedData($this->_lesson['category']['translations']);
        $attachments = array_map(function ($value) {
            return $value['content'];
        }, $this->_lesson['attachments']);

        return [
            'id' => $this->_lesson['id'],
            'category_id' => $this->_lesson['category_id'],
            'status' => $this->_lesson['status'],
            'type' => $this->_lesson['type'],
            'sort' => $this->_lesson['sort'],
            'xp' => (int)$this->_lesson['xp'],
            'title' => UtilityHelper::upperFirstLetter(ArrayHelper::remove($currentTranslation, 'title')),
            'attachment' => $attachments[0] ?? Yii::$app->mediaLayer->getThumb(null),
            'attachments' => $attachments,
            'category' => $categoryTranslations,
            'translations' => $currentTranslation,
        ];
    }
}