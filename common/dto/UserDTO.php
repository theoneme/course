<?php

namespace common\dto;

use common\interfaces\DTOInterface;
use common\mappers\TranslationsMapper;
use common\models\User;
use Yii;

/**
 * Class UserDTO
 * @package common\dto
 */
class UserDTO implements DTOInterface
{
    /**
     * @var User
     */
    private $_user;

    /**
     * UserDTO constructor.
     * @param User $user
     */
    public function __construct($user)
    {
        $this->_user = $user;
    }

    /**
     * @param int $mode
     * @return array
     */
    public function getData($mode = self::MODE_FULL)
    {
        if ($mode === self::MODE_SHORT) {
            return $this->getShortData();
        }

        if ($mode === self::MODE_FULL) {
            return $this->getFullData();
        }

        return [];
    }

    /**
     * @return array
     */
    public function getFullData()
    {
        $translations = TranslationsMapper::getMappedData($this->_user->translations);
        $avatar = !empty($this->_user->avatar) ? $this->_user->avatar : '/images/agent-no-image.png';

        return [
            'id' => $this->_user->id,
            'username' => $this->_user->username,
            'email' => $this->_user->email,
            'phone' => $this->_user->phone,
            'avatar' => Yii::$app->mediaLayer->getThumb($avatar),
            'translations' => $translations,
            'created_at' => $this->_user->created_at,
            'status' => $this->_user['status'],
            'customData' => $this->_user->customDataArray,
        ];
    }

    /**
     * @return array
     */
    public function getShortData()
    {
        $translations = TranslationsMapper::getMappedData($this->_user->translations);
        $avatar = !empty($this->_user->avatar) ? $this->_user->avatar : '/images/agent-no-image.png';
        return [
            'id' => $this->_user->id,
            'username' => $this->_user->username,
            'email' => $this->_user->email,
            'phone' => $this->_user->phone,
            'avatar' => Yii::$app->mediaLayer->getThumb($avatar, 'catalog'),
            'translations' => $translations,
            'created_at' => $this->_user->created_at,
            'status' => $this->_user['status'],
            'customData' => $this->_user->customDataArray,
        ];
    }
}