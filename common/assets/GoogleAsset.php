<?php

namespace common\assets;

use Yii;
use yii\web\AssetBundle;

/**
 * Class GoogleAsset
 * @package frontend\assets
 */
class GoogleAsset extends AssetBundle
{
    /**
     * @var array
     */
    public $options = [];

    /**
     * @inheritdoc
     */
    public function init()
    {
        $key = Yii::$app->params['googleAPIKey'];
        $libraries = 'places';
        $language = Yii::$app->params['supportedLocales'][Yii::$app->language];
        $this->options = array_merge($this->options, array_filter([
            'key' => $key,
            'libraries' => $libraries,
            'language' => $language,
            'callback' => 'googleLoad'
        ]));

        $this->js[] = '//maps.googleapis.com/maps/api/js?'. http_build_query($this->options);
    }
}
