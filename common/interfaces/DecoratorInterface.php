<?php

namespace common\interfaces;

/**
 * Interface DecoratorInterface
 * @package common\interfaces
 */
interface DecoratorInterface
{
    public static function decorate($rawData);
}