<?php

namespace common\interfaces\repositories;

use common\interfaces\RepositoryInterface;

/**
 * Interface TaskRepositoryInterface
 * @package common\interfaces\repositories
 */
interface TaskRepositoryInterface extends RepositoryInterface
{

}