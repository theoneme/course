<?php

namespace common\interfaces\repositories;

use common\interfaces\RepositoryInterface;

/**
 * Interface AttributeRepositoryInterface
 * @package common\interfaces\repositories
 */
interface AttributeRepositoryInterface extends RepositoryInterface
{

}