<?php

namespace common\interfaces\repositories;

use common\interfaces\RepositoryInterface;

/**
 * Interface AttributeToGroupRepositoryInterface
 * @package common\interfaces\repositories
 */
interface AttributeToGroupRepositoryInterface extends RepositoryInterface
{

}