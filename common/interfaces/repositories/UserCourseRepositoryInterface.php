<?php

namespace common\interfaces\repositories;

use common\interfaces\RepositoryInterface;

/**
 * Interface UserCourseRepositoryInterface
 * @package common\interfaces\repositories
 */
interface UserCourseRepositoryInterface extends RepositoryInterface
{

}