<?php

namespace common\interfaces\repositories;

use common\interfaces\RepositoryInterface;

/**
 * Interface AttributeGroupRepositoryInterface
 * @package common\interfaces\repositories
 */
interface AttributeGroupRepositoryInterface extends RepositoryInterface
{

}