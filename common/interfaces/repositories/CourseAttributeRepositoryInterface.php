<?php

namespace common\interfaces\repositories;

use common\interfaces\RepositoryInterface;

/**
 * Interface CourseAttributeRepositoryInterface
 * @package common\interfaces\repositories
 */
interface CourseAttributeRepositoryInterface extends RepositoryInterface
{

}