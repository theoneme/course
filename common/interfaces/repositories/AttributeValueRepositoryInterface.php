<?php

namespace common\interfaces\repositories;

use common\interfaces\RepositoryInterface;

/**
 * Interface AttributeValueRepositoryInterface
 * @package common\interfaces\repositories
 */
interface AttributeValueRepositoryInterface extends RepositoryInterface
{

}