<?php

namespace common\interfaces\repositories;

use common\interfaces\RepositoryInterface;

/**
 * Interface AddressTranslationRepositoryInterface
 * @package common\interfaces\repositories
 */
interface AddressTranslationRepositoryInterface extends RepositoryInterface
{

}