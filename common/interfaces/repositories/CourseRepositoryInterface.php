<?php

namespace common\interfaces\repositories;

use common\interfaces\RepositoryInterface;

/**
 * Interface CourseRepositoryInterface
 * @package common\interfaces\repositories
 */
interface CourseRepositoryInterface extends RepositoryInterface
{

}