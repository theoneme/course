<?php

namespace common\interfaces\repositories;

use common\interfaces\RepositoryInterface;

/**
 * Interface UserSocialRepositoryInterface
 * @package common\interfaces\repositories
 */
interface UserSocialRepositoryInterface extends RepositoryInterface
{

}