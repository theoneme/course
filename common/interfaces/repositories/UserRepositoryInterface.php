<?php

namespace common\interfaces\repositories;

use common\interfaces\RepositoryInterface;

/**
 * Interface UserRepositoryInterface
 * @package common\interfaces\repositories
 */
interface UserRepositoryInterface extends RepositoryInterface
{

}