<?php

namespace common\interfaces\repositories;

use common\interfaces\RepositoryInterface;

/**
 * Interface UserProgressRepositoryInterface
 * @package common\interfaces\repositories
 */
interface UserProgressRepositoryInterface extends RepositoryInterface
{

}