<?php

namespace common\interfaces\repositories;

use common\interfaces\RepositoryInterface;

/**
 * Interface CategoryRepositoryInterface
 * @package common\interfaces\repositories
 */
interface CategoryRepositoryInterface extends RepositoryInterface
{

}