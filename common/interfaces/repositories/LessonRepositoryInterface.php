<?php

namespace common\interfaces\repositories;

use common\interfaces\RepositoryInterface;

/**
 * Interface LessonRepositoryInterface
 * @package common\interfaces\repositories
 */
interface LessonRepositoryInterface extends RepositoryInterface
{

}