<?php

namespace common\interfaces\repositories;

use common\interfaces\RepositoryInterface;

/**
 * Interface PaymentRepositoryInterface
 * @package common\interfaces\repositories
 */
interface PaymentRepositoryInterface extends RepositoryInterface
{

}