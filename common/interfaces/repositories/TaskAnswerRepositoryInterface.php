<?php

namespace common\interfaces\repositories;

use common\interfaces\RepositoryInterface;

/**
 * Interface TaskAnswerRepositoryInterface
 * @package common\interfaces\repositories
 */
interface TaskAnswerRepositoryInterface extends RepositoryInterface
{

}