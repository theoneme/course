<?php

namespace common\interfaces\repositories;

use common\interfaces\RepositoryInterface;

/**
 * Interface TranslationRepositoryInterface
 * @package common\interfaces\repositories
 */
interface TranslationRepositoryInterface extends RepositoryInterface
{

}