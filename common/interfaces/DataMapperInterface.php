<?php

namespace common\interfaces;

/**
 * Interface DataMapperInterface
 * @package common\interfaces
 */
interface DataMapperInterface
{
    public static function getMappedData($rawData);
}