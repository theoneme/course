<?php

namespace common\interfaces;

/**
 * Class EntityServiceInterface
 * @package common\interfaces
 */
interface EntityServiceInterface
{
    public function getOne($criteria);

    public function create($data);

    public function getMany($params, $config);

    public function delete($criteria);

    public function update($data, $criteria);
}