<?php

namespace common\interfaces;

/**
 * Interface ConverterInterface
 * @package console\interfaces
 */
interface ConverterInterface
{
    /**
     * @param $rawData
     * @return mixed
     */
    public function convertObject($rawData);
}