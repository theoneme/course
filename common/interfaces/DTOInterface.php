<?php

namespace common\interfaces;

/**
 * Interface DTOInterface
 * @package common\interfaces
 */
interface DTOInterface
{
    public const MODE_FULL = 0;
    public const MODE_SHORT = 10;

    public function getData($mode);
}