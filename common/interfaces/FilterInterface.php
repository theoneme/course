<?php

namespace common\interfaces;

/**
 * Interface FilterInterface
 * @package common\interfaces
 */
interface FilterInterface
{
    public function buildFilterArray($params);
}