<?php

namespace common\interfaces;

/**
 * Class SeoServiceInterface
 * @package common\interfaces
 */
interface SeoServiceInterface
{
    public function getSeo($templateCategory, $params = [], $locale = null);

    public function getTemplates($templateCategory);

}