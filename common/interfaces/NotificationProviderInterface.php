<?php

namespace common\interfaces;

/**
 * Interface NotificationProviderInterface
 * @package common\interfaces
 */
interface NotificationProviderInterface
{
    public function send($params);

}