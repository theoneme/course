<?php

namespace common\services;

use common\interfaces\repositories\PaymentRepositoryInterface;
use common\models\Payment;
use common\repositories\sql\UserCourseRepository;
use frontend\modules\preview\services\UserProgressMigrationService;
use Yii;
use yii\helpers\Url;

/**
 * Class PaymentProcessService
 * @package common\services\entities
 */
class PaymentProcessService
{
    /**
     * @var PaymentRepositoryInterface
     */
    private $_paymentRepository;
    /**
     * @var UserCourseRepository
     */
    private $_userCourseRepository;

    /**
     * PaymentProcessService constructor.
     * @param PaymentRepositoryInterface $paymentRepository
     * @param UserCourseRepository $userCourseRepository
     */
    public function __construct(PaymentRepositoryInterface $paymentRepository, UserCourseRepository $userCourseRepository)
    {
        $this->_paymentRepository = $paymentRepository;
        $this->_userCourseRepository = $userCourseRepository;
    }


    /**
     * Mark payment as complete and give ordered items
     * @param $payment
     */
    public function markComplete($payment)
    {
        $this->_paymentRepository->updateOneByCriteria(['id' => $payment['id']], ['status' => Payment::STATUS_PAID]);

        switch ($payment['type']) {
            case Payment::TYPE_COURSE:
                $this->giveCourse($payment);
                break;
        }
    }

    /**
     * Mark payment as refused
     * @param $payment
     */
    public function markRefused($payment)
    {
        $this->_paymentRepository->updateOneByCriteria(['id' => $payment['id']], ['status' => Payment::STATUS_REFUSED]);
    }

    /**
     * Get return url based on payment type
     * @param $payment
     * @return string|null
     */
    public function getReturnUrl($payment)
    {
        $returnUrl = null;
        switch ($payment['type']) {
            case Payment::TYPE_COURSE:
                $returnUrl = Url::to(['/dashboard/index']);
                break;
        }
        return $returnUrl;
    }

    /**
     * @param Payment $payment
     */
    private function giveCourse($payment){
        $this->_userCourseRepository->create([
            'user_id' => $payment['user_id'],
            'course_id' => $payment['customData']['course_id'],
            'token' => Yii::$app->security->generateRandomString(32),
        ]);
        /** @var UserProgressMigrationService $progressMigrationService */
        $progressMigrationService = Yii::$container->get(UserProgressMigrationService::class);
        $progressMigrationService->sessionToDb($payment['user_id'], $payment['customData']['course_id']);
    }
}