<?php

namespace common\services\entities;

use common\dto\CourseDTO;
use common\interfaces\EntityServiceInterface;
use common\interfaces\repositories\CourseRepositoryInterface;
use common\interfaces\RepositoryInterface;
use common\models\search\CourseSearch;
use Yii;
use yii\db\ActiveQuery;

/**
 * Class CourseService
 * @package common\services
 */
class CourseService implements EntityServiceInterface
{
    /**
     * @var CourseRepositoryInterface
     */
    private $_courseRepository;
    /**
     * @var CourseRepositoryInterface
     */
    private $_attributeService;
    /**
     * @var CourseRepositoryInterface
     */
    private $_attributeValueService;

    /**
     * CourseService constructor.
     * @param CourseRepositoryInterface $courseRepository
     * @param AttributeService $attributeService
     * @param AttributeValueService $attributeValueService
     */
    public function __construct(
        CourseRepositoryInterface $courseRepository,
        AttributeService $attributeService,
        AttributeValueService $attributeValueService
    )
    {
        $this->_courseRepository = $courseRepository;
        $this->_attributeService = $attributeService;
        $this->_attributeValueService = $attributeValueService;
    }

    /**
     * @param $criteria
     * @return array|null
     * @throws \Throwable
     * @throws \yii\base\InvalidConfigException
     */
    public function getOne($criteria)
    {
        $course = $this->_courseRepository
            ->joinWith(['translations'])
            ->with(['attachments', 'courseAttributes', 'courseLessons', 'category.translations' => function (ActiveQuery $query) {
                return $query->andWhere(['category_translations.locale' => [Yii::$app->language, 'en-GB']]);
            }])
            ->findOneByCriteria($criteria, true);

        if ($course === null) {
            return null;
        }

        $attributes = $attributeValues = ['items' => []];

        if (!empty($course['courseAttributes'])) {
            $attributeIDs = array_map(function ($attribute) {
                return $attribute['attribute_id'];
            }, $course['courseAttributes']);

            $attributeValueIDs = array_map(function ($attribute) {
                return $attribute['value'];
            }, $course['courseAttributes']);

            $attributes = $this->_attributeService->getMany(['id' => $attributeIDs], ['indexBy' => 'id']);
            $attributeValues = $this->_attributeValueService->getMany(['id' => $attributeValueIDs]);
        }

        $courseDTO = new CourseDTO($course, $attributes['items'], $attributeValues['items']);
        return $courseDTO->getData();
    }

    /**
     * @param $params
     * @param array $config
     * @return array
     * @throws \common\exceptions\RepositoryException
     * @throws \yii\base\InvalidConfigException
     * @throws \yii\di\NotInstantiableException
     */
    public function getMany($params, $config = [])
    {
        $courseRepository = $this->_courseRepository;
        $courseSearch = new CourseSearch($courseRepository, $config);

        return $courseSearch->search($params);
    }

    /**
     * @param $data
     * @return mixed
     */
    public function create($data)
    {
        return $this->_courseRepository->create($data);
    }

    /**
     * @param $criteria
     * @return bool
     */
    public function delete($criteria)
    {
        return $this->_courseRepository->deleteOneByCriteria($criteria);
    }

    /**
     * @param $data
     * @param $criteria
     * @return bool
     */
    public function update($data, $criteria)
    {
        return $this->_courseRepository->updateOneByCriteria($criteria, $data);
    }

    /**
     * @param RepositoryInterface $repository
     */
    public function setRepository(RepositoryInterface $repository)
    {
        $this->_courseRepository = $repository;
    }

    /**
     *
     */
    public function getRepository()
    {
        return $this->_courseRepository;
    }
}