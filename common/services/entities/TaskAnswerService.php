<?php

namespace common\services\entities;

use common\dto\TaskAnswerDTO;
use common\interfaces\EntityServiceInterface;
use common\interfaces\repositories\TaskAnswerRepositoryInterface;
use common\interfaces\RepositoryInterface;
use common\models\search\TaskAnswerSearch;

/**
 * Class TaskAnswerService
 * @package common\services\entities
 */
class TaskAnswerService implements EntityServiceInterface
{
    /**
     * @var TaskAnswerRepositoryInterface
     */
    private $_taskAnswerRepository;

    /**
     * TaskAnswerService constructor.
     * @param TaskAnswerRepositoryInterface $taskAnswerRepository
     */
    public function __construct(TaskAnswerRepositoryInterface $taskAnswerRepository)
    {
        $this->_taskAnswerRepository = $taskAnswerRepository;
    }

    /**
     * @param $criteria
     * @return array|null
     * @throws \Throwable
     * @throws \yii\base\InvalidConfigException
     */
    public function getOne($criteria)
    {
        $task = $this->_taskAnswerRepository
            ->joinWith(['translations'])
            ->with()
            ->findOneByCriteria($criteria, true);

        if ($task === null) {
            return null;
        }

        $taskDTO = new TaskAnswerDTO($task);
        return $taskDTO->getData();
    }

    /**
     * @param $params
     * @param array $config
     * @return array
     * @throws \common\exceptions\RepositoryException
     * @throws \yii\base\InvalidConfigException
     * @throws \yii\di\NotInstantiableException
     */
    public function getMany($params, $config = [])
    {
        $taskAnswerRepository = $this->_taskAnswerRepository;
        $taskSearch = new TaskAnswerSearch($taskAnswerRepository, $config);

        return $taskSearch->search($params);
    }

    /**
     * @param $data
     * @return mixed
     */
    public function create($data)
    {
        return $this->_taskAnswerRepository->create($data);
    }

    /**
     * @param $criteria
     * @return bool
     */
    public function delete($criteria)
    {
        return $this->_taskAnswerRepository->deleteOneByCriteria($criteria);
    }

    /**
     * @param $data
     * @param $criteria
     * @return bool
     */
    public function update($data, $criteria)
    {
        return $this->_taskAnswerRepository->updateOneByCriteria($criteria, $data);
    }

    /**
     * @param RepositoryInterface $repository
     */
    public function setRepository(RepositoryInterface $repository)
    {
        $this->_taskAnswerRepository = $repository;
    }

    /**
     *
     */
    public function getRepository()
    {
        return $this->_taskAnswerRepository;
    }
}