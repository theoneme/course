<?php

namespace common\services\entities;

use common\dto\TaskDTO;
use common\interfaces\EntityServiceInterface;
use common\interfaces\repositories\TaskRepositoryInterface;
use common\interfaces\RepositoryInterface;
use common\models\search\TaskSearch;
use common\models\Task;

/**
 * Class TaskService
 * @package common\services
 */
class TaskService implements EntityServiceInterface
{
    /**
     * @var TaskRepositoryInterface
     */
    private $_taskRepository;

    /**
     * TaskService constructor.
     * @param TaskRepositoryInterface $taskRepository
     */
    public function __construct(TaskRepositoryInterface $taskRepository)
    {
        $this->_taskRepository = $taskRepository;
    }

    /**
     * @param $criteria
     * @param array $config
     * @return array|null
     */
    public function getOne($criteria, $config = [])
    {
        $task = $this->_taskRepository
            ->joinWith(['translations'])
            ->with(['attachments', 'answerAttachments', 'lesson.translations', 'answers.translations'])
            ->orderBy($config['orderBy'] ?? [])
            ->findOneByCriteria($criteria, true);

        if ($task === null) {
            return null;
        }

        $taskDTO = new TaskDTO($task);
        return $taskDTO->getData();
    }

    /**
     * @param $params
     * @param array $config
     * @return array
     * @throws \common\exceptions\RepositoryException
     * @throws \yii\base\InvalidConfigException
     * @throws \yii\di\NotInstantiableException
     */
    public function getMany($params, $config = [])
    {
        $taskRepository = $this->_taskRepository;
        $taskSearch = new TaskSearch($taskRepository, $config);

        return $taskSearch->search($params);
    }

    /**
     * @param $data
     * @return mixed
     */
    public function create($data)
    {
        return $this->_taskRepository->create($data);
    }

    /**
     * @param $criteria
     * @return bool
     */
    public function delete($criteria)
    {
        return $this->_taskRepository->deleteOneByCriteria($criteria);
    }

    /**
     * @param $data
     * @param $criteria
     * @return bool
     */
    public function update($data, $criteria)
    {
        return $this->_taskRepository->updateOneByCriteria($criteria, $data);
    }

    /**
     * @param RepositoryInterface $repository
     */
    public function setRepository(RepositoryInterface $repository)
    {
        $this->_taskRepository = $repository;
    }

    /**
     *
     */
    public function getRepository()
    {
        return $this->_taskRepository;
    }
}