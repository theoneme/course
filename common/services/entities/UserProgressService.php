<?php

namespace common\services\entities;

use common\dto\UserProgressDTO;
use common\helpers\UtilityHelper;
use common\interfaces\EntityServiceInterface;
use common\interfaces\repositories\UserProgressRepositoryInterface;
use common\models\search\UserProgressSearch;
use common\models\UserProgress;

/**
 * Class UserProgressService
 * @package common\services\entities
 */
class UserProgressService implements EntityServiceInterface
{
    /**
     * @var UserProgressRepositoryInterface
     */
    private $_userProgressRepository;

    /**
     * UserProgressService constructor.
     * @param UserProgressRepositoryInterface $userProgressRepository
     */
    public function __construct(UserProgressRepositoryInterface $userProgressRepository)
    {
        $this->_userProgressRepository = $userProgressRepository;
    }

    /**
     * @param $criteria
     * @return array|null
     */
    public function getOne($criteria)
    {
        $user = $this->_userProgressRepository
            ->findOneByCriteria($criteria);

        if ($user === null) {
            return null;
        }

        $dto = new UserProgressDTO($user);
        return $dto->getData();
    }

    /**
     * @param $params
     * @param array $config
     * @return array
     */
    public function getMany($params, $config = [])
    {
        $search = new UserProgressSearch($this->_userProgressRepository, $config);
        return $search->search($params);
    }

    /**
     * @param $data
     * @return mixed
     */
    public function create($data)
    {
        return $this->_userProgressRepository->create($data);
    }

    /**
     * @param $data
     * @param $criteria
     * @return bool
     */
    public function update($data, $criteria)
    {
        return $this->_userProgressRepository->updateOneByCriteria($criteria, $data);
    }

    /**
     * @param $criteria
     * @return bool
     */
    public function delete($criteria)
    {
        return $this->_userProgressRepository->deleteOneByCriteria($criteria);
    }

    /**
     * @param $data
     * @return mixed
     */
    public function createOne($data)
    {
        $criteria = array_diff_key($data, ['customDataArray' => 0, 'success' => 0]);
        /* @var UserProgress $model */
        $model = $this->_userProgressRepository->findOneByCriteria($criteria);
        if ($model !== null) {
            return $model->load($data, '') && $model->save();
        }
        else {
            return $this->_userProgressRepository->create($data);
        }
    }
}