<?php

namespace common\services\entities;

use common\dto\LessonDTO;
use common\interfaces\EntityServiceInterface;
use common\interfaces\repositories\LessonRepositoryInterface;
use common\interfaces\RepositoryInterface;
use common\models\search\LessonSearch;
use Yii;
use yii\db\ActiveQuery;

/**
 * Class LessonService
 * @package common\services
 */
class LessonService implements EntityServiceInterface
{
    /**
     * @var LessonRepositoryInterface
     */
    private $_lessonRepository;

    /**
     * LessonService constructor.
     * @param LessonRepositoryInterface $lessonRepository
     */
    public function __construct(LessonRepositoryInterface $lessonRepository)
    {
        $this->_lessonRepository = $lessonRepository;
    }

    /**
     * @param $criteria
     * @return array|null
     * @throws \Throwable
     * @throws \yii\base\InvalidConfigException
     */
    public function getOne($criteria)
    {
        $lesson = $this->_lessonRepository
            ->joinWith(['translations'])
            ->with(['attachments', 'category.translations' => function (ActiveQuery $query) {
                return $query->andWhere(['category_translations.locale' => [Yii::$app->language, 'en-GB']]);
            }])
            ->findOneByCriteria($criteria, true);

        if ($lesson === null) {
            return null;
        }

        $lessonDTO = new LessonDTO($lesson);
        return $lessonDTO->getData();
    }

    /**
     * @param $params
     * @param array $config
     * @return array
     * @throws \common\exceptions\RepositoryException
     * @throws \yii\base\InvalidConfigException
     * @throws \yii\di\NotInstantiableException
     */
    public function getMany($params, $config = [])
    {
        $lessonRepository = $this->_lessonRepository;
        $lessonSearch = new LessonSearch($lessonRepository, $config);

        return $lessonSearch->search($params);
    }

    /**
     * @param $data
     * @return mixed
     */
    public function create($data)
    {
        return $this->_lessonRepository->create($data);
    }

    /**
     * @param $criteria
     * @return bool
     */
    public function delete($criteria)
    {
        return $this->_lessonRepository->deleteOneByCriteria($criteria);
    }

    /**
     * @param $data
     * @param $criteria
     * @return bool
     */
    public function update($data, $criteria)
    {
        return $this->_lessonRepository->updateOneByCriteria($criteria, $data);
    }

    /**
     * @param RepositoryInterface $repository
     */
    public function setRepository(RepositoryInterface $repository)
    {
        $this->_lessonRepository = $repository;
    }

    /**
     *
     */
    public function getRepository()
    {
        return $this->_lessonRepository;
    }
}