<?php

namespace common\services\seo;

use common\components\CurrencyHelper;
use common\helpers\UtilityHelper;
use common\mappers\SeoAttributesMapper;
use Yii;

/**
 * Class CatalogSeoService
 * @package common\services
 */
class CatalogSeoService extends SeoService
{
    public const TEMPLATE_CATEGORY = 0;

    /**
     * @param $templateCategory
     * @param array $params
     * @param null $locale
     * @return array
     * @throws \yii\base\InvalidConfigException
     * @throws \yii\di\NotInstantiableException
     */
    public function getSeo($templateCategory, $params = [], $locale = null)
    {
        $processedParams = $this->getAttributeParams($templateCategory, $params, $locale);

        if (isset($params['count'])) {
            $processedParams['count'] = $params['count'];
        }
        if (!empty($params['attributes'])) {
            $attributesString = SeoAttributesMapper::getMappedData($params['attributes']);
            $attributesTitleString = SeoAttributesMapper::getMappedData($params['attributes'], SeoAttributesMapper::MODE_TITLE);
            if (!empty($attributesString)) {
                $processedParams['Attributes'] = " | {$attributesTitleString}";
                $processedParams['attributes'] = " | {$attributesString}";
            }
        }
        if (!empty($params['category'])) {
            $processedParams['Category'] = UtilityHelper::upperFirstLetter($params['category']);
            $processedParams['category'] = UtilityHelper::lowerFirstLetter($params['category']);
        }
        if (!empty($params['minPrice'])) {
            $processedParams['minPrice'] = ' | ' . Yii::t('seo', 'Prices from {value}', [
                    'value' => CurrencyHelper::format(Yii::$app->params['app_currency'], $params['minPrice'])
                ], $locale);
        }
        if (!empty($params['maxPrice'])) {
            $processedParams['maxPrice'] = Yii::t('seo', ' to {value}', [
                'value' => CurrencyHelper::format(Yii::$app->params['app_currency'], $params['maxPrice'])
            ], $locale);
        }
        return parent::getSeo($templateCategory, $processedParams, $locale);
    }

    /**
     * @param $templateCategory
     * @return array
     */
    public function getTemplates($templateCategory)
    {
        $heading = '';
        $title = '';
        $description = '';
        $keywords = '';
        $emptyText = '';
        $textView = '';
        switch ($templateCategory) {
            case self::TEMPLATE_CATEGORY:
                $heading = '{count, plural, =0{} other{#}} {Category} {Action} {City} {attributes}';
                $title = '{count, plural, =0{} other{#}} {Category} {forAction}{Attributes}{City}{minPrice}{maxPrice} | List your product for free on wine website';
                $description = 'On our site you can find {category} {forAction}, as well as place your ads {aboutAction} of {category} for free. wine is a convenient real estate portal. Free placement of ads about sale and rent of real estate{city}';
                $keywords = '{category} {action}{city}';
                $emptyText = 'There is no {category} matching your request in catalog.';
                break;
        }
        return [
            'heading' => $heading,
            'title' => $title,
            'description' => $description,
            'keywords' => $keywords,
            'emptyText' => $emptyText,
            'textView' => $textView,
        ];
    }


    /**
     * @param $templateCategory
     * @return array
     */
    public function getAttributeTemplates($templateCategory)
    {
        $templates = [];
        switch ($templateCategory) {
            case self::TEMPLATE_CATEGORY:
                $templates = [

                ];
                break;
        }
        return $templates;
    }
}