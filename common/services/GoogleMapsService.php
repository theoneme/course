<?php

namespace common\services;

use Yii;

/**
 * Class GoogleMapsService
 * @package common\services
 */
class GoogleMapsService
{
    /**
     * @var ApiClientService
     */
    private $_apiClientService;

    /**
     * GoogleMapsService constructor.
     * @param ApiClientService $apiClientService
     */
    public function __construct(ApiClientService $apiClientService)
    {
        $this->_apiClientService = $apiClientService;
    }

    /**
     * @param $address
     * @param null $language
     * @return array|bool
     */
    public function geocode($address, $language = null)
    {
        $data = $this->_apiClientService->makeRequest('get', 'https://maps.googleapis.com/maps/api/geocode/json', [
            'key' => Yii::$app->params['googleConsoleAPIKey'],
            'address' => $address,
            'language' => $language,
        ]);

        if ($data !== false && !empty($data['results'])) {
            return [
                'lat' => $data['results'][0]['geometry']['location']['lat'],
                'long' => $data['results'][0]['geometry']['location']['lng'],
            ];
        }

        return false;
    }

    /**
     * @param $lat
     * @param $long
     * @param null $language
     * @return bool|string
     */
    public function reverseGeocode($lat, $long, $language = null)
    {
        $data = $this->_apiClientService->makeRequest('get', 'https://maps.googleapis.com/maps/api/geocode/json', [
            'key' => Yii::$app->params['googleConsoleAPIKey'],
            'latlng' => $lat . ',' . $long,
            'language' => $language,
        ]);

        if ($data !== false && !empty($data['results'])) {
            $fullAddress = [];
            foreach ($data['results'][0]['address_components'] as $component) {
                $addressType = $component['types'][0];
                switch ($addressType) {
                    case 'route':
                        if (!preg_match("/unnamed road/i", $component['long_name'])) {
                            $fullAddress[0] = $component['long_name'];
                        }
                        break;
                    case 'street_number':
                        $fullAddress[1] = $component['long_name'];
                        break;
                    case 'locality':
                        $fullAddress[2] = $component['long_name'];
                        break;
                    case 'administrative_area_level_2':
                    case 'postal_town':
                        if (empty($fullAddress[2])) {
                            $fullAddress[2] = $component['long_name'];
                        }
                        break;
                    case 'administrative_area_level_1':
                        $fullAddress[3] = $component['long_name'];
                        break;
                    case 'country':
                        $fullAddress[4] = $component['long_name'];
                        break;
                }
            }

            ksort($fullAddress);
            return implode(', ', $fullAddress);
        }

        return false;
    }

    /**
     * @param $lat
     * @param $long
     * @param null $language
     * @return bool|array
     */
    public function reverseGeocodeAsData($lat, $long, $language = null)
    {
        $data = $this->_apiClientService->makeRequest('get', 'https://maps.googleapis.com/maps/api/geocode/json', [
            'key' => Yii::$app->params['googleConsoleAPIKey'],
            'latlng' => $lat . ',' . $long,
            'language' => $language,
        ]);

        if ($data !== false && !empty($data['results'])) {
            $addressData = [];
            foreach ($data['results'][0]['address_components'] as $component) {
                $addressType = $component['types'][0];
                switch ($addressType) {
                    case 'route':
                        if (!preg_match("/unnamed road/i", $component['long_name'])) {
                            $addressData['street'] = $component['long_name'];
                        }
                        break;
                    case 'street_number':
                        $addressData['street_number'] = $component['long_name'];
                        break;
                    case 'locality':
                        $addressData['city'] = $component['long_name'];
                        break;
                    case 'administrative_area_level_2':
                    case 'postal_town':
                        if (empty($addressData['city'])) {
                            $addressData['city'] = $component['long_name'];
                        }
                        break;
                    case 'administrative_area_level_1':
                        $addressData['region'] = $component['long_name'];
                        break;
                    case 'country':
                        $addressData['country'] = $component['long_name'];
                        break;
                    case 'neighbourhood':
                        $addressData['neighbourhood'] = $component['long_name'];
                        break;
                    case 'premise':
                        $addressData['premise'] = $component['long_name'];
                        break;
                    case 'postal_code':
                        $addressData['postal_code'] = $component['long_name'];
                        break;
                }
            }

            return $addressData;
        }

        return false;
    }

    /**
     * @param $addressData
     * @return string
     */
    public function getAddressFromData($addressData)
    {
        $data = [];
        foreach ($addressData as $type => $item) {
            switch ($type) {
                case 'street':
                    $data[0] = $item;
                    break;
                case 'street_number':
                    $data[1] = $item;
                    break;
                case 'city':
                    $data[2] = $item;
                    break;
                case 'region':
                    $data[3] = $item;
                    break;
                case 'country':
                    $data[4] = $item;
                    break;
            }
        }
        ksort($data);
        return implode(', ', $data);
    }
}