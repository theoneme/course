<?php

namespace common\traits;

/**
 * Trait ElasticAggregationTrait
 * @package common\traits
 */
trait ElasticAggregationTrait
{
    /**
     * @param $aggregations
     * @return $this
     */
    public function aggregations($aggregations)
    {
        $this->aggregations = $aggregations;
        return $this;
    }
}