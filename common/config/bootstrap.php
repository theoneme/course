<?php

use frostealth\yii2\aws\s3\Service;

Yii::setAlias('@common', dirname(__DIR__));
Yii::setAlias('@frontend', dirname(__DIR__, 2) . '/frontend');
Yii::setAlias('@backend', dirname(__DIR__, 2) . '/backend');
Yii::setAlias('@console', dirname(__DIR__, 2) . '/console');

Yii::$container->set(Service::class, [
    'credentials' => [
        'key' => 'AKIAJKAMNOAZSPXQ3LMA',
        'secret' => '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',
    ],
    'region' => 'eu-central-1',
    'defaultBucket' => 'eagent-media',
    'defaultAcl' => 'public-read',
]);

Yii::$container->set(\common\interfaces\repositories\AttributeValueRepositoryInterface::class, [
    'class' => \common\repositories\sql\AttributeValueRepository::class
]);
Yii::$container->set(\common\repositories\sql\AttributeValueRepository::class, function () {
    return new \common\repositories\sql\AttributeValueRepository(new \common\models\AttributeValue());
});

Yii::$container->set(\common\interfaces\repositories\AttributeRepositoryInterface::class, [
    'class' => \common\repositories\sql\AttributeRepository::class
]);
Yii::$container->set(\common\repositories\sql\AttributeRepository::class, function () {
    return new \common\repositories\sql\AttributeRepository(new \common\models\Attribute());
});

Yii::$container->set(\common\interfaces\repositories\UserRepositoryInterface::class, [
    'class' => \common\repositories\sql\UserRepository::class
]);
Yii::$container->set(\common\repositories\sql\UserRepository::class, function () {
    return new \common\repositories\sql\UserRepository(new \common\models\User());
});

Yii::$container->set(\common\interfaces\repositories\TranslationRepositoryInterface::class, [
    'class' => \common\repositories\sql\TranslationRepository::class
]);
Yii::$container->set(\common\repositories\sql\TranslationRepository::class, function () {
    return new \common\repositories\sql\TranslationRepository(new \common\models\Translation());
});

Yii::$container->set(\common\interfaces\repositories\CategoryRepositoryInterface::class, [
    'class' => \common\repositories\sql\CategoryRepository::class
]);
Yii::$container->set(\common\repositories\sql\CategoryRepository::class, function () {
    return new \common\repositories\sql\CategoryRepository(new \common\models\Category());
});

Yii::$container->set(\common\repositories\sql\AttributeGroupRepository::class, function () {
    return new \common\repositories\sql\AttributeGroupRepository(new \common\models\AttributeGroup());
});
Yii::$container->set(\common\repositories\sql\AttributeToGroupRepository::class, function () {
    return new \common\repositories\sql\AttributeToGroupRepository(new \common\models\AttributeToGroup());
});

Yii::$container->set(\common\interfaces\repositories\CourseRepositoryInterface::class, [
    'class' => \common\repositories\sql\CourseRepository::class
]);
Yii::$container->set(\common\repositories\sql\CourseRepository::class, function () {
    return new \common\repositories\sql\CourseRepository(new \common\models\Course());
});

Yii::$container->set(\common\interfaces\repositories\CourseAttributeRepositoryInterface::class, [
    'class' => \common\repositories\sql\CourseAttributeRepository::class
]);
Yii::$container->set(\common\repositories\sql\CourseAttributeRepository::class, function () {
    return new \common\repositories\sql\CourseAttributeRepository(new \common\models\CourseAttribute());
});

Yii::$container->set(\common\interfaces\repositories\LessonRepositoryInterface::class, [
    'class' => \common\repositories\sql\LessonRepository::class
]);
Yii::$container->set(\common\repositories\sql\LessonRepository::class, function () {
    return new \common\repositories\sql\LessonRepository(new \common\models\Lesson());
});

Yii::$container->set(\common\interfaces\repositories\TaskRepositoryInterface::class, [
    'class' => \common\repositories\sql\TaskRepository::class
]);
Yii::$container->set(\common\repositories\sql\TaskRepository::class, function () {
    return new \common\repositories\sql\TaskRepository(new \common\models\Task());
});

Yii::$container->set(\common\interfaces\repositories\UserProgressRepositoryInterface::class, [
    'class' => \common\repositories\sql\UserProgressRepository::class
]);
Yii::$container->set(\common\repositories\sql\UserProgressRepository::class, function () {
    return new \common\repositories\sql\UserProgressRepository(new \common\models\UserProgress());
});

Yii::$container->set(\common\interfaces\repositories\PaymentMethodRepositoryInterface::class, [
    'class' => \common\repositories\sql\PaymentMethodRepository::class
]);
Yii::$container->set(\common\repositories\sql\PaymentMethodRepository::class, function () {
    return new \common\repositories\sql\PaymentMethodRepository(new \common\models\PaymentMethod());
});

Yii::$container->set(\common\interfaces\repositories\PaymentRepositoryInterface::class, [
    'class' => \common\repositories\sql\PaymentRepository::class
]);
Yii::$container->set(\common\repositories\sql\PaymentRepository::class, function () {
    return new \common\repositories\sql\PaymentRepository(new \common\models\Payment());
});

Yii::$container->set(\common\repositories\sql\UserSocialRepository::class, function () {
    return new \common\repositories\sql\UserSocialRepository(new \common\models\UserSocial());
});