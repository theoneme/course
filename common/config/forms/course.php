<?php

return [
    'course' => [
        'default' => [
            1 => [
                'title' => Yii::t('model', 'Main Info'),
                'config' => [
                    [
                        'type' => 'view',
                        'value' => 'info-step'
                    ]
                ]
            ],
            2 => [
                'title' => Yii::t('model', 'Course Attributes'),
                'config' => [
                    [
                        'type' => 'view',
                        'value' => 'attributes-step'
                    ]
                ]
            ],
            3 => [
                'title' => Yii::t('model', 'Photo'),
                'config' => [
                    [
                        'type' => 'view',
                        'value' => 'photo-step'
                    ]
                ]
            ],
            4 => [
                'title' => Yii::t('model', 'Lessons'),
                'config' => [
                    [
                        'type' => 'view',
                        'value' => 'lessons-step'
                    ]
                ]
            ],
//            4 => [
//                'title' => Yii::t('model', 'Price and terms of deal'),
//                'config' => [
//                    [
//                        'type' => 'view',
//                        'value' => 'price-step',
//                    ],
//                ]
//            ],
//            5 => [
//                'title' => Yii::t('model', 'Additional Information'),
//                'config' => [
//                    [
//                        'type' => 'view',
//                        'value' => 'info-step'
//                    ]
//                ]
//            ],
        ],
    ],
];
