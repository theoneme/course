<?php

return [
    'user' => [
        1 => [
            'title' => Yii::t('model', 'Contact Info'),
            'config' => [
                [
                    'type' => 'view',
                    'value' => 'contact-step'
                ],
            ]
        ],
        2 => [
            'title' => Yii::t('model', 'Main Info'),
            'config' => [
                [
                    'type' => 'view',
                    'value' => 'photo-step'
                ],
                [
                    'type' => 'view',
                    'value' => 'info-step'
                ]
            ]
        ],
        3 => [
            'title' => Yii::t('model', 'Password Settings'),
            'config' => [
                [
                    'type' => 'view',
                    'value' => 'password-step'
                ]
            ]
        ],
    ],
];
