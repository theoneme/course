<?php

use common\components\CacheLayer;
use common\components\MediaLayer;
use yii\caching\FileCache;
use yii\i18n\PhpMessageSource;
use yii\rbac\DbManager;

return [
    'name' => 'Course',
    'aliases' => [
        '@bower' => '@vendor/bower-asset',
        '@npm' => '@vendor/npm-asset',
    ],
    'vendorPath' => dirname(__DIR__, 2) . '/vendor',
    'language' => 'ru-RU',
    'bootstrap' => [
        frontend\components\CurrencySelector::class,
    ],
    'components' => [
        'cache' => [
            'class' => FileCache::class,
        ],
        'cacheLayer' => [
            'class' => CacheLayer::class
        ],
        'mediaLayer' => [
            'class' => MediaLayer::class,
            'awsEnabled' => false
        ],
        'i18n' => [
            'translations' => [
                'seo*' => [
                    'class' => PhpMessageSource::class,
                    'basePath' => '@common/messages',
                    'fileMap' => [
                        'seo' => 'seo.php',
                    ],
                ],
            ],
        ],
        'authManager' => [
            'class' => DbManager::class,
        ],
    ],
];
