<?php

namespace common\helpers;

use common\models\Task;
use Yii;

/**
 * Class TaskHelper
 * @package common\helpers
 */
class TaskHelper
{
    /**
     * @param $task
     * @param $answer
     * @return array
     */
    public static function checkAsnwer($task, $answer){
        switch ($task['type']) {
            case Task::TYPE_NUMBER:
                $correctAnswer = array_reduce($task['answers'], function($carry, $value){
                    if ((int)$value['correct'] === 1) {
                        $carry = $value['title'];
                    }
                    return $carry;
                }, null);
                $answer = str_replace(',', '.', $answer);
                $answer = preg_replace('/[^0-9\.\-]/', '', $answer);
                $result = $answer === $correctAnswer;
//                return $answer === $correctAnswer;
                break;
            case Task::TYPE_RADIO:
                $correctAnswer = array_reduce($task['answers'], function($carry, $value){
                    if ((int)$value['correct'] === 1) {
                        $carry = $value['id'];
                    }
                    return $carry;
                }, null);
                $result = $answer === $correctAnswer;
//                return $answer === $correctAnswer;
                break;
            case Task::TYPE_CHECKBOX:
                $correctAnswers = array_reduce($task['answers'], function($carry, $value){
                    if ((int)$value['correct'] === 1) {
                        $carry[] = $value['id'];
                    }
                    return $carry;
                }, []);
                $correctDiff = array_diff($correctAnswers, $answer);
                if (count($correctDiff) > 0 && count($correctDiff) < count($correctAnswers)) {
                    $message = Yii::t('app', 'Not all correct answers selected');
                }
                $result = !$correctDiff && !array_diff($answer, $correctAnswers);
//                return !array_diff($correctAnswers, $answer) && !array_diff($answer, $correctAnswers);
                break;
            case Task::TYPE_TEXT:
            default:
                $correctAnswer = array_reduce($task['answers'], function($carry, $value){
                    if ((int)$value['correct'] === 1) {
                        $carry = $value['title'];
                    }
                    return $carry;
                }, null);
                $result = $answer === $correctAnswer;
//                return $answer === $correctAnswer;
                break;
        }
        return [
            'result' => $result,
            'message' => $message ?? null
        ];
    }
}