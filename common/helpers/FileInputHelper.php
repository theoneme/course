<?php

namespace common\helpers;

use common\models\Lesson;
use Yii;
use yii\helpers\Url;
use yii\web\JsExpression;

/**
 * Class FileInputHelper
 * @package common\helpers
 */
class FileInputHelper
{
    public const TYPE_IMAGE = 0;
    public const TYPE_VIDEO = 10;
    public const TYPE_PDF = 20;
    public const TYPE_OTHER = 100;

    /**
     * @return array
     */
    public static function getDefaultConfig()
    {
        return [
            'name' => 'uploaded_images[]',
            'options' => ['accept' => 'image/*', 'class' => 'file-upload-input'],
            'pluginOptions' => [
                'uploadUrl' => Url::to(['/image/upload']),
                'allowedFileExtensions' => [
                    'jpg', 'gif', 'png', 'jpeg'
                ],
                'minImageHeight' => 100,
                'minImageWidth' => 100,
                'maxFileCount' => 25,
                'showPreview' => true,
                'dropZoneEnabled' => true,
                'initialPreviewAsData' => true,
                'showCancel' => false,
                'showRemove' => false,
                'showUpload' => false,
                'fileActionSettings' => [
                    'showZoom' => false,
                    'showDrag' => false,
                    'showUpload' => false
                ],
                'previewThumbTags' => [
                    '{actions}' => '{actions}',
                ],
                'uploadExtraData' => new JsExpression("
                    function (previewId, index) {
                        if (previewId !== undefined) {
                            var obj = $('[data-fileid=\"' + previewId + '\"]').data();
                            return obj;
                        }
                    }
                "),
//                'otherActionButtons' =>
//                    '<button type="button" class="btn btn-sm btn-default crop-button" title="' . Yii::t('main', 'Crop image') . '">
//                        <i class="icon-crop" aria-hidden="true"></i>
//                    </button>
//                    <button type="button" class="btn btn-sm btn-default crop-button" title="' . Yii::t('main', 'Rotate image') . '">
//                        <i class="icon-rotate-left" aria-hidden="true"></i>
//                    </button>
//                    <button type="button" class="btn btn-sm btn-default crop-button" title="' . Yii::t('main', 'Rotate image') . '">
//                        <i class="icon-rotate-right" aria-hidden="true"></i>
//                    </button>',
            ]
        ];
    }

    /**
     * @param array $attachments
     * @param int $type
     * @return array
     */
    public static function buildPreviews(array $attachments, $type = self::TYPE_IMAGE)
    {
        $result = [];

        foreach ($attachments as $attachment) {
            switch ($type) {
                case self::TYPE_IMAGE:
                    $result[] = Yii::$app->mediaLayer->getThumb($attachment['content']);
                    break;
                case self::TYPE_VIDEO:
                case self::TYPE_PDF:
                case self::TYPE_OTHER:
                default:
                    $result[] = $attachment['content'];
                    break;
            }
        }

        return $result;
    }

    /**
     * @param array $attachments
     * @param int $type
     * @return array
     */
    public static function buildPreviewsConfig(array $attachments, $type = self::TYPE_IMAGE)
    {
        $output = [];

        foreach ($attachments as $key => $attachment) {
            $config = [
                'caption' => basename($attachment['content']),
                'url' => Url::toRoute('/image/delete'),
                'key' => "image_init_{$key}"
            ];
            switch ($type) {
                case self::TYPE_IMAGE:
                    break;
                case self::TYPE_PDF:
                    $config['type'] = 'pdf';
                    break;
                case self::TYPE_VIDEO:
                    $config['type'] = 'video';
                    $config['filetype'] = '';
                    break;
                case self::TYPE_OTHER:
                default:
                    $config['type'] = 'other';
                    break;
            }
            $output[] = $config;
        }
        return $output;
    }

    public static function getTypeByLessonType($type){
        switch ($type) {
            case Lesson::TYPE_VIDEO:
                return self::TYPE_VIDEO;
                break;
            case Lesson::TYPE_PDF:
                return self::TYPE_PDF;
                break;
            default:
                return self::TYPE_OTHER;
                break;
        }
    }

    /**
     * @param $path
     * @return array
     */
    public static function buildOriginImagePath($path)
    {
        $pos = strpos($path, '/uploads');

        $path = substr($path, $pos);
        return $path;
    }
}