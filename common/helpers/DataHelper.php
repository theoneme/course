<?php

namespace common\helpers;

use common\mappers\CategoryTreeMapper;
use common\models\Category;
use common\services\entities\CategoryService;
use common\services\entities\CourseService;
use common\services\entities\PaymentMethodService;
use Yii;
use yii\helpers\ArrayHelper;

/**
 * Class DataHelper
 * @package common\helpers
 */
class DataHelper
{
    /**
     * @param string $indexBy
     * @return array
     */
    public static function getCategoryTree($indexBy = 'id')
    {
        /** @var CategoryService $categoryService */
        $categoryService = Yii::$container->get(CategoryService::class);
        $categories = $categoryService->getMany(['minLvl' => 1], ['orderBy' => 'lft']);

        return CategoryTreeMapper::getMappedData($categories['items'], $indexBy);
    }

    /**
     * @param bool $leavesAllowed
     * @return array
     */
    public static function getCategories($leavesAllowed = true)
    {
        /** @var CategoryService $categoryService */
        $categoryService = Yii::$container->get(CategoryService::class);
        $categories = $categoryService->getMany(['lvl' => 1, 'leavesAllowed' => $leavesAllowed], ['orderBy' => 'lft']);

        return ArrayHelper::map($categories['items'], 'id', 'title');
    }

    /**
     * @param $parentId
     * @return array
     */
    public static function getCategoryChildren($parentId)
    {
        /** @var CategoryService $categoryService */
        $categoryService = Yii::$container->get(CategoryService::class);
        $categories = $categoryService->getMany(['parent_id' => $parentId], ['orderBy' => 'lft']);

        return ArrayHelper::map($categories['items'], 'id', 'title');
    }

    /**
     * @param int $lvl
     * @return false|null|string
     */
    public static function getFirstCategoryId($lvl = 1)
    {
        return Category::find()->select('id')->where(['lvl' => $lvl])->orderBy(['id' => SORT_ASC])->scalar();
    }

    /**
     * @throws \yii\base\InvalidConfigException
     * @throws \yii\di\NotInstantiableException
     */
    public static function getCourses()
    {
        /** @var CategoryService $service */
        $service = Yii::$container->get(CourseService::class);
        $courses = $service->getMany(['lvl' => 1]);

        return ArrayHelper::map($courses['items'], 'id', 'title');
    }

    /**
     * @throws \yii\base\InvalidConfigException
     * @throws \yii\di\NotInstantiableException
     */
    public static function getPaymentMethods()
    {
        /** @var PaymentMethodService $service */
        $service = Yii::$container->get(PaymentMethodService::class);
        $methods = $service->getMany(['enabled' => 1]);

        return ArrayHelper::map($methods['items'], 'id', 'title');
    }

    /**
     * @return array
     */
    public static function getCurrencies()
    {
        return array_map(function ($currency) {
            return $currency['title'];
        }, Yii::$app->params['currencies']);
    }

    /**
     * @param bool $sorted
     * @return mixed
     */
    public static function getLocales($sorted = false)
    {
        if ($sorted === false) {
            return Yii::$app->params['languages'];
        }

        $locales = Yii::$app->params['languages'];

        uksort($locales, function ($a) {
            return $a === Yii::$app->language ? -1 : 1;
        });

        return $locales;
    }

    /**
     * @param $array
     * @param $currentValue
     * @param string $attribute
     * @return mixed
     */
    public static function getPreviousItem($array, $currentValue, $attribute = 'id'){
        return array_reduce($array, function($carry, $value) use ($currentValue, $attribute) {
            if ($value[$attribute] < $currentValue && ($carry === null || $value[$attribute] > $carry[$attribute])) {
                $carry = $value;
            }
            return $carry;
        }, null);
    }

    /**
     * @param $array
     * @param $currentValue
     * @param string $attribute
     * @return mixed
     */
    public static function getNextItem($array, $currentValue, $attribute = 'id'){
        return array_reduce($array, function($carry, $value) use ($currentValue, $attribute) {
            if ($value[$attribute] > $currentValue && ($carry === null || $value[$attribute] < $carry[$attribute])) {
                $carry = $value;
            }
            return $carry;
        }, null);
    }
}