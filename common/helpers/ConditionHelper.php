<?php

namespace common\helpers;

use common\interfaces\RepositoryInterface;

/**
 * Class ConditionHelper
 * @package common\helpers
 */
class ConditionHelper
{
    /**
     * @param RepositoryInterface $repository
     * @param $relation
     * @return mixed
     */
    public static function getRelationName(RepositoryInterface $repository, $relation)
    {
        $className = get_class($repository);

        if (strpos($className, 'elastic')) {
            $entityClass = $repository->modelClass;
            $relations = $entityClass::getRelations();
            return $relations[$relation];
        }

        return $relation;
    }

    /**
     * @param RepositoryInterface $repository
     * @param $table
     * @param $field
     * @return string
     */
    public static function getTabledField(RepositoryInterface $repository, $table, $field)
    {
        $className = get_class($repository);

        if (strpos($className, 'elastic')) {
            return $field;
        }

        return "{$table}.{$field}";
    }
}