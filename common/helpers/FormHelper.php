<?php

namespace common\helpers;

use common\repositories\sql\CategoryRepository;
use Yii;

/**
 * Class FormHelper
 * @package common\helpers
 */
class FormHelper
{
    public const ENTITY_COURSE = 'course';
    public const ENTITY_USER = 'user';

    /**
     * @param $entity
     * @param $category
     * @return mixed
     */
    public static function getFormConfig($entity, $category = null)
    {
        $formConfig = require dirname(__DIR__, 1) . "/config/forms/{$entity}.php";

        switch ($entity) {
            case self::ENTITY_USER:
                $config = $formConfig[$entity];

                break;
            case self::ENTITY_COURSE:
//                /** @var CategoryRepository $categoryRepository */
//                $categoryRepository = Yii::$container->get(CategoryRepository::class);

//                $category = $categoryRepository->findOneByCriteria(['id' => $category]);
                $config = $formConfig[$entity]['default'];
//                $config = array_key_exists($category['form_group'], $formConfig[$entity])
//                    ? $formConfig[$entity][$category['form_group']]
//                    : $formConfig[$entity]['default'];

                break;
            default:
                $config = array_key_exists($category, $formConfig[$entity])
                    ? $formConfig[$entity][$category]
                    : $formConfig[$entity]['default'];

                break;

        }

        return $config;
    }
}