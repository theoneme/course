<?php

namespace common\helpers;

use common\components\CurrencyHelper;
use Yii;

/**
 * Class FilterConditionHelper
 * @package common\helpers
 */
class FilterConditionHelper
{
    protected $_config = [
        'status' => 'singleHandler',
        'category_id' => 'singleHandler',
        'region_id' => 'singleHandler',
        'price' => 'priceHandler',
        'category' => [
            'handler' => 'categoryHandler',
            'params' => ['root', 'lft', 'rgt']
        ],
    ];

    /**
     * @param $params
     * @param $fields
     * @param null $model
     * @return array
     */
    public function process($params, $fields, $model = null)
    {
        $criteria = [];

        foreach ($fields as $key => $field) {
            $criteria[] = self::handleHandlers($params, $field, $model);
        }

        $criteria = array_filter($criteria);
        if (!empty($criteria)) {
            $criteria = array_merge(['and'], $criteria);
        }

        return $criteria;
    }

    /**
     * @param $params
     * @param $field
     * @param $model
     * @return array|mixed
     */
    protected function handleHandlers($params, $field, $model)
    {
        $value = [];

        if (isset($this->_config[$field])) {
            if (is_array($this->_config[$field])) {
                $configParams = array_flip($this->_config[$field]['params']);
                $intersection = array_intersect_key($params, $configParams);
                if (count($intersection) === count($configParams)) {
                    $value = $this->{$this->_config[$field]['handler']}($params, $field, $model);
                }
            } else {
                if (isset($params[$field])) {
                    if (is_callable($this->_config[$field])) {
                        $value = call_user_func($this->_config[$field], $params, $field, $model);
                    } else {
                        $value = $this->{$this->_config[$field]}($params, $field, $model);
                    }
                }
            }
        }

        return $value;
    }

    /**
     * @param $params
     * @param $field
     * @param $model
     * @return array
     */
    protected function singleHandler($params, $field, $model)
    {
        return $model !== null ? ["{$model}.{$field}" => $params[$field]] : [$field => $params[$field]];
    }

    /**
     * @param $params
     * @param $field
     * @param $model
     * @return array
     */
    protected function categoryHandler($params, $field, $model)
    {
        return ['and', ['category.root' => $params['root']], ['>=', 'category.lft', $params['lft']], ['<=', 'category.rgt', $params['rgt']]];
    }

    /**
     * @param $params
     * @param $field
     * @param $model
     * @return array
     */
    protected function priceHandler($params, $field, $model)
    {
        $criteria = [];

        if (array_key_exists('min', $params['price'])) {
            $minPriceParam = CurrencyHelper::convert(Yii::$app->params['app_currency'], 'RUB', $params['price']['min']);
            $criteria[] = ['>', 'default_price', $minPriceParam];
        }
        if (array_key_exists('max', $params['price'])) {
            $maxPriceParam = CurrencyHelper::convert(Yii::$app->params['app_currency'], 'RUB', $params['price']['max']);
            $criteria[] = ['<=', 'default_price', $maxPriceParam];
        }

        if (!empty($criteria)) {
            $criteria = array_merge(['and'], $criteria);
        }

        return $criteria;
    }
}