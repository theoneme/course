<?php

namespace common\helpers;

use Voronkovich\SberbankAcquiring\Client;
use Voronkovich\SberbankAcquiring\Currency;

class SberbankHelper {
    public static $login = 'ujobs-api';
    public static $password = '1234Da4321aD';
    public static $url = Client::API_URI;

//    public static $login = 'ujobs-api';
//    public static $password = 'ujobs';
//    public static $url = Client::API_URI_TEST;

    public static function getConfigArray(){
        return [
            'userName' => self::$login,
            'password' => self::$password,
            'language' => 'ru',
            'currency' => Currency::RUB,
            'apiUri' => SberbankHelper::$url,
            'httpMethod' => 'POST',
        ];
    }
}