<?php

namespace common\forms\ar;

use common\models\UserCourse;

/**
 * Class UserCourseForm
 * @package common\forms\ar
 */
class UserCourseForm extends UserCourse
{
    /**
     * @param array $data
     * @param null $formName
     * @return bool
     * @throws \ReflectionException
     */
    public function load($data, $formName = null)
    {
        if (!array_key_exists((new \ReflectionClass($this))->getShortName(), $data)) {
            $formName = '';
        }

        return parent::load($data, $formName);
    }
}