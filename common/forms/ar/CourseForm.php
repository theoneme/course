<?php

namespace common\forms\ar;

use common\factories\DynamicFormFactory;
use common\forms\ar\composite\AttachmentForm;
use common\forms\ar\composite\CategoryForm;
use common\forms\ar\composite\MetaForm;
use common\forms\CompositeForm;
use common\mappers\DynamicFormValuesMapper;
use common\models\Attachment;
use common\models\AttributeGroup;
use common\models\Course;
use common\models\CourseAttribute;
use common\models\Translation;
use common\services\EntityAttributeValueCreator;
use Yii;
use yii\widgets\ActiveForm;
use \common\forms\ar\composite\LessonForm;

/**
 * Class CourseForm
 * @package common\forms\ar
 *
 * @property CategoryForm $category
 * @property MetaForm[] $meta
 * @property AttachmentForm[] $attachment
 * @property LessonForm[] $lessons
 */
class CourseForm extends CompositeForm
{
    /**
     * @var Course
     */
    public $_course;
    /**
     * @var DynamicForm
     */
    public $dynamicForm;
    /**
     * @var integer
     */
    public $status;
    /**
     * @var integer
     */
    public $type;
    /**
     * @var integer
     */
    public $price;
    /**
     * @var integer
     */
    public $unique_lessons;

    /**
     * CourseForm constructor.
     * @param array $config
     */
    public function __construct($config = [])
    {
        $this->meta = array_map(function () {
            return new MetaForm();
        }, Yii::$app->params['languages']);
        $this->category = new CategoryForm();
        $this->attachment = [];
        $this->lessons = [];

        parent::__construct($config);
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['status', 'type', 'price'], 'integer'],
            ['price', 'default', 'value' => 0],
            [['status'], 'in', 'range' => [
                Course::STATUS_DELETED,
                Course::STATUS_DRAFT,
                Course::STATUS_ACTIVE,
            ]],
            ['type', 'default', 'value' => Course::TYPE_DEFAULT],
            [['type'], 'in', 'range' => [
                Course::TYPE_DEFAULT,
                Course::TYPE_TASK_ONLY,
            ]],

            [['unique_lessons'], 'uniqueLessons', 'skipOnEmpty' => false],
        ];
    }

    /**
     * @param $attribute
     * @param $params
     */
    public function uniqueLessons($attribute, $params)
    {
        $ids = array_column($this->lessons, 'lesson_id');
        if (count(array_unique($ids)) !== count($ids)) {
            $this->addError($attribute, Yii::t('model', 'You cannot add same lesson multiple times'));
        }
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('model', 'ID'),
            'category_id' => Yii::t('model', 'Category ID'),
            'status' => Yii::t('model', 'Status'),
            'type' => Yii::t('model', 'Type'),
            'price' => Yii::t('model', 'Price'),
        ];
    }

    /**
     * @return array
     */
    protected function internalForms()
    {
        return array(
            'meta' => MetaForm::class,
            'category' => CategoryForm::class,
            'attachment' => AttachmentForm::class,
            'lessons' => LessonForm::class,
        );
    }

    /**
     * @param array $data
     * @param null $formName
     * @param array $exceptions
     * @return bool
     */
    public function load($data, $formName = null, $exceptions = ['meta'])
    {
        $success = parent::load($data, $formName, $exceptions);

        $this->dynamicForm = DynamicFormFactory::initByCategory(AttributeGroup::ENTITY_COURSE_CATEGORY, $this->category->category_id);
        $this->dynamicForm->load($data);

        return $success;
    }

    /**
     * @return array
     */
    public function getAjaxErrors()
    {
        return array_merge(parent::getAjaxErrors(), ActiveForm::validate($this->dynamicForm));
    }

    /**
     * @param bool $withValidation
     * @return bool
     */
    public function save($withValidation = true)
    {
        return $this->_course->save($withValidation);
    }

    /**
     * @param $dto
     * @throws \common\exceptions\RepositoryException
     * @throws \yii\base\InvalidConfigException
     * @throws \yii\di\NotInstantiableException
     */
    public function prepareUpdate($dto)
    {
        $this->category->load(array_intersect_key($dto, $this->category->attributes), '');
        $this->load(array_intersect_key($dto, $this->attributes), '');

        foreach ($dto['translations'] as $locale => $translation) {
            $this->meta[$locale]->load($translation, '');
        }

        $attachmentForms = [];
        foreach ($dto['images'] as $image) {
            $attachmentForms[] = new AttachmentForm(['content' => $image]);
        }
        $this->attachment = $attachmentForms;

        $this->lessons = array_values(array_map(function($var){
            return new LessonForm(['lesson_id' => $var]);
        }, $dto['lessonIds']));

        $this->dynamicForm = DynamicFormFactory::initByCategory(AttributeGroup::ENTITY_COURSE_CATEGORY, $this->category->category_id);
        $this->dynamicForm->load(DynamicFormValuesMapper::getMappedData($dto['attributes'], $this->dynamicForm->config), '');
    }

    /**
     * @return bool
     * @throws \common\exceptions\ClassNotFoundException
     * @throws \common\exceptions\EntityNotCreatedException
     * @throws \common\exceptions\RepositoryException
     * @throws \yii\base\InvalidConfigException
     * @throws \yii\di\NotInstantiableException
     */
    public function bindData()
    {
        $this->_course->load($this->attributes, '');
        $this->_course->load($this->category->attributes, '');

        foreach ($this->meta as $locale => $meta) {
            if (!empty($meta->title)) {
                $title = $this->_course->bind('translations');
                $title->attributes = [
                    'value' => $meta->title,
                    'locale' => $locale,
                    'key' => Translation::KEY_TITLE,
                    'entity' => Translation::ENTITY_COURSE
                ];
            }

            if (!empty($meta->description)) {
                $description = $this->_course->bind('translations');
                $description->attributes = [
                    'value' => $meta->description,
                    'locale' => $locale,
                    'key' => Translation::KEY_DESCRIPTION,
                    'entity' => Translation::ENTITY_COURSE
                ];
            }

            if (!empty($meta->slug)) {
                $description = $this->_course->bind('translations');
                $description->attributes = [
                    'value' => $meta->slug,
                    'locale' => $locale,
                    'key' => Translation::KEY_SLUG,
                    'entity' => Translation::ENTITY_COURSE
                ];
            }
        }

        foreach ($this->attachment as $attachment) {
            $this->_course->bindAttachment($attachment->content)->attributes = ['entity' => Attachment::ENTITY_COURSE, 'content' => $attachment->content];
        }

        foreach ($this->lessons as $lesson) {
            $this->_course->bind('courseLessons')->attributes = ['lesson_id' => $lesson->lesson_id];
        }

        /** @var EntityAttributeValueCreator $creator */
        $creator = Yii::$container->get(EntityAttributeValueCreator::class);

        foreach ($this->dynamicForm->attributes as $key => $courseAttribute) {
            if (empty($courseAttribute)) {
                continue;
            }

            $params = [
                'isId' => in_array($this->dynamicForm['config']['types'][$key], [
                    'dropdownlist',
                    'radiolist',
                    'checkboxlist',
                ]),
            ];
            $key = explode('_', $key);

            if (is_array($courseAttribute)) {
                $values = $courseAttribute;
            } else {
                $valueParts = array_filter(explode(',', $courseAttribute));
                $values = $valueParts;
            }

            foreach ($values as $value) {
                $object = $creator->makeObject(CourseAttribute::class, $key[1], $value, $params);

                $attr = $this->_course->bind('courseAttributes');
                $attr->attributes = $object->attributes;
            }
        }

        return true;
    }
}