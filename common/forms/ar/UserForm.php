<?php

namespace common\forms\ar;

use common\forms\ar\composite\MetaForm;
use common\forms\ar\composite\PasswordForm;
use common\forms\CompositeForm;
use common\models\Translation;
use common\models\User;
use Yii;

/**
 * Class UserForm
 *
 * @property MetaForm $meta
 * @property PasswordForm $password
 *
 * @package common\forms\ar
 */
class UserForm extends CompositeForm
{
    /**
     * @var User
     */
    public $_user;
    /**
     * @var DynamicForm
     */
    public $dynamicForm;
    /**
     * @var string
     */
    public $avatar;
    /**
     * @var string
     */
    public $email;
    /**
     * @var string
     */
    public $phone;
    /**
     * @var string
     */
    public $username;
    /**
     * @var array
     */
    public $customDataArray = [];

    /**
     * UserForm constructor.
     * @param array $config
     */
    public function __construct($config = [])
    {
        $this->password = new PasswordForm();
        $this->meta = new MetaForm();

        parent::__construct($config);
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['email'], 'email'],
            [['email'], 'phoneOrEmailRequired'],
            [['email'], 'unique', 'targetClass' => User::class, 'when' => function ($model) {
                return $model->email !== $this->_user->email;
            }],
            [['phone'], 'unique', 'targetClass' => User::class, 'when' => function ($model) {
                return $model->phone !== $this->_user->phone;
            }],
            [['email', 'phone'], 'default', 'value' => null],
            ['avatar', 'string', 'max' => 155],
            ['username', 'required'],
            ['username', 'string', 'max' => 35],
            [['customDataArray'], 'safe'],
        ];
    }

    /**
     * @param $attribute
     */
    public function phoneOrEmailRequired($attribute)
    {
        if (empty($this->email) && empty($this->phone)) {
            $this->addError($attribute, Yii::t('yii', '{attribute} cannot be blank.', ['attribute' => 'Email or phone']));
        }
    }

    /**
     * @return array
     */
    public function attributeLabels()
    {
        return [
            'avatar' => Yii::t('model', 'Avatar'),
            'email' => Yii::t('model', 'Email'),
            'username' => Yii::t('model', 'Username'),
            'phone' => Yii::t('model', 'Phone'),
        ];
    }

    /**
     * @return array
     */
    protected function internalForms()
    {
        return array(
            'password' => PasswordForm::class,
            'meta' => MetaForm::class,
        );
    }

    /**
     * @param bool $withValidation
     * @return bool
     */
    public function save($withValidation = true)
    {
        return $this->_user->save($withValidation);
    }

    /**
     * @param $userDTO
     * @throws \ReflectionException
     * @throws \common\exceptions\RepositoryException
     * @throws \yii\base\InvalidConfigException
     * @throws \yii\di\NotInstantiableException
     */
    public function prepareUpdate($userDTO)
    {
        $this->load($userDTO, '');
        $this->meta->load($userDTO['translations'], '');
    }

    /**
     * @return bool
     * @throws \yii\base\Exception
     * @throws \yii\base\InvalidConfigException
     * @throws \yii\di\NotInstantiableException
     */
    public function bindData()
    {
        $this->_user->load($this->attributes, '');

        if (!empty($this->meta->subtitle)) {
            $title = $this->_user->bind('translations');
            $title->attributes = [
                'value' => $this->meta->subtitle,
                'locale' => Yii::$app->language,
                'key' => Translation::KEY_SUBTITLE,
                'entity' => Translation::ENTITY_USER
            ];
        }
        if (!empty($this->meta->description)) {
            $description = $this->_user->bind('translations');
            $description->attributes = [
                'value' => $this->meta->description,
                'locale' => Yii::$app->language,
                'key' => Translation::KEY_DESCRIPTION,
                'entity' => Translation::ENTITY_USER
            ];
        }

        if (!empty($this->password->password)) {
            $this->_user->setPassword($this->password->password);
        }

        return true;
    }
}