<?php

namespace common\forms\ar;

use common\forms\ar\composite\MetaForm;
use common\forms\CompositeForm;
use common\models\PaymentMethod;
use common\models\Translation;
use Yii;

/**
 * Class PaymentMethodForm
 * @package common\forms\ar
 *
 * @property MetaForm[] $meta
 */
class PaymentMethodForm extends CompositeForm
{
    /**
     * @var PaymentMethod
     */
    public $_paymentMethod;
    /**
     * @var string
     */
    public $alias;
    /**
     * @var string
     */
    public $logo;
    /**
     * @var integer
     */
    public $sort;
    /**
     * @var integer
     */
    public $enabled;

    /**
     * PaymentMethodForm constructor.
     * @param array $config
     */
    public function __construct($config = [])
    {
        $this->meta = array_map(function () {
            return new MetaForm();
        }, Yii::$app->params['languages']);

        parent::__construct($config);
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['alias'], 'required'],
            [['sort', 'enabled'], 'integer'],
            [['logo', 'alias'], 'string'],
        ];
    }

    /**
     * @return array
     */
    protected function internalForms()
    {
        return [
            'meta' => MetaForm::class,
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('model', 'ID'),
            'alias' => Yii::t('model', 'Alias'),
            'logo' => Yii::t('model', 'Logo'),
            'sort' => Yii::t('model', 'Sort'),
            'enabled' => Yii::t('model', 'Enabled'),
        ];
    }

    /**
     * @param bool $withValidation
     * @return bool
     */
    public function save($withValidation = true)
    {
        return $this->_paymentMethod->save($withValidation);
    }

    /**
     * @param $dto
     */
    public function prepareUpdate($dto)
    {
        $this->load($dto, '');

        foreach ($dto['translations'] as $locale => $translation) {
            $this->meta[$locale]->load($translation, '');
        }
    }

    /**
     * @return bool
     * @throws \yii\base\InvalidConfigException
     * @throws \yii\di\NotInstantiableException
     */
    public function bindData()
    {
        $this->_paymentMethod->load($this->attributes, '');

        foreach ($this->meta as $locale => $meta) {
            foreach ([Translation::KEY_TITLE] as $key) {
                if (!empty($meta->{$key})) {
                    $title = $this->_paymentMethod->bind('translations');
                    $title->attributes = [
                        'value' => $meta->{$key},
                        'locale' => $locale,
                        'key' => $key,
                        'entity' => Translation::ENTITY_PAYMENT_METHOD
                    ];
                }
            }
        }

        return true;
    }
}