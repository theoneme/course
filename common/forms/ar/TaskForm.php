<?php

namespace common\forms\ar;

use common\forms\ar\composite\AttachmentForm;
use common\forms\ar\composite\MetaForm;
use common\forms\ar\composite\TaskAnswerAttachmentForm;
use common\forms\CompositeForm;
use common\helpers\UtilityHelper;
use common\models\Attachment;
use common\models\Task;
use common\models\Translation;
use Yii;
use common\forms\ar\composite\LessonForm;
use common\forms\ar\composite\TaskAnswerForm;

/**
 * Class TaskForm
 * @package common\forms\ar
 *
 * @property MetaForm[] $meta
 * @property AttachmentForm[] $attachment
 * @property TaskAnswerAttachmentForm[] $answerAttachment
 * @property LessonForm $lesson
 * @property TaskAnswerForm[] $answers
 */
class TaskForm extends CompositeForm
{
    /**
     * @var Task
     */
    public $_task;
    /**
     * @var integer
     */
    public $type;
    /**
     * @var integer
     */
    public $points;
    /**
     * @var integer
     */
    public $minutes;
    /**
     * @var integer
     */
    public $correct_answers;

    /**
     * TaskForm constructor.
     * @param array $config
     */
    public function __construct($config = [])
    {
        $this->meta = array_map(function () {
            return new MetaForm();
        }, Yii::$app->params['languages']);
        $this->lesson = new LessonForm();
        $this->attachment = [];
        $this->answerAttachment = [];
        $answers = [];
        for ($i = 0; $i < 5; $i++) {
            $answers[] = new TaskAnswerForm($i === 0 ? ['correct' => true] : []);
        }
        $this->answers = $answers;

        parent::__construct($config);
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['type', 'points', 'minutes'], 'integer'],
            ['type', 'default', 'value' => Task::TYPE_NUMBER],
            [['type'], 'in', 'range' => [
                Task::TYPE_NUMBER,
                Task::TYPE_RADIO,
                Task::TYPE_CHECKBOX,
                Task::TYPE_TEXT,
            ]],
            [['points', 'minutes'], 'number', 'min' => 1],

            [['correct_answers'], 'minCorrectAnswers', 'skipOnEmpty' => false],
        ];
    }

    /**
     * @param $attribute
     * @param $params
     */
    public function minCorrectAnswers($attribute, $params)
    {

        if (!count(array_filter($this->answers, function($var){ return (int)$var['correct'] === 1;}))) {
            $this->addError($attribute, Yii::t('model', 'Need to specify at least one correct answer'));
        }
    }

    /**
     * @return array
     */
    protected function internalForms()
    {
        return [
            'meta' => MetaForm::class,
            'lesson' => LessonForm::class,
            'attachment' => AttachmentForm::class,
            'answerAttachment' => TaskAnswerAttachmentForm::class,
            'answers' => TaskAnswerForm::class,
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('model', 'ID'),
            'lesson_id' => Yii::t('model', 'Lesson'),
            'type' => Yii::t('model', 'Type'),
            'points' => Yii::t('model', 'Points'),
            'minutes' => Yii::t('model', 'Minutes'),
        ];
    }

    /**
     * @param bool $withValidation
     * @return bool
     */
    public function save($withValidation = true)
    {
        return $this->_task->save($withValidation);
    }

    /**
     * @param $dto
     */
    public function prepareUpdate($dto)
    {
        $this->lesson->load($dto, '');
        $this->load($dto, '');

        foreach ($dto['translations'] as $locale => $translation) {
            $this->meta[$locale]->load($translation, '');
        }

        $this->attachment = array_map(function($var){
            return new AttachmentForm(['content' => $var]);
        }, $dto['attachments']);

        $this->answerAttachment = array_map(function($var){
            return new TaskAnswerAttachmentForm(['content' => $var]);
        }, $dto['answerAttachments']);

        $answers = array_map(function ($answer) {
            return new TaskAnswerForm(['id' => $answer['id'], 'correct' => $answer['correct']], $answer['translations']);
        }, $dto['answers']);

        if (count($answers) < 5){
            for ($i = 0; $i < 5 - count($answers); $i++) {
                $answers[] = new TaskAnswerForm([]);
            }
        }

        $this->answers = $answers;
    }

    /**
     * @return bool
     * @throws \yii\base\InvalidConfigException
     * @throws \yii\di\NotInstantiableException
     */
    public function bindData()
    {
        $this->_task->load($this->attributes, '');
        $this->_task->load($this->lesson->attributes, '');

        foreach ($this->meta as $locale => $meta) {
            foreach ([Translation::KEY_TITLE, Translation::KEY_DESCRIPTION, Translation::KEY_SOLUTION] as $key) {
                if (!empty($meta->{$key})) {
                    $title = $this->_task->bind('translations');
                    $title->attributes = [
                        'value' => $meta->{$key},
                        'locale' => $locale,
                        'key' => $key,
                        'entity' => Translation::ENTITY_TASK
                    ];
                }
            }
        }

        foreach ($this->attachment as $attachment) {
            $this->_task->getBehavior('attachment')->bindAttachment($attachment->content)->attributes = ['entity' => Attachment::ENTITY_TASK, 'content' => $attachment->content];
        }

        foreach ($this->answerAttachment as $attachment) {
            $this->_task->getBehavior('answerAttachment')->bindAttachment($attachment->content)->attributes = ['entity' => Attachment::ENTITY_TASK_ANSWER, 'content' => $attachment->content];
        }

        foreach ($this->answers as $aKey => $answer) {
            $answerRelation = $this->_task->bind('answers', (int)$answer->id);
            $answerRelation->attributes = $answer->attributes;
            foreach ($answer->meta as $mKey => $answerMeta) {
                if (!empty($answerMeta->title)) {
                    $answerRelation->bind('translations')->attributes = [
                        'value' => $answerMeta->title,
                        'locale' => $mKey,
                        'key' => Translation::KEY_TITLE,
                        'entity' => Translation::ENTITY_TASK_ANSWER
                    ];
                }
            }
        }

        return true;
    }
}