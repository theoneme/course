<?php

namespace common\forms\ar;

use common\models\Translation;

/**
 * Class TranslationForm
 * @package common\forms\ar
 */
class TranslationForm extends Translation
{
    /**
     * @param array $data
     * @param null $formName
     * @return bool
     * @throws \ReflectionException
     */
    public function load($data, $formName = null)
    {
        if (!array_key_exists((new \ReflectionClass($this))->getShortName(), $data)) {
            $formName = '';
        }

        return parent::load($data, $formName);
    }
}