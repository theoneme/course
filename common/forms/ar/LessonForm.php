<?php

namespace common\forms\ar;

use common\forms\ar\composite\AttachmentForm;
use common\forms\ar\composite\MetaForm;
use common\forms\CompositeForm;
use common\models\Attachment;
use common\models\Lesson;
use common\models\Translation;
use Yii;
use common\forms\ar\composite\CategoryForm;

/**
 * Class LessonForm
 * @package common\forms\ar
 *
 * @property MetaForm[] $meta
 * @property AttachmentForm[] $attachment
 * @property CategoryForm $category
 */
class LessonForm extends CompositeForm
{
    /**
     * @var Lesson
     */
    public $_lesson;
    /**
     * @var integer
     */
    public $status;
    /**
     * @var integer
     */
    public $type;
    /**
     * @var integer
     */
    public $sort;
    /**
     * @var integer
     */
    public $xp;

    /**
     * LessonForm constructor.
     * @param array $config
     */
    public function __construct($config = [])
    {
        $this->meta = array_map(function () {
            return new MetaForm();
        }, Yii::$app->params['languages']);
        $this->category = new CategoryForm();
        $this->attachment = [];

        parent::__construct($config);
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['status', 'type', 'sort', 'xp'], 'integer'],

            ['status', 'default', 'value' => Lesson::STATUS_ACTIVE],
            [['status'], 'in', 'range' => [
                Lesson::STATUS_DELETED,
                Lesson::STATUS_DRAFT,
                Lesson::STATUS_ACTIVE,
            ]],
            ['type', 'default', 'value' => Lesson::TYPE_VIDEO],
            [['type'], 'in', 'range' => [
                Lesson::TYPE_VIDEO,
                Lesson::TYPE_PDF,
            ]],
            [['xp'], 'number', 'min' => 1],
        ];
    }

    /**
     * @return array
     */
    protected function internalForms()
    {
        return [
            'meta' => MetaForm::class,
            'category' => CategoryForm::class,
            'attachment' => AttachmentForm::class,
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('model', 'ID'),
            'category_id' => Yii::t('model', 'Category ID'),
            'status' => Yii::t('model', 'Status'),
            'type' => Yii::t('model', 'Type'),
            'sort' => Yii::t('model', 'Sort'),
            'xp' => Yii::t('model', 'XP'),
        ];
    }

    /**
     * @param bool $withValidation
     * @return bool
     */
    public function save($withValidation = true)
    {
        return $this->_lesson->save($withValidation);
    }

    /**
     * @param $dto
     */
    public function prepareUpdate($dto)
    {
        $this->category->load($dto, '');
        $this->load($dto, '');

        foreach ($dto['translations'] as $locale => $translation) {
            $this->meta[$locale]->load($translation, '');
        }

        $this->attachment = array_map(function($var){
            return new AttachmentForm(['content' => $var]);
        }, $dto['attachments']);

    }

    /**
     * @return bool
     * @throws \yii\base\InvalidConfigException
     * @throws \yii\di\NotInstantiableException
     */
    public function bindData()
    {
        $this->_lesson->load($this->attributes, '');
        $this->_lesson->load($this->category->attributes, '');

        foreach ($this->meta as $key => $meta) {
            if (!empty($meta->title)) {
                $title = $this->_lesson->bind('translations');
                $title->attributes = [
                    'value' => $meta->title,
                    'locale' => $key,
                    'key' => Translation::KEY_TITLE,
                    'entity' => Translation::ENTITY_LESSON
                ];
            }

            if (!empty($meta->description)) {
                $description = $this->_lesson->bind('translations');
                $description->attributes = [
                    'value' => $meta->description,
                    'locale' => $key,
                    'key' => Translation::KEY_DESCRIPTION,
                    'entity' => Translation::ENTITY_LESSON
                ];
            }

            if (!empty($meta->slug)) {
                $title = $this->_lesson->bind('translations');
                $title->attributes = [
                    'value' => $meta->slug,
                    'locale' => $key,
                    'key' => Translation::KEY_SLUG,
                    'entity' => Translation::ENTITY_LESSON
                ];
            }
        }

        foreach ($this->attachment as $attachment) {
            $this->_lesson->bindAttachment($attachment->content)->attributes = ['entity' => Attachment::ENTITY_LESSON, 'content' => $attachment->content];
        }

        return true;
    }
}