<?php

namespace common\forms\ar\composite;

use common\forms\CompositeForm;
use common\helpers\UtilityHelper;
use Yii;

/**
 * Class TaskAnswerForm
 * @package common\forms\ar
 *
 * @property TaskAnswerMetaForm[] $meta
 */
class TaskAnswerForm extends CompositeForm
{
    /**
     * @var integer
     */
    public $id;
    /**
     * @var integer
     */
    public $correct;

    /**
     * TaskAnswerForm constructor.
     * @param array $config
     * @param array $translations
     */
    public function __construct($config = [], $translations = [])
    {
        $meta = [];
        foreach (Yii::$app->params['languages'] as $locale => $language) {
            $meta[$locale] = new TaskAnswerMetaForm([], $translations[$locale] ?? []);
        }
        $this->meta = $meta;

        parent::__construct($config);
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['correct'], 'integer'],
        ];
    }

    /**
     * @return array
     */
    protected function internalForms()
    {
        return [
            'meta' => TaskAnswerMetaForm::class,
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('model', 'ID'),
            'correct' => Yii::t('model', 'Correct'),
        ];
    }
}