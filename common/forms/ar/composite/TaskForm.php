<?php

namespace common\forms\ar\composite;

use common\models\Task;
use Yii;
use yii\base\Model;

/**
 * Class TaskForm
 * @package common\forms\ar\composite
 */
class TaskForm extends Model
{
    /**
     * @var integer
     */
    public $task_id;

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['task_id'], 'integer'],
            [['task_id'], 'required'],

            [['task_id'], 'exist', 'skipOnError' => true, 'targetClass' => Task::class, 'targetAttribute' => ['task_id' => 'id']],
        ];
    }

    /**
     * @return array
     */
    public function attributeLabels()
    {
        return [
            'task_id' => Yii::t('model', 'Task')
        ];
    }
}