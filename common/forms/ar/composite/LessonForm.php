<?php

namespace common\forms\ar\composite;

use common\models\Lesson;
use Yii;
use yii\base\Model;

/**
 * Class LessonForm
 * @package common\forms\ar\composite
 */
class LessonForm extends Model
{
    /**
     * @var integer
     */
    public $lesson_id;

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['lesson_id'], 'integer'],
            [['lesson_id'], 'required'],

            [['lesson_id'], 'exist', 'skipOnError' => true, 'targetClass' => Lesson::class, 'targetAttribute' => ['lesson_id' => 'id']],
        ];
    }

    /**
     * @return array
     */
    public function attributeLabels()
    {
        return [
            'lesson_id' => Yii::t('model', 'Lesson')
        ];
    }
}