<?php

namespace common\forms\ar\composite;

use common\models\Course;
use Yii;
use yii\base\Model;

/**
 * Class CourseForm
 * @package common\forms\ar\composite
 */
class CourseForm extends Model
{
    /**
     * @var integer
     */
    public $course_id;

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['course_id'], 'integer'],
            [['course_id'], 'required'],

            [['course_id'], 'exist', 'skipOnError' => true, 'targetClass' => Course::class, 'targetAttribute' => ['course_id' => 'id']],
        ];
    }

    /**
     * @return array
     */
    public function attributeLabels()
    {
        return [
            'course_id' => Yii::t('model', 'Course')
        ];
    }
}