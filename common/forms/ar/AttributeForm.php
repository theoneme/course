<?php

namespace common\forms\ar;

use common\forms\ar\composite\MetaForm;
use common\forms\CompositeForm;
use common\models\Attribute;
use common\models\Translation;
use Yii;

/**
 * Class AttributeForm
 *
 * @property MetaForm $meta
 *
 * @package common\models
 */
class AttributeForm extends CompositeForm
{
    /**
     * @var Attribute
     */
    public $_attribute;
    /**
     * @var string
     */
    public $alias;
    /**
     * @var string
     */
    public $type;
    /**
     * @var string
     */
    public $filter_type;

    /**
     * RequestForm constructor.
     * @param array $config
     */
    public function __construct($config = [])
    {
        $this->meta = array_map(function () {
            return new MetaForm();
        }, Yii::$app->params['languages']);

        parent::__construct($config);
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['alias'], 'unique', 'targetClass' => Attribute::class, 'when' => function ($model) {
                return $model->alias !== $this->_attribute->alias;
            }],
            [['alias'], 'required'],
            [['alias'], 'string', 'max' => 65],
            ['type', 'in', 'range' => [
                Attribute::TYPE_NUMBER,
                Attribute::TYPE_DROPDOWN,
                Attribute::TYPE_BOOLEAN,
                Attribute::TYPE_STRING,
            ]],
            [['type'], 'default', 'value' => Attribute::TYPE_STRING],
            ['filter_type', 'in', 'range' => [
                Attribute::FILTER_TYPE_DROPDOWN,
                Attribute::FILTER_TYPE_RANGE,
                Attribute::FILTER_TYPE_INPUT,
                Attribute::FILTER_TYPE_RADIOLIST,
                Attribute::FILTER_TYPE_CHECKBOXLIST,
                Attribute::FILTER_TYPE_CHECKBOX,
                Attribute::FILTER_TYPE_NONE,
            ]],
            [['filter_type'], 'default', 'value' => Attribute::FILTER_TYPE_NONE],
        ];
    }

    /**
     * @return array
     */
    protected function internalForms()
    {
        return array(
            'meta' => MetaForm::class,
        );
    }

    /**
     * @param bool $withValidation
     * @return bool
     */
    public function save($withValidation = true)
    {
        return $this->_attribute->save($withValidation);
    }

    /**
     * @param $dto
     */
    public function prepareUpdate($dto)
    {
        $this->load($dto, '');

        foreach ($dto['translations'] as $locale => $translation) {
            $this->meta[$locale]->load($translation, '');
        }
    }

    /**
     * @return bool
     * @throws \yii\base\InvalidConfigException
     * @throws \yii\di\NotInstantiableException
     */
    public function bindData()
    {
        $this->_attribute->load($this->attributes, '');

        foreach ($this->meta as $key => $meta) {
            if (!empty($meta->title)) {
                $title = $this->_attribute->bind('translations');
                $title->attributes = [
                    'value' => $meta->title,
                    'locale' => $key,
                    'key' => Translation::KEY_TITLE,
                    'entity' => Translation::ENTITY_ATTRIBUTE
                ];
            }
        }

        return true;
    }
}