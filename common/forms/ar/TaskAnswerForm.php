<?php

namespace common\forms\ar;

use common\forms\ar\composite\AttachmentForm;
use common\forms\ar\composite\MetaForm;
use common\forms\CompositeForm;
use common\models\TaskAnswer;
use common\models\Translation;
use Yii;
use common\forms\ar\composite\TaskForm;

/**
 * Class TaskAnswerForm
 * @package common\forms\ar
 *
 * @property MetaForm[] $meta
 * @property AttachmentForm[] $attachment
 * @property TaskForm $task
 */
class TaskAnswerForm extends CompositeForm
{
    /**
     * @var TaskAnswer
     */
    public $_taskAnswer;
    /**
     * @var integer
     */
    public $correct;

    /**
     * TaskAnswerForm constructor.
     * @param array $config
     */
    public function __construct($config = [])
    {
        $this->meta = array_map(function () {
            return new MetaForm();
        }, Yii::$app->params['languages']);
        $this->task = new TaskForm();

        parent::__construct($config);
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['correct'], 'integer'],
        ];
    }

    /**
     * @return array
     */
    protected function internalForms()
    {
        return [
            'meta' => MetaForm::class,
            'task' => TaskForm::class,
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('model', 'ID'),
            'correct' => Yii::t('model', 'Correct'),
        ];
    }

    /**
     * @param bool $withValidation
     * @return bool
     */
    public function save($withValidation = true)
    {
        return $this->_taskAnswer->save($withValidation);
    }

    /**
     * @param $dto
     */
    public function prepareUpdate($dto)
    {
        $this->task->load($dto, '');
        $this->load($dto, '');

        foreach ($dto['translations'] as $locale => $translation) {
            $this->meta[$locale]->load($translation, '');
        }
    }

    /**
     * @return bool
     * @throws \yii\base\InvalidConfigException
     * @throws \yii\di\NotInstantiableException
     */
    public function bindData()
    {
        $this->_taskAnswer->load($this->attributes, '');
        $this->_taskAnswer->load($this->task->attributes, '');

        foreach ($this->meta as $key => $meta) {
            if (!empty($meta->title)) {
                $title = $this->_taskAnswer->bind('translations');
                $title->attributes = [
                    'value' => $meta->title,
                    'locale' => $key,
                    'key' => Translation::KEY_TITLE,
                    'entity' => Translation::ENTITY_TASK_ANSWER
                ];
            }
        }

        return true;
    }
}