<?php

namespace common\forms\ar;

use common\models\UserProgress;

/**
 * Class UserProgressForm
 * @package common\forms\ar
 */
class UserProgressForm extends UserProgress
{
    /**
     * @param array $data
     * @param null $formName
     * @return bool
     * @throws \ReflectionException
     */
    public function load($data, $formName = null)
    {
        if (!array_key_exists((new \ReflectionClass($this))->getShortName(), $data)) {
            $formName = '';
        }

        return parent::load($data, $formName);
    }
}