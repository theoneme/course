<?php

namespace common\models;

use Yii;
use yii\db\ActiveRecord;

/**
 * This is the model class for table "translation".
 *
 * @property int $id
 * @property int $entity
 * @property int $entity_id
 * @property string $key
 * @property string $locale
 * @property string $value
 */
class Translation extends ActiveRecord
{
    public const ENTITY_CATEGORY = 0;
    public const ENTITY_ATTRIBUTE = 10;
    public const ENTITY_ATTRIBUTE_VALUE = 20;
    public const ENTITY_USER = 30;
    public const ENTITY_PAGE = 40;
    public const ENTITY_COURSE = 50;
    public const ENTITY_LESSON = 60;
    public const ENTITY_TASK = 70;
    public const ENTITY_TASK_ANSWER = 80;
    public const ENTITY_PAYMENT_METHOD = 90;

    public const KEY_SLUG = 'slug';
    public const KEY_TITLE = 'title';
    public const KEY_SUBTITLE = 'subtitle';
    public const KEY_NAME = 'name';
    public const KEY_DESCRIPTION = 'description';
    public const KEY_SOLUTION = 'solution';

    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'translation';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['entity', 'key', 'locale', 'value'], 'required'],
            [['entity', 'entity_id'], 'integer'],
            [['value'], 'string'],
            [['key'], 'string', 'max' => 55],
            [['locale'], 'string', 'max' => 5],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('model', 'ID'),
            'entity' => Yii::t('model', 'Entity'),
            'entity_id' => Yii::t('model', 'Entity ID'),
            'key' => Yii::t('model', 'Key'),
            'locale' => Yii::t('model', 'Locale'),
            'value' => Yii::t('model', 'Value'),
        ];
    }
}
