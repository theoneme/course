<?php

namespace common\models;

use common\behaviors\AttachmentBehavior;
use common\behaviors\LinkableBehavior;
use Yii;
use yii\db\ActiveRecord;

/**
 * This is the model class for table "lesson".
 *
 * @property int $id
 * @property int $category_id
 * @property int|null $status
 * @property int|null $type
 * @property int|null $sort
 * @property int|null $xp
 *
 * @property Category $category
 * @property LessonCourse[] $lessonCourses
 * @property Course[] $courses
 * @property Translation[] $translations
 * @property Attachment[] $attachments
 *
 * @mixin LinkableBehavior
 * @mixin AttachmentBehavior
 */
class Lesson extends ActiveRecord
{
    public const STATUS_DELETED = -10;
    public const STATUS_DRAFT = 0;
    public const STATUS_ACTIVE = 10;

    public const TYPE_VIDEO = 0;
    public const TYPE_PDF = 10;

    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'lesson';
    }

    /**
     * @return array
     */
    public function behaviors()
    {
        return [
            'linkable' => [
                'class' => LinkableBehavior::class,
                'relations' => ['translations'],
            ],
            'attachment' => [
                'class' => AttachmentBehavior::class,
                'folder' => 'lesson',
            ],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['category_id'], 'required'],
            [['category_id', 'status', 'type', 'sort', 'xp'], 'integer'],
            [['category_id'], 'exist', 'skipOnError' => true, 'targetClass' => Category::class, 'targetAttribute' => ['category_id' => 'id']],
            ['status', 'default', 'value' => self::STATUS_ACTIVE],
            [['status'], 'in', 'range' => [
                self::STATUS_DELETED,
                self::STATUS_DRAFT,
                self::STATUS_ACTIVE,
            ]],
            ['type', 'default', 'value' => self::TYPE_VIDEO],
            [['type'], 'in', 'range' => [
                self::TYPE_VIDEO,
                self::TYPE_PDF,
            ]],
            [['xp'], 'number', 'min' => 1],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('model', 'ID'),
            'category_id' => Yii::t('model', 'Category ID'),
            'status' => Yii::t('model', 'Status'),
            'type' => Yii::t('model', 'Type'),
            'sort' => Yii::t('model', 'Sort'),
            'xp' => Yii::t('model', 'XP'),
        ];
    }

    /**
     * Gets query for [[Category]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getCategory()
    {
        return $this->hasOne(Category::class, ['id' => 'category_id']);
    }

    /**
     * Gets query for [[Courses]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getCourses()
    {
        return $this->hasMany(Course::class, ['id' => 'course_id'])->viaTable('lesson_course', ['lesson_id' => 'id']);
    }

    /**
     * Gets query for [[Courses]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getLessonCourses()
    {
        return $this->hasMany(LessonCourse::class, ['lesson_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getTranslations()
    {
        return $this->hasMany(Translation::class, ['entity_id' => 'id'])
            ->from('translation lesson_translations')
            ->andOnCondition(['lesson_translations.entity' => Translation::ENTITY_LESSON]);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getAttachments()
    {
        return $this->hasMany(Attachment::class, ['entity_id' => 'id'])->andOnCondition(['attachment.entity' => Attachment::ENTITY_LESSON]);
    }

    /**
     * @throws \Throwable
     * @throws \yii\db\StaleObjectException
     */
    public function beforeDelete()
    {
        foreach ($this->attachments as $attachment) {
            $attachment->delete();
        }
        Translation::deleteAll(['entity' => Translation::ENTITY_LESSON, 'entity_id' => $this->id]);
        Flag::deleteAll(['entity' => 'lesson', 'entity_id' => $this->id]);
        LessonCourse::deleteAll(['lesson_id' => $this->id]);
        return parent::beforeDelete();
    }
}
