<?php

namespace common\models;

use common\behaviors\LinkableBehavior;
use Yii;
use yii\db\ActiveRecord;

/**
 * This is the model class for table "attribute".
 *
 * @property int $id
 * @property string $alias
 * @property int $type
 * @property int $filter_type
 *
 * @property AttributeValue[] $attributeValues
 * @property Category[] $categories
 * @property Translation[] $translations
 *
 * @mixin LinkableBehavior
 */
class Attribute extends ActiveRecord
{
    public const TYPE_NUMBER = 0;
    public const TYPE_DROPDOWN = 10;
    public const TYPE_BOOLEAN = 20;
    public const TYPE_STRING = 30;

    public const FILTER_TYPE_DROPDOWN = 0;
    public const FILTER_TYPE_RANGE = 10;
    public const FILTER_TYPE_INPUT = 20;
    public const FILTER_TYPE_RADIOLIST = 30;
    public const FILTER_TYPE_CHECKBOXLIST = 40;
    public const FILTER_TYPE_CHECKBOX = 50;
    public const FILTER_TYPE_NONE = 60;

    public static $filterTypeToView = [
        self::FILTER_TYPE_DROPDOWN => 'dropdown',
        self::FILTER_TYPE_RANGE => 'range',
        self::FILTER_TYPE_INPUT => 'textbox',
        self::FILTER_TYPE_RADIOLIST => 'radiolist',
        self::FILTER_TYPE_CHECKBOXLIST => 'checkboxlist',
        self::FILTER_TYPE_CHECKBOX => 'checkbox',
        self::FILTER_TYPE_NONE => 'none'
    ];

    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'attribute';
    }

    /**
     * @return array
     */
    public function behaviors()
    {
        return [
            'linkable' => [
                'class' => LinkableBehavior::class,
                'relations' => ['translations'],
            ],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['alias', 'type'], 'required'],
            [['alias'], 'unique'],
            [['alias'], 'string', 'max' => 65],
            [['type'], 'integer'],
            [['filter_type'], 'integer'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('model', 'ID'),
            'alias' => Yii::t('model', 'Alias'),
            'type' => Yii::t('model', 'Type'),
            'filter_type' => Yii::t('model', 'Filter type'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getAttributeValues()
    {
        return $this->hasMany(AttributeValue::class, ['attribute_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getTranslations()
    {
        return $this->hasMany(Translation::class, ['entity_id' => 'id'])
            ->from('translation attribute_translations')
            ->andOnCondition(['attribute_translations.entity' => Translation::ENTITY_ATTRIBUTE]);
    }

    /**
     * @throws \Throwable
     * @throws \yii\db\StaleObjectException
     */
    public function beforeDelete()
    {
        foreach($this->attributeValues as $value) {
            $value->delete();
        }
        Translation::deleteAll(['entity' => Translation::ENTITY_ATTRIBUTE, 'entity_id' => $this->id]);
        return parent::beforeDelete();
    }
}
