<?php

namespace common\models;

use common\behaviors\LinkableBehavior;
use creocoder\nestedsets\NestedSetsBehavior;

use Yii;
use yii\db\ActiveRecord;

/**
 * This is the model class for table "category".
 *
 * @property int $id
 * @property int $lft
 * @property int $rgt
 * @property int $lvl
 * @property int $root
 * @property string $attribute_group_alias
 *
 * @property Translation[] $translations
 *
 * @mixin LinkableBehavior
 * @mixin NestedSetsBehavior
 */
class Category extends ActiveRecord
{

    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'category';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['lft', 'rgt', 'lvl', 'root'], 'integer'],
            ['attribute_group_alias', 'string'],
            ['attribute_group_alias', 'default', 'value' => 'root'],
        ];
    }

    /**
     * @return array
     */
    public function behaviors()
    {
        return [
            'linkable' => [
                'class' => LinkableBehavior::class,
                'relations' => ['translations'],
            ],
            'tree' => [
                'class' => NestedSetsBehavior::class,
                'depthAttribute' => 'lvl',
                'treeAttribute' => 'root',
            ],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('model', 'ID'),
            'lft' => Yii::t('model', 'Lft'),
            'rgt' => Yii::t('model', 'Rgt'),
            'lvl' => Yii::t('model', 'Lvl'),
            'root' => Yii::t('model', 'Root'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getTranslations()
    {
        return $this->hasMany(Translation::class, ['entity_id' => 'id'])
            ->from('translation category_translations')
            ->andOnCondition(['category_translations.entity' => Translation::ENTITY_CATEGORY]);
    }

    /**
     * @inheritdoc
     */
    public function afterDelete()
    {
        Translation::deleteAll(['entity' => Translation::ENTITY_CATEGORY, 'entity_id' => $this->id]);

        parent::afterDelete();
    }
}
