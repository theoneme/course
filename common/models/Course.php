<?php

namespace common\models;

use common\behaviors\AttachmentBehavior;
use common\behaviors\LinkableBehavior;
use Yii;
use yii\db\ActiveRecord;

/**
 * This is the model class for table "course".
 *
 * @property int $id
 * @property int $category_id
 * @property int|null $status
 * @property int|null $type
 * @property int $price
 *
 * @property Category $category
 * @property CourseAttribute[] $courseAttributes
 * @property Lesson[] $lessons
 * @property LessonCourse[] $courseLessons
 * @property UserCourse[] $userCourses
 * @property User[] $users
 * @property Translation[] $translations
 * @property Attachment[] $attachments
 *
 * @mixin LinkableBehavior
 * @mixin AttachmentBehavior
 */
class Course extends ActiveRecord
{
    public const STATUS_DELETED = -10;
    public const STATUS_DRAFT = 0;
    public const STATUS_ACTIVE = 10;

    public const TYPE_DEFAULT = 0;
    public const TYPE_TASK_ONLY = 10;

    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'course';
    }

    /**
     * @return array
     */
    public function behaviors()
    {
        return [
            'linkable' => [
                'class' => LinkableBehavior::class,
                'relations' => ['translations', 'courseAttributes', 'courseLessons'],
            ],
            'attachment' => [
                'class' => AttachmentBehavior::class,
                'folder' => 'course',
            ],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['category_id'], 'required'],
            [['category_id', 'status', 'type', 'price'], 'integer'],
            [['category_id'], 'exist', 'skipOnError' => true, 'targetClass' => Category::class, 'targetAttribute' => ['category_id' => 'id']],
            ['price', 'default', 'value' => 0],
            ['status', 'default', 'value' => self::STATUS_ACTIVE],
            [['status'], 'in', 'range' => [
                self::STATUS_DELETED,
                self::STATUS_DRAFT,
                self::STATUS_ACTIVE,
            ]],
            ['type', 'default', 'value' => self::TYPE_DEFAULT],
            [['type'], 'in', 'range' => [
                self::TYPE_DEFAULT,
                self::TYPE_TASK_ONLY,
            ]],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('model', 'ID'),
            'category_id' => Yii::t('model', 'Category ID'),
            'status' => Yii::t('model', 'Status'),
            'type' => Yii::t('model', 'Type'),
            'price' => Yii::t('model', 'Price'),
        ];
    }

    /**
     * Gets query for [[Category]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getCategory()
    {
        return $this->hasOne(Category::class, ['id' => 'category_id']);
    }

    /**
     * Gets query for [[CourseAttributes]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getCourseAttributes()
    {
        return $this->hasMany(CourseAttribute::class, ['course_id' => 'id']);
    }

//    /**
//     * Gets query for [[Lessons]].
//     *
//     * @return \yii\db\ActiveQuery
//     */
//    public function getLessons()
//    {
//        return $this->hasMany(Lesson::class, ['course_id' => 'id']);
//    }

    /**
     * Gets query for [[Lessons]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getLessons()
    {
        return $this->hasMany(Lesson::class, ['id' => 'lesson_id'])->viaTable('lesson_course', ['course_id' => 'id']);
    }

    /**
     * Gets query for [[Courses]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getCourseLessons()
    {
        return $this->hasMany(LessonCourse::class, ['course_id' => 'id']);
    }

    /**
     * Gets query for [[UserCourses]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getUserCourses()
    {
        return $this->hasMany(UserCourse::class, ['course_id' => 'id']);
    }

    /**
     * Gets query for [[Users]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getUsers()
    {
        return $this->hasMany(User::class, ['id' => 'user_id'])->viaTable('user_course', ['course_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getTranslations()
    {
        return $this->hasMany(Translation::class, ['entity_id' => 'id'])
            ->from('translation course_translations')
            ->andOnCondition(['course_translations.entity' => Translation::ENTITY_COURSE]);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getAttachments()
    {
        return $this->hasMany(Attachment::class, ['entity_id' => 'id'])->andOnCondition(['attachment.entity' => Attachment::ENTITY_COURSE]);
    }

    /**
     * @throws \Throwable
     * @throws \yii\db\StaleObjectException
     */
    public function beforeDelete()
    {
        foreach ($this->attachments as $attachment) {
            $attachment->delete();
        }
        Translation::deleteAll(['entity' => Translation::ENTITY_COURSE, 'entity_id' => $this->id]);
        CourseAttribute::deleteAll(['course_id' => $this->id]);
        Flag::deleteAll(['entity' => 'course', 'entity_id' => $this->id]);
        LessonCourse::deleteAll(['course_id' => $this->id]);
        return parent::beforeDelete();
    }
}
