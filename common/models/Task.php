<?php

namespace common\models;

use common\behaviors\AttachmentBehavior;
use common\behaviors\LinkableBehavior;
use Yii;
use yii\db\ActiveRecord;

/**
 * This is the model class for table "task".
 *
 * @property int $id
 * @property int $lesson_id
 * @property int|null $type
 * @property int|null $points
 * @property int|null $minutes
 *
 * @property Lesson $lesson
 * @property TaskAnswer[] $answers
 * @property Translation[] $translations
 * @property Attachment[] $attachments
 * @property Attachment[] $answerAttachments
 *
 * @mixin LinkableBehavior
 * @mixin AttachmentBehavior
 */
class Task extends ActiveRecord
{
    public const TYPE_NUMBER = 0;
    public const TYPE_RADIO = 10;
    public const TYPE_CHECKBOX = 20;
    public const TYPE_TEXT = 30;

    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'task';
    }

    /**
     * @return array
     */
    public function behaviors()
    {
        return [
            'linkable' => [
                'class' => LinkableBehavior::class,
                'relations' => ['translations', 'answers'],
            ],
            'attachment' => [
                'class' => AttachmentBehavior::class,
                'folder' => 'task',
            ],
            'answerAttachment' => [
                'class' => AttachmentBehavior::class,
                'attachmentRelation' => 'answerAttachments',
                'folder' => 'task-answer',
            ],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['lesson_id'], 'required'],
            [['lesson_id', 'type', 'points', 'minutes'], 'integer'],
            [['lesson_id'], 'exist', 'skipOnError' => true, 'targetClass' => Lesson::class, 'targetAttribute' => ['lesson_id' => 'id']],
            ['type', 'default', 'value' => self::TYPE_NUMBER],
            [['type'], 'in', 'range' => [
                self::TYPE_NUMBER,
                self::TYPE_RADIO,
                self::TYPE_CHECKBOX,
                self::TYPE_TEXT,
            ]],
            [['points', 'minutes'], 'number', 'min' => 1],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('model', 'ID'),
            'lesson_id' => Yii::t('model', 'Lesson ID'),
            'type' => Yii::t('model', 'Type'),
            'points' => Yii::t('model', 'Points'),
            'minutes' => Yii::t('model', 'Minutes'),
        ];
    }

    /**
     * Gets query for [[Lesson]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getLesson()
    {
        return $this->hasOne(Lesson::class, ['id' => 'lesson_id']);
    }

    /**
     * Gets query for [[TaskAnswers]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getAnswers()
    {
        return $this->hasMany(TaskAnswer::class, ['task_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getTranslations()
    {
        return $this->hasMany(Translation::class, ['entity_id' => 'id'])
            ->from('translation task_translations')
            ->andOnCondition(['task_translations.entity' => Translation::ENTITY_TASK]);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getAttachments()
    {
        return $this->hasMany(Attachment::class, ['entity_id' => 'id'])->andOnCondition(['attachment.entity' => Attachment::ENTITY_TASK]);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getAnswerAttachments()
    {
        return $this->hasMany(Attachment::class, ['entity_id' => 'id'])
            ->from('attachment answer_attachments')
            ->andOnCondition(['answer_attachments.entity' => Attachment::ENTITY_TASK_ANSWER]);
    }

    /**
     * @throws \Throwable
     * @throws \yii\db\StaleObjectException
     */
    public function beforeDelete()
    {
        foreach ($this->attachments as $attachment) {
            $attachment->delete();
        }
        foreach ($this->answerAttachments as $attachment) {
            $attachment->delete();
        }
        Translation::deleteAll(['entity' => Translation::ENTITY_TASK, 'entity_id' => $this->id]);
        return parent::beforeDelete();
    }
}
