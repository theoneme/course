<?php

namespace common\models;

use Yii;
use yii\db\ActiveRecord;

/**
 * This is the model class for table "attachment".
 *
 * @property int $id
 * @property int $entity
 * @property int $entity_id
 * @property string $content
 *
 */
class Attachment extends ActiveRecord
{
    public const ENTITY_COURSE = 0;
    public const ENTITY_LESSON = 10;
    public const ENTITY_TASK = 20;
    public const ENTITY_TASK_ANSWER = 30;

    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'attachment';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['entity', 'content'], 'required'],
            [['entity', 'entity_id'], 'integer'],
            [['content'], 'string', 'max' => 255],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('model', 'ID'),
            'entity' => Yii::t('model', 'Entity'),
            'entity_id' => Yii::t('model', 'Entity ID'),
            'content' => Yii::t('model', 'Content'),
        ];
    }

    /**
     * @inheritdoc
     */
    public function afterSave($insert, $changedAttributes)
    {
        parent::afterSave($insert, $changedAttributes);
        if ($insert) {
            Yii::$app->mediaLayer->saveMedia($this->content);
        }
    }

    /**
     * @inheritdoc
     */
    public function afterDelete()
    {
        parent::afterDelete();
        Yii::$app->mediaLayer->removeMedia($this->content);
    }
}
