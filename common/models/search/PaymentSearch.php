<?php

namespace common\models\search;

use common\dto\PaymentDTO;
use common\helpers\UtilityHelper;
use common\interfaces\RepositoryInterface;
use common\models\Payment;
use yii\data\Pagination;

/**
 * Class PaymentSearch
 * @package common\models\search
 */
class PaymentSearch extends Payment
{
    /**
     * @var RepositoryInterface
     */
    private $_paymentRepository;
    /**
     * @var array
     */
    private $_config;

    /**
     * PaymentSearch constructor.
     * @param RepositoryInterface $paymentRepository
     * @param array $c
     * @param array $config
     */
    public function __construct(RepositoryInterface $paymentRepository, array $c, array $config = [])
    {
        parent::__construct($config);
        $this->_paymentRepository = $paymentRepository;
        $this->_config = $c;
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['user_id'], 'each', 'rule' => ['integer'], 'when' => function ($model) {
                return is_array($model->user_id);
            }],
            [['user_id'], 'integer', 'when' => function ($model) {
                return !is_array($model->user_id);
            }],
            [['payment_method_id'], 'each', 'rule' => ['integer'], 'when' => function ($model) {
                return is_array($model->payment_method_id);
            }],
            [['payment_method_id'], 'integer', 'when' => function ($model) {
                return !is_array($model->payment_method_id);
            }],
            [['currency_code'], 'each', 'rule' => ['string'], 'when' => function ($model) {
                return is_array($model->currency_code);
            }],
            [['currency_code'], 'string', 'when' => function ($model) {
                return !is_array($model->currency_code);
            }],
            ['status', 'in', 'range' => [self::STATUS_PENDING, self::STATUS_REFUSED, self::STATUS_PAID], 'allowArray' => true],
            ['type', 'in', 'range' => [self::TYPE_COURSE], 'allowArray' => true],
        ];
    }

    /**
     * @param $params
     * @return array
     * @throws \yii\base\InvalidArgumentException
     */
    public function search($params)
    {
        $limit = $this->_config['limit'] ?? null;
        $withPagination = $this->_config['pagination'] ?? false;
        $pageSize = $this->_config['perPage'] ?? 20;
        $indexBy = $this->_config['indexBy'] ?? null;

        $result = [
            'items' => []
        ];

        $formName = null;
        if (!array_key_exists((new \ReflectionClass($this))->getShortName(), $params)) {
            $formName = '';
        }
        $this->load($params, $formName);
        if (!$this->validate()) {
            return $result;
        }

        $criteria = array_filter($this->attributes);
        $criteria = UtilityHelper::fixAmbiguousCondition($criteria, self::tableName(), $this->_paymentRepository);

        /** @var mixed $query */
        $query = $this->initQuery();

        if ($limit !== null) {
            $query->limit($limit);
        }
        if ($indexBy !== null) {
            $query->indexBy($indexBy);
        }

        $criteria = $this->improveCriteria($criteria, $params);

        if ($withPagination === true && ($pageSize < $limit || $limit === null)) {
            $totalCount = $this->_paymentRepository->countByCriteria($criteria);
            if ($totalCount > $pageSize) {
                $pages = new Pagination([
                    'totalCount' => $totalCount,
                    'pageSize' => $pageSize,
                    'params' => $params
                ]);
                $result['pagination'] = $pages;
                $query->offset($pages->offset)->limit($pages->limit);
            }
        }

        $data = $query->findManyByCriteria($criteria);
        $result['items'] = array_filter(array_map(function ($value) {
            $dto = new PaymentDTO($value);
            return $dto->getData(PaymentDTO::MODE_SHORT);
        }, $data));

        return $result;
    }

    /**
     * @return RepositoryInterface
     */
    protected function initQuery()
    {
        $query = $this->_paymentRepository->groupBy('payment.id');

        return $query;
    }

    /**
     * @param $criteria
     * @param $params
     * @return mixed
     */
    protected function improveCriteria($criteria, $params)
    {
        return $criteria;
    }
}
