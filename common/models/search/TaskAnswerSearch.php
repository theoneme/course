<?php

namespace common\models\search;

use common\dto\TaskAnswerDTO;
use common\helpers\UtilityHelper;
use common\interfaces\repositories\TaskAnswerRepositoryInterface;
use common\interfaces\RepositoryInterface;
use common\models\TaskAnswer;
use Yii;
use yii\base\Model;
use yii\data\Pagination;
use yii\db\ActiveQuery;

/**
 * Class TaskAnswerSearch
 * @package common\models\search
 */
class TaskAnswerSearch extends TaskAnswer
{
    /**
     * @var RepositoryInterface
     */
    protected $_taskAnswerRepository;
    /**
     * @var array
     */
    protected $_config;

    /**
     * TaskAnswerSearch constructor.
     * @param TaskAnswerRepositoryInterface $taskAnswerRepository
     * @param array $c
     * @param array $config
     */
    public function __construct(TaskAnswerRepositoryInterface $taskAnswerRepository, array $c = [], array $config = [])
    {
        parent::__construct($config);
        $this->_taskAnswerRepository = $taskAnswerRepository;
        $this->_config = $c;
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id'], 'each', 'rule' => ['integer'], 'when' => function ($model) {
                return is_array($model->id);
            }],
            [['id'], 'integer', 'when' => function ($model) {
                return !is_array($model->id);
            }],
            [['task_id'], 'each', 'rule' => ['integer'], 'when' => function ($model) {
                return is_array($model->task_id);
            }],
            [['task_id'], 'integer', 'when' => function ($model) {
                return !is_array($model->task_id);
            }],
            [['correct'], 'integer'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        return Model::scenarios();
    }

    /**
     * @param $params
     * @return array
     */
    public function search($params)
    {
        $limit = $this->_config['limit'] ?? null;
        $indexBy = $this->_config['indexBy'] ?? null;
        $orderBy = $this->_config['orderBy'] ?? null;
        $exclude = $this->_config['exclude'] ?? null;

        $formName = null;
        if (!array_key_exists((new \ReflectionClass($this))->getShortName(), $params)) {
            $formName = '';
        }

        $this->load($params, $formName);
        if (!$this->validate()) {
            return [
                'items' => []
            ];
        }

        $criteria = array_filter($this->attributes, function ($val) {
            return $val !== null && $val !== '';
        });
        $criteria = UtilityHelper::fixAmbiguousCondition($criteria, self::tableName(), $this->_taskAnswerRepository);

        $query = $this->initQuery($params);

        if ($limit !== null) {
            $query->limit($limit);
        }
        if ($indexBy !== null) {
            $query->indexBy($indexBy);
        }
        if ($orderBy !== null) {
            $query->orderBy($orderBy);
        }
        if ($exclude !== null) {
            $criteria = ['and', $criteria, ['not', ['id' => $exclude]]];
        }

        $criteria = $this->improveCriteria($criteria, $params);

        return $this->processSearch($params, $query, $criteria);
    }

    /**
     * @param $params
     * @param mixed $query
     * @param $criteria
     * @return array
     */
    protected function processSearch($params, $query, $criteria)
    {
        $limit = $this->_config['limit'] ?? null;
        $withPagination = $this->_config['pagination'] ?? false;
        $pageSize = $this->_config['perPage'] ?? 20;
        $result = [
            'items' => []
        ];

        if ($withPagination === true && ($pageSize < $limit || $limit === null)) {
            $totalCount = $query->select(['task_answer.id'], true)->countByCriteria($criteria);
            $query->select(['*'], true);

            unset($params['type']);
            $pages = new Pagination([
                'totalCount' => $totalCount,
                'pageSize' => $pageSize,
                'params' => $params
            ]);
            $result['pagination'] = $pages;
            $query->offset($pages->offset)->limit($pages->limit);
        }

        $data = $query->findManyByCriteria($criteria, true);
        $result['items'] = array_filter(array_map(function ($value) {
            $dto = new TaskAnswerDTO($value);
            return $dto->getData(TaskAnswerDTO::MODE_SHORT);
        }, $data));

        return $result;
    }

    /**
     * @param array $params
     * @return mixed
     */
    protected function initQuery(array $params = [])
    {
        $query = $this->_taskAnswerRepository
            ->with(['translations' => function ($query) {
                /** @var ActiveQuery $query */
                $query->andOnCondition(['task_answer_translations.locale' => [Yii::$app->language, 'en-GB', 'ru-RU']]);
            }])
            ->groupBy('task_answer.id');

        return $query;
    }

    /**
     * @param $criteria
     * @param $params
     * @return mixed
     */
    protected function improveCriteria($criteria, $params)
    {
        return $criteria;
    }
}
