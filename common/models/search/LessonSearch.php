<?php

namespace common\models\search;

use common\dto\LessonDTO;
use common\helpers\UtilityHelper;
use common\interfaces\repositories\LessonRepositoryInterface;
use common\interfaces\RepositoryInterface;
use common\models\Lesson;
use common\models\Translation;
use Yii;
use yii\base\Model;
use yii\data\Pagination;
use yii\db\ActiveQuery;

/**
 * Class LessonSearch
 * @package common\models\search
 */
class LessonSearch extends Lesson
{
    /**
     * @var RepositoryInterface
     */
    protected $_lessonRepository;
    /**
     * @var array
     */
    protected $_config;
    /**
     * @var string
     */
    public $title;
    /**
     * @var string
     */
    public $course_id;

    /**
     * LessonSearch constructor.
     * @param LessonRepositoryInterface $lessonRepository
     * @param array $c
     * @param array $config
     */
    public function __construct(LessonRepositoryInterface $lessonRepository, array $c = [], array $config = [])
    {
        parent::__construct($config);
        $this->_lessonRepository = $lessonRepository;
        $this->_config = $c;
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            ['status', 'in', 'range' => [
                Lesson::STATUS_DELETED,
                Lesson::STATUS_DRAFT,
                Lesson::STATUS_ACTIVE,
            ], 'allowArray' => true],
            ['type', 'in', 'range' => [
                Lesson::TYPE_VIDEO,
                Lesson::TYPE_PDF,
            ], 'allowArray' => true],
            [['id'], 'each', 'rule' => ['integer'], 'when' => function ($model) {
                return is_array($model->id);
            }],
            [['id'], 'integer', 'when' => function ($model) {
                return !is_array($model->id);
            }],
            [['category_id'], 'each', 'rule' => ['integer'], 'when' => function ($model) {
                return is_array($model->category_id);
            }],
            [['category_id'], 'integer', 'when' => function ($model) {
                return !is_array($model->category_id);
            }],
            [['course_id'], 'each', 'rule' => ['integer'], 'when' => function ($model) {
                return is_array($model->course_id);
            }],
            [['course_id'], 'integer', 'when' => function ($model) {
                return !is_array($model->course_id);
            }],
            [['title'], 'string']
        ];
    }

    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        return Model::scenarios();
    }

    /**
     * @param $params
     * @return array
     */
    public function search($params)
    {
        $limit = $this->_config['limit'] ?? null;
        $indexBy = $this->_config['indexBy'] ?? null;
        $orderBy = $this->_config['orderBy'] ?? ['sort' => SORT_ASC];
        $exclude = $this->_config['exclude'] ?? null;

        $formName = null;
        if (!array_key_exists((new \ReflectionClass($this))->getShortName(), $params)) {
            $formName = '';
        }

        $this->load($params, $formName);
        if (!$this->validate()) {
            return [
                'items' => []
            ];
        }

        $criteria = array_filter($this->attributes, function ($val) {
            return $val !== null && $val !== '';
        });
        $criteria = UtilityHelper::fixAmbiguousCondition($criteria, self::tableName(), $this->_lessonRepository);

        if (!empty($params['root']) && !empty($params['lft']) && !empty($params['rgt'])) {
            $criteria = ['and', ['category.root' => $params['root']], ['>=', 'category.lft', $params['lft']], ['<=', 'category.rgt', $params['rgt']], $criteria];
        }

        $query = $this->initQuery($params);

        if ($limit !== null) {
            $query->limit($limit);
        }
        if ($indexBy !== null) {
            $query->indexBy($indexBy);
        }
        if ($orderBy !== null) {
            $query->orderBy($orderBy);
        }
        if ($exclude !== null) {
            $criteria = ['and', $criteria, ['not', ['id' => $exclude]]];
        }

        $criteria = $this->improveCriteria($criteria, $params);

        return $this->processSearch($params, $query, $criteria);
    }

    /**
     * @param $params
     * @param mixed $query
     * @param $criteria
     * @return array
     */
    protected function processSearch($params, $query, $criteria)
    {
        $limit = $this->_config['limit'] ?? null;
        $withPagination = $this->_config['pagination'] ?? false;
        $pageSize = $this->_config['perPage'] ?? 20;
        $result = [
            'items' => []
        ];

        if ($withPagination === true && ($pageSize < $limit || $limit === null)) {
            $totalCount = $query->select(['lesson.id'], true)->countByCriteria($criteria);
            $query->select(['*'], true);

            unset($params['category_id'], $params['type'], $params['status']);
            $pages = new Pagination([
                'totalCount' => $totalCount,
                'pageSize' => $pageSize,
                'params' => $params
            ]);
            $result['pagination'] = $pages;
            $query->offset($pages->offset)
                ->limit($pages->limit);
        }

        $data = $query->findManyByCriteria($criteria, true);
        $result['items'] = array_filter(array_map(function ($value) {
            $dto = new LessonDTO($value);
            return $dto->getData(LessonDTO::MODE_SHORT);
        }, $data));

        return $result;
    }

    /**
     * @param array $params
     * @return mixed
     */
    protected function initQuery(array $params = [])
    {
        $query = $this->_lessonRepository
            ->with(['translations' => function ($query) {
                /** @var ActiveQuery $query */
                $query->andOnCondition(['lesson_translations.locale' => [Yii::$app->language, 'en-GB', 'ru-RU']]);
            }, 'attachments', 'lessonCourses', 'category.translations'])
            ->groupBy('lesson.id');
        if (!empty($params['root']) && !empty($params['lft']) && !empty($params['rgt'])) {
            $query->joinWith(['category']);
        }
        if (!empty($this->title)) {
            $query->joinWith(['translations']);
        }
        if (!empty($this->course_id)) {
            $query->joinWith(['lessonCourses']);
        }

        return $query;
    }

    /**
     * @param $criteria
     * @param $params
     * @return mixed
     */
    protected function improveCriteria($criteria, $params)
    {
        if (!empty($this->title)) {
            $criteria = ['and',
                $criteria,
                ['or',
                    ['lesson.id' => $this->title],
                    ['and',
                        ['like', 'lesson_translations.value', "%{$this->title}%", false],
                        ['lesson_translations.key' => Translation::KEY_TITLE]
                    ]
                ],
            ];
        }
        if (!empty($this->course_id)) {
            $criteria = ['and', ['course_id' => $params['course_id']], $criteria];
        }
        return $criteria;
    }
}
