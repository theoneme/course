<?php

namespace common\models\search;

use common\dto\UserProgressDTO;
use common\helpers\UtilityHelper;
use common\interfaces\repositories\UserProgressRepositoryInterface;
use common\interfaces\RepositoryInterface;
use common\models\UserProgress;
use yii\base\Model;
use yii\data\Pagination;
use yii\helpers\ArrayHelper;

/**
 * Class UserProgressSearch
 * @package common\models\search
 */
class UserProgressSearch extends UserProgress
{
    /**
     * @var RepositoryInterface
     */
    protected $_userProgressRepository;
    /**
     * @var array
     */
    protected $_config;

    /**
     * UserProgressSearch constructor.
     * @param UserProgressRepositoryInterface $userProgressRepository
     * @param array $c
     * @param array $config
     */
    public function __construct(UserProgressRepositoryInterface $userProgressRepository, array $c = [], array $config = [])
    {
        parent::__construct($config);
        $this->_userProgressRepository = $userProgressRepository;
        $this->_config = $c;
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['user_id', 'entity', 'success'], 'integer'],
            ['entity', 'default', 'value' => self::ENTITY_LESSON],
            ['entity', 'in', 'range' => [self::ENTITY_LESSON, self::ENTITY_LESSON_TASK, self::ENTITY_TASK], 'allowArray' => true],
            [['entity_id'], 'each', 'rule' => ['integer'], 'when' => function ($model) {
                return is_array($model->entity_id);
            }],
            [['entity_id'], 'integer', 'when' => function ($model) {
                return !is_array($model->entity_id);
            }],
        ];
    }

    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        return Model::scenarios();
    }

    /**
     * @param $params
     * @return array
     * @throws \ReflectionException
     */
    public function search($params)
    {
        $limit = $this->_config['limit'] ?? null;
        $withPagination = $this->_config['pagination'] ?? false;
        $pageSize = $this->_config['perPage'] ?? 20;
        $indexBy = $this->_config['indexBy'] ?? null;
        $orderBy = $this->_config['orderBy'] ?? null;

        $result = [
            'items' => []
        ];

        $formName = null;
        if (!array_key_exists((new \ReflectionClass($this))->getShortName(), $params)) {
            $formName = '';
        }
        $this->load($params, $formName);
        if (!$this->validate()) {
            return $result;
        }

        $criteria = array_filter($this->attributes, function ($val) {
            return $val !== null && $val !== '';
        });
        $criteria = UtilityHelper::fixAmbiguousCondition($criteria, self::tableName(), $this->_userProgressRepository);

        /** @var mixed $query */
        $query = $this->initQuery();

        if ($limit !== null) {
            $query->limit($limit);
        }
        if ($indexBy !== null) {
            $query->indexBy($indexBy);
        }
        if ($orderBy !== null) {
            $query->orderBy($orderBy);
        }

        $criteria = $this->improveCriteria($criteria, $params);

        if ($withPagination === true && ($pageSize < $limit || $limit === null)) {
            $totalCount = $this->_userProgressRepository->countByCriteria($criteria);

            $pages = new Pagination([
                'totalCount' => $totalCount,
                'pageSize' => $pageSize,
                'params' => $params
            ]);
            $result['pagination'] = $pages;
            $query->offset($pages->offset)->limit($pages->limit);
        }

        $data = $query->findManyByCriteria($criteria);
        $result['items'] = array_filter(array_map(function ($value) {
            $dto = new UserProgressDTO($value);
            return $dto->getData(UserProgressDTO::MODE_SHORT);
        }, $data));

        return $result;
    }

    /**
     * @return mixed
     */
    protected function initQuery()
    {
        $query = $this->_userProgressRepository->groupBy('user_progress.id');

        return $query;
    }

    /**
     * @param $criteria
     * @param $params
     * @return mixed
     */
    protected function improveCriteria($criteria, $params)
    {
        return $criteria;
    }
}
