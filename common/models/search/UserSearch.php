<?php

namespace common\models\search;

use common\dto\UserDTO;
use common\helpers\UtilityHelper;
use common\interfaces\repositories\UserRepositoryInterface;
use common\interfaces\RepositoryInterface;
use common\models\User;
use yii\base\Model;
use yii\data\Pagination;
use yii\helpers\ArrayHelper;

/**
 * Class UserSearch
 * @package common\models\search
 */
class UserSearch extends User
{
    /**
     * @var RepositoryInterface
     */
    protected $_userRepository;
    /**
     * @var array
     */
    protected $_config;

    /**
     * UserSearch constructor.
     * @param UserRepositoryInterface $userRepository
     * @param array $c
     * @param array $config
     */
    public function __construct(UserRepositoryInterface $userRepository, array $c = [], array $config = [])
    {
        parent::__construct($config);
        $this->_userRepository = $userRepository;
        $this->_config = $c;
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            ['username', 'string'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        return Model::scenarios();
    }

    /**
     * @param $params
     * @return array
     * @throws \ReflectionException
     */
    public function search($params)
    {
        $limit = $this->_config['limit'] ?? null;
        $withPagination = $this->_config['pagination'] ?? false;
        $pageSize = $this->_config['perPage'] ?? 20;
        $indexBy = $this->_config['indexBy'] ?? null;
        $orderBy = $this->_config['orderBy'] ?? null;

        $result = [
            'items' => []
        ];

        $formName = null;
        if (!array_key_exists((new \ReflectionClass($this))->getShortName(), $params)) {
            $formName = '';
        }
        $this->load($params, $formName);
        if (!$this->validate()) {
            return $result;
        }

        $criteria = array_filter($this->attributes, function ($val) {
            return $val !== null && $val !== '';
        });
        $criteria = UtilityHelper::fixAmbiguousCondition($criteria, self::tableName(), $this->_userRepository);

        /** @var mixed $query */
        $query = $this->initQuery();

        if ($limit !== null) {
            $query->limit($limit);
        }
        if ($indexBy !== null) {
            $query->indexBy($indexBy);
        }
        if ($orderBy !== null) {
            $query->orderBy($orderBy);
        }

        $criteria = $this->improveCriteria($criteria, $params);

        if ($withPagination === true && ($pageSize < $limit || $limit === null)) {
            $totalCount = $this->_userRepository->countByCriteria($criteria);

            $pages = new Pagination([
                'totalCount' => $totalCount,
                'pageSize' => $pageSize,
                'params' => $params
            ]);
            $result['pagination'] = $pages;
            $query->offset($pages->offset)->limit($pages->limit);
        }

        $data = $query->findManyByCriteria($criteria);
        $result['items'] = array_filter(array_map(function ($value) {
            $dto = new UserDTO($value);
            return $dto->getData(UserDTO::MODE_SHORT);
        }, $data));

        return $result;
    }

    /**
     * @return mixed
     */
    protected function initQuery()
    {
        $query = $this->_userRepository
            ->with(['translations'])
            ->groupBy('user.id');

        return $query;
    }

    /**
     * @param $criteria
     * @param $params
     * @return mixed
     */
    protected function improveCriteria($criteria, $params)
    {
        $username = ArrayHelper::remove($criteria, 'username');

        if ($username) {
            $criteria = ['and',
                ['like', 'username', $username],
                $criteria
            ];
        }

        return $criteria;
    }
}
