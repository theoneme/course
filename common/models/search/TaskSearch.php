<?php

namespace common\models\search;

use common\dto\TaskDTO;
use common\helpers\UtilityHelper;
use common\interfaces\repositories\TaskRepositoryInterface;
use common\interfaces\RepositoryInterface;
use common\models\Task;
use common\models\Translation;
use Yii;
use yii\base\Model;
use yii\data\Pagination;
use yii\db\ActiveQuery;

/**
 * Class TaskSearch
 * @package common\models\search
 */
class TaskSearch extends Task
{
    /**
     * @var RepositoryInterface
     */
    protected $_taskRepository;
    /**
     * @var array
     */
    protected $_config;
    /**
     * @var string
     */
    public $lessonTitle;

    /**
     * TaskSearch constructor.
     * @param TaskRepositoryInterface $taskRepository
     * @param array $c
     * @param array $config
     */
    public function __construct(TaskRepositoryInterface $taskRepository, array $c = [], array $config = [])
    {
        parent::__construct($config);
        $this->_taskRepository = $taskRepository;
        $this->_config = $c;
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            ['type', 'in', 'range' => [
                Task::TYPE_TEXT,
                Task::TYPE_RADIO,
                Task::TYPE_CHECKBOX,
            ], 'allowArray' => true],
            [['id'], 'each', 'rule' => ['integer'], 'when' => function ($model) {
                return is_array($model->id);
            }],
            [['id'], 'integer', 'when' => function ($model) {
                return !is_array($model->id);
            }],
            [['lesson_id'], 'each', 'rule' => ['integer'], 'when' => function ($model) {
                return is_array($model->lesson_id);
            }],
            [['lesson_id'], 'integer', 'when' => function ($model) {
                return !is_array($model->lesson_id);
            }],
            [['lessonTitle'], 'string']
        ];
    }

    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        return Model::scenarios();
    }

    /**
     * @param $params
     * @return array
     */
    public function search($params)
    {
        $limit = $this->_config['limit'] ?? null;
        $indexBy = $this->_config['indexBy'] ?? null;
        $orderBy = $this->_config['orderBy'] ?? null;
        $exclude = $this->_config['exclude'] ?? null;

        $formName = null;
        if (!array_key_exists((new \ReflectionClass($this))->getShortName(), $params)) {
            $formName = '';
        }

        $this->load($params, $formName);
        if (!$this->validate()) {
            return [
                'items' => []
            ];
        }

        $criteria = array_filter($this->attributes, function ($val) {
            return $val !== null && $val !== '';
        });
        $criteria = UtilityHelper::fixAmbiguousCondition($criteria, self::tableName(), $this->_taskRepository);

        $query = $this->initQuery($params);

        if ($limit !== null) {
            $query->limit($limit);
        }
        if ($indexBy !== null) {
            $query->indexBy($indexBy);
        }
        if ($orderBy !== null) {
            $query->orderBy($orderBy);
        }
        if ($exclude !== null) {
            $criteria = ['and', $criteria, ['not', ['id' => $exclude]]];
        }

        $criteria = $this->improveCriteria($criteria, $params);

        return $this->processSearch($params, $query, $criteria);
    }

    /**
     * @param $params
     * @param mixed $query
     * @param $criteria
     * @return array
     */
    protected function processSearch($params, $query, $criteria)
    {
        $limit = $this->_config['limit'] ?? null;
        $withPagination = $this->_config['pagination'] ?? false;
        $pageSize = $this->_config['perPage'] ?? 20;
        $result = [
            'items' => []
        ];

        if ($withPagination === true && ($pageSize < $limit || $limit === null)) {
            $totalCount = $query->select(['task.id'], true)->countByCriteria($criteria);
            $query->select(['*'], true);

            unset($params['type']);
            $pages = new Pagination([
                'totalCount' => $totalCount,
                'pageSize' => $pageSize,
                'params' => $params
            ]);
            $result['pagination'] = $pages;
            $query->offset($pages->offset)->limit($pages->limit);
        }

        $data = $query->findManyByCriteria($criteria, true);
        $result['items'] = array_filter(array_map(function ($value) {
            $dto = new TaskDTO($value);
            return $dto->getData(TaskDTO::MODE_SHORT);
        }, $data));

        return $result;
    }

    /**
     * @param array $params
     * @return mixed
     */
    protected function initQuery(array $params = [])
    {
        $query = $this->_taskRepository
            ->with(['translations' => function ($query) {
                /** @var ActiveQuery $query */
                $query->andOnCondition(['task_translations.locale' => [Yii::$app->language, 'en-GB', 'ru-RU']]);
            }, 'attachments',  'answerAttachments', 'lesson.translations', 'answers.translations'])
            ->groupBy('task.id');

        if (!empty($this->lessonTitle)) {
            $query->joinWith(['lesson.translations']);
        }
        return $query;
    }

    /**
     * @param $criteria
     * @param $params
     * @return mixed
     */
    protected function improveCriteria($criteria, $params)
    {
        if (!empty($this->lessonTitle)) {
            $criteria = ['and', $criteria, ['like', 'lesson_translations.value', "{$this->lessonTitle}%", false], ['lesson_translations.key' => Translation::KEY_TITLE]];
        }
        return $criteria;
    }
}
