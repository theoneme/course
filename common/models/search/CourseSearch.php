<?php

namespace common\models\search;

use common\dto\CourseDTO;
use common\helpers\ConditionHelper;
use common\helpers\UtilityHelper;
use common\interfaces\repositories\CourseRepositoryInterface;
use common\interfaces\RepositoryInterface;
use common\models\Course;
use common\repositories\sql\AttributeRepository;
use common\helpers\FilterConditionHelper;
use Yii;
use yii\base\Model;
use yii\data\Pagination;
use yii\db\ActiveQuery;
use yii\db\Query;

/**
 * Class CourseSearch
 * @package common\models\search
 */
class CourseSearch extends Course
{
    /**
     * @var RepositoryInterface
     */
    protected $_courseRepository;
    /**
     * @var array
     */
    protected $_config;

    /**
     * CourseSearch constructor.
     * @param CourseRepositoryInterface $courseRepository
     * @param array $c
     * @param array $config
     */
    public function __construct(CourseRepositoryInterface $courseRepository, array $c = [], array $config = [])
    {
        parent::__construct($config);
        $this->_courseRepository = $courseRepository;
        $this->_config = $c;
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            ['status', 'in', 'range' => [
                Course::STATUS_DELETED,
                Course::STATUS_DRAFT,
                Course::STATUS_ACTIVE,
            ], 'allowArray' => true],
            ['type', 'in', 'range' => [
                Course::TYPE_DEFAULT,
                Course::TYPE_TASK_ONLY,
            ], 'allowArray' => true],
            [['id'], 'each', 'rule' => ['integer'], 'when' => function ($model) {
                return is_array($model->id);
            }],
            [['id'], 'integer', 'when' => function ($model) {
                return !is_array($model->id);
            }],
            [['category_id'], 'each', 'rule' => ['integer'], 'when' => function ($model) {
                return is_array($model->category_id);
            }],
            [['category_id'], 'integer', 'when' => function ($model) {
                return !is_array($model->category_id);
            }],
        ];
    }

    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        return Model::scenarios();
    }

    /**
     * @param $params
     * @return array
     */
    public function search($params)
    {
        $limit = $this->_config['limit'] ?? null;
        $indexBy = $this->_config['indexBy'] ?? null;
        $orderBy = $this->_config['orderBy'] ?? null;
        $exclude = $this->_config['exclude'] ?? null;

        $formName = null;
        if (!array_key_exists((new \ReflectionClass($this))->getShortName(), $params)) {
            $formName = '';
        }

        $this->load($params, $formName);
        if (!$this->validate()) {
            return [
                'items' => []
            ];
        }

        $criteria = array_filter($this->attributes, function ($val) {
            return $val !== null && $val !== '';
        });
        $criteria = UtilityHelper::fixAmbiguousCondition($criteria, self::tableName(), $this->_courseRepository);

        $query = $this->initQuery($params);

        $filterConditionHelper = new FilterConditionHelper();
        $criteria = ['and', $filterConditionHelper->process($params, ['category']), $criteria];

        /** @var AttributeRepository $attributeRepository */
        $attributeRepository = Yii::$container->get(AttributeRepository::class);
        $availableAttributes = $attributeRepository->select(['id', 'alias'])
            ->indexBy('alias')
            ->findManyByCriteria([
                'alias' => array_keys($params)
            ], true);


        /** @var mixed $query */
        $query = $this->applyQueryParams($query, $availableAttributes, $params);

        if ($limit !== null) {
            $query->limit($limit);
        }
        if ($indexBy !== null) {
            $query->indexBy($indexBy);
        }
        if ($orderBy !== null) {
            $query->orderBy($orderBy);
        }
        if ($exclude !== null) {
            $criteria = ['and', $criteria, ['not', ['id' => $exclude]]];
        }

        $criteria = $this->improveCriteria($criteria, $params);

        return $this->processSearch($params, $query, $criteria);
    }

    /**
     * @param $params
     * @param mixed $query
     * @param $criteria
     * @return array
     */
    protected function processSearch($params, $query, $criteria)
    {
        $limit = $this->_config['limit'] ?? null;
        $withPagination = $this->_config['pagination'] ?? false;
        $pageSize = $this->_config['perPage'] ?? 20;
        $result = [
            'items' => []
        ];

        if ($withPagination === true && ($pageSize < $limit || $limit === null)) {
            $totalCount = $query->select(['course.id'], true)->countByCriteria($criteria);
            $query->select(['*'], true);

            unset($params['category_id'], $params['status']);
            $pages = new Pagination([
                'totalCount' => $totalCount,
                'pageSize' => $pageSize,
                'params' => $params
            ]);
            $result['pagination'] = $pages;
            $query->offset($pages->offset)
                ->limit($pages->limit);
        }

        $data = $query->findManyByCriteria($criteria, true);
        $result['items'] = array_filter(array_map(function ($value) {
            $dto = new CourseDTO($value);
            return $dto->getData(CourseDTO::MODE_SHORT);
        }, $data));

        return $result;
    }

    /**
     * @param array $params
     * @return mixed
     */
    protected function initQuery(array $params = [])
    {
        $query = $this->_courseRepository
            ->with(['translations' => function ($query) {
                /** @var ActiveQuery $query */
                $query->andOnCondition(['course_translations.locale' => [Yii::$app->language, 'en-GB', 'ru-RU']]);
            }, 'courseAttributes', 'attachments', 'category.translations', 'lessons.translations'])
            ->groupBy('course.id');
        if (!empty($params['root']) && !empty($params['lft']) && !empty($params['rgt'])) {
            $query->joinWith(['category']);
        }

        return $query;
    }

    /**
     * @param $criteria
     * @param $params
     * @return mixed
     */
    protected function improveCriteria($criteria, $params)
    {
        return $criteria;
    }

    /**
     * @param RepositoryInterface $query
     * @param $availableAttributes
     * @param $params
     * @return Query
     */
    protected function applyQueryParams($query, $availableAttributes, $params)
    {
        $paramsToApply = array_intersect_key($params, $availableAttributes);
        foreach ($paramsToApply as $key => $param) {
            if (array_key_exists('min', (array)$param) || array_key_exists('max', (array)$param)) {
                $query = $this->applyRangeAttributeQuery($query, $key, $param);
            } else {
                $query = $this->applyAttributeQuery($query, $key, $param);
            }
        }

        return $query;
    }

    /**
     * @param RepositoryInterface $query
     * @param $key
     * @param $param
     * @return mixed
     */
    protected function applyAttributeQuery($query, $key, $param)
    {
        $relationAlias = ConditionHelper::getRelationName($this->_courseRepository, 'course_attribute');

        $condition = [
            "{$relationAlias}.entity_alias" => $key,
            "{$relationAlias}.value_alias" => $param
        ];

        return $query->relationExists($relationAlias, 'course_id', $condition);
    }

    /**
     * @param RepositoryInterface $query
     * @param $key
     * @param $param
     * @return mixed
     */
    protected function applyRangeAttributeQuery($query, $key, $param)
    {
        $relationAlias = ConditionHelper::getRelationName($this->_courseRepository, 'course_attribute');

        $rangeCondition = ['and', [
            "{$relationAlias}.entity_alias" => $key
        ]];

        if (array_key_exists('min', $param)) {
            $rangeCondition[] = ['>=', "{$relationAlias}.value_number", (float)$param['min']];
        }
        if (array_key_exists('max', $param)) {
            $rangeCondition[] = ['<=', "{$relationAlias}.value_number", (float)$param['max']];
        }

        return $query->relationExists($relationAlias, 'course_id', $rangeCondition);
    }
}
