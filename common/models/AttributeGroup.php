<?php

namespace common\models;

use common\behaviors\LinkableBehavior;
use Yii;
use yii\db\ActiveRecord;

/**
 * This is the model class for table "attribute_group".
 *
 * @property int $id
 * @property string $alias
 * @property integer $entity
 *
 * @property AttributeToGroup[] $groupAttributes
 * @property Attribute[] $attrs
 *
 * @mixin LinkableBehavior
 */
class AttributeGroup extends ActiveRecord
{
    public const ENTITY_COURSE_CATEGORY = 0;

    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'attribute_group';
    }

    /**
     * @return array
     */
    public function behaviors()
    {
        return [
            'linkable' => [
                'class' => LinkableBehavior::class,
                'relations' => ['groupAttributes'],
            ],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['entity'], 'integer'],
            ['alias', 'string', 'max' => 35],
            [['alias'], 'required'],
            [['alias', 'entity'], 'unique', 'targetAttribute' => ['alias', 'entity']]
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('model', 'ID'),
            'alias' => Yii::t('model', 'Alias'),
            'entity' => Yii::t('model', 'Entity'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getGroupAttributes()
    {
        return $this->hasMany(AttributeToGroup::class, ['attribute_group_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getAttrs()
    {
        return $this->hasMany(Attribute::class, ['id' => 'attribute_id'])->viaTable('attribute_to_group', ['attribute_group_id' => 'id']);
    }
}
