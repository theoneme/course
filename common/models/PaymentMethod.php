<?php

namespace common\models;

use common\behaviors\ImageBehavior;
use common\behaviors\LinkableBehavior;
use Yii;
use yii\db\ActiveRecord;

/**
 * This is the model class for table "payment_method".
 *
 * @property int $id
 * @property int $alias
 * @property int $logo
 * @property int $sort
 * @property int $enabled
 *
 * @property Payment[] $payments
 * @property Translation[] $translations
 *
 * @mixin LinkableBehavior
 * @mixin ImageBehavior
 */
class PaymentMethod extends ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'payment_method';
    }

    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            'image' => [
                'class' => ImageBehavior::class,
                'folder' => 'payment-method',
                'imageField' => 'logo',
                'labelField' => 'alias'
            ],
            'linkable' => [
                'class' => LinkableBehavior::class,
                'relations' => ['translations'],
            ],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['alias'], 'required'],
            [['sort', 'enabled'], 'integer'],
            [['logo', 'alias'], 'string'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('model', 'ID'),
            'alias' => Yii::t('model', 'Alias'),
            'logo' => Yii::t('model', 'Logo'),
            'sort' => Yii::t('model', 'Sort'),
            'enabled' => Yii::t('model', 'Enabled'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPayments()
    {
        return $this->hasMany(Payment::class, ['payment_method_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getTranslations()
    {
        return $this->hasMany(Translation::class, ['entity_id' => 'id'])
            ->from('translation pm_translations')
            ->andOnCondition(['pm_translations.entity' => Translation::ENTITY_PAYMENT_METHOD]);
    }

    /**
     * @throws \Throwable
     * @throws \yii\db\StaleObjectException
     */
    public function beforeDelete()
    {
        Yii::$app->mediaLayer->removeMedia($this->logo);
        Translation::deleteAll(['entity' => Translation::ENTITY_PAYMENT_METHOD, 'entity_id' => $this->id]);
        return parent::beforeDelete();
    }
}
