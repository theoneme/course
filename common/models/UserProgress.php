<?php

namespace common\models;

use common\behaviors\JsonFieldBehavior;
use Yii;
use yii\db\ActiveRecord;

/**
 * This is the model class for table "user_progress".
 *
 * @property int $id
 * @property int $user_id
 * @property int $entity
 * @property int $entity_id
 * @property int $success
 * @property resource|null $custom_data
 *
 * @property User $user
 */
class UserProgress extends ActiveRecord
{
    public const ENTITY_LESSON = 0;
    public const ENTITY_LESSON_TASK = 10;
    public const ENTITY_TASK = 20;

    /**
     * @var array
     */
    public $customDataArray = [];

    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'user_progress';
    }

    /**
     * @return array
     */
    public function behaviors()
    {
        return [
            'json' => [
                'class' => JsonFieldBehavior::class,
            ],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['user_id', 'entity', 'entity_id'], 'required'],
            [['user_id', 'entity', 'entity_id', 'success'], 'integer'],
            [['custom_data'], 'string'],
            [['user_id'], 'exist', 'skipOnError' => true, 'targetClass' => User::class, 'targetAttribute' => ['user_id' => 'id']],
            ['entity', 'default', 'value' => self::ENTITY_LESSON],
            ['entity', 'in', 'range' => [self::ENTITY_LESSON, self::ENTITY_LESSON_TASK, self::ENTITY_TASK]],
            [['customDataArray'], 'safe'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('model', 'ID'),
            'user_id' => Yii::t('model', 'User ID'),
            'entity' => Yii::t('model', 'Entity'),
            'entity_id' => Yii::t('model', 'Entity ID'),
            'success' => Yii::t('model', 'Success'),
            'custom_data' => Yii::t('model', 'Custom Data'),
        ];
    }

    /**
     * Gets query for [[User]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getUser()
    {
        return $this->hasOne(User::class, ['id' => 'user_id']);
    }
}
