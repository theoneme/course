<?php

namespace common\models;

use common\behaviors\LinkableBehavior;
use Yii;
use yii\db\ActiveRecord;

/**
 * This is the model class for table "task_answer".
 *
 * @property int $id
 * @property int $task_id
 * @property int|null $correct
 *
 * @property Task $task
 * @property Translation[] $translations
 *
 * @mixin LinkableBehavior
 */
class TaskAnswer extends ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'task_answer';
    }

    /**
     * @return array
     */
    public function behaviors()
    {
        return [
            'linkable' => [
                'class' => LinkableBehavior::class,
                'relations' => ['translations'],
            ],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
//            [['task_id'], 'required'],
            [['task_id', 'correct'], 'integer'],
            [['task_id'], 'exist', 'skipOnError' => true, 'targetClass' => Task::class, 'targetAttribute' => ['task_id' => 'id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('model', 'ID'),
            'task_id' => Yii::t('model', 'Task ID'),
            'correct' => Yii::t('model', 'Correct'),
        ];
    }

    /**
     * Gets query for [[Task]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getTask()
    {
        return $this->hasOne(Task::class, ['id' => 'task_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getTranslations()
    {
        return $this->hasMany(Translation::class, ['entity_id' => 'id'])
            ->from('translation task_answer_translations')
            ->andOnCondition(['task_answer_translations.entity' => Translation::ENTITY_TASK_ANSWER]);
    }
    /**
     * @throws \Throwable
     * @throws \yii\db\StaleObjectException
     */
    public function beforeDelete()
    {
        Translation::deleteAll(['entity' => Translation::ENTITY_TASK_ANSWER, 'entity_id' => $this->id]);
        return parent::beforeDelete();
    }
}
