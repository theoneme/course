<?php

namespace common\models;

use common\behaviors\ImageBehavior;
use common\behaviors\JsonFieldBehavior;
use common\behaviors\LinkableBehavior;
use Yii;
use yii\base\NotSupportedException;
use yii\behaviors\TimestampBehavior;
use yii\db\ActiveRecord;
use yii\web\IdentityInterface;

/**
 * User model
 *
 * @property integer $id
 * @property string $username
 * @property string $password_hash
 * @property string $password_reset_token
 * @property string $email
 * @property string $phone
 * @property string $auth_key
 * @property integer $status
 * @property integer $created_at
 * @property integer $updated_at
 * @property integer $role
 * @property string $avatar
 *
 * @property Translation[] $translations
 * @property UserCourse[] $userCourses
 * @property Course[] $courses
 * @property resource|null $custom_data
 *
 * @mixin LinkableBehavior
 */
class User extends ActiveRecord implements IdentityInterface
{
    public const STATUS_DELETED = -20;
    public const STATUS_REQUIRES_MODIFICATION = -10;
    public const STATUS_REQUIRES_MODERATION = 0;
    public const STATUS_ACTIVE = 10;

    public const ROLE_USER = 0;
    public const ROLE_MODERATOR = 10;
    public const ROLE_ADMIN = 20;

    /**
     * @var string
     */
    public $password;

    /**
     * @var array
     */
    public $customDataArray = [];

    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return '{{%user}}';
    }

    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            TimestampBehavior::class,
            'image' => [
                'class' => ImageBehavior::class,
                'folder' => 'user',
                'imageField' => 'avatar',
                'labelField' => 'username'
            ],
            'linkable' => [
                'class' => LinkableBehavior::class,
                'relations' => ['translations'],
            ],
            'json' => [
                'class' => JsonFieldBehavior::class,
            ],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['username'], 'required'],
            ['status', 'default', 'value' => self::STATUS_ACTIVE],
            ['status', 'in', 'range' => [self::STATUS_ACTIVE, self::STATUS_DELETED, self::STATUS_REQUIRES_MODIFICATION, self::STATUS_REQUIRES_MODERATION]],
            ['avatar', 'string', 'max' => 155],
            [['username', 'email'], 'string', 'max' => 255],
            [['email'], 'email'],
//            [['email'], 'unique'],
            [['email', 'phone'], 'unique'],
            [['password', 'password_reset_token'], 'string', 'min' => 6, 'max' => 72],
            [['customDataArray'], 'safe'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public static function findIdentity($id)
    {
        return static::find()->where(['and', ['id' => $id], ['not', ['status' => self::STATUS_DELETED]]])->one();
    }

    /**
     * {@inheritdoc}
     * @throws \yii\base\NotSupportedException
     */
    public static function findIdentityByAccessToken($token, $type = null)
    {
        throw new NotSupportedException('"findIdentityByAccessToken" is not implemented.');
    }

    /**
     * Finds user by username
     *
     * @param string $username
     * @return static|null
     */
    public static function findByUsername($username)
    {
        return static::findOne(['and', ['username' => $username], ['not', ['status' => self::STATUS_DELETED]]]);
    }

    /**
     * Finds user by password reset token
     *
     * @param string $token password reset token
     * @return static|null
     */
    public static function findByPasswordResetToken($token)
    {
        if (!static::isPasswordResetTokenValid($token)) {
            return null;
        }

        return static::findOne([
            'password_reset_token' => $token,
            'status' => self::STATUS_ACTIVE,
        ]);
    }

    /**
     * Finds out if password reset token is valid
     *
     * @param string $token password reset token
     * @return bool
     */
    public static function isPasswordResetTokenValid($token)
    {
        if (empty($token)) {
            return false;
        }

        $timestamp = (int)substr($token, strrpos($token, '_') + 1);
        $expire = Yii::$app->params['user.passwordResetTokenExpire'];
        return $timestamp + $expire >= time();
    }

    /**
     * {@inheritdoc}
     */
    public function getId()
    {
        return $this->getPrimaryKey();
    }

    /**
     * {@inheritdoc}
     */
    public function getAuthKey()
    {
        return $this->auth_key;
    }

    /**
     * {@inheritdoc}
     */
    public function validateAuthKey($authKey)
    {
        return $this->getAuthKey() === $authKey;
    }

    /**
     * Validates password
     *
     * @param string $password password to validate
     * @return bool if password provided is valid for current user
     * @throws \yii\base\InvalidArgumentException
     */
    public function validatePassword($password)
    {
        return Yii::$app->security->validatePassword($password, $this->password_hash);
    }

    /**
     * Generates password hash from password and sets it to the model
     *
     * @param string $password
     * @throws \yii\base\Exception
     */
    public function setPassword($password)
    {
        $this->password_hash = Yii::$app->security->generatePasswordHash($password);
    }

    /**
     * Generates "remember me" authentication key
     * @throws \yii\base\Exception
     */
    public function generateAuthKey()
    {
        $this->auth_key = Yii::$app->security->generateRandomString();
    }

    /**
     * @return string
     * @throws \yii\base\Exception
     */
    public function generatePasswordResetToken()
    {
        return Yii::$app->security->generateRandomString() . '_' . time();
    }

    /**
     * Removes password reset token
     */
    public function removePasswordResetToken()
    {
        $this->password_reset_token = null;
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getTranslations()
    {
        return $this->hasMany(Translation::class, ['entity_id' => 'id'])
            ->from('translation user_translations')
            ->andOnCondition(['user_translations.entity' => Translation::ENTITY_USER]);
    }

    /**
     * Gets query for [[UserCourses]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getUserCourses()
    {
        return $this->hasMany(UserCourse::class, ['user_id' => 'id'])->indexBy('course_id');
    }

    /**
     * Gets query for [[Courses]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getCourses()
    {
        return $this->hasMany(Course::class, ['id' => 'course_id'])->viaTable('user_course', ['user_id' => 'id']);
    }

    /**
     * @throws \Throwable
     * @throws \yii\db\StaleObjectException
     */
    public function beforeDelete()
    {
        Yii::$app->mediaLayer->removeMedia($this->avatar);
        Translation::deleteAll(['entity' => Translation::ENTITY_USER, 'entity_id' => $this->id]);
        return parent::beforeDelete();
    }

    public function afterSave($insert, $changedAttributes)
    {
        parent::afterSave($insert, $changedAttributes);
        //
    }
}
