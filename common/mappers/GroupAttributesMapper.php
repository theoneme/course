<?php

namespace common\mappers;

use common\interfaces\DataMapperInterface;
use yii\helpers\ArrayHelper;

/**
 * Class DynamicFormValuesMapper
 * @package common\mappers
 */
class GroupAttributesMapper implements DataMapperInterface
{
    /**
     * @param $rawData
     * @return mixed
     * @throws \yii\base\InvalidArgumentException
     */
    public static function getMappedData($rawData)
    {
        return array_map(function ($attribute) {
            $customDataArray = json_decode($attribute['custom_data'], true);
            $translations = TranslationsMapper::getMappedData($attribute['attr']['translations']);
            return [
                'title' => ArrayHelper::remove($translations, 'title'),
                'type' => $customDataArray['type'] ?? 'textbox',
                'id' => $attribute['attribute_id'],
                'customDataArray' => $customDataArray
            ];
        }, $rawData);
    }
}