<?php

namespace common\mappers;

use common\interfaces\DataMapperInterface;

/**
 * Class SelectizeAttributeValuesMapper
 * @package common\mappers
 */
class SelectizeAttributeValuesMapper implements DataMapperInterface
{
    /**
     * @param $rawData
     * @return array
     */
    public static function getMappedData($rawData)
    {
        $result = array_map(function($value) {
            return [
                'id' => $value['id'],
                'value' => $value['translations']['title'],
            ];
        }, $rawData);

        return $result;
    }
}