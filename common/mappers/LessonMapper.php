<?php

namespace common\mappers;

use common\interfaces\DataMapperInterface;
use common\models\Translation;
use yii\helpers\ArrayHelper;

/**
 * Class LessonMapper
 * @package common\mappers
 */
class LessonMapper implements DataMapperInterface
{

    /**
     * @param $rawData
     * @return array
     */
    public static function getMappedData($rawData)
    {
        ArrayHelper::multisort($rawData, 'sort');
        return ArrayHelper::map($rawData, 'id', function($var){
            $currentTranslation = TranslationsMapper::getMappedData($var['translations']);
            return [
                'sort' => $var['sort'],
                'title' => ArrayHelper::remove($currentTranslation, Translation::KEY_TITLE)
            ];
        });
    }
}