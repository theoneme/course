<?php

namespace common\mappers;

use common\helpers\UtilityHelper;
use common\interfaces\DataMapperInterface;
use common\models\Translation;
use yii\helpers\ArrayHelper;

/**
 * Class TaskAnswerMapper
 * @package common\mappers
 */
class TaskAnswerMapper implements DataMapperInterface
{
    public const MODE_FULL = 0;
    public const MODE_SHORT = 10;

    /**
     * @param $rawData
     * @param int $mode
     * @return mixed
     */
    public static function getMappedData($rawData, $mode = self::MODE_SHORT)
    {
        return array_map(function($var) use ($mode){
            $currentTranslation = TranslationsMapper::getMappedData($var['translations']);
            $translations = $mode === self::MODE_FULL
                ? TranslationsMapper::getMappedData($var['translations'], TranslationsMapper::MODE_FULL)
                : $currentTranslation;
            return [
                'id' => $var['id'],
                'correct' => $var['correct'],
                'title' => ArrayHelper::remove($currentTranslation, Translation::KEY_TITLE),
                'description' => ArrayHelper::remove($currentTranslation, Translation::KEY_DESCRIPTION),
                'translations' => $translations,
            ];
        }, $rawData);
    }
}