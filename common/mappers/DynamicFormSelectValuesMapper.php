<?php

namespace common\mappers;

use common\helpers\UtilityHelper;
use common\interfaces\DataMapperInterface;

/**
 * Class DynamicFormSelectValuesMapper
 * @package common\mappers
 */
class DynamicFormSelectValuesMapper implements DataMapperInterface
{
    /**
     * @param $rawData
     * @return array
     */
    public static function getMappedData($rawData)
    {
        $result = [];

        foreach ($rawData as $attribute) {
            $result["at_{$attribute['attribute_id']}"][$attribute['id']] = $attribute['translations']['title']
                ?? UtilityHelper::upperFirstLetter(str_replace('-', ' ', $attribute['alias']));
        }

        return $result;
    }
}