<?php

namespace common\validators;

use Yii;
use yii\validators\Validator;

/**
 * Class RegexValidator
 * @package common\validators
 */
class RegexValidator extends Validator
{
    /**
     * @var string
     */
    public $regex;
    /**
     * @var string
     */
    public $wrap = '';

    /**
     * {@inheritdoc}
     */
    public function init()
    {
        parent::init();
        if ($this->message === null) {
            $this->message = Yii::t('main', '{attribute} is invalid');
        }
    }

    /**
     * {@inheritdoc}
     */
    public function validateAttribute($model, $attribute)
    {
        $value = $model->$attribute;
        $pattern = $this->regex;

        if (!preg_match($pattern, $value)) {
            $this->addError($model, $attribute, $this->message);
        }
    }

    /**
     * {@inheritdoc}
     */
    protected function validateValue($value)
    {
        $pattern = $this->regex;

        if (!preg_match($pattern, $value)) {
            return [$this->message, []];
        }

        return null;
    }
}