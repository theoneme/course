<?php

namespace common\repositories\sql;

use common\forms\ar\TaskForm;
use common\interfaces\repositories\TaskRepositoryInterface;
use common\models\Task;
use Yii;

/**
 * Class TaskRepository
 * @package common\repositories\sql
 */
class TaskRepository extends AbstractSqlArRepository implements TaskRepositoryInterface
{
    /**
     * @param array $data
     * @return int|mixed
     * @throws \common\exceptions\ClassNotFoundException
     * @throws \common\exceptions\EntityNotCreatedException
     * @throws \common\exceptions\RepositoryException
     * @throws \yii\base\InvalidConfigException
     * @throws \yii\di\NotInstantiableException
     */
    public function create(array $data)
    {

        $taskForm = new TaskForm(['_task' => new Task()]);
        $taskForm->load($data);

        $taskForm->bindData();
        if (!$taskForm->save()) {
            Yii::error('******* Task has failed to save: ' . PHP_EOL);
            Yii::error($taskForm->_task->errors);
        }

        return $taskForm->_task->id;
    }
}