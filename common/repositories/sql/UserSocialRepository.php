<?php

namespace common\repositories\sql;

use common\interfaces\repositories\UserSocialRepositoryInterface;

/**
 * Class UserSocialRepository
 * @package common\repositories\sql
 */
class UserSocialRepository extends AbstractSqlArRepository implements UserSocialRepositoryInterface
{

}