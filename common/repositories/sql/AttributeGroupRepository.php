<?php

namespace common\repositories\sql;

use common\interfaces\repositories\AttributeGroupRepositoryInterface;

/**
 * Class AttributeGroupRepository
 * @package common\repositories\sql
 */
class AttributeGroupRepository extends AbstractSqlArRepository implements AttributeGroupRepositoryInterface
{

}