<?php

namespace common\repositories\sql;

use common\forms\ar\AttributeValueForm;
use common\interfaces\repositories\AttributeValueRepositoryInterface;
use common\models\AttributeValue;
use Yii;

/**
 * Class AttributeValueRepository
 * @package common\repositories\sql
 */
class AttributeValueRepository extends AbstractSqlArRepository implements AttributeValueRepositoryInterface
{
    /**
     * @param array $data
     * @return int|mixed
     * @throws \ReflectionException
     */
    public function create(array $data)
    {
        $form = new AttributeValueForm(['_attributeValue' => new AttributeValue()]);
        $form->load($data);
        $form->bindData();

        if (!$form->save()) {
            Yii::error('******* Attribute value has failed to save: ' . PHP_EOL);
            Yii::error($form->_attributeValue->errors);
        }

        return $form->_attributeValue->id;
    }
}