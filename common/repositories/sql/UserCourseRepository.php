<?php

namespace common\repositories\sql;

use common\forms\ar\UserCourseForm;
use common\interfaces\repositories\UserCourseRepositoryInterface;
use Yii;

/**
 * Class UserCourseMethodRepository
 * @package common\repositories\sql
 */
class UserCourseRepository extends AbstractSqlArRepository implements UserCourseRepositoryInterface
{
    /**
     * @param array $data
     * @return int|mixed
     * @throws \ReflectionException
     */
    public function create(array $data)
    {
        $form = new UserCourseForm();
        $form->load($data);

        if (!$form->save()) {
            Yii::error('******* User Course has failed to save: ' . PHP_EOL);
            Yii::error($form->errors);
        }

        return true;
    }
}