<?php

namespace common\repositories\sql;

use common\forms\ar\AttributeForm;
use common\interfaces\repositories\AttributeRepositoryInterface;
use common\models\Attribute;
use Yii;

/**
 * Class AttributeValueRepository
 * @package common\repositories\sql
 */
class AttributeRepository extends AbstractSqlArRepository implements AttributeRepositoryInterface
{
    /**
     * @param array $data
     * @return int|mixed
     * @throws \ReflectionException
     */
    public function create(array $data)
    {
        $form = new AttributeForm(['_attribute' => new Attribute()]);
        $form->load($data);
        $form->bindData();

        if (!$form->save()) {
            Yii::error('******* Attribute has failed to save: ' . PHP_EOL);
            Yii::error($form->_attribute->errors);
        }

        return $form->_attribute->id;
    }
}