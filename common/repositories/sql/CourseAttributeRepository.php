<?php

namespace common\repositories\sql;
use common\interfaces\repositories\CourseAttributeRepositoryInterface;


/**
 * Class CourseAttributeRepository
 * @package common\repositories\sql
 */
class CourseAttributeRepository extends AbstractSqlArRepository implements CourseAttributeRepositoryInterface
{

}