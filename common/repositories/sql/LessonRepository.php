<?php

namespace common\repositories\sql;

use common\forms\ar\LessonForm;
use common\interfaces\repositories\LessonRepositoryInterface;
use common\models\Lesson;
use Yii;

/**
 * Class LessonRepository
 * @package common\repositories\sql
 */
class LessonRepository extends AbstractSqlArRepository implements LessonRepositoryInterface
{
    /**
     * @param array $data
     * @return int|mixed
     * @throws \common\exceptions\ClassNotFoundException
     * @throws \common\exceptions\EntityNotCreatedException
     * @throws \common\exceptions\RepositoryException
     * @throws \yii\base\InvalidConfigException
     * @throws \yii\di\NotInstantiableException
     */
    public function create(array $data)
    {

        $lessonForm = new LessonForm(['_lesson' => new Lesson()]);
        $lessonForm->load($data);

        $lessonForm->bindData();
        if (!$lessonForm->save()) {
            Yii::error('******* Lesson has failed to save: ' . PHP_EOL);
            Yii::error($lessonForm->_lesson->errors);
        }

        return $lessonForm->_lesson->id;
    }
}