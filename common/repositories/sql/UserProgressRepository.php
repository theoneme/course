<?php

namespace common\repositories\sql;

use common\forms\ar\UserProgressForm;
use common\interfaces\repositories\UserProgressRepositoryInterface;
use common\models\UserProgress;
use Yii;

/**
 * Class UserProgressRepository
 * @package common\repositories\sql
 */
class UserProgressRepository extends AbstractSqlArRepository implements UserProgressRepositoryInterface
{
    /**
     * @param array $data
     * @return int|mixed
     * @throws \common\exceptions\ClassNotFoundException
     * @throws \common\exceptions\EntityNotCreatedException
     * @throws \common\exceptions\RepositoryException
     * @throws \yii\base\InvalidConfigException
     * @throws \yii\di\NotInstantiableException
     */
    public function create(array $data)
    {

        $userProgressForm = new UserProgressForm();
        $userProgressForm->load($data);

        if (!$userProgressForm->save()) {
            Yii::error('******* UserProgress has failed to save: ' . PHP_EOL);
            Yii::error($userProgressForm->errors);
        }

        return $userProgressForm->id;
    }
}