<?php

namespace common\repositories\sql;

use common\forms\ar\TaskAnswerForm;
use common\interfaces\repositories\TaskAnswerRepositoryInterface;
use common\models\TaskAnswer;
use Yii;

/**
 * Class TaskAnswerRepository
 * @package common\repositories\sql
 */
class TaskAnswerRepository extends AbstractSqlArRepository implements TaskAnswerRepositoryInterface
{
    /**
     * @param array $data
     * @return int|mixed
     * @throws \common\exceptions\ClassNotFoundException
     * @throws \common\exceptions\EntityNotCreatedException
     * @throws \common\exceptions\RepositoryException
     * @throws \yii\base\InvalidConfigException
     * @throws \yii\di\NotInstantiableException
     */
    public function create(array $data)
    {

        $taskForm = new TaskAnswerForm(['_taskAnswer' => new TaskAnswer()]);
        $taskForm->load($data);

        $taskForm->bindData();
        if (!$taskForm->save()) {
            Yii::error('******* Task answer has failed to save: ' . PHP_EOL);
            Yii::error($taskForm->_taskAnswer->errors);
        }

        return $taskForm->_taskAnswer->id;
    }
}