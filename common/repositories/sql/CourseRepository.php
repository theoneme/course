<?php

namespace common\repositories\sql;

use common\forms\ar\CourseForm;
use common\interfaces\repositories\CourseRepositoryInterface;
use common\models\Course;
use Yii;

/**
 * Class CourseRepository
 * @package common\repositories\sql
 */
class CourseRepository extends AbstractSqlArRepository implements CourseRepositoryInterface
{
    /**
     * @param array $data
     * @return int|mixed
     * @throws \common\exceptions\ClassNotFoundException
     * @throws \common\exceptions\EntityNotCreatedException
     * @throws \common\exceptions\RepositoryException
     * @throws \yii\base\InvalidConfigException
     * @throws \yii\di\NotInstantiableException
     */
    public function create(array $data)
    {

        $courseForm = new CourseForm(['_course' => new Course()]);
        $courseForm->load($data);

        $courseForm->bindData();
        if (!$courseForm->save()) {
            Yii::error('******* Course has failed to save: ' . PHP_EOL);
            Yii::error($courseForm->_course->errors);
        }

        return $courseForm->_course->id;
    }
}