<?php

namespace common\repositories\sql;

use common\forms\ar\PaymentForm;
use common\interfaces\repositories\PaymentRepositoryInterface;
use Yii;

/**
 * Class PaymentMethodRepository
 * @package common\repositories\sql
 */
class PaymentRepository extends AbstractSqlArRepository implements PaymentRepositoryInterface
{
    /**
     * @param array $data
     * @return int|mixed
     * @throws \ReflectionException
     */
    public function create(array $data)
    {
        $form = new PaymentForm();
        $form->load($data);

        if (!$form->save()) {
            Yii::error('******* Payment has failed to save: ' . PHP_EOL);
            Yii::error($form->errors);
        }

        return $form->id;
    }
}