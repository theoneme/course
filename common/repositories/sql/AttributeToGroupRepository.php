<?php

namespace common\repositories\sql;

use common\interfaces\repositories\AttributeToGroupRepositoryInterface;

/**
 * Class AttributeToGroupRepository
 * @package common\repositories\sql
 */
class AttributeToGroupRepository extends AbstractSqlArRepository implements AttributeToGroupRepositoryInterface
{

}