<?php

namespace common\behaviors;

use yii\base\Behavior;
use yii\base\InvalidConfigException;
use yii\base\Model;
use yii\db\ActiveQuery;
use yii\db\ActiveRecord;
use yii\widgets\ActiveForm;

/**
 * Class LinkableBehavior
 * @property ActiveRecord $owner
 * @package commmon\behaviors
 */
class LinkableBehavior extends Behavior
{
    /**
     * @var array
     */
    public $relations = [];

    /**
     * @var array
     */
    public $idFields = [];

    /**
     * @var array
     */
    private $changes = [];

    /**
     * @var array
     */
    private $relationsBuffer = [];

    /**
     * @inheritdoc
     */
    public function events()
    {
        return [
            ActiveRecord::EVENT_AFTER_VALIDATE => 'afterValidate',
            ActiveRecord::EVENT_AFTER_INSERT => 'afterSave',
            ActiveRecord::EVENT_AFTER_UPDATE => 'afterSave',
        ];
    }

    /**
     * @throws InvalidConfigException
     */
    public function init()
    {
        if ($this->relations === null) {
            throw new InvalidConfigException('The "relations" property must be set.');
        }
    }

    /**
     * @param null $relationName
     * @param null $id
     * @param null $index
     * @return ActiveRecord
     * @throws \yii\base\InvalidArgumentException
     */
    public function bind($relationName = null, $id = null, $index = null)
    {
        return $this->getBind($relationName, $id, $index);
    }

    /**
     * @param null $relationName
     * @param null $id
     * @param null $index
     * @return ActiveRecord
     * @throws \yii\base\InvalidArgumentException
     */
    public function getBind($relationName = null, $id = null, $index = null)
    {
        if (!empty($id)) {
            $this->changes[$relationName][] = $id;
        }

        /* @var ActiveRecord $class */
        $class = $this->owner->getRelation($relationName)->modelClass;

        /* @var ActiveRecord[] $records */
        $records = $this->owner->{$relationName};
        if (!empty($records)) {
            foreach ($records as $record) {
                if ($id !== null && $record->getAttribute($this->idFields[$relationName] ?? 'id') === $id) {
                    if ($index !== null) {
                        $this->relationsBuffer[$relationName][$index] = $record;
                    } else {
                        $this->relationsBuffer[$relationName][] = $record;
                    }

                    return $record;
                }
            }
        }

        $record = new $class();
        if ($index !== null) {
            $records[$index] = $record;
        } else {
            $records[] = $record;
        }

        $this->relationsBuffer[$relationName] = $records;
        $this->owner->populateRelation($relationName, $records);

        return $record;
    }

    /**
     * @return void
     */
    public function afterValidate()
    {
        foreach ($this->relations as $relation) {
            if (isset($this->owner->{$relation}) && !Model::validateMultiple($this->owner->{$relation})) {
                foreach ($this->owner->{$relation} as $item) {
                    $this->owner->addError($relation, $item->errors);
                }
            }
        }
    }

    /**
     * @return void
     * @throws \Throwable
     * @throws \yii\db\StaleObjectException
     */
    public function afterSave()
    {
        foreach ($this->relations as $relation) {
            $idField = $this->idFields[$relation] ?? 'id';
            $ids = array_key_exists($relation, $this->changes) ? $this->changes[$relation] : [];

            /** @var ActiveRecord $relationClass */
            $relationClass = $this->owner->getRelation($relation)->modelClass;
            $relationQuery = $this->owner->getRelation($relation);
            $foreignKeyName = array_keys($relationQuery->link)[0];

            $whereCondition = $relationQuery->where ?? [];
            $onCondition = $relationQuery->on ?? [];

            /* @var ActiveQuery $queryCondition */
            $queryCondition = $relationClass::find()
                ->andWhere($whereCondition)
                ->andWhere([$foreignKeyName => $this->owner->getPrimaryKey()])
                ->andFilterWhere($onCondition);
            if (!empty($relationQuery->from)) {
                $queryCondition->from($relationQuery->from);
            }
            if (!empty($ids)) {
                $queryCondition->andWhere(['not', [$idField => $ids]]);
            }
            $items = $queryCondition->all();

            $deletedItems = [];
            if ($items !== null) {
                foreach ($items as $item) {
                    if (isset($item[$idField])) {
                        $deletedItems[] = $item[$idField];
                    }
                    $item->delete();
                }
            }

            if (!empty($deletedItems) && array_key_exists($relation, $this->relationsBuffer) && is_array($this->relationsBuffer[$relation])) {
                $this->relationsBuffer[$relation] = array_filter($this->relationsBuffer[$relation], function ($value) use ($deletedItems, $idField) {
                    return !in_array($value[$idField], $deletedItems);
                });
            }

            $this->owner->populateRelation($relation, []);

            /* @var ActiveRecord $record */
            if (!empty($this->relationsBuffer[$relation])) {
                foreach ($this->relationsBuffer[$relation] as $record) {
                    $this->owner->link($relation, $record);
                }
            }
        }
    }

    /**
     * @return array
     */
    public function validateWithRelations()
    {
        $errors = ActiveForm::validate($this->owner);
        foreach ($this->relations as $relation) {
            if (array_key_exists($relation, $this->relationsBuffer)) {
                $errors = array_merge($errors, ActiveForm::validateMultiple($this->relationsBuffer[$relation]));
            }
        }

        return $errors;
    }
}