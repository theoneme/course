<?php

namespace common\behaviors;

use yii\base\Behavior;
use yii\db\ActiveRecord;

/**
 * Class JsonFieldBehavior
 * @package common\behaviors
 */
class JsonFieldBehavior extends Behavior
{
    /**
     * @var string
     */
    public $field = 'custom_data';
    /**
     * @var string
     */
    public $arrayField = 'customDataArray';

    /**
     * @inheritdoc
     */
    public function events()
    {
        return [
            ActiveRecord::EVENT_AFTER_FIND => 'afterFind',
            ActiveRecord::EVENT_BEFORE_VALIDATE => 'beforeValidate',
            ActiveRecord::EVENT_BEFORE_INSERT => 'beforeSave',
            ActiveRecord::EVENT_BEFORE_UPDATE => 'beforeSave',
        ];
    }

    /**
     * @inheritdoc
     */
    public function afterFind()
    {
        if ($this->owner->hasProperty($this->field) && $this->owner->{$this->field} !== null) {
            $this->owner->{$this->arrayField} = json_decode($this->owner->{$this->field}, true);
        }
    }

    /**
     *
     */
    public function beforeValidate()
    {
        if ($this->owner->hasProperty($this->field) && $this->owner->{$this->field} !== null && empty($this->owner->{$this->arrayField})) {
            $this->owner->{$this->arrayField} = json_decode($this->owner->{$this->field}, true);
        }
    }

    /**
     * @inheritdoc
     */
    public function beforeSave($insert)
    {
        if ($this->owner->hasProperty($this->field)) {
            $this->owner->{$this->field} = json_encode($this->owner->{$this->arrayField});
        }
    }
}