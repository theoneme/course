<?php

namespace common\behaviors;

use common\components\CurrencyHelper;
use yii\base\Behavior;
use yii\db\ActiveRecord;

/**
 * Class DefaultPriceBehavior
 * @package common\behaviors
 */
class DefaultPriceBehavior extends Behavior
{
    /**
     * @var string
     */
    public $field = 'default_price';

    /**
     * @inheritdoc
     */
    public function events()
    {
        return [
            ActiveRecord::EVENT_BEFORE_INSERT => 'beforeSave',
            ActiveRecord::EVENT_BEFORE_UPDATE => 'beforeSave',
        ];
    }

    /**
     * @return void
     * @throws \RuntimeException
     * @throws \yii\base\InvalidArgumentException
     */
    public function beforeSave()
    {
        if($this->owner->getAttribute('default_price') === null || $this->owner->getOldAttribute('price') !== $this->owner->getAttribute('price')) {
            $this->owner->setAttribute('default_price', CurrencyHelper::convert($this->owner->getAttribute('currency_code'), 'RUB', $this->owner->getAttribute('price')));
        }
    }
}