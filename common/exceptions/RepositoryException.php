<?php

namespace common\exceptions;

use yii\db\Exception;

/**
 * Class RepositoryException
 * @package common\exceptions
 */
class RepositoryException extends Exception
{

}