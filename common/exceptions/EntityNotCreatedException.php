<?php

namespace common\exceptions;
use yii\base\Exception;

/**
 * Class EntityNotCreatedException
 * @package common\exceptions
 */
class EntityNotCreatedException extends Exception
{

}