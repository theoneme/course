<?php

namespace common\exceptions;
use yii\base\Exception;

/**
 * Class EntityNotFoundException
 * @package common\exceptions
 */
class EntityNotFoundException extends Exception
{

}