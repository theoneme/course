<?php

namespace common\exceptions;

use yii\base\InvalidConfigException;

/**
 * Class ClassNotFoundException
 * @package common\exceptions
 */
class ClassNotFoundException extends InvalidConfigException
{

}