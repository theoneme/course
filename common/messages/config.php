<?php

return [
    'color' => null,
    'interactive' => true,
    'sourcePath' => ['backend', 'frontend', 'common'],
    'messagePath' => 'common/messages',
    'languages' => array_diff(array_keys(Yii::$app->params['supportedLocales']), ['en-GB']),
    'translator' => 'Yii::t',
    'sort' => true,
    'overwrite' => true,
    'removeUnused' => true,
    'markUnused' => true,
    'except' => [
        '.svn',
        '.git',
        '.gitignore',
        '.gitkeep',
        '.hgignore',
        '.hgkeep',
        '/messages',
        '/BaseYii.php',
    ],
    'only' => [
        '*.php',
    ],
    'format' => 'php',
    'db' => 'db',
    'sourceMessageTable' => '{{%source_message}}',
    'messageTable' => '{{%message}}',
    'catalog' => 'messages',
    'ignoreCategories' => ['main', 'catalog', 'index', 'yii', 'account'],
];
