<?php

namespace common\decorators;

use common\interfaces\DecoratorInterface;
use common\models\Lesson;
use Yii;
use yii\helpers\Html;

/**
 * Class LessonTypeDecorator
 * @package common\decorators
 */
class LessonTypeDecorator implements DecoratorInterface
{
    /**
     * @param $rawData
     * @return mixed|string
     */
    public static function decorate($rawData)
    {
        $labels = static::getTypeLabels();
        return array_key_exists($rawData, $labels) ? $labels[$rawData] : Yii::t('labels', 'Unknown type');
    }

    /**
     * @param bool $colored
     * @return array
     */
    public static function getTypeLabels($colored = true)
    {
        return [
            Lesson::TYPE_VIDEO => $colored === true
                ? Html::tag('span', Yii::t('labels', 'Video'), ['style' => 'color: #3ab845'])
                : Yii::t('labels', 'Video'),
            Lesson::TYPE_PDF => $colored === true
                ? Html::tag('span', Yii::t('labels', 'PDF'), ['style' => 'color: #2d618c'])
                : Yii::t('labels', 'PDF'),
        ];
    }
}
