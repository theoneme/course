<?php

namespace common\decorators;

use common\interfaces\DecoratorInterface;
use common\models\Task;
use Yii;
use yii\helpers\Html;

/**
 * Class TaskTypeDecorator
 * @package common\decorators
 */
class TaskTypeDecorator implements DecoratorInterface
{
    /**
     * @param $rawData
     * @return mixed|string
     */
    public static function decorate($rawData)
    {
        $labels = static::getTypeLabels();
        return array_key_exists($rawData, $labels) ? $labels[$rawData] : Yii::t('labels', 'Unknown type');
    }

    /**
     * @param bool $colored
     * @return array
     */
    public static function getTypeLabels($colored = true)
    {
        return [
            Task::TYPE_NUMBER => $colored === true
                ? Html::tag('span', Yii::t('labels', 'Number'), ['style' => 'color: #3ab845'])
                : Yii::t('labels', 'Number'),
            Task::TYPE_RADIO => $colored === true
                ? Html::tag('span', Yii::t('labels', 'Radio'), ['style' => 'color: #2d618c'])
                : Yii::t('labels', 'Radio'),
            Task::TYPE_CHECKBOX => $colored === true
                ? Html::tag('span', Yii::t('labels', 'Checkbox'), ['style' => 'color: #ffd288'])
                : Yii::t('labels', 'Checkbox'),
            Task::TYPE_TEXT => $colored === true
                ? Html::tag('span', Yii::t('labels', 'Text'), ['style' => 'color: #3ab845'])
                : Yii::t('labels', 'Text'),
        ];
    }
}
