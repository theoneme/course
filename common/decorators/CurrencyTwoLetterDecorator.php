<?php

namespace common\decorators;

use common\interfaces\DecoratorInterface;

/**
 * Class CurrencyTwoLetterDecorator
 * @package common\decorators
 */
class CurrencyTwoLetterDecorator implements DecoratorInterface
{
    /**
     * @param $rawData
     * @return mixed|string
     */
    public static function decorate($rawData)
    {
        $letters = static::getLetters();
        return array_key_exists($rawData, $letters) ? $letters[$rawData] : '';
    }

    /**
     * @return array
     */
    public static function getLetters()
    {
        return [
            'RUB' => 'RR',
            'USD' => 'UD',
            'EUR' => 'EE',
            'UAH' => 'UH',
            'GEL' => 'GL',
            'CNY' => 'YY',
        ];
    }
}
