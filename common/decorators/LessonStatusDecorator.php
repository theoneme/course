<?php

namespace common\decorators;

use common\interfaces\DecoratorInterface;
use common\models\Lesson;
use Yii;
use yii\helpers\Html;

/**
 * Class LessonStatusDecorator
 * @package common\decorators
 */
class LessonStatusDecorator implements DecoratorInterface
{
    /**
     * @param $rawData
     * @return mixed|string
     */
    public static function decorate($rawData)
    {
        $labels = static::getStatusLabels();
        return array_key_exists($rawData, $labels) ? $labels[$rawData] : Yii::t('labels', 'Unknown status');
    }

    /**
     * @param bool $colored
     * @return array
     */
    public static function getStatusLabels($colored = true)
    {
        return [
            Lesson::STATUS_ACTIVE => $colored === true
                ? Html::tag('span', Yii::t('labels', 'Published'), ['style' => 'color: #3ab845'])
                : Yii::t('labels', 'Published'),
            Lesson::STATUS_DRAFT => $colored === true
                ? Html::tag('span', Yii::t('labels', 'Draft'), ['style' => 'color: #2d618c'])
                : Yii::t('labels', 'Draft'),
            Lesson::STATUS_DELETED => $colored === true
                ? Html::tag('span', Yii::t('labels', 'Disabled'), ['style' => 'color: #ac4137'])
                : Yii::t('labels', 'Disabled')
        ];
    }
}
