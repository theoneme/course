<?php

namespace common\decorators;

use common\interfaces\DecoratorInterface;
use common\models\Payment;
use Yii;
use yii\helpers\Html;

/**
 * Class PaymentTypeDecorator
 * @package common\decorators
 */
class PaymentTypeDecorator implements DecoratorInterface
{
    /**
     * @param $rawData
     * @return mixed|string
     */
    public static function decorate($rawData)
    {
        $labels = static::getTypeLabels();
        return array_key_exists($rawData, $labels) ? $labels[$rawData] : Yii::t('labels', 'Unknown type');
    }

    /**
     * @param bool $colored
     * @return array
     */
    public static function getTypeLabels($colored = true)
    {
        return [
            Payment::TYPE_COURSE => $colored === true
                ? Html::tag('span', Yii::t('labels', 'Course'), ['style' => 'color: #3ab845'])
                : Yii::t('labels', 'Course'),
        ];
    }
}
