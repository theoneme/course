<?php

namespace common\decorators;

use common\interfaces\DecoratorInterface;
use common\models\AttributeValue;
use Yii;
use yii\helpers\Html;

/**
 * Class AttributeValueStatusDecorator
 * @package common\decorators
 */
class AttributeValueStatusDecorator implements DecoratorInterface
{
    /**
     * @param $rawData
     * @return mixed|string
     */
    public static function decorate($rawData)
    {
        $labels = static::getStatusLabels();
        return array_key_exists($rawData, $labels) ? $labels[$rawData] : Yii::t('labels', 'Unknown status');
    }

    /**
     * @param bool $colored
     * @return array
     */
    public static function getStatusLabels($colored = true)
    {
        return [
            AttributeValue::STATUS_APPROVED => $colored === true
                ? Html::tag('span', Yii::t('labels', 'Approved'), ['style' => 'color: #3ab845'])
                : Yii::t('labels', 'Published'),
            AttributeValue::STATUS_PENDING => $colored === true
                ? Html::tag('span', Yii::t('labels', 'Requires moderation'), ['style' => 'color: #2d618c'])
                : Yii::t('labels', 'Requires moderation')
        ];
    }
}
