<?php

namespace common\decorators;

use common\interfaces\DecoratorInterface;
use common\models\Course;
use Yii;
use yii\helpers\Html;

/**
 * Class CourseTypeDecorator
 * @package common\decorators
 */
class CourseTypeDecorator implements DecoratorInterface
{
    /**
     * @param $rawData
     * @return mixed|string
     */
    public static function decorate($rawData)
    {
        $labels = static::getTypeLabels();
        return array_key_exists($rawData, $labels) ? $labels[$rawData] : Yii::t('labels', 'Unknown type');
    }

    /**
     * @param bool $colored
     * @return array
     */
    public static function getTypeLabels($colored = true)
    {
        return [
            Course::TYPE_DEFAULT => $colored === true
                ? Html::tag('span', Yii::t('labels', 'Lessons and tasks'), ['style' => 'color: #3ab845'])
                : Yii::t('labels', 'Lessons and tasks'),
            Course::TYPE_TASK_ONLY => $colored === true
                ? Html::tag('span', Yii::t('labels', 'Tasks only'), ['style' => 'color: #2d618c'])
                : Yii::t('labels', 'Tasks only'),
        ];
    }
}
