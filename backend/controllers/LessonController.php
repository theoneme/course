<?php

namespace backend\controllers;

use backend\services\FormCategoryService;
use common\data\MyArrayDataProvider;
use common\forms\ar\LessonForm;
use common\helpers\DataHelper;
use common\interfaces\repositories\CategoryRepositoryInterface;
use common\models\Lesson;
use common\models\search\LessonSearch;
use common\repositories\sql\LessonRepository;
use common\services\entities\LessonService;
use Yii;
use yii\base\Module;
use yii\db\Expression;
use yii\filters\AccessControl;
use yii\web\Response;

/**
 * Class LessonController
 * @package backend\controllers\location
 */
class LessonController extends BackEndController
{
    /**
     * @var LessonRepository
     */
    private $_lessonRepository;
    /**
     * @var LessonService
     */
    private $_lessonService;
    /**
     * @var CategoryRepositoryInterface
     */
    private $_categoryRepository;
    /**
     * @var FormCategoryService
     */
    private $_formCategoryService;

    /**
     * LessonController constructor.
     * @param string $id
     * @param Module $module
     * @param LessonService $lessonService
     * @param LessonRepository $lessonRepository
     * @param FormCategoryService $formCategoryService
     * @param CategoryRepositoryInterface $categoryRepository
     * @param array $config
     */
    public function __construct(string $id, Module $module,
                                LessonService $lessonService,
                                LessonRepository $lessonRepository,
                                FormCategoryService $formCategoryService,
                                CategoryRepositoryInterface $categoryRepository,
                                array $config = []
    )
    {
        parent::__construct($id, $module, $config);
        $this->_lessonRepository = $lessonRepository;
        $this->_lessonService = $lessonService;
        $this->_categoryRepository = $categoryRepository;
        $this->_formCategoryService = $formCategoryService;
    }

    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return array_merge(parent::behaviors(), [
            'access' => [
                'class' => AccessControl::class,
                'rules' => [
                    [
                        'actions' => ['index', 'update', 'create', 'delete'],
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
        ]);
    }

    /**
     * @return string
     * @throws \ReflectionException
     * @throws \common\exceptions\RepositoryException
     * @throws \yii\base\InvalidConfigException
     * @throws \yii\di\NotInstantiableException
     */
    public function actionIndex()
    {
        $search = new LessonSearch($this->_lessonRepository, [
            'pagination' => true,
            'perPage' => 20,
        ]);
        $queryParams = Yii::$app->request->queryParams;
        $items = $search->search($this->extractParams($queryParams));

        $dataProvider = new MyArrayDataProvider([
            'data' => $items['items'],
            'totalCount' => $items['pagination']->totalCount ?? count($items['items']),
            'pagination' => [
                'pageSize' => 20
            ],
            'sort' => ['attributes' => ['sort']]
        ]);
        $search->category_id = $queryParams['LessonSearch']['category_id'] ?? null;

        return $this->render('index', [
            'dataProvider' => $dataProvider,
            'search' => $search,
            'categories' => DataHelper::getCategoryTree(),
        ]);
    }


    /**
     * @return array|mixed|string
     * @throws \common\exceptions\ClassNotFoundException
     * @throws \common\exceptions\EntityNotCreatedException
     * @throws \common\exceptions\RepositoryException
     * @throws \yii\base\InvalidConfigException
     * @throws \yii\di\NotInstantiableException
     */
    public function actionCreate()
    {
        $lessonForm = new LessonForm(['_lesson' => new Lesson()]);

        $input = Yii::$app->request->post();
        if (!empty($input)) {
            $lessonForm->load($input);

            if (array_key_exists('ajax', $input)) {
                return $this->validateAjax($lessonForm);
            }
            if ($lessonForm->validate()) {
                $lessonForm->bindData();
                $lessonForm->save();

                Yii::$app->response->format = Response::FORMAT_JSON;
                return ['success' => true];
            }
        } else {
            $lessonForm->load([
                'CategoryForm' => [
                    'category_id' => (int)Yii::$app->request->get('category_id', DataHelper::getFirstCategoryId(2))
                ],
            ]); // проинициализировать DynamicForm
        }

        $categoryData = $this->_formCategoryService->getData($lessonForm->category->category_id);

        return $this->renderAjax('form', [
            'lessonForm' => $lessonForm,
            'categories' => DataHelper::getCategories(false),
            'subcategories' => $categoryData['subcategories'],
            'parentCategoryId' => $categoryData['parentCategoryId'],
            'action' => ['/lesson/create'],
        ]);
    }

    /**
     * @param $id
     * @return array|mixed|string
     */
    public function actionUpdate($id)
    {
        $lessonForm = new LessonForm();
        $lesson = $this->_lessonRepository->findOneByCriteria(['lesson.id' => $id]);
        $lessonForm->_lesson = $lesson;
        $dto = $this->_lessonService->getOne(['lesson.id' => $id]);
        $dto['category_id'] = Yii::$app->request->get('category_id', $dto['category_id']);

        $lessonForm->prepareUpdate($dto);

        $input = Yii::$app->request->post();
        if (!empty($input)) {
            $lessonForm->load($input);
            if (array_key_exists('ajax', $input)) {
                return $this->validateAjax($lessonForm);
            }

            if ($lessonForm->validate()) {
                $lessonForm->bindData();
                $lessonForm->save();

                Yii::$app->response->format = Response::FORMAT_JSON;
                return ['success' => true];
            }
        }

        $categoryData = $this->_formCategoryService->getData($lessonForm->category->category_id);

        return $this->renderAjax('form', [
            'lessonForm' => $lessonForm,
            'categories' => DataHelper::getCategories(false),
            'subcategories' => $categoryData['subcategories'],
            'parentCategoryId' => $categoryData['parentCategoryId'],
            'action' => ['/lesson/update', 'id' => $id]
        ]);
    }

    /**
     * @param $id
     * @return Response
     */
    public function actionDelete($id)
    {
        $this->_lessonService->delete(['lesson.id' => $id]);

        return $this->redirect(Yii::$app->request->referrer);
    }

    /**
     * @param $params
     * @return array
     */
    protected function extractParams($params)
    {
        if (array_key_exists('LessonSearch', $params)) {
            $search = $params['LessonSearch'];
            unset($params['LessonSearch']);

            $params = array_merge($params, $search);
        }

        if (!empty($params['category_id'])) {
            $category = $this->_categoryRepository->findOneByCriteria(['id' => $params['category_id']]);

            unset($params['category_id']);
            $params['lft'] = $category['lft'];
            $params['rgt'] = $category['rgt'];
            $params['root'] = $category['root'];
        }

        return $params;
    }
}
