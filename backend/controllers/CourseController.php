<?php

namespace backend\controllers;

use backend\forms\ar\CourseForm;
use backend\models\search\CourseSearch;
use backend\services\FormCategoryService;
use common\data\MyArrayDataProvider;
use common\factories\DynamicFormFactory;
use common\helpers\DataHelper;
use common\helpers\FormHelper;
use common\interfaces\repositories\CategoryRepositoryInterface;
use common\mappers\DynamicFormValuesMapper;
use common\mappers\TranslationsMapper;
use common\models\AttributeGroup;
use common\models\Course;
use common\repositories\sql\CourseRepository;
use common\services\entities\CourseService;
use common\services\entities\LessonService;
use Yii;
use yii\base\Module;
use yii\filters\AccessControl;
use yii\helpers\ArrayHelper;
use yii\web\Response;

/**
 * Class CourseController
 * @package backend\controllers
 */
class CourseController extends BackEndController
{
    /**
     * @var CourseRepository
     */
    private $_courseRepository;
    /**
     * @var CourseService
     */
    private $_courseService;
    /**
     * @var CategoryRepositoryInterface
     */
    private $_categoryRepository;
    /**
     * @var LessonService
     */
    private $_lessonService;

    /**
     * CourseController constructor.
     * @param string $id
     * @param Module $module
     * @param CourseRepository $courseRepository
     * @param CourseService $courseService
     * @param CategoryRepositoryInterface $categoryRepository
     * @param LessonService $lessonService
     * @param array $config
     */
    public function __construct(string $id, Module $module,
                                CourseRepository $courseRepository,
                                CourseService $courseService,
                                CategoryRepositoryInterface $categoryRepository,
                                LessonService $lessonService,
                                array $config = [])
    {
        parent::__construct($id, $module, $config);
        $this->_courseRepository = $courseRepository;
        $this->_courseService = $courseService;
        $this->_categoryRepository = $categoryRepository;
        $this->_lessonService = $lessonService;
    }

    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return array_merge(parent::behaviors(), [
            'access' => [
                'class' => AccessControl::class,
                'rules' => [
                    [
                        'actions' => ['index', 'update', 'create', 'attributes', 'delete'],
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
        ]);
    }

    /**
     * @return string
     * @throws \ReflectionException
     * @throws \common\exceptions\RepositoryException
     * @throws \yii\base\InvalidConfigException
     * @throws \yii\di\NotInstantiableException
     */
    public function actionIndex()
    {
        $search = new CourseSearch($this->_courseRepository, [
            'pagination' => true,
            'perPage' => 20,
            'orderBy' => ['id' => SORT_DESC]
        ]);

        $queryParams = Yii::$app->request->queryParams;
        $params = $this->extractParams($queryParams);
        $courses = $search->search($params);

        $dataProvider = new MyArrayDataProvider([
            'data' => $courses['items'],
            'totalCount' => $courses['pagination']->totalCount ?? count($courses['items']),
            'pagination' => [
                'pageSize' => 20
            ]
        ]);
        $search->category_id = $queryParams['CourseSearch']['category_id'] ?? null;

        $categories = DataHelper::getCategories();

        return $this->render('index', [
            'dataProvider' => $dataProvider,
            'search' => $search,
            'categories' => $categories
        ]);
    }

    /**
     * @return array|mixed|string
     * @throws \common\exceptions\ClassNotFoundException
     * @throws \common\exceptions\EntityNotCreatedException
     * @throws \common\exceptions\RepositoryException
     * @throws \yii\base\InvalidConfigException
     * @throws \yii\di\NotInstantiableException
     */
    public function actionCreate()
    {
        $courseForm = new CourseForm(['_course' => new Course()]);

        $input = Yii::$app->request->post();
        if (!empty($input)) {
            $courseForm->load($input);

            if (array_key_exists('ajax', $input)) {
                return $this->validateAjax($courseForm);
            }
            if ($courseForm->validate()) {
                $courseForm->bindData();
                if ($courseForm->save()) {
                    return $this->redirect(['/course/index']);
                }
            }
        } else {
            $courseForm->load([
                'CategoryForm' => [
                    'category_id' => (int)Yii::$app->request->get('category_id', DataHelper::getFirstCategoryId())
                ],
                'CourseForm' => [

                ]
            ]); // проинициализировать DynamicForm
        }

        $formConfig = FormHelper::getFormConfig(FormHelper::ENTITY_COURSE, $courseForm->category->category_id);
        $courseForm->buildLayout($formConfig);

        return $this->renderAjax('form', [
            'courseForm' => $courseForm,
            'categories' => DataHelper::getCategories(),
            'action' => ['/course/create'],
            'lessonsData' => []
        ]);
    }

    /**
     * @param $id
     * @return array|mixed|string
     */
    public function actionUpdate($id)
    {
        $course = $this->_courseRepository->findOneByCriteria(['course.id' => $id]);
        $courseForm = new CourseForm(['_course' => $course]);
        $courseDTO = $this->_courseService->getOne(['course.id' => $id]);
        $courseDTO['category_id'] = Yii::$app->request->get('category_id', $courseDTO['category_id']);
        $courseForm->prepareUpdate($courseDTO);

        $input = Yii::$app->request->post();
        if (!empty($input)) {
            $courseForm->load($input);
            if (array_key_exists('ajax', $input)) {
                return $this->validateAjax($courseForm);
            }

            if ($courseForm->validate()) {
                $courseForm->bindData();
                $courseForm->save();

                Yii::$app->response->format = Response::FORMAT_JSON;
                return ['success' => true];
            }
        }

        $formConfig = FormHelper::getFormConfig(FormHelper::ENTITY_COURSE, $courseForm->category->category_id);
        $courseForm->buildLayout($formConfig);

        $lessonsData = $this->_lessonService->getMany(['id' => $courseDTO['lessonIds']]);

        $lessonsData =  ArrayHelper::map($lessonsData['items'], 'id', function($var){
            return [$var['id'] => "{$var['title']} (Sort: {$var['sort']}) (ID: {$var['id']})"];
        });

        return $this->renderAjax('form', [
            'courseForm' => $courseForm,
            'categories' => DataHelper::getCategories(),
            'action' => ['/course/update', 'id' => $id],
            'lessonsData' => $lessonsData
        ]);
    }

    /**
     * @param $id
     * @return Response
     */
    public function actionDelete($id)
    {
        /** @var Course $course */
        $course = $this->_courseRepository->findOneByCriteria(['course.id' => $id]);

        if ($course !== null) {
            $course->updateAttributes(['status' => Course::STATUS_DELETED]);
        }

        return $this->redirect(Yii::$app->request->referrer);
    }

    /**
     * @param $category_id
     * @param null $entity_id
     * @return string
     */
    public function actionAttributes($category_id, $entity_id = null)
    {
        $dynamicForm = DynamicFormFactory::initByCategory(AttributeGroup::ENTITY_COURSE_CATEGORY, $category_id);

        if (!empty($entity_id) && !empty($dynamicForm->attributes)) {
            $course = $this->_courseService->getOne(['course.id' => $entity_id]);
            $dynamicForm->load(DynamicFormValuesMapper::getMappedData($course['attributes'], $dynamicForm->config), '');
        }

        return $this->renderAjax('partial/attributes', [
            'dynamicForm' => $dynamicForm
        ]);
    }

    /**
     * @param $queryParams
     * @return array
     */
    protected function extractParams($queryParams)
    {
        $params = $queryParams;

        if (array_key_exists('CourseSearch', $params)) {
            $courseSearch = $params['CourseSearch'];
            unset($params['CourseSearch']);

            $params = array_merge($params, $courseSearch);
        }

        if (!empty($params['category_id'])) {
            $category = $this->_categoryRepository->findOneByCriteria(['id' => $params['category_id']]);

            unset($params['category_id']);
            $params['lft'] = $category['lft'];
            $params['rgt'] = $category['rgt'];
            $params['root'] = $category['root'];
        }

        return $params;
    }
}
