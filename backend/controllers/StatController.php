<?php

namespace backend\controllers;

use common\repositories\sql\UserRepository;
use Yii;
use yii\base\Module;
use yii\data\ArrayDataProvider;

/**
 * Class StatController
 * @package backend\controllers
 */
class StatController extends BackEndController
{
    /**
     * @var UserRepository
     */
    private $_userRepository;

    /**
     * StatController constructor.
     * @param string $id
     * @param Module $module
     * @param UserRepository $userRepository
     * @param array $config
     */
    public function __construct(string $id, Module $module, UserRepository $userRepository, array $config = [])
    {
        parent::__construct($id, $module, $config);

        $this->_userRepository = $userRepository;
    }

    /**
     * @return string
     */
    public function actionIndex()
    {
        $data = [];
        $y = Yii::$app->request->get('year');
        $m = Yii::$app->request->get('month');
        $now = new \DateTime('now');
        $year = $y ?? $now->format('Y');
        $month = $m ?? $now->format('m');
        $day = (($m == $now->format('m') && $y == $now->format('Y')) || $m == null)
            ? $now->format('d')
            : cal_days_in_month(CAL_GREGORIAN, $month, $year);

        for ($i = 1; $i < $day + 1; $i++) {
            $currentDayUsers = $this->_userRepository->select(['id'], true)->countByCriteria([
                'and',
                [
                    'between',
                    'created_at',
                    strtotime("{$i}-{$month}-{$year} 00:00:00"),
                    strtotime("{$i}-{$month}-{$year} 23:59:59")
                ],
            ], [], true);

            $data[] = [
                'id' => $i,
                'currentDayUsers' => $currentDayUsers,
            ];
        }
//
        $data = array_reverse($data);

        $provider = new ArrayDataProvider([
            'allModels' => $data,
            'sort' => [
                'attributes' => ['id'],
            ],
            'pagination' => [
                'pageSize' => 35,
            ],
        ]);

        return $this->render('index', [
            'provider' => $provider,
            'month' => $month,
            'year' => $year
        ]);
    }
}
