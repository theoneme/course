<?php

namespace backend\controllers;

use common\data\MyArrayDataProvider;
use common\forms\ar\TaskForm;
use common\models\Task;
use common\models\search\TaskSearch;
use common\repositories\sql\TaskRepository;
use common\services\entities\TaskService;
use Yii;
use yii\base\Module;
use yii\db\Expression;
use yii\filters\AccessControl;
use yii\web\Response;

/**
 * Class TaskController
 * @package backend\controllers\location
 */
class TaskController extends BackEndController
{
    /**
     * @var TaskRepository
     */
    private $_taskRepository;
    /**
     * @var TaskService
     */
    private $_taskService;

    /**
     * TaskController constructor.
     * @param string $id
     * @param Module $module
     * @param TaskService $taskService
     * @param TaskRepository $taskRepository
     * @param array $config
     */
    public function __construct(string $id, Module $module,
                                TaskService $taskService,
                                TaskRepository $taskRepository,
                                array $config = []
    )
    {
        parent::__construct($id, $module, $config);
        $this->_taskRepository = $taskRepository;
        $this->_taskService = $taskService;
    }

    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return array_merge(parent::behaviors(), [
            'access' => [
                'class' => AccessControl::class,
                'rules' => [
                    [
                        'actions' => ['index', 'update', 'create', 'delete'],
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
        ]);
    }

    /**
     * @return string
     * @throws \ReflectionException
     * @throws \common\exceptions\RepositoryException
     * @throws \yii\base\InvalidConfigException
     * @throws \yii\di\NotInstantiableException
     */
    public function actionIndex()
    {
        $search = new TaskSearch($this->_taskRepository, [
            'pagination' => true,
            'perPage' => 20,
            'orderBy' => new Expression('id desc')
        ]);
        $queryParams = Yii::$app->request->queryParams;
        $items = $search->search($this->extractParams($queryParams));

        $dataProvider = new MyArrayDataProvider([
            'data' => $items['items'],
            'totalCount' => $items['pagination']->totalCount ?? count($items['items']),
            'pagination' => [
                'pageSize' => 20
            ]
        ]);

        return $this->render('index', [
            'dataProvider' => $dataProvider,
            'search' => $search,
        ]);
    }


    /**
     * @return array|mixed|string
     * @throws \common\exceptions\ClassNotFoundException
     * @throws \common\exceptions\EntityNotCreatedException
     * @throws \common\exceptions\RepositoryException
     * @throws \yii\base\InvalidConfigException
     * @throws \yii\di\NotInstantiableException
     */
    public function actionCreate()
    {
        $taskForm = new TaskForm(['_task' => new Task()]);

        $input = Yii::$app->request->post();
        if (!empty($input)) {
            $taskForm->load($input);

            if (array_key_exists('ajax', $input)) {
                return $this->validateAjax($taskForm);
            }
            if ($taskForm->validate()) {
                $taskForm->bindData();
                $taskForm->save();

                Yii::$app->response->format = Response::FORMAT_JSON;
                return ['success' => true];
            }
        }


        return $this->renderAjax('form', [
            'taskForm' => $taskForm,
            'selectizeData' => [],
            'action' => ['/task/create'],
        ]);
    }

    /**
     * @param $id
     * @return array|mixed|string
     */
    public function actionUpdate($id)
    {
        $taskForm = new TaskForm();
        $task = $this->_taskRepository->findOneByCriteria(['task.id' => $id]);
        $taskForm->_task = $task;
        $dto = $this->_taskService->getOne(['task.id' => $id]);

        $taskForm->prepareUpdate($dto);

        $input = Yii::$app->request->post();
        if (!empty($input)) {
            $taskForm->load($input);
            if (array_key_exists('ajax', $input)) {
                return $this->validateAjax($taskForm);
            }

            if ($taskForm->validate()) {
                $taskForm->bindData();
                $taskForm->save();

                Yii::$app->response->format = Response::FORMAT_JSON;
                return ['success' => true];
            }
        }
        $selectizeData = [$dto['lesson_id'] => "{$dto['lesson']['title']} (ID: {$dto['lesson_id']})"];

        return $this->renderAjax('form', [
            'taskForm' => $taskForm,
            'selectizeData' => $selectizeData,
            'action' => ['/task/update', 'id' => $id]
        ]);
    }

    /**
     * @param $id
     * @return Response
     */
    public function actionDelete($id)
    {
        $this->_taskService->delete(['task.id' => $id]);

        return $this->redirect(Yii::$app->request->referrer);
    }

    /**
     * @param $params
     * @return array
     */
    protected function extractParams($params)
    {
        if (array_key_exists('TaskSearch', $params)) {
            $search = $params['TaskSearch'];
            unset($params['TaskSearch']);

            $params = array_merge($params, $search);
        }

        return $params;
    }
}
