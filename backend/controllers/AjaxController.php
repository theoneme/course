<?php

namespace backend\controllers;

use common\forms\ar\composite\LessonForm;
use common\mappers\SelectizeAttributeValuesMapper;
use common\mappers\SelectizeUserMapper;
use common\services\entities\AttributeValueService;
use common\services\entities\LessonService;
use common\services\entities\UserService;
use Yii;
use yii\filters\AjaxFilter;
use yii\filters\ContentNegotiator;
use yii\web\Response;

/**
 * Class AjaxController
 * @package backend\controllers
 */
class AjaxController extends BackEndController
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return array_merge(parent::behaviors(), [
            [
                'class' => ContentNegotiator::class,
                'only' => ['attribute-values', 'users', 'lessons', 'render-course-lesson'],
                'formats' => [
                    'application/json' => Response::FORMAT_JSON,
                ],
            ],
            [
                'class' => AjaxFilter::class,
                'only' => ['attribute-values', 'users', 'lessons', 'render-course-lesson'],
            ]
        ]);
    }

    /**
     * @param $attribute
     * @param $query
     * @param null $exceptId
     * @return array
     */
    public function actionAttributeValues($attribute, $query, $exceptId = null)
    {
        /** @var AttributeValueService $attributeValueService */
        $attributeValueService = Yii::$container->get(AttributeValueService::class);

        $values = $attributeValueService->getMany(['title' => $query, 'attribute_id' => $attribute, 'except_id' => $exceptId]);
        if (!empty($values['items'])) {
            return SelectizeAttributeValuesMapper::getMappedData($values['items']);
        }

        return [];
    }

    /**
     * @param $request
     * @return array
     */
    public function actionUsers($request)
    {
        /** @var UserService $userService */
        $userService = Yii::$container->get(UserService::class);
        $users = $userService->getMany(['username' => $request], ['limit' => 10]);
        $users = SelectizeUserMapper::getMappedData($users['items']);

        return $users;
    }

    /**
     * @param $request
     * @return array
     */
    public function actionLessons($request)
    {
        /** @var LessonService $service */
        $service = Yii::$container->get(LessonService::class);
        $items = $service->getMany(['title' => $request], ['limit' => 10]);

        return array_map(function($var){
            return ['id' => $var['id'], 'value' => "{$var['title']} (Sort: {$var['sort']}) (ID: {$var['id']})"];
        }, $items['items']);
    }

    /**
     * @param $iterator
     * @return array
     */
    public function actionRenderCourseLesson($iterator)
    {
        return [
            'html' => $this->renderAjax('@backend/views/course/steps/lesson-partial', [
                'model' => new LessonForm(),
                'key' => $iterator,
                'createForm' => true,
                'lessonData' => []
            ]),
            'success' => true,
        ];
    }
}
