<?php

namespace backend\controllers;

use common\data\MyArrayDataProvider;
use common\forms\ar\PaymentForm;
use common\helpers\DataHelper;
use common\helpers\SberbankHelper;
use common\helpers\UtilityHelper;
use common\models\Payment;
use common\models\search\PaymentSearch;
use common\repositories\sql\PaymentRepository;
use common\services\entities\PaymentService;
use Voronkovich\SberbankAcquiring\Client;
use Yii;
use yii\base\Module;
use yii\db\Expression;
use yii\filters\AccessControl;
use yii\web\Response;

/**
 * Class PaymentController
 * @package backend\controllers
 */
class PaymentController extends BackEndController
{
    /**
     * @var PaymentRepository
     */
    private $_paymentRepository;
    /**
     * @var PaymentService
     */
    private $_paymentService;

    /**
     * PaymentController constructor.
     * @param string $id
     * @param Module $module
     * @param PaymentService $paymentService
     * @param PaymentRepository $paymentRepository
     * @param array $config
     */
    public function __construct(string $id, Module $module,
                                PaymentService $paymentService,
                                PaymentRepository $paymentRepository,
                                array $config = []
    )
    {
        parent::__construct($id, $module, $config);
        $this->_paymentRepository = $paymentRepository;
        $this->_paymentService = $paymentService;
    }

    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return array_merge(parent::behaviors(), [
            'access' => [
                'class' => AccessControl::class,
                'rules' => [
                    [
                        'actions' => ['index', 'update', 'create', 'result', 'delete'],
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
        ]);
    }

    /**
     * @return string
     * @throws \ReflectionException
     * @throws \common\exceptions\RepositoryException
     * @throws \yii\base\InvalidConfigException
     * @throws \yii\di\NotInstantiableException
     */
    public function actionIndex()
    {
        $search = new PaymentSearch($this->_paymentRepository, [
            'pagination' => true,
            'perPage' => 20,
            'orderBy' => new Expression('id desc')
        ]);
        $queryParams = Yii::$app->request->queryParams;
        $items = $search->search($this->extractParams($queryParams));

        $dataProvider = new MyArrayDataProvider([
            'data' => $items['items'],
            'totalCount' => $items['pagination']->totalCount ?? count($items['items']),
            'pagination' => [
                'pageSize' => 20
            ]
        ]);

        return $this->render('index', [
            'dataProvider' => $dataProvider,
            'paymentMethods' => DataHelper::getPaymentMethods(),
            'search' => $search,
        ]);
    }

//    /**
//     * @return array|mixed|string
//     * @throws \common\exceptions\ClassNotFoundException
//     * @throws \common\exceptions\EntityNotCreatedException
//     * @throws \common\exceptions\RepositoryException
//     * @throws \yii\base\InvalidConfigException
//     * @throws \yii\di\NotInstantiableException
//     */
//    public function actionCreate()
//    {
//        $paymentForm = new PaymentForm([]);
//
//        $input = Yii::$app->request->post();
//        if (!empty($input)) {
//            $paymentForm->load($input);
//
//            if (array_key_exists('ajax', $input)) {
//                return $this->validateAjax($paymentForm);
//            }
//            if ($paymentForm->validate()) {
//                $paymentForm->save();
//
//                Yii::$app->response->format = Response::FORMAT_JSON;
//                return ['success' => true];
//            }
//        }
//
//
//        return $this->renderAjax('form', [
//            'paymentForm' => $paymentForm,
//            'paymenyMethods' => DataHelper::getPaymentMethods(),
//            'action' => ['/payment/create'],
//        ]);
//    }
//
//    /**
//     * @param $id
//     * @return array|mixed|string
//     */
//    public function actionUpdate($id)
//    {
//        $paymentForm = new PaymentForm();
//        $dto = $this->_paymentService->getOne(['payment.id' => $id]);
//
//        $paymentForm->load($dto);
//
//        $input = Yii::$app->request->post();
//        if (!empty($input)) {
//            $paymentForm->load($input);
//            if (array_key_exists('ajax', $input)) {
//                return $this->validateAjax($paymentForm);
//            }
//
//            if ($paymentForm->validate()) {
//                $paymentForm->save();
//
//                Yii::$app->response->format = Response::FORMAT_JSON;
//                return ['success' => true];
//            }
//        }
//
//        return $this->renderAjax('form', [
//            'paymentForm' => $paymentForm,
//            'paymenyMethods' => DataHelper::getPaymentMethods(),
//            'action' => ['/payment/update', 'id' => $id]
//        ]);
//    }

    /**
     * @param $id
     */
    public function actionResult($id)
    {
        $payment = $this->_paymentService->getOne(['payment.id' => $id]);
        $result = null;
        if (!empty($payment['code'])) {
            try {
                /** @var Client $client */
                $client = new Client(SberbankHelper::getConfigArray());
                $result = $client->getOrderStatusExtended($payment['code']);
            } catch (\Exception $e) {
                $result = $e->getMessage();
            }
        }
        UtilityHelper::debug($result);
    }

    /**
     * @param $id
     * @return Response
     */
    public function actionDelete($id)
    {
        $this->_paymentService->delete(['payment.id' => $id]);

        return $this->redirect(Yii::$app->request->referrer);
    }

    /**
     * @param $params
     * @return array
     */
    protected function extractParams($params)
    {
        if (array_key_exists('PaymentSearch', $params)) {
            $search = $params['PaymentSearch'];
            unset($params['PaymentSearch']);

            $params = array_merge($params, $search);
        }

        return $params;
    }
}
