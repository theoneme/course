<?php

namespace backend\controllers;

use common\data\MyArrayDataProvider;
use common\forms\ar\PaymentMethodForm;
use common\models\PaymentMethod;
use common\models\search\PaymentMethodSearch;
use common\repositories\sql\PaymentMethodRepository;
use common\services\entities\PaymentMethodService;
use Yii;
use yii\base\Module;
use yii\db\Expression;
use yii\filters\AccessControl;
use yii\web\Response;

/**
 * Class PaymentMethodController
 * @package backend\controllers\location
 */
class PaymentMethodController extends BackEndController
{
    /**
     * @var PaymentMethodRepository
     */
    private $_paymentMethodRepository;
    /**
     * @var PaymentMethodService
     */
    private $_paymentMethodService;

    /**
     * PaymentMethodController constructor.
     * @param string $id
     * @param Module $module
     * @param PaymentMethodService $paymentMethodService
     * @param PaymentMethodRepository $paymentMethodRepository
     * @param array $config
     */
    public function __construct(string $id, Module $module,
                                PaymentMethodService $paymentMethodService,
                                PaymentMethodRepository $paymentMethodRepository,
                                array $config = []
    )
    {
        parent::__construct($id, $module, $config);
        $this->_paymentMethodRepository = $paymentMethodRepository;
        $this->_paymentMethodService = $paymentMethodService;
    }

    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return array_merge(parent::behaviors(), [
            'access' => [
                'class' => AccessControl::class,
                'rules' => [
                    [
                        'actions' => ['index', 'update', 'create', 'delete'],
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
        ]);
    }

    /**
     * @return string
     * @throws \ReflectionException
     * @throws \common\exceptions\RepositoryException
     * @throws \yii\base\InvalidConfigException
     * @throws \yii\di\NotInstantiableException
     */
    public function actionIndex()
    {
        $search = new PaymentMethodSearch($this->_paymentMethodRepository, [
            'pagination' => true,
            'perPage' => 20,
        ]);
        $queryParams = Yii::$app->request->queryParams;
        $items = $search->search($this->extractParams($queryParams));

        $dataProvider = new MyArrayDataProvider([
            'data' => $items['items'],
            'totalCount' => $items['pagination']->totalCount ?? count($items['items']),
            'pagination' => [
                'pageSize' => 20
            ]
        ]);

        return $this->render('index', [
            'dataProvider' => $dataProvider,
            'search' => $search,
        ]);
    }


    /**
     * @return array|mixed|string
     * @throws \common\exceptions\ClassNotFoundException
     * @throws \common\exceptions\EntityNotCreatedException
     * @throws \common\exceptions\RepositoryException
     * @throws \yii\base\InvalidConfigException
     * @throws \yii\di\NotInstantiableException
     */
    public function actionCreate()
    {
        $paymentMethodForm = new PaymentMethodForm(['_paymentMethod' => new PaymentMethod()]);

        $input = Yii::$app->request->post();
        if (!empty($input)) {
            $paymentMethodForm->load($input);

            if (array_key_exists('ajax', $input)) {
                return $this->validateAjax($paymentMethodForm);
            }
            if ($paymentMethodForm->validate()) {
                $paymentMethodForm->bindData();
                $paymentMethodForm->save();

                Yii::$app->response->format = Response::FORMAT_JSON;
                return ['success' => true];
            }
        }


        return $this->renderAjax('form', [
            'paymentMethodForm' => $paymentMethodForm,
            'action' => ['/payment-method/create'],
        ]);
    }

    /**
     * @param $id
     * @return array|mixed|string
     */
    public function actionUpdate($id)
    {
        $paymentMethodForm = new PaymentMethodForm();
        $paymentMethod = $this->_paymentMethodRepository->findOneByCriteria(['payment_method.id' => $id]);
        $paymentMethodForm->_paymentMethod = $paymentMethod;
        $dto = $this->_paymentMethodService->getOne(['payment_method.id' => $id]);

        $paymentMethodForm->prepareUpdate($dto);

        $input = Yii::$app->request->post();
        if (!empty($input)) {
            $paymentMethodForm->load($input);
            if (array_key_exists('ajax', $input)) {
                return $this->validateAjax($paymentMethodForm);
            }

            if ($paymentMethodForm->validate()) {
                $paymentMethodForm->bindData();
                $paymentMethodForm->save();

                Yii::$app->response->format = Response::FORMAT_JSON;
                return ['success' => true];
            }
        }

        return $this->renderAjax('form', [
            'paymentMethodForm' => $paymentMethodForm,
            'action' => ['/payment-method/update', 'id' => $id]
        ]);
    }

    /**
     * @param $id
     * @return Response
     */
    public function actionDelete($id)
    {
        $this->_paymentMethodService->delete(['payment_method.id' => $id]);

        return $this->redirect(Yii::$app->request->referrer);
    }

    /**
     * @param $params
     * @return array
     */
    protected function extractParams($params)
    {
        if (array_key_exists('PaymentMethodSearch', $params)) {
            $search = $params['PaymentMethodSearch'];
            unset($params['PaymentMethodSearch']);

            $params = array_merge($params, $search);
        }

        return $params;
    }
}
