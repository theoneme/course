<?php

namespace backend\controllers;

use common\interfaces\repositories\UserRepositoryInterface;
use Yii;
use yii\base\Module;
use yii\filters\AccessControl;
use yii\helpers\FileHelper;

/**
 * Class SiteController
 * @package backend\controllers
 */
class SiteController extends BackEndController
{
    /**
     * @var UserRepositoryInterface
     */
    private $_userRepository;

    /**
     * SiteController constructor.
     * @param string $id
     * @param Module $module
     * @param UserRepositoryInterface $userRepository
     * @param array $config
     */
    public function __construct(string $id, Module $module, UserRepositoryInterface $userRepository, array $config = [])
    {
        parent::__construct($id, $module, $config);

        $this->_userRepository = $userRepository;
    }

    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return array_merge(parent::behaviors(), [
            'access' => [
                'class' => AccessControl::class,
                'rules' => [
                    [
                        'actions' => ['index', 'clear-cache'],
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
        ]);
    }

    /**
     * Displays homepage.
     *
     * @return string
     */
    public function actionIndex()
    {
        $counts = [
            'totalUsers' => $this->_userRepository->select(['id'], true)->countByCriteria(),
        ];

        return $this->render('index', [
            'counts' => $counts
        ]);
    }

    /**
     * @return \yii\web\Response
     */
    public function actionClearCache()
    {
        Yii::$app->cache->flush();
        $folders = [
            Yii::getAlias('@backend') . '/runtime/cache/*',
            Yii::getAlias('@backend') . '/web/assets/*',
            Yii::getAlias('@frontend') . '/runtime/cache/*',
            Yii::getAlias('@frontend') . '/web/assets/*',
//            Yii::getAlias('@console') . '/runtime/cache/*',
        ];
        foreach ($folders as $folder) {
            $files = glob($folder);
            foreach ($files as $file) {
                FileHelper::removeDirectory($file);
            }
        }
        return $this->redirect(Yii::$app->request->referrer);
    }
}
