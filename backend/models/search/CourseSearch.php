<?php

namespace backend\models\search;

use common\interfaces\repositories\CourseRepositoryInterface;
use common\interfaces\RepositoryInterface;
use common\models\search\CourseSearch as BaseCourseSearch;
use common\models\Translation;

/**
 * Class CourseSearch
 * @package common\models\search
 */
class CourseSearch extends BaseCourseSearch
{
    /**
     * @var string
     */
    public $title;

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return array_merge(parent::rules(), [
            [['id'], 'each', 'rule' => ['integer'], 'when' => function ($model) {
                return is_array($model->id);
            }],
            [['id'], 'integer', 'when' => function ($model) {
                return !is_array($model->id);
            }],
            [['title'], 'string']
        ]);
    }

    /**
     * @param array $params
     * @return CourseRepositoryInterface|RepositoryInterface
     */
    protected function initQuery(array $params = [])
    {
        $query = parent::initQuery($params);

        if (!empty($this->title)) {
            $query->joinWith(['translations']);
        }

        return $query;
    }

    /**
     * @param $criteria
     * @param $params
     * @return mixed
     */
    protected function improveCriteria($criteria, $params)
    {
        $criteria = parent::improveCriteria($criteria, $params);

        if (!empty($this->title)) {
            $criteria = ['and', $criteria, ['like', 'course_translations.value', "{$this->title}%", false], ['course_translations.key' => Translation::KEY_TITLE]];
        }

        return $criteria;
    }
}
