<?php

namespace backend\models\search;

use common\models\search\UserSearch as BaseUserSearch;
use yii\helpers\ArrayHelper;

/**
 * Class UserSearch
 * @package backend\models\search
 */
class UserSearch extends BaseUserSearch
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['username', 'email', 'phone'], 'string'],
            [['status', 'id'], 'integer'],
        ];
    }

    /**
     * @param $criteria
     * @param $params
     * @return mixed
     */
    protected function improveCriteria($criteria, $params)
    {
        $username = ArrayHelper::remove($criteria, 'username');
        $email = ArrayHelper::remove($criteria, 'email');
        $phone = ArrayHelper::remove($criteria, 'phone');

        if ($username) {
            $criteria = ['and',
                ['like', 'username', $username],
                $criteria
            ];
        }

        if ($email) {
            $criteria = ['and',
                ['like', 'email', $email],
                $criteria
            ];
        }

        if ($phone) {
            $criteria = ['and',
                ['like', 'phone', $email],
                $criteria
            ];
        }

        return $criteria;
    }
}
