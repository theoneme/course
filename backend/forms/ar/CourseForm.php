<?php

namespace backend\forms\ar;

use common\forms\ar\CourseForm as BaseCourseForm;
use common\models\Course;

/**
 * Class CourseForm
 * @package backend\forms\ar
 */
class CourseForm extends BaseCourseForm
{
    /**
     * @var array
     */
    public $steps;

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return array_merge(parent::rules(), [
            [['status'], 'required'],
        ]);
    }

    /**
     * @param $config
     */
    public function buildLayout($config)
    {
        $this->steps = $config;
    }
}