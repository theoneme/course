<?php

namespace backend\forms\ar;

use common\forms\ar\UserForm as BaseUserForm;

/**
 * Class UserForm
 * @package backend\forms\ar
 */
class UserForm extends BaseUserForm
{
    /**
     * @var array
     */
    public $steps;

    /**
     * @param $config
     */
    public function buildLayout($config)
    {
        $this->steps = $config;
    }
}