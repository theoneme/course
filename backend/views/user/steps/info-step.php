<?php

/**
 * @var array $step
 * @var \backend\forms\ar\UserForm $userForm
 * @var \yii\widgets\ActiveForm $form
 */
?>


<?= $form->field($userForm->meta, 'subtitle')->textarea([
    'placeholder' => Yii::t('model', 'Profile Short Description'),
    'rows' => 4
]) ?>
<?= $form->field($userForm->meta, 'description')->textarea([
    'placeholder' => Yii::t('model', 'Profile Description'),
    'rows' => 8
]) ?>