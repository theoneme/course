<?php

/**
 * @var array $step
 * @var \backend\forms\ar\UserForm $userForm
 * @var \yii\widgets\ActiveForm $form
 */

?>

<p><?= Yii::t('model', 'If you wish to change your password, you can do it here') ?></p>

<?= $form->field($userForm->password, 'password')->passwordInput() ?>
<?= $form->field($userForm->password, 'confirm')->passwordInput() ?>
