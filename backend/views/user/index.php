<?php

use common\decorators\UserStatusDecorator;
use yii\grid\GridView;
use yii\helpers\Html;
use yii\web\User;
use yii\widgets\Pjax;
use yii\helpers\Url;

/**
 * @var \backend\models\search\UserSearch $search
 * @var \yii\web\View $this
 */

$this->title = Yii::t('main', "Users");
?>

<div class="box box-danger">
    <div class="box-header with-border">
        <h3 class="box-title">Users list</h3>
    </div>
    <!-- /.box-header -->
    <!-- form start -->

    <div class="box-body">
        <?php Pjax::begin(['id' => 'user-pjax', 'timeout' => 6000]) ?>
        <?= GridView::widget([
            'dataProvider' => $dataProvider,
            'filterModel' => $search,
            'layout' => "{items}\n{pager}",
            'id' => 'user-grid',
            'columns' => [
                [
                    'attribute' => 'id',
                    'contentOptions' => ['style' => 'width: 72px; white-space: normal; font-size: 12px;'],
                    'options' => ['style' => 'width: 72px']
                ],
                [
                    'attribute' => 'username',
                    'header' => 'Имя',
                    'value' => function ($model) {
                        /** @var User $model */
                        return Html::a($model['username'],
                            ['/user/update', 'id' => $model['id']],
                            [
                                'title' => Yii::t('main', 'Update {0}', ['user']),
                                'data-action' => 'load-edit-modal',
                                'data-pjax' => 0
                            ]
                        );
                    },
                    'format' => 'raw',
                    'contentOptions' => ['style' => 'width: 200px; white-space: normal; text-align: center;'],
                    'options' => ['style' => 'width: 200px'],
                ],
                'email',
                'created_at:datetime',
                [
                    'header' => 'UTM',
                    'value' => function ($model) {
                        $data = [];
                        foreach ($model['customData']['utm'] ?? [] as $key => $value) {
                            $data[] = "$key: $value";
                        }
                        return implode('<br>', $data);
                    },
                    'format' => 'raw',
                ],
                [
                    'attribute' => 'status',
                    'header' => 'Статус',
                    'value' => function ($model) {
                        /* @var $model User */
                        return UserStatusDecorator::decorate($model['status']);
                    },

                    'format' => 'html',
                    'contentOptions' => ['style' => 'width: 100px; white-space: normal; text-align: center;'],
                    'options' => ['style' => 'width: 100px'],
                    'filter' => \common\decorators\UserStatusDecorator::getStatusLabels(false)
                ],
                [
                    'class' => 'yii\grid\ActionColumn',
                    'header' => 'Actions',
                    'template' => '{update} {delete}',
                    'buttons' => [
                        'update' => function ($url, $model) {
                            return Html::a('<span class="glyphicon glyphicon-pencil"></span>', $url, [
                                'title' => 'Update User',
                                'data-pjax' => 0,
                                'data-action' => 'load-edit-modal'
                            ]);
                        }
                    ],
                    'urlCreator' => function ($action, $model, $key, $index) {
                        switch ($action) {
                            case 'update':
                                $url = Url::toRoute(["/user/{$action}", 'id' => $model['id']]);
                                break;
                            default:
                                $url = Url::toRoute(["/user/{$action}", 'id' => $model['id']]);
                                break;
                        }
                        return $url;
                    }
                ],
            ],
        ]); ?>
        <?php Pjax::end() ?>
    </div>
    <!-- /.box-body -->
</div>
