<?php

use common\decorators\CourseStatusDecorator;
use common\decorators\CourseTypeDecorator;
use yii\grid\GridView;
use yii\helpers\Html;
use yii\helpers\Url;
use yii\widgets\Pjax;

/**
 * @var \common\models\search\CourseSearch $search
 * @var array $categories
 * @var array $address
 */

$this->title = Yii::t('main', "Courses");
?>

<div class="box box-danger">
    <div class="box-header with-border">
        <h3 class="box-title">Courses list</h3>
    </div>
    <!-- /.box-header -->
    <!-- form start -->

    <div class="box-body">
        <?php Pjax::begin(['id' => 'course-pjax', 'timeout' => 6000]) ?>

        <div class="row">
            <div class="col-md-3">
                <div class="form-group">
                    <?= Html::a('Create course', ['/course/create'], [
                        'data-action' => 'load-edit-modal',
                        'title' => 'Create course',
                        'class' => 'btn btn-success',
                        'data-pjax' => 0
                    ]) ?>
                </div>
            </div>
        </div>

        <?= GridView::widget([
            'dataProvider' => $dataProvider,
            'filterModel' => $search,
            'layout' => "{summary}\n<div class='table-responsive table-fixed'>{items}</div>\n{pager}",
            'id' => 'user-grid',
            'columns' => [
                [
                    'attribute' => 'id',
                    'contentOptions' => ['style' => 'width: 72px; white-space: normal; font-size: 12px;'],
                    'options' => ['style' => 'width: 72px']
                ],
                [
                    'attribute' => 'title',
                    'header' => 'Название',
                    'value' => function ($model) {
                        /** @var array $model */
                        return Html::a($model['title'],
                            ['/course/update', 'id' => $model['id']],
                            [
                                'title' => Yii::t('main', 'Update {0}', ['course']),
                                'data-action' => 'load-edit-modal',
                                'data-pjax' => 0
                            ]
                        );
                    },
                    'format' => 'raw',
                ],
                [
                    'attribute' => 'category_id',
                    'header' => 'Категория',
                    'value' => function ($model) {
                        return $model['category']['title'];
                    },

                    'format' => 'html',
                    'contentOptions' => ['style' => 'width: 200px; white-space: normal; text-align: center;'],
                    'options' => ['style' => 'width: 200px'],
                    'filter' => $categories,
                ],
                [
                    'attribute' => 'status',
                    'header' => 'Статус',
                    'value' => function ($model) {
                        return CourseStatusDecorator::decorate($model['status']);
                    },

                    'format' => 'html',
                    'contentOptions' => ['style' => 'width: 150px; white-space: normal; text-align: center;'],
                    'options' => ['style' => 'width: 150px'],
                    'filter' => CourseStatusDecorator::getStatusLabels(false),
                ],
                [
                    'attribute' => 'type',
                    'header' => 'Тип',
                    'value' => function ($model) {
                        return CourseTypeDecorator::decorate($model['type']);
                    },

                    'format' => 'html',
                    'contentOptions' => ['style' => 'width: 150px; white-space: normal; text-align: center;'],
                    'options' => ['style' => 'width: 150px'],
                    'filter' => CourseTypeDecorator::getTypeLabels(false),
                ],
                [
                    'class' => 'yii\grid\ActionColumn',
                    'header' => 'Actions',
                    'template' => '{generate} {update} {delete}',
                    'buttons' => [
                        'update' => function ($url) {
                            return Html::a('<span class="glyphicon glyphicon-pencil"></span>', $url, [
                                'title' => 'Update Course',
                                'data-pjax' => 0,
                                'data-action' => 'load-edit-modal'
                            ]);
                        }
                    ],
                    'urlCreator' => function ($action, $model) {
                        switch ($action) {
                            case 'update':
                                $url = Url::toRoute(["/course/{$action}", 'id' => $model['id']]);
                                break;
                            default:
                                $url = Url::toRoute(["/course/{$action}", 'id' => $model['id']]);
                                break;
                        }
                        return $url;
                    }
                ],
            ],
        ]); ?>
        <?php Pjax::end() ?>
    </div>
    <!-- /.box-body -->
</div>

<?php $script = <<<JS
    $('#gmaps-input-address').on('change', function() {
        if($(this).val() === '') {
            $('#gmaps-input-lat').val('');
            $('#gmaps-input-lng').lng('');
        } 
    });
JS;

$this->registerJs($script);