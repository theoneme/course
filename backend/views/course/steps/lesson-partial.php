<?php

use common\forms\ar\composite\LessonForm;
use yii\helpers\Html;
use yii\helpers\Json;
use yii\helpers\Url;
use yii\web\View;
use yii\widgets\ActiveForm;

/**
 * @var integer $key
 * @var LessonForm $model
 * @var ActiveForm $form
 * @var boolean $createForm
 * @var View $this
 * @var array $lessonData
 */

$autocompleteId = 'lesson-autocomplete-' . $key;
?>

<?php if ($createForm === true) { ?>
    <?php $form = ActiveForm::begin([
        'id' => 'course-form',
        'enableAjaxValidation' => true,
        'enableClientValidation' => false,
    ]);
    ob_end_clean(); ?>
<?php } ?>

<div class="row dynamic-relation-item" data-role="relation-item">
    <?= Html::a('<i class="fa fa-close"></i>', '#', ['class' => 'dynamic-relation-remove', 'data-action' => 'remove-relation']) ?>
<!--    --><?//= Html::activeHiddenInput($model, "[{$key}]id")?>
    <div class="col-md-12">
        <?= $form->field($model, "[{$key}]lesson_id", ['template' => "{input}{error}"])->dropDownList($lessonData, ['id' => $autocompleteId, 'class' => 'lesson-autocomplete']) ?>
    </div>
</div>

<?php
    $lessonUrl = Url::to(['/ajax/lessons']);
    $script = <<<JS
        $("#$autocompleteId").selectize({
            inputClass: 'form-control selectize-input', 
            valueField: "id",
            labelField: "value",
            searchField: ["value"],
            maxOptions: 10,
            "load": function(query, callback) {
                if (!query.length)
                    return callback();
                $.get("$lessonUrl", {
                        request: query
                    },
                    function(data) {
                        callback(data);
                    }).fail(function() {
                    callback();
                });
            },
        });
JS;
    $this->registerJs($script);

if ($createForm === true) {
    $attributes = Json::htmlEncode($form->attributes);
    $script = <<<JS
        var attributes = $attributes;
        $.each(attributes, function() {
            $("#course-form").yiiActiveForm("add", this);
        });
JS;
    $this->registerJs($script);
}