<?php

use common\forms\ar\CourseForm;
use yii\helpers\Html;
use yii\web\View;

/**
 * @var array $step
 * @var CourseForm $courseForm
 * @var \yii\widgets\ActiveForm $form
 * @var View $this
 * @var array $lessonsData
 */

?>

<div data-role="dynamic-relation-container" data-relation="lessons">
    <?php
    $index = 0;
    foreach ($courseForm->lessons as $key => $lesson) {
        echo $this->render('lesson-partial', [
            'model' => $lesson,
            'form' => $form,
            'key' => $index,
            'createForm' => false,
            'lessonData' => $lessonsData[$lesson['lesson_id']]
        ]);
        $index++;
    } ?>
</div>
<?= Html::a('<i class="fa fa-plus"></i>&nbsp;' . Yii::t('main', 'Add Lesson'), '#', [
    'data-action' => 'add-new-relation',
    'data-relation' => 'lessons'
]) ?>