<?php

use common\helpers\FileInputHelper;
use kartik\file\FileInput;
use yii\helpers\ArrayHelper;

/**
 * @var array $step
 * @var \backend\forms\ar\CourseForm $courseForm
 * @var \yii\widgets\ActiveForm $form
 */

?>

<div class="form-group">
    <div>
        <?= FileInput::widget(
            ArrayHelper::merge(FileInputHelper::getDefaultConfig(), [
                'id' => 'file-upload-input',
                'name' => 'uploaded_images[]',
                'options' => ['multiple' => true],
                'pluginOptions' => [
                    'maxFileCount' => 1,
                    'dropZoneTitle' => Yii::t('model', 'Drag & drop photos here &hellip;'),
                    'overwriteInitial' => false,
                    'initialPreview' => FileInputHelper::buildPreviews($courseForm->attachment),
                    'initialPreviewConfig' => FileInputHelper::buildPreviewsConfig($courseForm->attachment),
                ]
            ])
        ) ?>
    </div>
    <div class="images-container">
        <?php foreach ($courseForm->attachment as $key => $attachment) { ?>
            <?= $form->field($attachment, "[]content", ['template' => '{input}'])->hiddenInput([
                'value' => FileInputHelper::buildOriginImagePath($attachment['content']),
                'data-key' => "image_init_{$key}"
            ])->label(false) ?>
        <?php } ?>
    </div>
</div>

<?php $attachments = count($courseForm->attachment);
$script = <<<JS
    var hasUploadError = false;
    
    $("#file-upload-input").on("fileuploaded", function(event, data, previewId, index) {
        let response = data.response;
        $(".images-container").append("<input name=\'AttachmentForm[][content]\' data-key=\'" + response.imageKey + "\' type=\'hidden\' value=\'" + response.uploadedPath + "\'>");
    }).on("filedeleted", function(event, key) {
        $(".images-container input[data-key=\'" + key + "\']").remove();
    }).on("filebatchuploadcomplete", function() {
        if (hasUploadError === false) {
            $(this).closest("form").submit();
        } else {
            hasUploadError = false;
        }
    }).on("fileuploaderror", function(event, data, msg) {
        hasUploadError = true;
        $('#' + data.id).find('.kv-file-remove').click();
    });
JS;

$this->registerJs($script);

