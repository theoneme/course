<?php

/**
 * @var array $step
 * @var \backend\forms\ar\CourseForm $courseForm
 * @var \yii\widgets\ActiveForm $form
 */

?>


<?php foreach ($courseForm->meta as $locale => $meta) { ?>
    <?= $form->field($meta, "[{$locale}]title")
        ->textInput()
        ->label($meta->getAttributeLabel('title') . "({$locale})")
    ?>
    <?= $form->field($meta, "[{$locale}]description")->textarea([
        'placeholder' => Yii::t('model', 'Course description'),
        'rows' => 3
    ])->label($meta->getAttributeLabel('description') . "({$locale})") ?>
<?php } ?>
