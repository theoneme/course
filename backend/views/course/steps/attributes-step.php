<?php

use yii\helpers\Json;
use yii\widgets\ActiveForm;

$createForm = $createForm ?? true;

/**
 * @var \backend\forms\ar\CourseForm $courseForm
 * @var boolean $createForm
 * @var ActiveForm|null $form
 */

?>

<?php if ($createForm === true) { ?>
    <?php $form = new ActiveForm([
        'id' => 'course-form',
        'enableAjaxValidation' => true,
        'enableClientValidation' => false,
        'fieldConfig' => [
            'template' => "<div class='row'>
                    <div class='col-md-4'>
                        {label}
                    </div>
                    <div class='col-md-8'>
                        {input}
                        {error}
                    </div>
                </div>",
        ],
    ]);
    ob_end_clean(); ?>
<?php } ?>

<?php foreach ($courseForm->dynamicForm->attributes as $key => $attribute) { ?>
        <?= $this->render("../partial/{$courseForm->dynamicForm->config['types'][$key]}", [
            'form' => $form,
            'dynamicForm' => $courseForm->dynamicForm,
            'attribute' => $key
        ]) ?>
<?php } ?>


<?php if ($createForm === true) { ?>
    <?php $attributes = Json::htmlEncode($form->attributes);
    $script = <<<JS
    var attributes = $attributes;
    $.each(attributes, function() {
        $("#course-form").yiiActiveForm("add", this);
    });
JS;
    $this->registerJs($script);
} ?>