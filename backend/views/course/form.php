<?php

use backend\assets\SelectizeAsset;
use common\decorators\CourseStatusDecorator;
use common\decorators\CourseTypeDecorator;
use yii\bootstrap\ActiveForm;
use yii\helpers\Html;
use yii\helpers\Url;

/**
 * @var yii\web\View $this
 * @var \backend\forms\ar\CourseForm $courseForm
 * @var array $categories
 * @var array $action
 * @var integer $parentCategoryId
 * @var array $lessonsData
 */

SelectizeAsset::register($this);
$this->title = Yii::t('main', "Course");
?>

<?php $form = ActiveForm::begin([
    'action' => Url::to($action),

    'enableAjaxValidation' => true,
    'enableClientValidation' => false,
    'fieldConfig' => [
        'template' => "<div class='row'>
                <div class='col-md-3'>
                    {label}
                </div>
                <div class='col-md-9'>
                    {input}
                    {error}
                </div>
            </div>",
    ],
    'options' => [
        'id' => 'course-form',
        'enctype' => 'multipart/form-data',
        'data-role' => 'edit-modal-form',
        'data-pj-container' => 'course-pjax'
    ],
]); ?>

    <div>
        <div>
            <?= $form->field($courseForm->category, 'category_id', ['template' => '{input}{error}'])->radioList($categories, [
                'item' => function ($index, $label, $name, $checked, $value) {
                    $chk = $checked ? 'checked' : '';
                    $active = $checked ? 'active' : '';
                    $output = "<div class='chover'>
                                <input name='{$name}' id='courseform-category-id-{$index}' class='radio-checkbox hidden category-input' value='{$value}' {$chk} type='radio'>
                                <label class='btn btn-default {$active}' for='courseform-category-id-{$index}'>{$label}</label>
                            </div>";
                    return $output;
                },
                'class' => 'flex',
                'style' => 'flex-wrap: wrap;'
            ]) ?>
        </div>
        <?= $form->field($courseForm, 'status')->dropDownList(CourseStatusDecorator::getStatusLabels(false), [
            'prompt' => '-- ' . Yii::t('main', 'Select Status')
        ]) ?>
        <?= $form->field($courseForm, 'type')->dropDownList(CourseTypeDecorator::getTypeLabels(false), [
            'prompt' => '-- ' . Yii::t('main', 'Select Type')
        ]) ?>
        <?= $form->field($courseForm, 'price')->textInput() ?>
    </div>

    <?php foreach ($courseForm->steps as $key => $step) { ?>
        <div class="box box-danger">
            <div class="box-header">
                <h3 class="box-title"><?= $step['title'] ?></h3>
                <span><?= Yii::t('model', 'Step {first} from {total}', ['first' => $key, 'total' => count($courseForm->steps)]) ?></span>
            </div>
            <div class="box-body">
                <?php foreach ($step['config'] as $stepConfig) { ?>
                    <?php if ($stepConfig['type'] === 'view') { ?>
                        <?= $this->render("steps/{$stepConfig['value']}", [
                            'form' => $form,
                            'courseForm' => $courseForm,
                            'step' => $step,
                            'createForm' => false,
                            'lessonsData' => $lessonsData
                        ]) ?>
                    <?php } ?>

                    <?php if ($stepConfig['type'] === 'constructor') { ?>
                        <?php foreach ($stepConfig['groups'] as $group) { ?>
                            <?php $attributes = $courseForm->dynamicForm->getAttributesByGroup($group) ?>
                            <?= $this->render("partial/attributes-group", [
                                'form' => $form,
                                'attributes' => array_flip($attributes),
                                'dynamicForm' => $courseForm->dynamicForm
                            ]) ?>
                        <?php } ?>
                    <?php } ?>
                <?php } ?>
            </div>
        </div>
    <?php } ?>

    <?= $form->field($courseForm, 'unique_lessons', ['template' => "{input}{error}"])->hiddenInput()?>
    <div class="form-group">
        <?= Html::submitButton(Yii::t('labels', 'Save'), ['class' => 'btn btn-block btn-success', 'id' => 'submit-button']) ?>
    </div>

<?php ActiveForm::end(); ?>

<?php
$renderAttributesRoute = Url::toRoute('/course/attributes');
$canonical = Url::current(['id' => null, 'type' => null, 'category_id' => null]);
$id = $courseForm->_course['id'] ?? 'null';
$lessonCount = count($courseForm->lessons);
$lessonUrl = Url::to(['/ajax/render-course-lesson']);
//$attachments = count($courseForm->attachment);
$script = <<<JS
    var relations = {
        'lessons' : {
            'count' : $lessonCount,
            'url' : '$lessonUrl'
        },
    };
    $('#course-form').on('submit', function() {
        let hasToUpload = false;
        $.each($(".file-upload-input"), function() {
            if ($(this).fileinput("getFilesCount") > 0) {
                $(this).fileinput("upload");
                hasToUpload = true;
                return false;
            }
        });
        
        return !hasToUpload;
    });
    
    function loadAttributes(categoryId, entityId) {
        $.get('$renderAttributesRoute', {
            category_id: categoryId,
            entity_id: entityId
        }, function(data) {
            $("#attributes-info").html(data);
        }, "html");
    }
    
    function reloadModal(categoryId) {
        let params =  {
            category_id: categoryId,
            id: {$id}
        };
        let query = $.param(params);
        $('#edit-modal').find('.modal-body').load('{$canonical}' + '?' + query);
    }
    
    $(".category-input").on("change", function(e) {
        reloadModal($(this).val());
        return false;
    });
    
    $('[data-action=add-new-relation]').on('click', function() {
        let relation = $(this).data('relation');
        if (relations[relation]) {
            $.get(relations[relation]['url'], {iterator: relations[relation]['count']}, function(result) {
                if(result.success === true) {
                    $('[data-role=dynamic-relation-container][data-relation=' + relation + ']').append(result.html);
                } 
                relations[relation]['count']++;
            });
        }
        
        return false;
    });

    $(document).off('click', '[data-action=remove-relation]').on('click', '[data-action=remove-relation]', function() {
        $(this).closest('[data-role=relation-item]').remove(); 
        return false;
    });
JS;

$this->registerJs($script);

