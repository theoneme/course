<?php

use common\forms\ar\DynamicForm;

/**
 * @var DynamicForm $dynamicForm
 * @var integer $attribute
 */

?>

<?= $form->field($dynamicForm, $attribute, ['template' => "
    <div class='row'>
        <div class='col-md-3'>
            {label}
        </div>
        <div class='col-md-9'>
            {input}
            Yes
            {error}
        </div>
    </div>
"])->checkbox([], false) ?>
