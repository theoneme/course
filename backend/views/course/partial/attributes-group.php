<?php

use yii\widgets\ActiveForm;

/**
 * @var \common\forms\ar\DynamicForm $dynamicForm
 * @var ActiveForm|null $form
 * @var array $attributes
 */

?>

<?php foreach ($attributes as $key => $attribute) { ?>
        <?= $this->render($dynamicForm->config['types'][$key], [
            'form' => $form,
            'dynamicForm' => $dynamicForm,
            'attribute' => $key
        ]) ?>
<?php } ?>
