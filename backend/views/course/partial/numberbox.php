<?php

use common\forms\ar\DynamicForm;

/**
 * @var DynamicForm $dynamicForm
 * @var integer $attribute
 */

?>

<?= $form->field($dynamicForm, $attribute)->textInput(['type' => 'number', 'step' => '0.01']) ?>
