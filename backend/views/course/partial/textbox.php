<?php

use common\forms\ar\DynamicForm;

/**
 * @var DynamicForm $dynamicForm
 * @var integer $attribute
 * @var \yii\widgets\ActiveForm $form
 */

?>

<?= $form->field($dynamicForm, $attribute)->textInput() ?>
