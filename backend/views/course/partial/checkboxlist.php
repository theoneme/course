<?php

use common\forms\ar\DynamicForm;

/**
 * @var DynamicForm $dynamicForm
 * @var integer $attribute
 */

?>

<?= $form->field($dynamicForm, $attribute)->checkboxList($dynamicForm['config']['values'][$attribute]) ?>
