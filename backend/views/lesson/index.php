<?php

use common\decorators\LessonStatusDecorator;
use common\decorators\LessonTypeDecorator;
use common\models\search\LessonSearch;
use yii\grid\GridView;
use yii\helpers\Html;
use yii\helpers\Url;
use yii\widgets\Pjax;

/**
 * @var LessonSearch $search
 * @var array $categories
 * @var array $courses
 */

//\common\assets\GoogleAsset::register($this);

$this->title = Yii::t('main', "Lessons");
?>

<div class="box box-danger">
    <div class="box-header with-border">
        <h3 class="box-title">Lesson list</h3>
    </div>
    <!-- /.box-header -->
    <!-- form start -->

    <div class="box-body">
        <?php Pjax::begin(['id' => 'lesson-pjax', 'timeout' => 6000]) ?>

        <div class="form-group">
            <?= Html::a('Create lesson', ['/lesson/create'], [
                'data-action' => 'load-edit-modal',
                'title' => 'Create lesson',
                'class' => 'btn btn-success',
                'data-pjax' => 0
            ]) ?>
        </div>

        <?= GridView::widget([
            'dataProvider' => $dataProvider,
            'filterModel' => $search,
            'layout' => "{items}\n{pager}",
            'id' => 'user-grid',
            'columns' => [
                'sort',
                [
                    'attribute' => 'id',
                    'contentOptions' => ['style' => 'width: 72px; white-space: normal; font-size: 12px;'],
                    'options' => ['style' => 'width: 72px']
                ],
                [
                    'attribute' => 'title',
                    'header' => 'Название',
                    'value' => function ($model) {
                        /** @var array $model */
                        return Html::a($model['title'],
                            ['/lesson/update', 'id' => $model['id']],
                            [
                                'title' => Yii::t('main', 'Update {0}', ['lesson']),
                                'data-action' => 'load-edit-modal',
                                'data-pjax' => 0
                            ]
                        );
                    },
                    'format' => 'raw',
                ],
                [
                    'attribute' => 'category_id',
                    'header' => 'Категория',
                    'value' => function ($model) {
                        return $model['category']['title'];
                    },

                    'format' => 'html',
                    'contentOptions' => ['style' => 'width: 200px; white-space: normal; text-align: center;'],
                    'options' => ['style' => 'width: 200px'],
                    'filter' => $categories,
                ],
                [
                    'attribute' => 'status',
                    'header' => 'Статус',
                    'value' => function ($model) {
                        return LessonStatusDecorator::decorate($model['status']);
                    },

                    'format' => 'html',
                    'contentOptions' => ['style' => 'width: 150px; white-space: normal; text-align: center;'],
                    'options' => ['style' => 'width: 150px'],
                    'filter' => LessonStatusDecorator::getStatusLabels(false),
                ],
                [
                    'attribute' => 'type',
                    'header' => 'Тип',
                    'value' => function ($model) {
                        return LessonTypeDecorator::decorate($model['type']);
                    },

                    'format' => 'html',
                    'contentOptions' => ['style' => 'width: 150px; white-space: normal; text-align: center;'],
                    'options' => ['style' => 'width: 150px'],
                    'filter' => LessonTypeDecorator::getTypeLabels(false),
                ],
                [
                    'class' => 'yii\grid\ActionColumn',
                    'header' => 'Actions',
                    'template' => '{update} {delete}',
                    'buttons' => [
                        'update' => function ($url, $model) {
                            return Html::a('<span class="glyphicon glyphicon-pencil"></span>', $url, [
                                'title' => 'Update Lesson',
                                'data-pjax' => 0,
                                'data-action' => 'load-edit-modal'
                            ]);
                        }
                    ],
                    'urlCreator' => function ($action, $model, $key, $index) {
                        switch ($action) {
                            case 'update':
                                $url = Url::toRoute(["/lesson/{$action}", 'id' => $model['id']]);
                                break;
                            default:
                                $url = Url::toRoute(["/lesson/{$action}", 'id' => $model['id']]);
                                break;
                        }
                        return $url;
                    }
                ],
            ],
        ]); ?>
        <?php Pjax::end() ?>
    </div>
    <!-- /.box-body -->
</div>
