<?php

use common\decorators\LessonStatusDecorator;
use common\decorators\LessonTypeDecorator;
use common\forms\ar\LessonForm;
use common\helpers\FileInputHelper;
use kartik\file\FileInput;
use yii\bootstrap\ActiveForm;
use yii\helpers\ArrayHelper;
use yii\helpers\Html;
use yii\helpers\Url;

/**
 * @var yii\web\View $this
 * @var LessonForm $lessonForm
 * @var array $action
 * @var array $categories
 * @var array $subcategories
 * @var integer $parentCategoryId
 */

$this->title = Yii::t('main', "Lessons");
?>

<?php $form = ActiveForm::begin([
    'action' => Url::to($action),

    'enableAjaxValidation' => true,
    'enableClientValidation' => false,
    'fieldConfig' => [
        'template' => "<div class='row'>
                <div class='col-md-3'>
                    {label}
                </div>
                <div class='col-md-9'>
                    {input}
                    {error}
                </div>
            </div>",
    ],
    'options' => [
        'id' => 'lesson-form',
        'enctype' => 'multipart/form-data',
        'data-role' => 'edit-modal-form',
        'data-pj-container' => 'lesson-pjax'
    ],
]); ?>

    <?= $form->field($lessonForm, 'type')->dropDownList(LessonTypeDecorator::getTypeLabels(false), [
        'prompt' => '-- ' . Yii::t('main', 'Select Type')
    ]) ?>
    <?= $form->field($lessonForm, 'status')->dropDownList(LessonStatusDecorator::getStatusLabels(false), [
        'prompt' => '-- ' . Yii::t('main', 'Select Status')
    ]) ?>
    <?= $form->field($lessonForm, 'sort')->textInput() ?>
    <?= $form->field($lessonForm, "xp")->textInput()?>
    <div>
        <div class='row'>
            <div class='col-md-3'>
                <?= Html::label('Категория')?>
            </div>
            <div class='col-md-9'>
                <div class="form-group">
                    <div class="flex">
                        <?php foreach ($categories as $id => $category) {
                            $chk = $id === $parentCategoryId ? 'checked' : '';
                            $active = $id === $parentCategoryId ? 'active' : '';
                            echo "<div class='chover'>
                            <input name='category_id' id='parent-category-id-{$id}' class='radio-checkbox hidden category-input' value='{$id}' {$chk} type='radio'>
                            <label class='btn btn-default {$active}' for='parent-category-id-{$id}'>{$category}</label>
                        </div>";
                        }?>
                    </div>
                </div>
            </div>
        </div>
        <div class='row'>
            <div class='col-md-3'>
                Подкатегория
            </div>
            <div class='col-md-9'>
                <?= $form->field($lessonForm->category, 'category_id', ['template' => '{input}{error}'])->radioList($subcategories, [
                    'item' => function ($index, $label, $name, $checked, $value) {
                        $chk = $checked ? 'checked' : '';
                        $active = $checked ? 'active' : '';
                        $output = "<div class='chover'>
                                    <input name='{$name}' id='lessonform-category-id-{$index}' class='radio-checkbox hidden subcategory-input' value='{$value}' {$chk} type='radio'>
                                    <label class='btn btn-default {$active}' for='lessonform-category-id-{$index}'>{$label}</label>
                                </div>";
                        return $output;
                    },
                    'class' => 'flex',
                    'style' => 'flex-wrap: wrap;'
                ]) ?>
            </div>
        </div>
    </div>
    <div class="panel panel-default" data-role="translations-panel">
        <div class="panel-heading">Переводы</div>
        <div class="panel-body">
            <div class="alert alert-danger">Надо заполнить хотя бы 1 язык</div>
            <?php foreach ($lessonForm->meta as $locale => $meta) { ?>
                <?= $form->field($meta, "[{$locale}]title")
                    ->textInput(['data-role' => 'lesson-translation-input'])
                    ->label($meta->getAttributeLabel('title') . "({$locale})")
                ?>
                <?= $form->field($meta, "[{$locale}]description")
                    ->textarea(['placeholder' => Yii::t('model', 'Lesson description'), 'rows' => 3])
                    ->label($meta->getAttributeLabel('description') . "({$locale})")
                ?>
            <?php } ?>
        </div>
    </div>
    <div class="form-group">
        <div>
            <?= FileInput::widget(
                ArrayHelper::merge(FileInputHelper::getDefaultConfig(), [
                    'id' => 'file-upload-input',
                    'name' => 'uploaded_files[]',
                    'options' => ['accept' => 'video/*|.pdf', 'multiple' => true],
                    'pluginOptions' => [
                        'uploadUrl' => Url::to(['/file/upload']),
                        'allowedFileExtensions' => new \yii\helpers\ReplaceArrayValue(['mp4', 'ogg', 'webm', 'pdf']),
                        'maxFileCount' => 1,
                        'dropZoneTitle' => Yii::t('model', 'Drag & drop lesson here &hellip;'),
                        'overwriteInitial' => false,
                        'initialPreview' => FileInputHelper::buildPreviews($lessonForm->attachment, FileInputHelper::getTypeByLessonType($lessonForm->type)),
//                        'initialPreview' => array_map(function($var){return $var['content'];}, $lessonForm->attachment),
                        'initialPreviewConfig' => FileInputHelper::buildPreviewsConfig($lessonForm->attachment, FileInputHelper::getTypeByLessonType($lessonForm->type)),
                    ]
                ])
            ) ?>
        </div>
        <div class="images-container">
            <?php foreach ($lessonForm->attachment as $key => $attachment) { ?>
                <?= $form->field($attachment, "[]content", ['template' => '{input}'])->hiddenInput([
                    'value' => FileInputHelper::buildOriginImagePath($attachment['content']),
                    'data-key' => "image_init_{$key}"
                ])->label(false) ?>
            <?php } ?>
        </div>
    </div>

    <div class="form-group">
        <?= Html::submitButton(Yii::t('labels', 'Save'), ['class' => 'btn btn-block btn-success', 'id' => 'submit-button']) ?>
    </div>

<?php ActiveForm::end(); ?>

<?php
$canonical = Url::current(['id' => null, 'category_id' => null]);
$id = $lessonForm->_lesson['id'] ?? 'null';
$attachments = count($lessonForm->attachment);

$script = <<<JS
    $('#lesson-form').on('submit', function() {
        let hasToUpload = false;
        $.each($(".file-upload-input"), function() {
            if ($(this).fileinput("getFilesCount") > 0) {
                $(this).fileinput("upload");
                hasToUpload = true;
                return false;
            }
        });
        
        return !hasToUpload;
    });
    
    function reloadModal(categoryId) {
        let params =  {
            category_id: categoryId,
            id: {$id}
        };
        $('#edit-modal').find('.modal-body').load('{$canonical}' + '?' + $.param(params));
    }
    
    $(".category-input, .subcategory-input").on("change", function(e) {
        reloadModal($(this).val());
        return false;
    });
    
    
    var hasUploadError = false;
    
    $("#file-upload-input").on("fileuploaded", function(event, data, previewId, index) {
        let response = data.response;
        $(".images-container").append("<input name=\'AttachmentForm[][content]\' data-key=\'" + response.imageKey + "\' type=\'hidden\' value=\'" + response.uploadedPath + "\'>");
    }).on("filedeleted", function(event, key) {
        $(".images-container input[data-key=\'" + key + "\']").remove();
    }).on("filebatchuploadcomplete", function() {
        if (hasUploadError === false) {
            $(this).closest("form").submit();
        } else {
            hasUploadError = false;
        }
    }).on("fileuploaderror", function(event, data, msg) {
        hasUploadError = true;
        $('#' + data.id).find('.kv-file-remove').click();
    });
JS;

$this->registerJs($script);