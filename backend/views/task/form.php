<?php

use backend\assets\SelectizeAsset;
use common\decorators\TaskTypeDecorator;
use common\forms\ar\TaskForm;
use common\helpers\FileInputHelper;
use kartik\file\FileInput;
use yii\bootstrap\ActiveForm;
use yii\helpers\ArrayHelper;
use yii\helpers\Html;
use yii\helpers\Url;

/**
 * @var yii\web\View $this
 * @var TaskForm $taskForm
 * @var array $action
 * @var array $selectizeData
 */

SelectizeAsset::register($this);

$this->title = Yii::t('main', "Task");
?>

<?php $form = ActiveForm::begin([
    'action' => Url::to($action),

    'enableAjaxValidation' => true,
    'enableClientValidation' => false,
    'fieldConfig' => [
        'template' => "<div class='row'>
                <div class='col-md-3'>
                    {label}
                </div>
                <div class='col-md-9'>
                    {input}
                    {error}
                </div>
            </div>",
    ],
    'options' => [
        'id' => 'task-form',
        'enctype' => 'multipart/form-data',
        'data-role' => 'edit-modal-form',
        'data-pj-container' => 'task-pjax'
    ],
]); ?>

    <?= $form->field($taskForm->lesson, 'lesson_id')->dropDownList($selectizeData, ['class' => 'lesson-autocomplete']) ?>
    <?= $form->field($taskForm, 'type')->dropDownList(TaskTypeDecorator::getTypeLabels(false), [
        'prompt' => '-- ' . Yii::t('main', 'Select Type')
    ]) ?>
    <div class="row">
        <div class="col-md-6">
            <?= $form->field($taskForm, "points")->textInput()?>
        </div>
        <div class="col-md-6">
            <?= $form->field($taskForm, "minutes")->textInput()?>
        </div>
    </div>
    <div class="panel panel-default" data-role="translations-panel">
        <div class="panel-heading">Переводы</div>
        <div class="panel-body">
            <div class="alert alert-danger">Надо заполнить хотя бы 1 язык</div>
            <?php foreach ($taskForm->meta as $locale => $meta) { ?>
                <?= $form->field($meta, "[{$locale}]title")
                    ->textInput(['data-role' => 'task-translation-input'])
                    ->label($meta->getAttributeLabel('title') . "({$locale})")
                ?>
                <?= $form->field($meta, "[{$locale}]description")
                    ->textarea(['placeholder' => Yii::t('model', 'Task text'), 'rows' => 3])
                    ->label(Yii::t('model', 'Task text') . "({$locale})")
                ?>
                <?= $form->field($meta, "[{$locale}]solution")
                    ->textarea(['placeholder' => Yii::t('model', 'Task solution'), 'rows' => 3])
                    ->label(Yii::t('model', 'Task solution') . "({$locale})")
                ?>
                <hr>
            <?php } ?>
        </div>
    </div>
    <div class="panel panel-default" data-role="translations-panel">
        <div class="panel-heading">Варианты ответов</div>
        <div class="panel-body">
            <div class="alert alert-danger">Должен быть хотя бы 1 правильный ответ</div>
            <?= $form->field($taskForm, 'correct_answers', ['template' => "{input}{error}"])->hiddenInput()?>
            <?php foreach ($taskForm->answers as $key => $answer) { ?>
                <?= Html::activeHiddenInput($answer, "[{$key}]id")?>
                <div class="row">
                    <div class="col-md-9">
                        <?php foreach ($answer->meta as $locale => $meta) {
                            echo $form->field($answer, "[{$key}][TaskAnswerMetaForm][{$locale}]title")
                                ->textInput(['value' => $meta['title'] ?? ''])
                                ->label(Yii::t('model', 'Answer') . "({$locale})");
                        } ?>
                    </div>
                    <div class="col-md-3 checkbox-container">
                        <?= $form->field($answer, "[{$key}]correct", ['template' => "{input}{label}{error}"])->checkbox([], false)?>
                    </div>
                </div>
                <hr>
            <?php } ?>
        </div>
    </div>
    <div class="form-group file-input-container" data-model="AttachmentForm">
        <?= Html::label('Картинка задачи', 'file-upload-input', ['control-label'])?>
        <div>
            <?= FileInput::widget(
                ArrayHelper::merge(FileInputHelper::getDefaultConfig(), [
                    'id' => 'file-upload-input',
                    'name' => 'uploaded_images[]',
                    'options' => ['multiple' => true],
                    'pluginOptions' => [
                        'maxFileCount' => 1,
                        'dropZoneTitle' => Yii::t('model', 'Drag & drop photo here &hellip;'),
                        'overwriteInitial' => false,
                        'initialPreview' => FileInputHelper::buildPreviews($taskForm->attachment),
                        'initialPreviewConfig' => FileInputHelper::buildPreviewsConfig($taskForm->attachment),
                    ]
                ])
            ) ?>
        </div>
        <div class="images-container">
            <?php foreach ($taskForm->attachment as $key => $attachment) { ?>
                <?= $form->field($attachment, "[]content", ['template' => '{input}'])->hiddenInput([
                    'value' => FileInputHelper::buildOriginImagePath($attachment['content']),
                    'data-key' => "image_init_{$key}"
                ])->label(false) ?>
            <?php } ?>
        </div>
    </div>


    <div class="form-group file-input-container" data-model="TaskAnswerAttachmentForm">
        <?= Html::label('Картинка решения', 'answer-file-upload-input', ['control-label'])?>
        <div>
            <?= FileInput::widget(
                ArrayHelper::merge(FileInputHelper::getDefaultConfig(), [
                    'id' => 'answer-file-upload-input',
                    'name' => 'uploaded_images[]',
                    'options' => ['multiple' => true],
                    'pluginOptions' => [
                        'maxFileCount' => 1,
                        'dropZoneTitle' => Yii::t('model', 'Drag & drop photo here &hellip;'),
                        'overwriteInitial' => false,
                        'initialPreview' => FileInputHelper::buildPreviews($taskForm->answerAttachment),
                        'initialPreviewConfig' => FileInputHelper::buildPreviewsConfig($taskForm->answerAttachment),
                    ]
                ])
            ) ?>
        </div>
        <div class="images-container">
            <?php foreach ($taskForm->answerAttachment as $key => $attachment) { ?>
                <?= $form->field($attachment, "[]content", ['template' => '{input}'])->hiddenInput([
                    'value' => FileInputHelper::buildOriginImagePath($attachment['content']),
                    'data-key' => "image_init_{$key}"
                ])->label(false) ?>
            <?php } ?>
        </div>
    </div>

    <div class="form-group">
        <?= Html::submitButton(Yii::t('labels', 'Save'), ['class' => 'btn btn-block btn-success', 'id' => 'submit-button']) ?>
    </div>

<?php ActiveForm::end(); ?>

<?php
$attachments = count($taskForm->attachment);
$lessonUrl = Url::to(['/ajax/lessons']);

$script = <<<JS
    $('#task-form').on('submit', function() {
        let hasToUpload = false;
        $.each($(".file-upload-input"), function() {
            if ($(this).fileinput("getFilesCount") > 0) {
                $(this).fileinput("upload");
                hasToUpload = true;
                return false;
            }
        });
        
        return !hasToUpload;
    });
    
    
    var hasUploadError = false;
    
    $(".file-upload-input").on("fileuploaded", function(event, data, previewId, index) {
        let imagesContainer = $(this).closest('.file-input-container').find('.images-container');
        let model = $(this).closest('.file-input-container').data('model');
        let response = data.response;
        imagesContainer.append("<input name=\'" + model + "[][content]\' data-key=\'" + response.imageKey + "\' type=\'hidden\' value=\'" + response.uploadedPath + "\'>");
    }).on("filedeleted", function(event, key) {
        $(this).closest('.file-input-container').children('.images-container').find("input[data-key=\'" + key + "\']").remove();
    }).on("filebatchuploadcomplete", function() {
        if (hasUploadError === false) {
            $(this).closest("form").submit();
        } else {
            hasUploadError = false;
        }
    }).on("fileuploaderror", function(event, data, msg) {
        hasUploadError = true;
        $('#' + data.id).find('.kv-file-remove').click();
    });
    
    $(".lesson-autocomplete").selectize({
        inputClass: 'form-control selectize-input', 
        valueField: "id",
        labelField: "value",
        searchField: ["value"],
        maxOptions: 10,
        "load": function(query, callback) {
            if (!query.length)
                return callback();
            $.get("$lessonUrl", {
                    request: query
                },
                function(data) {
                    callback(data);
                }).fail(function() {
                callback();
            });
        },
    });
JS;

$this->registerJs($script);