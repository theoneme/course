<?php

use common\decorators\TaskTypeDecorator;
use common\models\search\TaskSearch;
use yii\grid\GridView;
use yii\helpers\Html;
use yii\helpers\Url;
use yii\widgets\Pjax;

/**
 * @var TaskSearch $search
 * @var array $categories
 * @var array $courses
 */

$this->title = Yii::t('main', "Tasks");

?>

<div class="box box-danger">
    <div class="box-header with-border">
        <h3 class="box-title">Task list</h3>
    </div>
    <!-- /.box-header -->
    <!-- form start -->

    <div class="box-body">
        <?php Pjax::begin(['id' => 'task-pjax', 'timeout' => 6000]) ?>

        <div class="form-group">
            <?= Html::a('Create task', ['/task/create'], [
                'data-action' => 'load-edit-modal',
                'title' => 'Create task',
                'class' => 'btn btn-success',
                'data-pjax' => 0
            ]) ?>
        </div>

        <?= GridView::widget([
            'dataProvider' => $dataProvider,
            'filterModel' => $search,
            'layout' => "{items}\n{pager}",
            'id' => 'user-grid',
            'columns' => [
                [
                    'attribute' => 'title',
                    'header' => 'Название',
                    'value' => function ($model) {
                        /** @var array $model */
                        return Html::a($model['title'],
                            ['/task/update', 'id' => $model['id']],
                            [
                                'title' => Yii::t('main', 'Update {0}', ['task']),
                                'data-action' => 'load-edit-modal',
                                'data-pjax' => 0
                            ]
                        );
                    },
                    'format' => 'raw',
                ],
                [
                    'attribute' => 'lessonTitle',
                    'header' => 'Урок',
                    'value' => function ($model) {
                        return $model['lesson']['title'] ?? '';
                    },
                    'format' => 'html',
                    'contentOptions' => ['style' => 'width: 200px; white-space: normal; text-align: center;'],
                    'options' => ['style' => 'width: 200px'],
                ],
                [
                    'attribute' => 'type',
                    'header' => 'Тип',
                    'value' => function ($model) {
                        return TaskTypeDecorator::decorate($model['type']);
                    },
                    'format' => 'html',
                    'contentOptions' => ['style' => 'width: 150px; white-space: normal; text-align: center;'],
                    'options' => ['style' => 'width: 150px'],
                    'filter' => TaskTypeDecorator::getTypeLabels(false),
                ],
                [
                    'class' => 'yii\grid\ActionColumn',
                    'header' => 'Actions',
                    'template' => '{update} {delete}',
                    'buttons' => [
                        'update' => function ($url, $model) {
                            return Html::a('<span class="glyphicon glyphicon-pencil"></span>', $url, [
                                'title' => 'Update Task',
                                'data-pjax' => 0,
                                'data-action' => 'load-edit-modal'
                            ]);
                        }
                    ],
                    'urlCreator' => function ($action, $model, $key, $index) {
                        switch ($action) {
                            case 'update':
                                $url = Url::toRoute(["/task/{$action}", 'id' => $model['id']]);
                                break;
                            default:
                                $url = Url::toRoute(["/task/{$action}", 'id' => $model['id']]);
                                break;
                        }
                        return $url;
                    }
                ],
            ],
        ]); ?>
        <?php Pjax::end() ?>
    </div>
    <!-- /.box-body -->
</div>
