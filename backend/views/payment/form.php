<?php

use backend\assets\SelectizeAsset;
use common\decorators\PaymentTypeDecorator;
use common\forms\ar\PaymentForm;
use yii\bootstrap\ActiveForm;
use yii\helpers\Html;
use yii\helpers\Url;

/**
 * @var yii\web\View $this
 * @var PaymentForm $paymentForm
 * @var array $action
 * @var array $selectizeData
 * @var array $paymentMethods
 */

SelectizeAsset::register($this);

$this->title = Yii::t('main', "Payments");
?>

<?php $form = ActiveForm::begin([
    'action' => Url::to($action),

    'enableAjaxValidation' => true,
    'enableClientValidation' => false,
    'fieldConfig' => [
        'template' => "<div class='row'>
                <div class='col-md-3'>
                    {label}
                </div>
                <div class='col-md-9'>
                    {input}
                    {error}
                </div>
            </div>",
    ],
    'options' => [
        'id' => 'payment-form',
        'enctype' => 'multipart/form-data',
        'data-role' => 'edit-modal-form',
        'data-pj-container' => 'payment-pjax'
    ],
]); ?>

    <?= $form->field($paymentForm, 'user_id')->dropDownList([], ['class' => 'lesson-autocomplete']) ?>
    <?= $form->field($paymentForm, 'payment_method_id')->dropDownList($paymentMethods, ['class' => 'lesson-autocomplete']) ?>
    <?= $form->field($paymentForm, 'type')->dropDownList(PaymentTypeDecorator::getTypeLabels(false), [
        'prompt' => '-- ' . Yii::t('main', 'Select Type')
    ]) ?>
    <div class="row">
        <div class="col-md-6">
            <?= $form->field($paymentForm, "points")->textInput()?>
        </div>
        <div class="col-md-6">
            <?= $form->field($paymentForm, "minutes")->textInput()?>
        </div>
    </div>

        <div class="form-group">
        <?= Html::submitButton(Yii::t('labels', 'Save'), ['class' => 'btn btn-block btn-success', 'id' => 'submit-button']) ?>
    </div>

<?php ActiveForm::end(); ?>

<?php
$attachments = count($paymentForm->attachment);
$lessonUrl = Url::to(['/ajax/lessons']);

$script = <<<JS
    $('#payment-form').on('submit', function() {
        let hasToUpload = false;
        $.each($(".file-upload-input"), function() {
            if ($(this).fileinput("getFilesCount") > 0) {
                $(this).fileinput("upload");
                hasToUpload = true;
                return false;
            }
        });
        
        return !hasToUpload;
    });
    
    
    var hasUploadError = false;
    
    $(".file-upload-input").on("fileuploaded", function(event, data, previewId, index) {
        let imagesContainer = $(this).closest('.file-input-container').find('.images-container');
        let model = $(this).closest('.file-input-container').data('model');
        let response = data.response;
        imagesContainer.append("<input name=\'" + model + "[][content]\' data-key=\'" + response.imageKey + "\' type=\'hidden\' value=\'" + response.uploadedPath + "\'>");
    }).on("filedeleted", function(event, key) {
        $(this).closest('.file-input-container').children('.images-container').find("input[data-key=\'" + key + "\']").remove();
    }).on("filebatchuploadcomplete", function() {
        if (hasUploadError === false) {
            $(this).closest("form").submit();
        } else {
            hasUploadError = false;
        }
    }).on("fileuploaderror", function(event, data, msg) {
        hasUploadError = true;
        $('#' + data.id).find('.kv-file-remove').click();
    });
    
    $(".lesson-autocomplete").selectize({
        inputClass: 'form-control selectize-input', 
        valueField: "id",
        labelField: "value",
        searchField: ["value"],
        maxOptions: 10,
        "load": function(query, callback) {
            if (!query.length)
                return callback();
            $.get("$lessonUrl", {
                    request: query
                },
                function(data) {
                    callback(data);
                }).fail(function() {
                callback();
            });
        },
    });
JS;

$this->registerJs($script);