<?php

use common\components\CurrencyHelper;
use common\decorators\PaymentStatusDecorator;
use common\decorators\PaymentTypeDecorator;
use common\models\search\PaymentSearch;
use yii\grid\GridView;
use yii\helpers\Html;
use yii\helpers\Url;
use yii\widgets\Pjax;

/**
 * @var PaymentSearch $search
 * @var array $paymentMethods
 */

$this->title = Yii::t('main', "Payments");
?>

<div class="box box-danger">
    <div class="box-header with-border">
        <h3 class="box-title">Payment list</h3>
    </div>
    <!-- /.box-header -->
    <!-- form start -->

    <div class="box-body">
        <?php Pjax::begin(['id' => 'payment-pjax', 'timeout' => 6000]) ?>

<!--        <div class="form-group">-->
<!--            --><?//= Html::a('Create payment', ['/payment/create'], [
//                'data-action' => 'load-edit-modal',
//                'title' => 'Create payment',
//                'class' => 'btn btn-success',
//                'data-pjax' => 0
//            ]) ?>
<!--        </div>-->

        <?= GridView::widget([
            'dataProvider' => $dataProvider,
            'filterModel' => $search,
            'layout' => "{items}\n{pager}",
            'id' => 'user-grid',
            'columns' => [
                'id',
                'user_id',
                'code',
                [
                    'attribute' => 'type',
                    'header' => 'Тип',
                    'value' => function ($model) {
                        return PaymentTypeDecorator::decorate($model['type']);
                    },
                    'format' => 'html',
                    'contentOptions' => ['style' => 'width: 150px; white-space: normal; text-align: center;'],
                    'options' => ['style' => 'width: 150px'],
                    'filter' => PaymentTypeDecorator::getTypeLabels(false),
                ],
                [
                    'attribute' => 'status',
                    'header' => 'Статус',
                    'value' => function ($model) {
                        return PaymentStatusDecorator::decorate($model['status']);
                    },
                    'format' => 'html',
                    'contentOptions' => ['style' => 'width: 150px; white-space: normal; text-align: center;'],
                    'options' => ['style' => 'width: 150px'],
                    'filter' => PaymentStatusDecorator::getStatusLabels(false),
                ],
                [
                    'header' => 'Сумма',
                    'attribute' => 'total',
                    'value' => function ($model) {
                        return CurrencyHelper::convertAndFormat($model['currency_code'], Yii::$app->params['app_currency_code'], $model['total']);
                    },
                    'contentOptions' => ['style' => 'width: 72px; white-space: normal; font-size: 12px;'],
                    'options' => ['style' => 'width: 72px']
                ],
                [
                    'header' => 'Способ оплаты',
                    'attribute' => 'payment_method_id',
                    'value' => function ($model) {
                        return $model['paymentMethod']['title'];
                    },
                    'contentOptions' => ['style' => 'width: 300px'],
                    'options' => ['style' => 'width: 300px'],
                    'filter' => $paymentMethods,
                ],
                'created_at:datetime',
                [
                    'class' => 'yii\grid\ActionColumn',
                    'header' => 'Actions',
                    'template' => '{result}',
                    'buttons' => [
                        'result' => function ($url, $model) {
                            return Html::a('<span class="glyphicon glyphicon-eye-open"></span>', $url, [
                                'title' => 'View Result',
                                'data-pjax' => 0,
                                'data-action' => 'load-edit-modal'
                            ]);
                        }
                    ],
                    'urlCreator' => function ($action, $model, $key, $index) {
                        switch ($action) {
                            default:
                                $url = Url::toRoute(["/payment/{$action}", 'id' => $model['id']]);
                                break;
                        }
                        return $url;
                    },
                ],
            ],
        ]); ?>
        <?php Pjax::end() ?>
    </div>
    <!-- /.box-body -->
</div>
