<?php

use backend\assets\SelectizeAsset;
use common\forms\ar\PaymentMethodForm;
use common\helpers\FileInputHelper;
use kartik\file\FileInput;
use yii\bootstrap\ActiveForm;
use yii\helpers\ArrayHelper;
use yii\helpers\Html;
use yii\helpers\Url;

/**
 * @var yii\web\View $this
 * @var PaymentMethodForm $paymentMethodForm
 * @var array $action
 * @var array $selectizeData
 */

SelectizeAsset::register($this);

$this->title = Yii::t('main', "Payment Methods");
?>

<?php $form = ActiveForm::begin([
    'action' => Url::to($action),

    'enableAjaxValidation' => true,
    'enableClientValidation' => false,
    'fieldConfig' => [
        'template' => "<div class='row'>
                <div class='col-md-3'>
                    {label}
                </div>
                <div class='col-md-9'>
                    {input}
                    {error}
                </div>
            </div>",
    ],
    'options' => [
        'id' => 'payment-method-form',
        'enctype' => 'multipart/form-data',
        'data-role' => 'edit-modal-form',
        'data-pj-container' => 'payment-method-pjax'
    ],
]); ?>

    <?= $form->field($paymentMethodForm, 'alias')->textInput() ?>
    <div class="row">
        <div class="col-md-6">
            <?= $form->field($paymentMethodForm, 'sort')->textInput() ?>
        </div>
        <div class="col-md-6">
            <?= $form->field($paymentMethodForm, 'enabled')->checkbox() ?>
        </div>
    </div>
    <div class="panel panel-default" data-role="translations-panel">
        <div class="panel-heading">Переводы</div>
        <div class="panel-body">
            <div class="alert alert-danger">Надо заполнить хотя бы 1 язык</div>
            <?php foreach ($paymentMethodForm->meta as $locale => $meta) { ?>
                <?= $form->field($meta, "[{$locale}]title")
                    ->textInput()
                    ->label($meta->getAttributeLabel('title') . "({$locale})")
                ?>
            <?php } ?>
        </div>
    </div>

    <div class="form-group file-input-container">
        <?= Html::label('Лого', 'file-upload-input', ['control-label'])?>
        <div>
            <?= FileInput::widget(
                ArrayHelper::merge(FileInputHelper::getDefaultConfig(), [
                    'id' => 'file-upload-input',
                    'name' => 'uploaded_images',
                    'options' => ['multiple' => false],
                    'pluginOptions' => [
                        'dropZoneTitle' => Yii::t('model', 'Drag & drop photos here &hellip;'),
                        'overwriteInitial' => true,
                        'initialPreview' => !empty($paymentMethodForm->_paymentMethod->logo) ? $paymentMethodForm->logo : [],
                        'initialPreviewConfig' => !empty($paymentMethodForm->_paymentMethod->logo) ? [[
                            'caption' => basename($paymentMethodForm->logo),
                            'url' => Url::toRoute('/image/delete'),
                            'key' => 0
                        ]] : [],
                    ]
                ])
            ) ?>
        </div>
        <div class="images-container">
            <?= $form->field($paymentMethodForm, 'logo', ['template' => '{input}'])->hiddenInput([
                'value' => FileInputHelper::buildOriginImagePath($paymentMethodForm->logo),
                'data-key' => 'image_init_0'
            ])->label(false) ?>
        </div>
    </div>

    <div class="form-group">
        <?= Html::submitButton(Yii::t('labels', 'Save'), ['class' => 'btn btn-block btn-success', 'id' => 'submit-button']) ?>
    </div>

<?php ActiveForm::end(); ?>

<?php
$script = <<<JS
    $('#payment-method-form').on('submit', function() {
        let hasToUpload = false;
        $.each($(".file-upload-input"), function() {
            if ($(this).fileinput("getFilesCount") > 0) {
                $(this).fileinput("upload");
                hasToUpload = true;
                return false;
            }
        });
        
        return !hasToUpload;
    });
    
    
    var hasUploadError = false;
    
    $(".file-upload-input").on("fileuploaded", function(event, data, previewId, index) {
        let imagesContainer = $(this).closest('.file-input-container').find('.images-container');
        let response = data.response;
        imagesContainer.find('input').val(response.uploadedPath).data('key', response.imageKey);
    }).on("filedeleted", function(event, key) {
        $(this).closest('.file-input-container').find('.images-container input').val(null);
    }).on("filebatchuploadcomplete", function() {
        if (hasUploadError === false) {
            $(this).closest("form").submit();
        } else {
            hasUploadError = false;
        }
    }).on("fileuploaderror", function(event, data, msg) {
        hasUploadError = true;
        $('#' + data.id).find('.kv-file-remove').click();
    });
JS;

$this->registerJs($script);