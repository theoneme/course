<?php

use common\models\search\PaymentMethodSearch;
use yii\grid\GridView;
use yii\helpers\Html;
use yii\helpers\Url;
use yii\widgets\Pjax;

/**
 * @var PaymentMethodSearch $search
 * @var array $categories
 * @var array $courses
 */

$this->title = Yii::t('main', "Payment Methods");
?>

<div class="box box-danger">
    <div class="box-header with-border">
        <h3 class="box-title">Payment Method list</h3>
    </div>
    <!-- /.box-header -->
    <!-- form start -->

    <div class="box-body">
        <?php Pjax::begin(['id' => 'payment-method-pjax', 'timeout' => 6000]) ?>

        <div class="form-group">
            <?= Html::a('Create payment method', ['/payment-method/create'], [
                'data-action' => 'load-edit-modal',
                'title' => 'Create payment method',
                'class' => 'btn btn-success',
                'data-pjax' => 0
            ]) ?>
        </div>

        <?= GridView::widget([
            'dataProvider' => $dataProvider,
            'filterModel' => $search,
            'layout' => "{items}\n{pager}",
            'id' => 'user-grid',
            'columns' => [
                [
                    'header' => 'Название',
                    'value' => function ($model) {
                        /** @var array $model */
                        return Html::a($model['title'],
                            ['/payment-method/update', 'id' => $model['id']],
                            [
                                'title' => Yii::t('main', 'Update {0}', ['Payment Method']),
                                'data-action' => 'load-edit-modal',
                                'data-pjax' => 0
                            ]
                        );
                    },
                    'format' => 'raw',
                ],
                'alias',
                'sort',
                [
                    'attribute' => 'enabled',
                    'value' => function ($model) {
                        return $model['enabled'] ? '<div class="btn btn-success btn-xs">Да</div>' : '<div class="btn btn-danger btn-xs">Нет</div>';
                    },
                    'format' => 'html',
                    'filter' => [0 => 'Нет', 1 => 'Да']
                ],
                [
                    'class' => 'yii\grid\ActionColumn',
                    'header' => 'Actions',
                    'template' => '{update} {delete}',
                    'buttons' => [
                        'update' => function ($url, $model) {
                            return Html::a('<span class="glyphicon glyphicon-pencil"></span>', $url, [
                                'title' => 'Update Payment Method',
                                'data-pjax' => 0,
                                'data-action' => 'load-edit-modal'
                            ]);
                        }
                    ],
                    'urlCreator' => function ($action, $model, $key, $index) {
                        switch ($action) {
                            case 'update':
                                $url = Url::toRoute(["/payment-method/{$action}", 'id' => $model['id']]);
                                break;
                            default:
                                $url = Url::toRoute(["/payment-method/{$action}", 'id' => $model['id']]);
                                break;
                        }
                        return $url;
                    }
                ],
            ],
        ]); ?>
        <?php Pjax::end() ?>
    </div>
    <!-- /.box-body -->
</div>
