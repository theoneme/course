<?php

use backend\controllers\BackEndController;
use common\helpers\UtilityHelper;
use yii\helpers\Html;
use yii\helpers\Url;

/* @var BackEndController $controller */
$controller = Yii::$app->controller;

$request = Yii::$app->request;

?>

<aside class="main-sidebar">
    <section class="sidebar">
        <!-- Sidebar user panel -->
        <div class="user-panel">
            <div class="pull-left image">
                <?php echo Html::img(UtilityHelper::user()->avatar, ['class' => 'img-circle']) ?>
            </div>
            <div class="pull-left info">
                <?php if (!Yii::$app->user->isGuest) { ?>
                    <p><?= UtilityHelper::user()->username ?></p>
                    <a href="#"><i class="circle text-success"></i> Online</a>
                <?php } ?>
            </div>
        </div>

        <?= dmstr\widgets\Menu::widget(
            [
                'options' => ['class' => 'sidebar-menu', 'data-widget' => 'tree'],
                'items' => [
                    [
                        'label' => Yii::t('main', 'Menu Yii2'),
                        'options' => ['class' => 'header']
                    ],
                    [
                        'label' => Yii::t('main', 'Dashboard'),
                        'icon' => 'dashboard',
                        'url' => Url::toRoute('/site/index'),
                        'active' => $controller->id === 'site',
                        'visible' => $controller->isAllowed('site', 'index'),
                    ],
                    [
                        'label' => Yii::t('main', 'Stats'),
                        'icon' => 'dashboard',
                        'url' => Url::toRoute('/stat/index'),
                        'active' => $controller->id === 'stat',
                        'visible' => $controller->isAllowed('stat', 'index'),
                    ],
                    [
                        'label' => Yii::t('main', 'System'),
                        'url' => '#',
                        'icon' => 'list-alt',
                        'active' => in_array($controller->id, ['gii', 'debug', 'setting', 'test']),
                        'items' => [
                            [
                                'label' => 'Gii',
                                'icon' => 'file-code-o', 'url' => ['/gii'],
                                'visible' => YII_ENV_DEV && $controller->isAllowed('gii', 'index'),
                            ],
                            [
                                'label' => Yii::t('main', 'Debug'),
                                'icon' => 'dashboard', 'url' => ['/debug'],
                                'visible' => YII_ENV_DEV && $controller->isAllowed('debug', 'index'),
                            ],
                            [
                                'label' => Yii::t('main', 'Clear cache'),
                                'icon' => 'folder',
                                'url' => Url::toRoute('/site/clear-cache'),
                                'visible' => $controller->isAllowed('site', 'clear-cache')
                            ],
                        ],
                    ],
                    [
                        'label' => Yii::t('main', 'Attributes'),
                        'url' => '#',
                        'icon' => 'list-alt',
                        'active' => in_array($controller->id, ['attribute/attribute', 'attribute/attribute-value']),
                        'items' => [
                            [
                                'label' => Yii::t('main', 'Attributes'),
                                'icon' => 'file-code-o',
                                'url' => ['/attribute/attribute/index'],
                                'visible' => $controller->isAllowed('attribute', 'index'),
                                'active' => $controller->id === 'attribute/attribute' && $controller->action->id === 'index',
                            ],
                            [
                                'label' => Yii::t('main', 'Moderation'),
                                'icon' => 'file-code-o',
                                'url' => ['/attribute/attribute/moderation'],
                                'visible' => $controller->isAllowed('attribute', 'moderation'),
                                'active' => $controller->id === 'attribute/attribute' && $controller->action->id === 'moderation',
                            ],
                        ],
                    ],
                    [
                        'label' => Yii::t('main', 'Categories'),
                        'icon' => 'list-alt',
                        'url' => Url::toRoute('/category/index'),
                        'active' => $controller->id === 'category' && $controller->action->id === 'index',
                        'visible' => $controller->isAllowed('category', 'index')
                    ],
                    [
                        'label' => Yii::t('main', 'Users'),
                        'icon' => 'user',
                        'url' => Url::toRoute('/user/index'),
                        'active' => $controller->id === 'user' && $controller->action->id === 'index',
                        'visible' => $controller->isAllowed('admin', 'index')
                    ],
                    [
                        'label' => Yii::t('main', 'Courses'),
                        'icon' => 'university',
                        'url' => Url::toRoute('/course/index'),
                        'active' => $controller->id === 'course' && $controller->action->id === 'index',
                        'visible' => $controller->isAllowed('course', 'index')
                    ],
                    [
                        'label' => Yii::t('main', 'Lessons'),
                        'icon' => 'video-camera',
                        'url' => Url::toRoute('/lesson/index'),
                        'active' => $controller->id === 'lesson' && $controller->action->id === 'index',
                        'visible' => $controller->isAllowed('lesson', 'index')
                    ],
                    [
                        'label' => Yii::t('main', 'Tasks'),
                        'icon' => 'file-text',
                        'url' => Url::toRoute('/task/index'),
                        'active' => $controller->id === 'task' && $controller->action->id === 'index',
                        'visible' => $controller->isAllowed('task', 'index')
                    ],
                    [
                        'label' => Yii::t('main', 'Payment Methods'),
                        'icon' => 'file-text',
                        'url' => Url::toRoute('/payment-method/index'),
                        'active' => $controller->id === 'payment-method' && $controller->action->id === 'index',
                        'visible' => $controller->isAllowed('payment-method', 'index')
                    ],
                    [
                        'label' => Yii::t('main', 'Payments'),
                        'icon' => 'file-text',
                        'url' => Url::toRoute('/payment/index'),
                        'active' => $controller->id === 'payment' && $controller->action->id === 'index',
                        'visible' => $controller->isAllowed('payment', 'index')
                    ],
                    [
                        'label' => Yii::t('main', 'Logout'),
                        'url' => ['/auth/logout'],
                        'icon' => 'user',
                        'template' => '<a href="{url}" data-method="post">{icon} {label}</a>',
                        'visible' => !Yii::$app->user->isGuest
                    ],
                ],
            ]
        ) ?>
    </section>
</aside>
