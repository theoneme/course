$(document).on("click", ".child-elements .add-element", function() {
    let self = $(this);
    let id = self.siblings('.elements-container').data('index') + 1;
    let index = '[' + id + ']';
    let parentElement = $(this).closest('.element-item');
    while (parentElement.length) {
        index = '[' + parentElement.data('id') + ']' + index;
        parentElement = parentElement.parent().closest('.element-item');
    }
    self.hide();
    $.post($(this).attr('href'),
        {index: index},
        function(data) {
            self.siblings('.elements-container')
                .prepend('<div class="element-item" data-id="' + id + '">' + data + '</div>')
                .data('index', id)
                .find('.element-item:first > .element-form')
                .slideDown(400);
        }
    );
    return false;
});
$(document).on("click", ".child-elements .remove-element", function() {
    $(this).closest('.element-item').remove();
    return false;
});
$(document).on("click", ".child-elements .edit-element", function() {
    let elementBlock = $(this).closest('.element-item');
    elementBlock.children('.element-preview').slideUp(400, function() {
        elementBlock.children('.element-form').slideDown(400);
    });
    return false;
});
$(document).on("click", ".child-elements .submit-button", function() {
    let formBlock = $(this).closest('.element-form');
    formBlock.slideUp(400, function() {
        let itemBlock = formBlock.closest('.element-item');
        let title = formBlock.find('.title-source').val();
        let image = formBlock.find('.image-source').val();
        let price = formBlock.find('.price-source').val();
        if (!formBlock.siblings('.element-preview').length) {
            renderElementPreview(title, image, price, itemBlock);
        }
        else {
            itemBlock.find('img.element-image:first').attr('src', image);
            itemBlock.find('.element-name:first').html(title);
            itemBlock.find('.element-price:first').html(price + 'р. ');
        }
        formBlock.siblings('.element-preview, .element-children').slideDown(400);
        formBlock.closest('.elements-container').siblings('.add-element').show();
    });
    return false;
});

$(document).on("click", ".child-elements .cancel-button", function() {
    let elementBlock = $(this).closest('.element-item');
    elementBlock.children('.element-form').slideUp(400, function() {
        if (elementBlock.children('.element-preview').length) {
            elementBlock.children('.element-preview').slideDown(400);
        }
        else {
            elementBlock.closest('.elements-container').siblings('.add-element').show();
        }
    });
    return false;
});

function renderElementPreview(title, image, price, prependTo) {
    let imageBlock = image !== undefined ? '<img src="' + image + '" class="element-image">' : '';
    let titleBlock = title !== undefined ? '<div class="element-name">' + title + '</div>' : '';
    let priceBlock = price !== undefined ? '<div class="element-price pull-right">' + price + 'р. ' + '</div>' : '';
    prependTo.prepend(
        '<div class="element-preview">' +
        imageBlock +
        titleBlock +
        '<div class="element-actions pull-right">' +
        '<a class="edit-element"><i class="icon-pencil"></i></a>' +
        '<a class="remove-element"><i class="icon-close"></i></a>' +
        '</div>' +
        priceBlock +
        '</div>'
    );
}