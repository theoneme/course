<?php

namespace frontend\components;

use yii\base\BaseObject;
use yii\base\BootstrapInterface;
use yii\web\Application;

/**
 * Class CurrencySelector
 * @package frontend\components
 */
class CurrencySelector extends BaseObject implements BootstrapInterface
{
    /**
     * @param Application $app
     */
    public function bootstrap($app)
    {
        if ($app instanceof Application) {
            $currency_code = strtoupper($app->request->get('app_currency'));
            if ($currency_code && isset($app->params['currencies'][$currency_code])) { //
                $app->params['app_currency'] = $currency_code;
                $app->session->set('app_currency', $currency_code);
            } else if (isset($app->params['currencies'][$app->session->get('app_currency')])) {
                $app->params['app_currency'] = $app->session['app_currency'];
            } else {
                $currency_code = 'RUB';

                $app->params['app_currency'] = $currency_code;
                $app->session->set('app_currency', $currency_code);
            }
        }
    }
}