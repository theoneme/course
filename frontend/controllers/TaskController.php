<?php

namespace frontend\controllers;

use common\helpers\TaskHelper;
use common\helpers\UtilityHelper;
use common\models\UserProgress;
use common\services\entities\TaskService;
use common\services\entities\UserProgressService;
use Yii;
use yii\base\Module;
use yii\filters\AjaxFilter;
use yii\filters\ContentNegotiator;
use yii\web\Response;

/**
 * Class TaskController
 * @package frontend\controllers
 */
class TaskController extends CourseBaseController
{
    /**
     * @var TaskService
     */
    private $_taskService;

    /**
     * @var UserProgressService
     */
    private $_userProgressService;

    /**
     * TaskController constructor.
     * @param string $id
     * @param Module $module
     * @param TaskService $taskService
     * @param UserProgressService $userProgressService
     * @param array $config
     */
    public function __construct(string $id, Module $module, TaskService $taskService, UserProgressService $userProgressService, array $config = [])
    {
        parent::__construct($id, $module, $config);
        $this->_taskService = $taskService;
        $this->_userProgressService = $userProgressService;
    }

    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return array_merge(parent::behaviors(), [
            [
                'class' => ContentNegotiator::class,
                'only' => ['check', 'mark-failed'],
                'formats' => [
                    'application/json' => Response::FORMAT_JSON,
                ],
            ],
            [
                'class' => AjaxFilter::class,
                'only' => ['check', 'mark-failed'],
            ]
        ]);
    }

    /**
     * @param $id
     * @return array
     */
    public function actionCheck($id)
    {
        $response = ['success' => false, 'message' => Yii::t('app', 'Task not found')];
        $task = $this->_taskService->getOne(['task.id' => $id]);
        if ($task !== null) {
            $taskStartTime = Yii::$app->session->get('taskTime', []);
            $taskStartTime = $taskStartTime[$task['id']] ?? 0;
            $taskTime = time() - $taskStartTime;
            if ($taskTime < $task['minutes'] * 60) {
                $response['message'] = Yii::t('app', 'Incorrect answer');
                $input = Yii::$app->request->post();
                if (!empty($input['answer'])) {
                    $check = TaskHelper::checkAsnwer($task, $input['answer']);
                    if ($check['result']) {
                        $response = ['success' => true, 'message' => Yii::t('app', 'Correct answer')];
                    }
                    if (!empty($check['message'])) {
                        $response['message'] = $check['message'];
                    }
                }
            }
            else {
                $response['message'] = Yii::t('app', 'Time is up');
            }
            $user = UtilityHelper::user();
//            if ($user) {
            $this->_userProgressService->createOne([
                'entity' => UserProgress::ENTITY_TASK,
                'entity_id' => $id,
                'user_id' => $user->id,
                'success' => (int)$response['success'],
                'customDataArray' => !empty($taskTime) ? ['time' => $taskTime] : []
            ]);
            $this->_userProgressService->createOne([
                'entity' => UserProgress::ENTITY_LESSON_TASK,
                'entity_id' => $task['lesson_id'],
                'user_id' => $user->id,
                'success' => (int)$response['success'],
            ]);
//            }
        }
        return $response;
    }

    /**
     * @param $id
     * @return bool
     */
    public function actionMarkFailed($id) {
        $task = $this->_taskService->getOne(['task.id' => $id]);
        if ($task !== null) {
            $user = UtilityHelper::user();
//            if ($user) {
            $this->_userProgressService->createOne([
                'entity' => UserProgress::ENTITY_TASK,
                'entity_id' => $id,
                'user_id' => $user->id,
                'success' => 0,
            ]);
            $this->_userProgressService->createOne([
                'entity' => UserProgress::ENTITY_LESSON_TASK,
                'entity_id' => $task['lesson_id'],
                'user_id' => $user->id,
                'success' => 0,
            ]);
//            }
        }
        return true;
    }
}