<?php

namespace frontend\controllers;

use common\helpers\UtilityHelper;
use common\models\UserProgress;
use common\repositories\sql\LessonRepository;
use common\repositories\sql\TaskRepository;
use common\repositories\sql\UserProgressRepository;
use common\services\entities\CourseService;
use Yii;
use yii\base\Module;

/**
 * Class CourseController
 * @package frontend\controllers
 */
class CourseController extends CourseBaseController
{
    /**
     * @var CourseService
     */
    private $_courseService;
    /**
     * @var LessonRepository
     */
    private $_lessonRepository;
    /**
     * @var TaskRepository
     */
    private $_taskRepository;
    /**
     * @var TaskRepository
     */
    private $_progressRepository;

    /**
     * CourseController constructor.
     * @param string $id
     * @param Module $module
     * @param CourseService $courseService
     * @param LessonRepository $lessonRepository
     * @param TaskRepository $taskRepository
     * @param UserProgressRepository $progressRepository
     * @param array $config
     */
    public function __construct(string $id, Module $module, CourseService $courseService, LessonRepository $lessonRepository, TaskRepository $taskRepository, UserProgressRepository $progressRepository, array $config = [])
    {
        parent::__construct($id, $module, $config);
        $this->_courseService = $courseService;
        $this->_lessonRepository = $lessonRepository;
        $this->_taskRepository = $taskRepository;
        $this->_progressRepository = $progressRepository;
    }

    /**
     *
     */
    public function actionView($id)
    {
        return $this->render('view', []);
    }

    /**
     *
     */
    public function actionClearProgress(){
        $user = UtilityHelper::user();
        $lessonIds = $this->_lessonRepository->joinWith(['lessonCourses'])->findColumnByCriteria(['course_id' => Yii::$app->params['runtime']['course']['id']], 'lesson.id');
        $taskIds = $this->_taskRepository->findColumnByCriteria(['lesson_id' => $lessonIds], 'id');
        $this->_progressRepository->deleteManyByCriteria(['user_id' => $user->id, 'entity' => [UserProgress::ENTITY_LESSON, UserProgress::ENTITY_LESSON_TASK], 'entity_id' => $lessonIds]);
        $this->_progressRepository->deleteManyByCriteria(['user_id' => $user->id, 'entity' => UserProgress::ENTITY_TASK, 'entity_id' => $taskIds]);
        return $this->redirect(['/dashboard/index']);
    }
}