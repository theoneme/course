<?php

namespace frontend\controllers;

use common\services\entities\TaskService;
use common\services\MetaService;
use Yii;
use yii\base\Module;
use yii\db\Expression;

/**
 * Class SiteController
 * @package frontend\controllers
 */
class SiteController extends FrontEndController
{
    /**
     * @var TaskService
     */
    private $_taskService;

    /**
     * SiteController constructor.
     * @param string $id
     * @param Module $module
     * @param TaskService $taskService
     * @param array $config
     */
    public function __construct(string $id, Module $module, TaskService $taskService, array $config = [])
    {
        parent::__construct($id, $module, $config);
        $this->_taskService = $taskService;
    }

    /**
     *
     */
    public function actionIndex()
    {
        $this->layout = 'landing';

        $metaService = new MetaService($this);
        $metaService->registerMeta([
            'title' => 'Подготовка к сдаче ЕГЭ 2020 | Онлайн курс',
            'description' => 'Первый интерактивный курс подготовке к ЕГЭ по математике 2020. Экспресс подготовка к сдаче ЕГЭ по математике.',
            'keywords' => 'ЕГЭ математика 2020 | Курсы подготовки к ЕГЭ 2020',
        ]);

        $showLoginModal = Yii::$app->session->has('showLoginModal') || Yii::$app->request->post('showLoginModal');
        Yii::$app->session->remove('showLoginModal');
        $task = $this->_taskService->getOne(['lesson_id' => 2], ['orderBy' => new Expression('rand()')]);

        return $this->render('landing', ['showLoginModal' => $showLoginModal, 'task' => $task]);
    }

    /**
     * @return string
     */
    public function actionPrivacyPolicy()
    {
        $this->layout = 'page';
        $metaService = new MetaService($this);
        $metaService->registerMeta([
            'title' => 'Обработка персональных данных',
            'description' => 'Обработка персональных данных',
            'keywords' => 'Обработка персональных данных',
        ]);
        return $this->render('privacyPolicy');
    }

    /**
     * @return string
     */
    public function actionTermsOfService()
    {
        $this->layout = 'page';
        $metaService = new MetaService($this);
        $metaService->registerMeta([
            'title' => 'Пользовательское соглашение',
            'description' => 'Пользовательское соглашение.',
            'keywords' => 'Пользовательское соглашение',
        ]);
        return $this->render('termsOfService');
    }

}