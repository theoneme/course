<?php

namespace frontend\controllers;

use common\helpers\UtilityHelper;
use common\services\entities\CourseService;
use Yii;
use yii\filters\AccessControl;
use yii\helpers\Html;

/**
 * Class CourseBaseController
 * @package frontend\controllers
 */
class CourseBaseController extends FrontEndController
{
    /**
     * @var array
     */
    public $currentCourse;


    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::class,
                'rules' => [
                    // Не пускаем гостей
                    [
                        'allow' => false,
                        'roles' => ['?'],
                        'denyCallback' => function ($rule, $action) {
                            Yii::$app->session->setFlash('warning', Yii::t('app', 'To access online courses you must {link} and pay for the selected course.', [
                                'link' => Html::a(Yii::t('app', 'sign up'), '#', ['data-dismiss' => 'modal'])
                            ]));
//                            return $this->redirect(['/auth/login']);
                            Yii::$app->user->loginRequired();
                        },
                    ],
                    // Проверяем токен
                    [
                        'allow' => false,
                        'matchCallback' => function () {
                            $user = UtilityHelper::user();
                            $result = true;
                            $token = Yii::$app->request->get('course_token');
                            if (!empty($token)) {
                                $userCourses = array_column($user->userCourses, 'course_id', 'token');
                                if (isset($userCourses[$token])) {
                                    $service = Yii::$container->get(CourseService::class);
                                    $course = $service->getOne(['course.id' => $userCourses[$token]]);
                                    if ($course !== null) {
                                        Yii::$app->params['runtime']['course'] = $course;
                                        Yii::$app->params['runtime']['course']['token'] = $token;
                                        Yii::$app->params['runtime']['course']['type'] = (int)$course['type'];
                                        $result = false;
                                    }
                                }
                            }
                            return $result;
                        },
                        'denyCallback' => function ($rule, $action) {
                            Yii::$app->session->setFlash('warning', Yii::t('app', 'To access online course you must pay for the selected course.'));
                            return $this->redirect(['/dashboard/index']);
                        },
                    ],
                    // Сюда могут добавлятся другие запрещающие правила
                    // Всё что не запрещено - разрешено
                    [
                        'allow' => true,
                    ],
                ],
            ],
        ];
    }
}