<?php

namespace frontend\controllers;

use common\helpers\CourseHelper;
use common\helpers\UtilityHelper;
use common\models\Course;
use common\models\UserProgress;
use common\services\entities\LessonService;
use common\services\entities\TaskService;
use common\services\entities\UserProgressService;
use common\services\MetaService;
use Yii;
use yii\base\Module;
use yii\db\Expression;
use yii\web\NotFoundHttpException;

/**
 * Class LessonController
 * @package frontend\controllers
 */
class LessonController extends CourseBaseController
{

    /**
     * @var LessonService
     */
    private $_lessonService;

    /**
     * @var TaskService
     */
    private $_taskService;

    /**
     * @var UserProgressService
     */
    private $_userProgressService;

    /**
     * LessonController constructor.
     * @param string $id
     * @param Module $module
     * @param LessonService $lessonService
     * @param TaskService $taskService
     * @param UserProgressService $userProgressService
     * @param array $config
     */
    public function __construct(string $id, Module $module, LessonService $lessonService, TaskService $taskService, UserProgressService $userProgressService, array $config = [])
    {
        parent::__construct($id, $module, $config);
        $this->_lessonService = $lessonService;
        $this->_taskService = $taskService;
        $this->_userProgressService = $userProgressService;
    }

    /**
     * @param $id
     * @return string
     * @throws NotFoundHttpException
     */
    public function actionView($id)
    {
        if (Yii::$app->params['runtime']['course']['type'] === Course::TYPE_TASK_ONLY) {
            return $this->redirect(['/lesson/task', 'id' => $id, 'course_token' => Yii::$app->params['runtime']['course']['token']]);
        }
        $lesson = $this->_lessonService->getOne(['lesson.id' => $id]);
        if ($lesson === null) {
            Yii::$app->session->setFlash('error', Yii::t('app', 'Lesson not found'));
            return $this->redirect(['/dashboard/index']);
//            throw new NotFoundHttpException();
        }
        Yii::$app->params['runtime']['courseData'] = CourseHelper::getCourseData($id);

        $metaService = new MetaService($this);
        $metaService->registerMeta([
            'title' => $lesson['title'],
            'description' => $lesson['description'],
            'keywords' => $lesson['title'],
        ]);
        return $this->render('view', [
            'lesson' => $lesson
        ]);
    }


    /**
     * @param $id
     * @return string
     * @throws NotFoundHttpException
     */
    public function actionTask($id)
    {
        $task = $this->_taskService->getOne(['lesson_id' => $id], ['orderBy' => new Expression('rand()')]);
        if ($task === null) {
            Yii::$app->session->setFlash('error', Yii::t('app', 'Task not found'));
            return $this->redirect(['/dashboard/index']);
//            throw new NotFoundHttpException();
        }
        $user = UtilityHelper::user();
        if ($user) {
            $this->_userProgressService->createOne(['entity' => UserProgress::ENTITY_LESSON, 'entity_id' => $id, 'user_id' => $user->id]);
        }
        Yii::$app->params['runtime']['courseData'] = CourseHelper::getCourseData($id, true);
        $taskTime = Yii::$app->session->get('taskTime', []);
        $taskTime[$task['id']] = time();
        Yii::$app->session->set('taskTime', $taskTime);

        $metaService = new MetaService($this);
        $metaService->registerMeta([
            'title' => Yii::$app->params['runtime']['courseData']['currentLesson']['title'],
            'description' => Yii::$app->params['runtime']['courseData']['currentLesson']['description'],
            'keywords' => Yii::$app->params['runtime']['courseData']['currentLesson']['title'],
        ]);

        return $this->render('task', [
            'task' => $task
        ]);
    }
}