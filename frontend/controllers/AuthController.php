<?php

namespace frontend\controllers;

use common\helpers\FileHelper;
use common\models\User;
use common\models\UserSocial;
use common\repositories\sql\UserRepository;
use common\repositories\sql\UserSocialRepository;
use common\services\entities\UserService;
use common\services\NotificationService;
use frontend\mappers\SocialAttributesMapper;
use frontend\models\auth\LoginForm;
use frontend\models\auth\SignupForm;
use Yii;
use yii\authclient\AuthAction;
use yii\authclient\ClientInterface;
use yii\base\Module;
use yii\filters\AccessControl;
use yii\helpers\Url;
use yii\web\Response;
use yii\widgets\ActiveForm;

/**
 * Class AuthController
 * @package frontend\controllers
 */
class AuthController extends FrontEndController
{
    /**
     * @var UserRepository
     */
    private $_userRepository;
    /**
     * @var UserService
     */
    private $_userService;
    /**
     * @var NotificationService
     */
    private $_notificationService;
    /**
     * @var UserSocialRepository
     */
    private $_userSocialRepository;

    /**
     * AuthController constructor.
     * @param string $id
     * @param Module $module
     * @param UserRepository $userRepository
     * @param UserService $userService
     * @param NotificationService $notificationService
     * @param UserSocialRepository $userSocialRepository
     * @param array $config
     */
    public function __construct(string $id, Module $module,
                                UserRepository $userRepository,
                                UserService $userService,
                                NotificationService $notificationService,
                                UserSocialRepository $userSocialRepository,
                                array $config = [])
    {
        parent::__construct($id, $module, $config);
        $this->_userRepository = $userRepository;
        $this->_userService = $userService;
        $this->_notificationService = $notificationService;
        $this->_userSocialRepository = $userSocialRepository;
    }

    /**
     * @return array
     */
    public function actions()
    {
        return array_merge(parent::actions(), [
            'social' => [
                'class' => AuthAction::class,
                'successCallback' => [$this, 'onSocialSuccess'],
            ],
        ]);
    }

    public function beforeAction($action)
    {
        if ($action->id === 'login') {
            Url::remember(Yii::$app->user->returnUrl, 'returnUrl2');
            Yii::$app->session->set('showLoginModal', true);
        }

//        if ($action->id === 'login-ajax') {
//            Url::remember(Yii::$app->request->referrer, 'returnUrl');
//        }

        return parent::beforeAction($action);
    }

    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return array_merge(parent::behaviors(), [
            'access' => [
                'class' => AccessControl::class,
                'only' => ['logout', 'signup', 'detach'],
                'rules' => [
                    [
                        'actions' => ['signup'],
                        'allow' => true,
                        'roles' => ['?'],
                    ],
                    [
                        'actions' => ['logout', 'detach'],
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
        ]);
    }

    public function actionLogin()
    {
        $message = Yii::$app->session->getFlash('warning');
        Yii::$app->session->setFlash('warning', Yii::t('app', $message ?? 'This page is only available to authorized users'));
        return $this->redirect(['/site/index']);
    }

    /**
     * @return array|string|Response
     * @throws \common\exceptions\RepositoryException
     * @throws \yii\base\InvalidConfigException
     */
    public function actionLoginAjax()
    {
        if (!Yii::$app->user->isGuest) {
            return $this->goHome();
        }

        $loginForm = new LoginForm($this->_userRepository);
        $input = Yii::$app->request->post();

        $returnUrl = Url::to(['/site/index']);

        if(!empty($input)) {
            $loginForm->load($input);

            if (array_key_exists('ajax', $input)) {
                Yii::$app->response->format = Response::FORMAT_JSON;

                return ActiveForm::validate($loginForm);
            }

            if ($loginForm->validate() && $loginForm->login()) {
                Yii::$app->session->setFlash('success', Yii::t('app', 'You have successfully logged in'));

                $returnUrl = Url::previous('returnUrl2') ?? Url::previous('returnUrl') ?? ['/dashboard/index'];
            }
        }

        return $this->redirect($returnUrl);
    }

    /**
     * @return array|string|Response
     * @throws \common\exceptions\RepositoryException
     * @throws \yii\base\InvalidConfigException
     */
    public function actionSignup()
    {
        $loginForm = new LoginForm($this->_userRepository);
        $signupForm = new SignupForm($this->_userService);
        $input = Yii::$app->request->post();

        if(!empty($input)) {
            $utm = Yii::$app->session->get('utm', []);
            if (!empty($utm)) {
                $input['SignupForm']['customDataArray'] = ['utm' => $utm];
            }
            $signupForm->load($input);

            $returnUrl = Url::previous('returnUrl2') ?? Url::previous('returnUrl') ?? ['/dashboard/index'];

            $loginForm->load($signupForm->attributes, '');
            if ($loginForm->validate() && $loginForm->login()) {
                Yii::$app->session->setFlash('success', Yii::t('app', 'You have successfully logged in'));
                return $this->redirect($returnUrl);
            }
            if (array_key_exists('ajax', $input)) {
                Yii::$app->response->format = Response::FORMAT_JSON;
                return ActiveForm::validate($signupForm);
            }

            if ($signupForm->validate() && $signupForm->signup()) {
//                $loginForm = new LoginForm($this->_userRepository);
//                $loginForm->setAttributes($signupForm->attributes);
                Yii::$app->session->setFlash('success', Yii::t('app', 'You have successfully signed up'));

                $loginForm->login();
                Yii::$app->session->remove('utm');

                return $this->redirect($returnUrl);
            }
        }
        Yii::$app->session->set('showLoginModal', true);

        return $this->redirect(['/site/index']);
    }

    /**
     * Logs out the current user.
     *
     * @return mixed
     */
    public function actionLogout()
    {
        Yii::$app->user->logout();

        return $this->goHome();
    }


    /**
     * @param ClientInterface $client
     */
    public function onSocialSuccess(ClientInterface $client)
    {
        $mappedAttributes = SocialAttributesMapper::getMappedData([
            'attributes' => $client->getUserAttributes(),
            'provider' => $client->getName()
        ]);

        if (!empty($mappedAttributes['image'])) {
            $pathInfo = FileHelper::createTempDirectory('@frontend');
            $filename = uniqid('social', true) . '.jpg';
            $newAbsolutePath = $pathInfo['basePath'] . $filename;
            $newRelativePath = $pathInfo['relativePath'] . $filename;

            $file = file_get_contents($mappedAttributes['image']);
            if ($file) {
                file_put_contents($newAbsolutePath, $file);
                $mappedAttributes['image'] = $newRelativePath;
            }
        }

        /** @var UserSocial $social */
        $social = $this->_userSocialRepository->with(['user'])->findOneByCriteria([
            'source' => $client->getId(),
            'source_id' => $mappedAttributes['id'],
        ]);

        if (Yii::$app->user->isGuest) {
            if ($social !== null) {
                $user = $social->user;
                Yii::$app->user->login($user);
            } else {
                $user = $this->_userRepository->findOneByCriteria(['email' => $mappedAttributes['email']]);

                if ($user !== null && isset($mappedAttributes['email'])) {
                    Yii::$app->getSession()->setFlash('error', Yii::t('app', 'User with such email already exists'));
                } else {
                    $password = Yii::$app->security->generateRandomString(6);
                    $data = [
                        'UserForm' => [
                            'email' => $mappedAttributes['email'],
                            'username' => $mappedAttributes['username'],
                            'avatar' => $mappedAttributes['image'],
                        ],
                        'PasswordForm' => [
                            'password' => $password,
                            'confirm' => $password
                        ]
                    ];
                    $utm = Yii::$app->session->get('utm', []);
                    if (!empty($utm)) {
                        $data['UserForm']['customDataArray'] = ['utm' => $utm];
                        Yii::$app->session->remove('utm');
                    }
                    $userId = $this->_userRepository->create($data);

                    if ($userId) {
                        $userSocial = $this->_userSocialRepository->create([
                            'user_id' => $userId,
                            'source' => $client->getId(),
                            'source_id' => (string)$mappedAttributes['id'],
                            'customDataArray' => $mappedAttributes
                        ]);
                        if ($userSocial->id !== null) {
                            /** @var User $user */
                            $user = $this->_userRepository->findOneByCriteria(['id' => $userId]);
                            Yii::$app->user->login($user);
                        } else {
                            Yii::error($user->getErrors());
                        }
                    } else {
                        Yii::error($user->getErrors());
                    }
                }
            }
        } else if (!$social) {
            $this->_userSocialRepository->create([
                'user_id' => Yii::$app->user->id,
                'source' => $client->getId(),
                'source_id' => $mappedAttributes['id'],
                'customDataArray' => $mappedAttributes
            ]);
        }
    }

    /**
     * @param $provider
     * @return Response
     */
    public function actionDetach($provider)
    {
        $this->_userSocialRepository->deleteOneByCriteria(['user_id' => Yii::$app->user->getId(), 'source' => $provider]);
        return $this->redirect(Yii::$app->request->referrer);
    }
}
