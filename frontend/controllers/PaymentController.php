<?php

namespace frontend\controllers;

use common\components\CurrencyHelper;
use common\helpers\SberbankHelper;
use common\helpers\UtilityHelper;
use common\models\Payment;
use common\repositories\sql\PaymentMethodRepository;
use common\services\entities\CourseService;
use common\services\PaymentProcessService;
use common\services\entities\PaymentService;
use Voronkovich\SberbankAcquiring\Client;
use Voronkovich\SberbankAcquiring\Currency;
use Voronkovich\SberbankAcquiring\OrderStatus;
use Yii;
use yii\base\Module;
use yii\filters\AccessControl;
use yii\helpers\Url;
use yii\web\Controller;
use yii\web\NotFoundHttpException;

/**
 * PaymentController.
 */
class PaymentController extends Controller
{
    /**
     * @var CourseService
     */
    private $_courseService;

    /**
     * @var PaymentService
     */
    private $_paymentService;

    /**
     * @var PaymentProcessService
     */
    private $_paymentProcessService;

    /**
     * @var PaymentMethodRepository
     */
    private $_paymentMethodRepository;

    /**
     * PaymentController constructor.
     * @param string $id
     * @param Module $module
     * @param CourseService $courseService
     * @param PaymentService $paymentService
     * @param PaymentProcessService $paymentProcessService
     * @param PaymentMethodRepository $paymentMethodRepository
     * @param array $config
     */
    public function __construct(
        string $id,
        Module $module,
        CourseService $courseService,
        PaymentService $paymentService,
        PaymentProcessService $paymentProcessService,
        PaymentMethodRepository $paymentMethodRepository,
        array $config = []
    )
    {
        parent::__construct($id, $module, $config);
        $this->_courseService = $courseService;
        $this->_paymentService = $paymentService;
        $this->_paymentProcessService = $paymentProcessService;
        $this->_paymentMethodRepository = $paymentMethodRepository;
    }

    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return array_merge(parent::behaviors(), [
            'access' => [
                'class' => AccessControl::class,
                'rules' => [
                    ['allow' => true, 'roles' => ['@']],
                ],
            ],
        ]);
    }

    /**
     * @inheritdoc
     */
    public function beforeAction($action)
    {
        $this->enableCsrfValidation = false;

        return parent::beforeAction($action);
    }

    /**
     * @param null $payment_id
     * @return \yii\web\Response
     * @throws NotFoundHttpException
     */
    public function actionReturn($payment_id = null)
    {
        $returnUrl = Url::to(['/dashboard/index']);

        if ($payment_id === null) {
            $payment_id = Yii::$app->request->post('InvId');
            if ($payment_id === null) {
                throw new NotFoundHttpException(Yii::t('app', 'Page not found'));
            }
        }

        /** @var $payment Payment */
        $payment = Payment::findOne((int)$payment_id);
        if ($payment === null) {
            throw new NotFoundHttpException(Yii::t('app', 'Page not found'));
        }
        if ($payment->status == Payment::STATUS_PAID) {
//            Yii::$app->session->set('paymentId', (int)$payment_id);
//            Yii::$app->session->set('livePaymentId', (int)$payment_id);
            $message = 'Услуга успешно оплачена.';

//            $returnUrl = $payment->getReturnUrl();

        } elseif ($payment->status == Payment::STATUS_REFUSED) {
            $message = Yii::t('app', 'Payment cancelled');
        } else {
            $message = Yii::t('app', 'Payment is in processing');
        }
        Yii::$app->session->setFlash('success', $message);

        return $this->redirect($returnUrl);
    }

    /**
     * @param $course_id
     * @return \yii\web\Response
     */
    public function actionSberbank($course_id)
    {
        $pm = $this->_paymentMethodRepository->findOneByCriteria(['alias' => 'sber']);
        if ($pm) {
            $course = $this->_courseService->getOne(['course.id' => $course_id]);
            if ($course !== null) {
                $paymentId = $this->_paymentService->create([
                    'payment_method_id' => $pm['id'],
                    'type' => Payment::TYPE_COURSE,
                    'user_id' => UtilityHelper::user()->id,
                    'total' => $course['price'],
                    'currency_code' => 'RUB',
                    'customDataArray' => ['course_id' => $course_id]
                ]);
                if ($paymentId) {
                    /** @var Client $client */
                    $client = new Client(SberbankHelper::getConfigArray());

                    $orderAmount = $course['price'] * 100;
                    $returnUrl = Yii::$app->urlManager->createAbsoluteUrl(['/payment/sberbank-callback', 'payment_id' => $paymentId]);
                    $params = [
                        'currency' => Currency::RUB,
                        'failUrl' => Yii::$app->urlManager->createAbsoluteUrl(['/payment/sberbank-callback', 'payment_id' => $paymentId]),
                        'description' => Yii::t('app', 'Payment for course on {site} site', ['site' => Yii::$app->name]),
                    ];

                    $result = $client->registerOrder($paymentId, $orderAmount, $returnUrl, $params);

                    $paymentFormUrl = $result['formUrl'];

//                    header('Location: ' . $paymentFormUrl);
                    return $this->redirect($paymentFormUrl);
                }
                else {
                    Yii::$app->session->setFlash('error', Yii::t('app', 'Error occured while creating payment'));
                }
            }
            else {
                Yii::$app->session->setFlash('error', Yii::t('app', 'Course not found'));
            }
        }
        else {
            Yii::$app->session->setFlash('error', Yii::t('app', 'Payment method not found'));
        }
        return $this->redirect(['/dashboard/index']);
    }

    /**
     * @return \yii\web\Response
     * @throws NotFoundHttpException
     */
    public function actionSberbankCallback()
    {
        $paymentId = (int)Yii::$app->request->get('payment_id');
        $orderId = Yii::$app->request->get('orderId');

        $messageStatus = 'error';
        $message = Yii::t('app', 'Page not found');
        $returnUrl = Url::to(['/dashboard/index']);

        /** @var $payment Payment */
        $payment = $this->_paymentService->getOne(['payment.id' => $paymentId]);
        if ($payment !== null && $payment['status'] === Payment::STATUS_PENDING) {
            /** @var Client $client */
            $client = new Client(SberbankHelper::getConfigArray());
            $status = $client->getOrderStatusExtended($orderId);

            if ($status['amount'] / 100 != CurrencyHelper::convert($payment['currency_code'], 'RUB', $payment['total'])) {
                $this->_paymentProcessService->markRefused($payment);
                $message = Yii::t('app', 'Incorrect payment amount');
            }
            else {
                switch ($status['orderStatus']) {
                    case OrderStatus::DEPOSITED:
                        Yii::$app->session->set('paymentId', $payment['id']);
                        $this->_paymentProcessService->markComplete($payment);
                        $returnUrl = $this->_paymentProcessService->getReturnUrl($payment);
                        $messageStatus = 'success';
                        $message = [
                            'message' => Yii::t('app', 'Payment successfull'),
                            'subtitle' => Yii::t('app', 'Thank you for being with us!')
                        ];
                        break;
                    case OrderStatus::DECLINED:
                        $this->_paymentProcessService->markRefused($payment);
                        $message = Yii::t('app', 'Payment failed');
                        break;
                    default:
                        $messageStatus = 'info';
                        $message = Yii::t('app', 'Payment is in processing');
                        break;
                }
                $this->_paymentService->update(['code' => $orderId], ['payment.id' => $paymentId]);
            }
        }
        Yii::$app->session->addFlash($messageStatus, $message);
        return $this->redirect($returnUrl);
    }
}