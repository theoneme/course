<?php

namespace frontend\controllers;

use common\helpers\UtilityHelper;
use common\models\UserProgress;
use common\services\entities\CourseService;
use common\services\entities\UserProgressService;
use common\services\MetaService;
use Yii;
use yii\base\Module;
use yii\filters\AccessControl;
use yii\helpers\ArrayHelper;
use yii\helpers\Html;
use yii\web\NotFoundHttpException;

/**
 * Class DashboardController
 * @package frontend\controllers
 */
class DashboardController extends FrontEndController
{
    /**
     * @var CourseService
     */
    private $_courseService;
    /**
     * @var CourseService
     */
    private $_progressService;

    /**
     * DashboardController constructor.
     * @param string $id
     * @param Module $module
     * @param CourseService $courseService
     * @param UserProgressService $progressService
     * @param array $config
     */
    public function __construct(string $id, Module $module, CourseService $courseService, UserProgressService $progressService, array $config = [])
    {
        parent::__construct($id, $module, $config);
        $this->_courseService = $courseService;
        $this->_progressService = $progressService;
    }

//    /**
//     * {@inheritdoc}
//     */
//    public function behaviors()
//    {
//        return array_merge(parent::behaviors(), [
//            'access' => [
//                'class' => AccessControl::class,
//                'rules' => [
//                    [
//                        'allow' => true, 'roles' => ['@'],
//                    ],
//                ],
//                'denyCallback' => function ($rule, $action) {
//                    Yii::$app->session->setFlash('warning', Yii::t('app', 'To access online courses you must {link}', [
//                        'link' => Html::a(Yii::t('app', 'sign up'), '#', ['data-dismiss' => 'modal'])
//                    ]));
//                    Yii::$app->user->loginRequired();
////                    return $this->redirect(['/auth/login']);
//                },
//            ],
//        ]);
//    }

    /**
     * @return string
     * @throws NotFoundHttpException
     */
    public function actionIndex()
    {
        $courses = $this->_courseService->getMany([]);
        $user = UtilityHelper::user();
        $lessonIds = array_reduce($courses['items'], function($carry, $value){
            return array_merge($carry, array_keys($value['lessons']));
        }, []);
        if ($user) {
            $lessonProgress = $this->_progressService->getMany([
                'entity' => UserProgress::ENTITY_LESSON,
                'entity_id' => $lessonIds,
                'user_id' => $user['id']
            ]);
            $lessonProgress = ArrayHelper::map($lessonProgress['items'], 'entity_id', 'success');
        }

        $metaService = new MetaService($this);
        $metaService->registerMeta([
            'title' => 'Подготовка к сдаче ЕГЭ 2020 | Онлайн курсы',
            'description' => 'Подготовка к сдаче ЕГЭ 2020 | Онлайн курсы',
            'keywords' => 'Подготовка к сдаче ЕГЭ 2020 | Онлайн курсы',
        ]);

        return $this->render('index', [
            'courses' => $courses['items'],
            'lessonProgress' => $lessonProgress ?? [],
            'userCourses' => $user ? $user->userCourses : []
        ]);
    }
}