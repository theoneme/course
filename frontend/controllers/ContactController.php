<?php

namespace frontend\controllers;

use common\services\NotificationService;
use frontend\forms\ContactForm;
use frontend\mappers\ContactFormToMessageMapper;
use Yii;
use yii\base\Module;
use yii\filters\AjaxFilter;
use yii\filters\ContentNegotiator;
use yii\web\Response;
use yii\widgets\ActiveForm;

/**
 * Class AjaxController
 * @package frontend\controllers
 */
class ContactController extends FrontEndController
{
    /**
     * @var NotificationService
     */
    private $_notificationService;

    /**
     * ContactController constructor.
     * @param string $id
     * @param Module $module
     * @param NotificationService $notificationService
     * @param array $config
     */
    public function __construct(string $id, Module $module, NotificationService $notificationService, array $config = [])
    {
        parent::__construct($id, $module, $config);
        $this->_notificationService = $notificationService;
    }

    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            [
                'class' => ContentNegotiator::class,
                'only' => ['send'],
                'formats' => [
                    'application/json' => Response::FORMAT_JSON,
                ],
            ],
            [
                'class' => AjaxFilter::class,
                'only' => ['send'],
            ]
        ];
    }

    /**
     * @return array|Response
     */
    public function actionSend()
    {
        $form = new ContactForm();
        $input = Yii::$app->request->post();
        $form->load($input);

        if (array_key_exists('ajax', $input)) {
            Yii::$app->response->format = Response::FORMAT_JSON;

            return ActiveForm::validate($form);
        }

        if ($form->validate()) {
            $params = ContactFormToMessageMapper::getMappedData([
                'attributes' => $form->attributes,
            ]);
            $this->_notificationService->sendNotification('email', $params);

            return ['success' => true,];
        }

        return ['success' => false];
    }
}
