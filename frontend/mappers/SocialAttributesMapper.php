<?php

namespace frontend\mappers;

use common\interfaces\DataMapperInterface;
use Yii;
use yii\base\InvalidConfigException;

/**
 * Class SocialAttributesMapper
 * @package frontend\mappers\social
 */
class SocialAttributesMapper implements DataMapperInterface
{
    /**
     * @param $rawData
     * @return array
     * @throws InvalidConfigException
     */
    public static function getMappedData($rawData)
    {
        if(!array_key_exists('provider', $rawData)) {
            throw new InvalidConfigException('Missing provider');
        }

        if(!array_key_exists('attributes', $rawData)) {
            throw new InvalidConfigException('Missing data');
        }

        $data = [
            'id' => null,
            'username' => null,
            'email' => null,
            'image' => null,
            'url' => null
        ];

        switch($rawData['provider']) {
            case 'facebook':
                $data['username'] = $rawData['attributes']['name'];
                $data['email'] = $rawData['attributes']['email'];
                $data['image'] = "https://graph.facebook.com/{$rawData['attributes']['id']}/picture?width=600&height=600";
                $data['url'] = "https://www.facebook.com/?fbid={$rawData['attributes']['id']}";
                $data['id'] = $rawData['attributes']['id'];

                break;

            case 'google':
                $data['username'] = $rawData['attributes']['displayName'];
                if(array_key_exists('email', $rawData['attributes'])) {
                    $email = $rawData['attributes']['email'];
                } else {
                    $email = array_pop($rawData['attributes']['emails'])['value'];
                }
                $data['email'] = $email;
                $data['image'] = str_replace('sz=50', 'sz=600', $rawData['attributes']['image']['url']);
                $data['url'] = $rawData['attributes']['url'];
                $data['id'] = $rawData['attributes']['id'];

                break;

            case 'vkontakte':
                $data['username'] = $rawData['attributes']['first_name'];
                $data['email'] = !empty($rawData['attributes']['email']) ? $rawData['attributes']['email'] : "{$rawData['attributes']['id']}@vk.com";
                $data['image'] = $rawData['attributes']['photo'];
                $data['url'] = "https://vk.com/id{$rawData['attributes']['id']}";
                $data['id'] = $rawData['attributes']['id'];

                break;
        }

        return $data;
    }
}