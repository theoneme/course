<?php
namespace frontend\mappers;

use common\interfaces\DataMapperInterface;
use Yii;

/**
 * Class ContactFormToMessageMapper
 * @package common\mappers\forms
 */
class ContactFormToMessageMapper implements DataMapperInterface
{
    /**
     * @param $rawData
     * @return mixed
     * @throws \yii\base\InvalidArgumentException
     */
    public static function getMappedData($rawData)
    {
        return [
            'to' => Yii::$app->params['ourEmails'],
            'receiverName' => Yii::$app->name,
            'senderName' => $rawData['attributes']['name'],
            'senderEmail' => $rawData['attributes']['email'],
            'subject' => $rawData['attributes']['subject'],
            'senderMessage' => $rawData['attributes']['message'],
            'view' => 'contact-message',
            'locale' => Yii::$app->language,
            'viewPath' => '@frontend/views/mail',
            'viewParams' => []
        ];
    }
}