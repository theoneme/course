<?php

namespace frontend\forms;

use Yii;
use yii\base\Model;

/**
 * Class ContactForm
 * @package frontend\forms
 */
class ContactForm extends Model
{
    /**
     * @var array
     */
    public $name;
    /**
     * @var string
     */
    public $subject;
    /**
     * @var string
     */
    public $email;
    /**
     * @var string
     */
    public $message;

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['message'], 'required'],
            [['email'], 'email'],
            [['name', 'message'], 'string'],
            [['subject'], 'string', 'max' => 255],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'subject' => Yii::t('app', 'Subject'),
            'name' => Yii::t('app', 'Your Name'),
            'message' => Yii::t('app', 'Your Message'),
            'email' => Yii::t('app', 'Your Email Address'),
        ];
    }
}
