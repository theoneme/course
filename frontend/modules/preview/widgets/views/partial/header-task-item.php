<?php

use yii\helpers\Url;
use yii\web\View;

/**
 * @var View $this
 * @var array $lesson
 * @var integer $points
 * @var boolean $done
 * @var boolean $success
 * @var string $index
 */

?>
<div class="lesson <?= $done ? 'done ' : ' '?> py-3 px-2 px-sm-4">
    <a href="<?= Url::to(['/preview/lesson/task', 'id' => $lesson['id'], 'course_id' => Yii::$app->params['runtime']['course']['id']])?>" class="d-flex justify-content-between">
        <div class="pr-2 d-flex align-items-center">
            <div class="badge-course bg-blue color-white d-flex rounded-circle mr-2 align-items-center justify-content-center">
                <i class="i-exam"></i>
            </div>
            <?= $index . ". " . Yii::t('app', 'Task on "{lesson}" chapter', ['lesson' =>$lesson['title']]) ?>
        </div>
        <div class="d-flex align-items-center text-nowrap">
            <i class="result-icon <?= $success ? 'i-correct' : 'i-minus'?> mr-2"></i>
            <?= Yii::t('app', '{points, plural, one{# point} other{# points}}', ['points' => $points])?>
        </div>
    </a>
</div>