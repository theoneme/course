<?php

namespace frontend\modules\preview;

use common\helpers\UtilityHelper;
use yii\base\BootstrapInterface;

/**
 * Class Module
 * @package frontend\modules\preview
 */
class Module extends \yii\base\Module implements BootstrapInterface
{
    /**
     * {@inheritdoc}
     */
    public $controllerNamespace = 'frontend\modules\preview\controllers';

    /**
     * @param \yii\base\Application $app
     */
    public function bootstrap($app)
    {
//        Yii::$app->i18n->translations['preview*'] = [
//            'class' => PhpMessageSource::class,
//            'basePath' => '@frontend/modules/preview/messages',
//
//            'fileMap' => [
//                'preview' => 'preview.php',
//            ],
//        ];

        $app->getUrlManager()->addRules([
            'preview/<course_id:\d+>/lesson/<id:\d+>/task' => 'preview/lesson/task',
            'preview/<course_id:\d+>/lesson/<id:\d+>' => 'preview/lesson/view',
            'preview/<course_id:\d+>/lesson/continue' => 'preview/lesson/continue',

            'preview/<course_id:\d+>/task/<action:[\w-]+>' => 'preview/task/<action>',
            'preview/<course_id:\d+>/course/<action:[\w-]+>' => 'preview/course/<action>',
        ], false);
    }
}
