<?php

namespace frontend\modules\preview\helpers;

use common\helpers\DataHelper;
use common\helpers\UtilityHelper;
use common\models\Course;
use common\models\UserProgress;
use common\repositories\sql\TaskRepository;
use common\services\entities\LessonService;
use frontend\modules\preview\services\UserProgressSessionService;
use Yii;
use yii\helpers\ArrayHelper;

/**
 * Class CourseHelper
 * @package common\helpers
 */
class CourseHelper
{
    /**
     * @param $currentLessonId
     * @param bool $isTask
     * @return mixed
     */
    public static function getCourseData($currentLessonId, $isTask = false){
        /** @var LessonService $lessonService */
        $lessonService = Yii::$container->get(LessonService::class);
        /** @var UserProgressSessionService $progressService */
        $progressService = Yii::$container->get(UserProgressSessionService::class);
        /** @var TaskRepository $taskRepository */
        $taskRepository = Yii::$container->get(TaskRepository::class);

        $params = [];
        $params['type'] = Yii::$app->params['runtime']['course']['type'];
        $params['currentLesson'] = $lessonService->getOne(['lesson.id' => $currentLessonId]);
        $params['lessons'] = $lessonService->getMany(['course_id' => Yii::$app->params['runtime']['course']['id']], ['limit' => 3])['items'];
        $params['lessonIds'] = array_column($params['lessons'], 'id');
        $params['tasks'] = $taskRepository->indexBy('lesson_id')->groupBy('lesson_id')->findColumnByCriteria(['lesson_id' => $params['lessonIds']], 'max(points)');
        $lessonUrl = $params['type'] === Course::TYPE_DEFAULT ? '/preview/lesson/view' : '/preview/lesson/task';

        $lessonProgress = $progressService->getMany([
            'entity' => UserProgress::ENTITY_LESSON,
            'entity_id' => $params['lessonIds'],
        ]);
        $params['lessonProgress'] = ArrayHelper::map($lessonProgress, 'entity_id', 'success');
        $lessonTaskProgress = $progressService->getMany([
            'entity' => UserProgress::ENTITY_LESSON_TASK,
            'entity_id' => $params['lessonIds'],
        ]);
        $params['lessonTaskProgress'] = ArrayHelper::map($lessonTaskProgress, 'entity_id', 'success');

        $params['progressPercent'] = (int)(count($params['lessonProgress'] ?? []) / count($params['lessonIds']) * 100);
        if ($isTask && $params['type'] === Course::TYPE_DEFAULT) {
            $params['prev'] = [
                'url' => [$lessonUrl, 'id' => $params['currentLesson']['id'], 'course_id' => Yii::$app->params['runtime']['course']['id']],
                'title' => Yii::t('app', 'Show video')
            ];
        }
        else {
            $prevLesson = DataHelper::getPreviousItem($params['lessons'], $params['currentLesson']['sort'], 'sort');
            if ($prevLesson) {
                $params['prev'] = [
                    'url' => [$lessonUrl, 'id' => $prevLesson['id'], 'course_id' => Yii::$app->params['runtime']['course']['id']],
                    'title' => Yii::t('app', 'Previous lesson')
                ];
            }
        }
        $nextLesson = DataHelper::getNextItem($params['lessons'], $params['currentLesson']['sort'], 'sort');
        if ($nextLesson) {
            $params['next'] = [
                'url' => [$lessonUrl, 'id' => $nextLesson['id'], 'course_id' => Yii::$app->params['runtime']['course']['id']],
                'title' => Yii::t('app', 'Next lesson')
            ];
        }
        return $params;
    }
}