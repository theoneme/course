<?php

namespace frontend\modules\preview\controllers;

use Yii;
use yii\base\Module;

/**
 * Class CourseController
 * @package frontend\controllers
 */
class CourseController extends CourseBaseController
{
    /**
     * CourseController constructor.
     * @param string $id
     * @param Module $module
     * @param array $config
     */
    public function __construct(string $id, Module $module, array $config = [])
    {
        parent::__construct($id, $module, $config);
    }

    /**
     *
     */
    public function actionView($id)
    {
        return $this->render('view', []);
    }

    /**
     *
     */
    public function actionClearProgress(){
        Yii::$app->session->set('userProgress', []);
        return $this->redirect(['/dashboard/index']);
    }
}