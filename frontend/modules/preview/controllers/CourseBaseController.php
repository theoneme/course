<?php

namespace frontend\modules\preview\controllers;

use common\services\entities\CourseService;
use frontend\controllers\FrontEndController;
use Yii;
use yii\filters\AccessControl;

/**
 * Class CourseBaseController
 * @package frontend\controllers
 */
class CourseBaseController extends FrontEndController
{
    /**
     * @var array
     */
    public $currentCourse;

    /**
     * @var string
     */
    public $layout = '@frontend/modules/preview/views/layouts/main';


    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::class,
                'rules' => [
                    // Проверяем id
                    [
                        'allow' => false,
                        'matchCallback' => function () {
                            $result = true;
                            $id = Yii::$app->request->get('course_id');
                            if (!empty($id)) {
                                $service = Yii::$container->get(CourseService::class);
                                $course = $service->getOne(['course.id' => $id]);
                                if ($course !== null) {
                                    Yii::$app->params['runtime']['course'] = $course;
                                    Yii::$app->params['runtime']['course']['type'] = (int)$course['type'];
                                    $result = false;
                                }
                            }
                            return $result;
                        },
                        'denyCallback' => function ($rule, $action) {
                            Yii::$app->session->setFlash('warning', Yii::t('app', 'Course not found'));
                            return $this->redirect(['/dashboard/index']);
                        },
                    ],
                    // Сюда могут добавлятся другие запрещающие правила
                    // Всё что не запрещено - разрешено
                    [
                        'allow' => true,
                    ],
                ],
            ],
        ];
    }
}