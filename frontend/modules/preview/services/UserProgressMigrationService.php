<?php

namespace frontend\modules\preview\services;

use common\models\UserProgress;
use common\repositories\sql\LessonRepository;
use common\repositories\sql\TaskRepository;
use common\services\entities\CourseService;
use common\services\entities\UserProgressService;

/**
 * Class UserProgressMigrationService
 * @package frontend\modules\preview\services
 */
class UserProgressMigrationService
{

    /**
     * @var CourseService
     */
    private $_courseService;
    /**
     * @var LessonRepository
     */
    private $_lessonRepository;
    /**
     * @var TaskRepository
     */
    private $_taskRepository;
    /**
     * @var UserProgressService
     */
    private $_progressService;
    /**
     * @var UserProgressSessionService
     */
    private $_sessionProgressService;

    /**
     * UserProgressMigrationService constructor.
     * @param CourseService $courseService
     * @param LessonRepository $lessonRepository
     * @param TaskRepository $taskRepository
     * @param UserProgressService $progressService
     * @param UserProgressSessionService $sessionProgressService
     */
    public function __construct(
        CourseService $courseService,
        LessonRepository $lessonRepository,
        TaskRepository $taskRepository,
        UserProgressService $progressService,
        UserProgressSessionService $sessionProgressService
    )
    {
        $this->_courseService = $courseService;
        $this->_lessonRepository = $lessonRepository;
        $this->_taskRepository = $taskRepository;
        $this->_progressService = $progressService;
        $this->_sessionProgressService = $sessionProgressService;
    }

    /**
     * @param $user_id
     * @param $course_id
     */
    public function sessionToDb($user_id, $course_id){
        if (!empty($user_id) && !empty($course_id)) {
            $lessonIds = $this->_lessonRepository->joinWith(['lessonCourses'])->findColumnByCriteria(['course_id' => $course_id], 'lesson.id');
            $taskIds = $this->_taskRepository->findColumnByCriteria(['lesson_id' => $lessonIds], 'id');
            $progress = [
                'lessons' => $this->_sessionProgressService->getMany(['entity' => UserProgress::ENTITY_LESSON, 'entity_id' => $lessonIds]),
                'lessonTasks' => $this->_sessionProgressService->getMany(['entity' => UserProgress::ENTITY_LESSON_TASK, 'entity_id' => $lessonIds]),
                'tasks' => $this->_sessionProgressService->getMany(['entity' => UserProgress::ENTITY_TASK, 'entity_id' => $taskIds]),
            ];
            foreach ($progress as $progressGroup) {
                foreach ($progressGroup as $progressItem) {
                    $this->_progressService->createOne(array_merge($progressItem, ['user_id' => $user_id]));
                }
            }
            $this->_sessionProgressService->delete(['entity' => UserProgress::ENTITY_LESSON, 'entity_id' => $lessonIds]);
            $this->_sessionProgressService->delete(['entity' => UserProgress::ENTITY_LESSON_TASK, 'entity_id' => $lessonIds]);
            $this->_sessionProgressService->delete(['entity' => UserProgress::ENTITY_TASK, 'entity_id' => $taskIds]);
        }
    }
}