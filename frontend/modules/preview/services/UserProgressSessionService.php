<?php

namespace frontend\modules\preview\services;

use common\interfaces\EntityServiceInterface;
use Yii;
use yii\helpers\ArrayHelper;
use yii\web\Session;

/**
 * Class UserProgressSessionService
 * @package frontend\modules\preview\services
 */
class UserProgressSessionService implements EntityServiceInterface
{
    /**
     * @var Session
     */
    private $session;

    /**
     * UserProgressSessionService constructor.
     */
    public function __construct()
    {
        $this->session = Yii::$app->session;
    }

    /**
     * @param $criteria
     * @return array|null
     */
    public function getOne($criteria)
    {
        $entity = ArrayHelper::remove($criteria, 'entity');
        if ($entity === null) {
            return [];
        }
        $progress = $this->session->get('userProgress', []);
        $progress = $progress[$entity] ?? [];
//        $progress = array_reduce($progress, function($carry, $value){
//            return array_merge($carry, $value);
//        }, []);
        if (!empty($criteria)){
            $progress = array_filter($progress, function($var) use ($criteria){
                return $this->checkCriteria($var, $criteria);
            });
        }
        return !empty($progress) ? array_values($progress)[0] : [];
    }

    /**
     * @param $criteria
     * @param array $config
     * @return array
     */
    public function getMany($criteria, $config = [])
    {
        $entity = ArrayHelper::remove($criteria, 'entity');
        if ($entity === null) {
            return [];
        }
        $progress = $this->session->get('userProgress', []);
        $progress = $progress[$entity] ?? [];

        if (!empty($criteria)){
            $progress = array_filter($progress, function($var) use ($criteria){
                return $this->checkCriteria($var, $criteria);
            });
        }
        return $progress;
    }

    /**
     * @param $data
     * @return mixed
     */
    public function create($data)
    {
        $entity = ArrayHelper::remove($data, 'entity');
        if ($entity === null) {
            return null;
        }
        $progress = $this->session->get('userProgress', []);
        $data['success'] = $data['success'] ?? true;
        $progress[$entity][] = $data;
        $this->session->set('userProgress', $progress);
        return $data;
    }

    /**
     * @param $data
     * @param $criteria
     * @return bool
     */
    public function update($data, $criteria)
    {
        $entity = ArrayHelper::remove($criteria, 'entity');
        if ($entity !== null) {
            $progress = $this->session->get('userProgress', []);
            $searchProgress = $progress[$entity] ?? [];
            $data['success'] = $data['success'] ?? true;
            if (!empty($criteria)){
                foreach ($searchProgress as $key => $value) {
                    if ($this->checkCriteria($value, $criteria)) {
                        $progress[$entity][$key] = $data;
                        $this->session->set('userProgress', $progress);
                        return true;
                    }
                }
            }
        }
        return false;
    }

    /**
     * @param $criteria
     * @return bool
     */
    public function delete($criteria)
    {
        $entity = ArrayHelper::remove($criteria, 'entity');
        if ($entity !== null) {
            $progress = $this->session->get('userProgress', []);
            $progress = $progress[$entity] ?? [];
            if (!empty($criteria)){
                foreach ($progress as $key => $value) {
                    if ($this->checkCriteria($value, $criteria)) {
                        unset($progress[$entity][$key]);
                        $this->session->set('userProgress', $progress);
                        return true;
                    }
                }
            }
        }
        return false;
    }

    /**
     * @param $data
     * @return mixed
     */
    public function createOne($data)
    {
        $criteria = array_diff_key($data, ['customDataArray' => 0, 'success' => 0]);
        $entity = ArrayHelper::remove($criteria, 'entity');
        if ($entity !== null) {
            $progress = $this->session->get('userProgress', []);
            $searchProgress = $progress[$entity] ?? [];
            $data['success'] = $data['success'] ?? true;
            if (!empty($criteria)){
                foreach ($searchProgress as $key => $value) {
                    if ($this->checkCriteria($value, $criteria)) {
                        $progress[$entity][$key] = $data;
                        $this->session->set('userProgress', $progress);
                        return true;
                    }
                }
            }
            $progress[$entity][] = $data;
            $this->session->set('userProgress', $progress);
            return true;
        }
        return false;
    }

    /**
     * @param $item
     * @param $criteria
     * @return bool
     */
    private function checkCriteria($item, $criteria){
        foreach ($criteria as $key => $condition) {
            if (is_array($condition)) {
                if (!in_array($item[$key] ?? null, $condition)) {
                    return false;
                }
            }
            else {
                if (($item[$key] ?? null) != $condition) {
                    return false;
                }
            }
        }
        return true;
    }
}