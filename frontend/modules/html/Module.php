<?php

namespace frontend\modules\html;

use yii\base\BootstrapInterface;

/**
 * Class Module
 * @package frontend\modules\instance
 */
class Module extends \yii\base\Module implements BootstrapInterface
{
    /**
     * {@inheritdoc}
     */
    public $controllerNamespace = 'frontend\modules\html\controllers';

    /**
     * @param \yii\base\Application $app
     */
    public function bootstrap($app)
    {

    }
}
