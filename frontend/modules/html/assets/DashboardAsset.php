<?php
/**
 * Created by PhpStorm.
 * User: Алёнка
 * Date: 19.03.2020
 * Time: 12:58
 */

namespace frontend\modules\html\assets;

use frontend\assets\CommonAsset;
use yii\web\AssetBundle;

/**
 * Class DashboardAsset
 * @package frontend\modules\html\assets
 */
class DashboardAsset extends AssetBundle
{
    public $sourcePath = '@frontend/modules/html/web';
    public $js = [
    ];
    public $css = [
        'css/site/dashboard.css',
        'css/vendor/courses-icon.css'
    ];
    public $depends = [
        CommonAsset::class
    ];
}