<?php
/**
 * Created by PhpStorm.
 * User: Алёнка
 * Date: 12.03.2020
 * Time: 10:26
 */

namespace frontend\modules\html\assets;

use frontend\assets\CommonAsset;
use yii\web\AssetBundle;

/**
 * Class CourseAsset
 * @package frontend\modules\html\assets
 */
class CourseAsset extends AssetBundle
{
    public $sourcePath = '@frontend/modules/html/web';
    public $js = [
    ];
    public $css = [
        'css/site/course.css',
        'css/vendor/courses-icon.css'
    ];
    public $depends = [
        CommonAsset::class
    ];
}