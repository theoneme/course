<?php
/**
 * Created by PhpStorm.
 * User: Единоличница млин
 * Date: 19.03.2020
 * Time: 17:09
 */

namespace frontend\modules\html\assets;

use frontend\assets\CommonAsset;
use yii\web\AssetBundle;

/**
 * Class ProfileAsset
 * @package frontend\modules\html\assets
 */
class ProfileAsset extends AssetBundle
{
    public $sourcePath = '@frontend/modules/html/web';
    public $js = [
    ];
    public $css = [
        'css/site/profile.css',
        'css/vendor/courses-icon.css'
    ];
    public $depends = [
        CommonAsset::class
    ];
}