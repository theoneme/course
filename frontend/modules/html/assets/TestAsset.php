<?php
/**
 * Created by PhpStorm.
 * User: Алёнка
 * Date: 12.03.2020
 * Time: 10:31
 */

namespace frontend\modules\html\assets;

use frontend\assets\CommonAsset;
use yii\web\AssetBundle;

/**
 * Class TestAsset
 * @package frontend\modules\html\assets
 */
class TestAsset extends AssetBundle
{
    public $sourcePath = '@frontend/modules/html/web';
    public $js = [
    ];
    public $css = [
        'css/site/test.css',
        'css/vendor/courses-icon.css'
    ];
    public $depends = [
        CommonAsset::class
    ];
}