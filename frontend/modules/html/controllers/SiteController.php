<?php

namespace frontend\modules\html\controllers;

use frontend\controllers\FrontEndController;

/**
 * Class SiteController
 * @package frontend\modules\html\controllers
 */
class SiteController extends FrontEndController
{
    /**
     * 
     */
    public function actionIndex($view)
    {
        $this->layout = $view;

        return $this->render($view);
    }
}