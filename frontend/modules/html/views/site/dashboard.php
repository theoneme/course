<?php
$bundle = $this->getAssetManager()->getBundle(\frontend\modules\html\assets\DashboardAsset::class)->baseUrl;
?>
<div class="wrapper-dashboard p-3 mx-auto">
    <section class="w-100 border rounded bg-white">
        <div class="course-block border-bottom">
                <div class="d-flex p-4">
                    <div class="img-course pr-3 d-none d-sm-block d-md-block d-lg-block">
                        <img class="w-100" src="<?= $bundle?>/images/board.png" title="Математика" alt="Математика">
                    </div>
                    <div class="">
                        <h3 class="font-weight-bold dashboard-course-name my-2">
                            <a href="#" class="course-name">Математика <i class="i-down-arrow d-inline-block ml-2"></i></a>
                        </h3>
                        <div class="lessons-list">
                            <div class="lesson-progress done position-relative font-weight-bold d-flex align-items-center">
                                <div class="circle-progress position-relative bg-white rounded-circle d-flex align-items-center justify-content-around mr-3">
                                    <i class="i-correct"></i>
                                </div>
                                <div class="">Формулы</div>
                            </div>
                            <div class="lesson-progress done position-relative font-weight-bold d-flex align-items-center">
                                <div class="circle-progress position-relative bg-white rounded-circle d-flex align-items-center justify-content-around mr-3">
                                    <i class="i-correct"></i>
                                </div>
                                <div class="">Логарифмы</div>
                            </div>
                            <div class="lesson-progress position-relative font-weight-bold d-flex align-items-center">
                                <div class="circle-progress position-relative bg-white rounded-circle d-flex align-items-center justify-content-around mr-3">
                                    3
                                </div>
                                <div class="">Теория вероятности</div>
                            </div>
                            <div class="lesson-progress done position-relative font-weight-bold d-flex align-items-center">
                                <div class="circle-progress position-relative bg-white rounded-circle d-flex align-items-center justify-content-around mr-3">
                                    <i class="i-correct"></i>
                                </div>
                                <div class="">Логарифмы</div>
                            </div>
                            <div class="lesson-progress position-relative font-weight-bold d-flex align-items-center">
                                <div class="circle-progress position-relative bg-white rounded-circle d-flex align-items-center justify-content-around mr-3">
                                    <i class="i-correct"></i>
                                </div>
                                <div class="">Формулы</div>
                            </div>
                        </div>
                    </div>
                </div>

            <a href="#" class="button orange rounded-0 w-100 d-block text-center font-big">Продожить изучение</a>
        </div>
    </section>
</div>