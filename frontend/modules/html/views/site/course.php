

    <div class="center-block overflow-hidden">
        <div class="d-flex align-items-center overflow-y-auto p-3 h-100 w-100">
            <section class="wrapper-course mx-auto w-100">
                <div class="d-flex justify-content-between">
                    <div>
                        <h1 class="h1-course">Basic plots with matplotlib</h1>
                    </div>
                    <div class="d-flex align-items-center">
                        <div class="badge bg-white color-black py-2 px-3 rounded-pill font-weight-normal tag-xp letter-spacing-02">50 XP</div>
                    </div>
                </div>
                <div class="py-3 video">
                    <video width="100%" height="auto" controls="controls">
                        <source src="/uploads/video/course.mp4">
                    </video>
                </div>
                <div class="w-100 d-flex justify-content-end">
                    <a href="#" class="button orange small">Got it!</a>
                </div>
            </section>
        </div>
    </div>

    <div class="progress-bar-lesson position-absolute px-3 d-flex w-100 justify-content-around">
        <div class="progress custom-bar mw-100 pop-top" data-toggle="popover" data-placement="top" data-content="25%">
            <div class="progress-bar" role="progressbar" style="width: 25%;" aria-valuenow="25" aria-valuemin="0" aria-valuemax="100"></div>
        </div>
    </div>
