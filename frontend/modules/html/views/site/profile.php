<h1 class="text-left px-3 px-md-5">Редактирование профиля</h1>
<div class="wizard-content w-100 p-3 p-md-5 p-lg-5">
    <form>
        <div class="row">
            <div class="col-12 col-lg-8">
                <div class="wizards-steps position-relative" style="position: relative;">
                    <section class="py-5" id="step1">
                        <div class="wizard-step-header">
                            Контактные данные <span>Шаг 1 из 6</span>
                        </div>
                        <div class="row">
                            <div class="col-12 col-lg-8">
                                <div class="pop-note" data-toggle="popover" data-placement="right"
                                     data-title="Телефон" data-content="Укажите ваш телефон">
                                    <div class="form-group field-userform-phone has-success">
                                        <div class="row">
                                            <div class="col-12 col-lg-4">
                                                <label class="control-label" for="userform-phone">Телефон</label>
                                            </div>
                                            <div class="col-12 col-lg-8">
                                                <input type="text" id="userform-phone" class="form-control"
                                                       name="UserForm[phone]" aria-invalid="false">
                                                <div class="help-block"></div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="pop-note" data-toggle="popover" data-placement="right"
                                     data-title="Описание профиля" data-content="Расскажите немного о себе">
                                    <div class="form-group form-group-disabled field-userform-email">
                                        <div class="row">
                                            <div class="col-12 col-lg-4">
                                                <label class="control-label" for="userform-email">Email</label>
                                            </div>
                                            <div class="col-12 col-lg-8">

                                                <input type="text" id="userform-email" class="form-control"
                                                       name="UserForm[email]" value="naughty-child@mail.ru" disabled="">
                                                <div class="help-block"></div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="pop-note" data-toggle="popover" data-placement="right"
                                     data-title="Имя пользователя" data-content="Укажите ваше имя">
                                    <div class="form-group field-userform-username required has-success">
                                        <div class="row">
                                            <div class="col-12 col-lg-4">
                                                <label class="control-label" for="userform-username">Имя
                                                    пользователя</label>
                                            </div>
                                            <div class="col-12 col-lg-8">

                                                <input type="text" id="userform-username" class="form-control"
                                                       name="UserForm[username]" value="Helen1" aria-required="true"
                                                       aria-invalid="false">
                                                <div class="help-block"></div>
                                            </div>
                                        </div>
                                    </div>
                                </div>

                            </div>
                        </div>
                    </section>
                    <section class="py-5" id="step2">
                        <div class="wizard-step-header">
                            Дополнительная контактная информация <span>Шаг 2 из 6</span>
                        </div>
                        <div class="row">
                            <div class="col-12 col-lg-8">

                                <div data-role="dynamic-contacts-container">
                                    <div class="row" data-role="contact-item">
                                        <div class="col-12 col-sm-5">
                                            <div class="form-group field-contactform-0-type required has-success validating">

                                                <select id="contactform-0-type" class="form-control" name="ContactForm[0][type]" aria-invalid="false">
                                                    <option value="">-- Выберите тип контакта</option>
                                                    <option value="0">Email</option>
                                                    <option value="10">Телефон</option>
                                                    <option value="30">Веб-сайт</option>
                                                    <option value="100">Skype</option>
                                                    <option value="110">WhatsApp</option>
                                                    <option value="120">Viber</option>
                                                    <option value="130">Telegram</option>
                                                    <option value="200">Facebook</option>
                                                    <option value="210">Vk</option>
                                                    <option value="220">Twitter</option>
                                                    <option value="230">Google</option>
                                                    <option value="240">Youtube</option>
                                                    <option value="250">Одноклассники</option>
                                                </select>

                                                <div class="help-block"></div>
                                            </div>
                                        </div>
                                        <div class="col-10 col-sm-6">
                                            <div class="form-group field-contactform-0-value required has-success">

                                                <input type="text" id="contactform-0-value" class="form-control" name="ContactForm[0][value]" placeholder="Введите контактные данные" aria-invalid="false">

                                                <div class="help-block"></div>
                                            </div>
                                        </div>
                                        <div class="col-2 col-sm-1">
                                            <a href="#" data-action="remove-contact"><i class="i-close"></i></a>            </div>
                                    </div>
                                </div>

                                <a href="#" data-action="add-new-contact"><i class="icon-plus-black-symbol"></i>&nbsp;Добавить
                                    новый контакт</a>

                            </div>
                        </div>
                    </section>
                    <section class="py-5" id="step3">
                        <div class="wizard-step-header">
                            Основная информация <span>Шаг 3 из 6</span>
                        </div>
                        <div class="row">
                            <div class="col-12 col-lg-8">

                                <p>
                                    Загрузите вашу фотографию</p>
                                <div>
                                    <div class="pop-note" data-toggle="popover" data-placement="right"
                                         data-title="Фотографие профиоя" data-content="Загрузите вашу фотографию">
                                    </div>
                                </div>
                                <div class="images-container">
                                    <div class="form-group field-userform-avatar has-success">
                                        <input type="hidden" id="userform-avatar" class="form-control"
                                               name="UserForm[avatar]"
                                               value=""
                                               data-key="" aria-invalid="false">
                                    </div>
                                </div>


                                <div class="pop-note" data-toggle="popover" data-placement="right"
                                     data-title="Краткое описание профиля"
                                     data-content="Укажите краткое описание профиля">
                                    <div class="form-group field-metaform-subtitle has-success">
                                        <div class="row">
                                            <div class="col-12 col-lg-4">
                                                <label class="control-label" for="metaform-subtitle">Краткое
                                                    описание</label>
                                            </div>
                                            <div class="col-12 col-lg-8">

                                            <textarea id="metaform-subtitle" class="form-control"
                                                      name="MetaForm[subtitle]" rows="4"
                                                      placeholder="Краткое описание профиля"
                                                      aria-invalid="false"></textarea>
                                                <div class="help-block"></div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="pop-note" data-toggle="popover" data-placement="right"
                                     data-title="Описание профиля" data-content="Расскажите немного о себе">
                                    <div class="form-group field-metaform-description has-success">
                                        <div class="row">
                                            <div class="col-12 col-lg-4">
                                                <label class="control-label" for="metaform-description">Описание</label>
                                            </div>
                                            <div class="col-12 col-lg-8">

                                            <textarea id="metaform-description" class="form-control"
                                                      name="MetaForm[description]" rows="8"
                                                      placeholder="Описание профиля" aria-invalid="false"></textarea>
                                                <div class="help-block"></div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </section>
                    <section class="py-5" id="step4">
                        <div class="wizard-step-header">
                            Настройки местоположения <span>Шаг 4 из 6</span>
                        </div>
                        <div class="row">
                            <div class="col-12 col-lg-8">


                                <div class="pop-note" data-toggle="popover" data-placement="right"
                                     data-title="Адрес" data-content="Укажите адрес">

                                    <div class="form-group field-gmaps-input-address has-success">
                                        <div class="row">
                                            <div class="col-12 col-lg-4">
                                                <label class="control-label" for="gmaps-input-address">Адрес</label>
                                            </div>
                                            <div class="col-12 col-lg-8">

                                                <input type="text" id="gmaps-input-address" class="form-control"
                                                       name="GeoForm[address]" value="" placeholder="Введите адрес"
                                                       autocomplete="off" aria-invalid="false">
                                                <div class="help-block"></div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="form-group field-gmaps-input-lat has-success">
                                        <input type="hidden" id="gmaps-input-lat" class="form-control"
                                               name="GeoForm[lat]"
                                               value="0.00000000" aria-invalid="false">
                                        <div class="help-block"></div>
                                    </div>
                                    <div class="form-group field-gmaps-input-lng has-success">
                                        <input type="hidden" id="gmaps-input-lng" class="form-control"
                                               name="GeoForm[lng]"
                                               value="0.00000000" aria-invalid="false">
                                        <div class="help-block"></div>
                                    </div>
                                </div>
                                <div id="google-map" style="height: 300px; position: relative; overflow: hidden;">

                                </div>


                            </div>
                        </div>
                    </section>
                    <section class="py-5" id="step5">
                        <div class="wizard-step-header">
                            Настройки пароля <span>Шаг 5 из 6</span>
                        </div>
                        <div class="row">
                            <div class="col-12 col-lg-8">

                                <p>Если вы хотите изменить свой пароль, можете сделать это здесь</p>

                                <div class="pop-note" data-toggle="popover" data-placement="right"
                                     data-title="Пароль" data-content="Укажите пароль">
                                    <div class="form-group field-passwordform-password has-success">
                                        <div class="row">
                                            <div class="col-12 col-lg-4">
                                                <label class="control-label" for="passwordform-password">Новый
                                                    пароль</label>
                                            </div>
                                            <div class="col-12 col-lg-8">

                                                <input type="password" id="passwordform-password" class="form-control"
                                                       name="PasswordForm[password]" aria-invalid="false">
                                                <div class="help-block"></div>
                                            </div>
                                        </div>
                                    </div>
                                </div>

                                <div class="pop-note" data-toggle="popover" data-placement="right"
                                     data-title="Подтверждение пароля" data-content="Введите подтверждение пароля">
                                    <div class="form-group field-passwordform-confirm has-success">
                                        <div class="row">
                                            <div class="col-12 col-lg-4">
                                                <label class="control-label" for="passwordform-confirm">Подтверждение
                                                    пароля</label>
                                            </div>
                                            <div class="col-12 col-lg-8">

                                                <input type="password" id="passwordform-confirm" class="form-control"
                                                       name="PasswordForm[confirm]" aria-invalid="false">
                                                <div class="help-block"></div>
                                            </div>
                                        </div>
                                    </div>
                                </div>

                            </div>
                        </div>
                    </section>
                    <section class="py-5" id="step6">
                        <div class="wizard-step-header">
                            Настройки социальных сетей <span>Шаг 6 из 6</span>
                        </div>
                        <div class="row">
                            <div class="col-12 col-lg-8">

                                <div class="pop-note pb-4" data-toggle="popover" data-placement="right"
                                     data-title="Присоединить или отсоединить учетную запись facebook"
                                     data-content="Если вы присоедините аккаунт социальной сети к данному, то вы сможете авторизовываться на этом сайте через выбранную социальную сеть.">
                                        <a class="button fb d-flex align-items-center" href="#">
                                            <i class="i-facebook"></i>&nbsp;Присоединить учетную запись Facebook
                                        </a>
                                </div>
                                <div class="pop-note pb-4" data-toggle="popover" data-placement="right"
                                     data-title="Присоединить или отсоединить учетную запись google"
                                     data-content="Если вы присоедините аккаунт социальной сети к данному, то вы сможете авторизовываться на этом сайте через выбранную социальную сеть.">
                                        <a class="button google d-flex align-items-center" href="#">
                                            <i class="i-google"></i>&nbsp;Присоединить учетную запись Google
                                        </a>
                                </div>
                            </div>
                        </div>
                    </section>
                    <div class="text-center">
                        <a class="button blue mr-4" href="#">Назад</a>
                        <button type="submit" class="button orange">Сохранить изменения</button>
                    </div>
                </div>
            </div>

            <div class="col-12 col-lg-4 d-none d-md-block ">
                <div class="position-sticky progress-menu-nav">
                    <div id="progress-menu" class="nav no-list progress-menu list-group">
                        <a href="#step1" class="active list-group-item">
                            Контактные данные
                            <div class="progress-error">1 ошибка</div>
                        </a>
                        <a href="#step2" class="list-group-item">
                            Дополнительная контактная информация
                            <div class="good-mark">✔</div>
                        </a>
                        <a href="#step3" class="list-group-item">
                            Основная информация
                            <div class="good-mark">✔</div>
                        </a>
                        <a href="#step4" class="list-group-item">
                            Настройки местоположения
                            <div class="good-mark">✔</div>
                        </a>
                        <a href="#step5" class="list-group-item">
                            Настройки пароля
                            <div class="good-mark">✔</div>
                        </a>
                        <a href="#step6" class="list-group-item">
                            Настройки социальных сетей
                            <div class="good-mark">✔</div>
                        </a>
                    </div>

                    <div class="text-center">
                        <button type="submit" class="button orange">Сохранить изменения</button>
                    </div>
                </div>
            </div>
        </div>
    </form>
</div>
