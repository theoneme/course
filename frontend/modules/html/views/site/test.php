
<?php

frontend\modules\html\assets\TestAsset::register($this);

?>

    <div class=p-3">
        <div class="d-flex align-items-center overflow-y-auto h-100 w-100">
            <section class="wrapper-test mx-auto w-100 border rounded bg-white py-4">
                <div class="p-4">
                    <h1 class="h1-test font-weight-bold">Which of these is a classification problem?</h1>
                    <div>
                       <img src="https://i.ytimg.com/vi/f9VOMSB1PMM/maxresdefault.jpg">
                    </div>
                </div>
                <div class="d-flex bg-grey justify-content-between bg-light-blue grey-box px-4 py-2">
                    <div class="d-flex align-items-center">
                        <div class="mr-3">
                            <i class="i-verified font-weight-bold"></i>
                        </div>
                        <div>
                            <h5 class="font-weight-bold">Answer the question</h5>
                        </div>
                    </div>
                    <div class="d-flex align-items-center">
                        <div class="badge badge-light bg-white py-2 px-3 rounded-pill font-weight-normal tag-xp letter-spacing-02">50 XP</div>
                    </div>
                </div>
                <div class="p-4">
                    <h5 class="font-weight-bold py-2">Possible Answers</h5>
                    <div>
                        <div class="d-flex justify-content-between p-2">
                            <div class="d-flex pr-2 align-items-center">
                                <input type="radio" id="answer1" name="answer" value="answer1" checked="checked">
                                <label for="answer1"><span class="cr-text">Using labeled financial data to predict whether the value of a stock will go up or go down next week.</span></label>
                            </div>
                            <div class="d-flex align-items-center">
                                press
                                <div class="badge text-white bg-black p-2 ml-3 font-weight-normal tag-button">1</div>
                            </div>
                        </div>
                        <div class="d-flex justify-content-between p-2">
                            <div class="d-flex pr-2 align-items-center">
                                <input type="radio" id="answer2" name="answer" value="answer2" checked="">
                                <label for="answer2"><span class="cr-text">Using labeled housing price data to predict the price of a new house based on various features.</span></label>
                            </div>
                            <div class="d-flex align-items-center">
                                press
                                <div class="badge text-white bg-black p-2 ml-3 font-weight-normal tag-button cursor-pointer">2</div>
                            </div>
                        </div>
                        <div class="d-flex justify-content-between p-2">
                            <div class="d-flex pr-2 align-items-center">
                                <input type="radio" id="answer3" name="answer" value="answer3" checked="">
                                <label for="answer3"><span class="cr-text">Using unlabeled data to cluster the students of an online education company into different categories based on their learning styles.</span></label>
                            </div>
                            <div class="d-flex align-items-center">
                                press
                                <div class="badge text-white bg-black p-2 ml-3 font-weight-normal tag-button">3</div>
                            </div>
                        </div>
                        <div class="d-flex justify-content-between p-2">
                            <div class="d-flex pr-2 align-items-center">
                                <input type="radio" id="answer4" name="answer" value="answer4" checked="">
                                <label for="answer4"><span class="cr-text">Using labeled financial data to predict what the value of a stock will be next week.</span></label>
                            </div>
                            <div class="d-flex align-items-center">
                                press
                                <div class="badge text-white bg-black p-2 ml-3 font-weight-normal tag-button">4</div>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="p-4">
                    <h5 class="font-weight-bold py-2">Answer</h5>
                    <input class="form-control" type="text" placeholder="Введите ответ">
                    <div class="d-flex my-4 justify-content-end align-items-center">
                        <div class="d-flex mr-3">Времени прошло: <div id="timeStart"></div></div>
                        <div class="d-flex mr-3">Времени осталось: <div id="timeEnd"></div></div>
                        <a href="#" class="button orange small pause" data-toggle="modal" data-backdrop="static" data-keyboard="false" data-target="#pause">Пауза</a>
                    </div>
                </div>
                <div class="d-flex flex-wrap justify-content-between pt-4 pl-4 pr-4">
                    <div class="d-flex align-items-end take-hint font-weight-bold pr-3">
                        <a class="ml-2 p-2 hint" href="#hint" data-toggle="collapse" role="button" aria-expanded="false">
                            <i class="i-lamp-bulb p-2 color-blue font-weight-bold rounded-top"></i>
                            <span class="hint-text">Take hint (-15xp)</span>
                        </a>
                    </div>
                    <div class="d-flex align-items-center ml-auto mb-2">
                        <a href="#" class="button blue" data-toggle="modal" data-target="#finish-test" data-backdrop="false">Submit Answer</a>
                    </div>
                </div>
                <div class="pb-3 pl-3 pr-3 collapse" id="hint">
                    <div class="p-2 bg-light-blue p-3 collapse-hint">
                        <h6 class="font-weight-bold py-2">Hint</h6>
                        <p class="">
                            Anim pariatur cliche reprehenderit, enim eiusmod high life accusamus terry richardson ad squid. Nihil anim keffiyeh helvetica, craft beer labore wes anderson cred nesciunt sapiente ea proident. Anim pariatur cliche reprehenderit, enim eiusmod high life accusamus terry richardson ad squid. Nihil anim keffiyeh helvetica, craft beer labore wes anderson cred nesciunt sapiente ea proident.
                        </p>
                    </div>
                </div>
            </section>
        </div>
    </div>

