<?php

use yii\helpers\Html;
use yii\web\View;

/**
 * @var View $this
 * @var string $content
 */

frontend\modules\html\assets\CourseAsset::register($this);
$bundle = $this->getAssetManager()->getBundle(\frontend\modules\html\assets\CourseAsset::class)->baseUrl;

?>
<?php $this->beginPage() ?>
<!DOCTYPE html>
<html lang="<?= Yii::$app->language ?>">
<head>
    <meta charset="<?= Yii::$app->charset ?>">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no">
    <link rel="icon"
          href=""
          type="image/png"/>
    <link rel="shortcut icon"
          href=""
          type="image/png"/>
    <?= Html::csrfMetaTags() ?>
    <title><?= Html::encode($this->title) ?></title>

    <?php $this->head() ?>
</head>
<body>
<?php $this->beginBody() ?>
<div class="main-block">
    <header class="d-flex flex-wrap justify-content-between p-3">
        <a href="/" class="d-flex align-items-center logo pb-2 order-1">
            <img src="<?= $bundle?>/images/logo.png" alt="ЕГЭ-ОНЛАЙН" title="ЕГЭ-ОНЛАЙН">
            <div class="px-2">ЕГЭ-ОНЛАЙН</div>
        </a>
        <nav class="nav-course d-flex justify-content-between bg-white border rounded p-0 order-3 order-sm-2 order-md-2 order-xl-2">
            <a href="#" class="pop-bottom h-100 d-flex align-items-center border-right p-3"  data-toggle="popover" data-placement="bottom" data-content="Предыдущее упражнение">
                <i class="i-back"></i>
            </a>
            <a  href="#" data-toggle="modal" data-target="#lessons" class="h-100 d-flex align-items-center h-100">
                <i class="i-menu color-blue pr-2"></i>
                Courses Outline
            </a>
            <a href="#" class="pop-bottom h-100 d-flex align-items-center border-left p-3" data-toggle="popover" data-placement="bottom" data-content="Следующее упражнение">
                <i class="i-next"></i>
            </a>
        </nav>
        <div class="d-flex align-items-center order-2 order-sm-3 order-md-3 order-xl-3">
            <a href="#" data-toggle="popover" data-placement="left" data-content="Показать видео" class="pop-left btn-top p-2 d-inline-flex align-items-center">
                <i class="i-film color-grey"></i>
            </a>
            <a href="#" data-toggle="modal" data-placement="left" data-target="#feedback" data-content="Напишите отзыв" class="pop-left btn-top p-2 d-inline-flex align-items-center">
                <i class="i-info color-grey"></i>
            </a>
        </div>

    </header>

    <?= $content ?>
</div>

<div id="lessons" class="modal modal-standart modal-lessons" tabindex="-1" role="dialog">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h3 class="modal-title">Предмет</h3>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <i class="i-close"></i>
                </button>
            </div>
            <div class="modal-body">
                <div class="border border-light-blue">
                    <div class="p-3">
                        <div class="d-none justify-content-between">
                            <div class="d-flex">
                                <div class="badge rounded-circle bg-blue tag-circle">
                                    1
                                </div>
                                <h4 class="font-weight-bold mx-2">Математика</h4>
                                <div class="badge bg-orange color-black tag-free">
                                    FREE
                                </div>
                            </div>
                            <div class="d-flex">
                                <div class="mr-2 color-blue">
                                    6%
                                </div>
                                <div class="progress custom-bar">
                                    <div class="progress-bar" role="progressbar" style="width: 6%" aria-valuenow="6" aria-valuemin="0" aria-valuemax="100"></div>
                                </div>
                            </div>
                        </div>
                        <p>
                            Data visualization is a key skill for aspiring data scientists. Matplotlib makes it easy to create meaningful and insightful plots. In this chapter, you’ll learn how to build various types of plots, and customize them to be more visually appealing and interpretable.
                        </p>
                    </div>
                    <div>
                        <div class="lesson done py-3 px-4">
                            <a href="#" class="d-flex justify-content-between">
                                <div class="pr-2 d-flex align-items-center">
                                    <div class="badge-course bg-blue color-white d-flex rounded-circle mr-2 align-items-center justify-content-center">
                                        <i class="i-exam"></i>
                                    </div>
                                    Теория вероятности
                                </div>
                                <div class="d-flex align-items-center"><i class="result-icon i-correct mr-2"></i>50 XP</div>
                            </a>
                        </div>
                        <div class="lesson py-3 px-4">
                            <a href="#" class="d-flex justify-content-between">
                                <div class="pr-2 d-flex align-items-center">
                                    <div class="badge-course bg-blue color-white d-flex rounded-circle mr-2 align-items-center justify-content-center">
                                        <i class="i-video"></i>
                                    </div>Пределы</div>
                                <div class="d-flex align-items-center"><i class="result-icon i-minus mr-2"></i>50 XP</div>
                            </a>
                        </div>
                        <div class="lesson py-3 px-4">
                            <a href="#" class="d-flex justify-content-between">
                                <div class="pr-2">Теорема Кронекера-Капелли</div>
                                <div class="d-flex"><i class="i-correct mr-2"></i>100 XP</div>
                            </a>
                        </div>
                        <div class="lesson py-3 px-4">
                            <a href="#" class="d-flex justify-content-between">
                                <div class="pr-2">Матрица</div>
                                <div class="d-flex"><i class="i-correct mr-2"></i>50 XP</div>
                            </a>
                        </div>
                        <div class="lesson py-3 px-4">
                            <a href="#" class="d-flex justify-content-between">
                                <div class="pr-2">Интегралы</div>
                                <div class="d-flex"><i class="i-correct mr-2"></i>150 XP</div>
                            </a>
                        </div>
                        <div class="lesson py-3 px-4">
                            <a href="#" class="d-flex justify-content-between">
                                <div class="pr-2">Косинус, Синус</div>
                                <div class="d-flex"><i class="i-correct mr-2"></i>50 XP</div>
                            </a>
                        </div>
                        <div class="lesson py-3 px-4">
                            <a href="#" class="d-flex justify-content-between">
                                <div class="pr-2">Формулы</div>
                                <div class="d-flex"><i class="i-correct mr-2"></i>50 XP</div>
                            </a>
                        </div>
                        <div class="lesson done py-3 px-4">
                            <a href="#" class="d-flex justify-content-between">
                                <div class="pr-2">Теория вероятности</div>
                                <div class="d-flex"><i class="i-correct mr-2"></i>50 XP</div>
                            </a>
                        </div>
                        <div class="lesson py-3 px-4">
                            <a href="#" class="d-flex justify-content-between">
                                <div class="pr-2">Логарифмы</div>
                                <div class="d-flex"><i class="i-correct mr-2"></i>100 XP</div>
                            </a>
                        </div>
                        <div class="lesson py-3 px-4">
                            <a href="#" class="d-flex justify-content-between">
                                <div class="pr-2">Пределы</div>
                                <div class="d-flex"><i class="i-correct mr-2"></i>50 XP</div>
                            </a>
                        </div>
                        <div class="lesson py-3 px-4">
                            <a href="#" class="d-flex justify-content-between">
                                <div class="pr-2">Теорема Кронекера-Капелли</div>
                                <div class="d-flex"><i class="i-correct mr-2"></i>100 XP</div>
                            </a>
                        </div>
                        <div class="lesson py-3 px-4">
                            <a href="#" class="d-flex justify-content-between">
                                <div class="pr-2">Матрица</div>
                                <div class="d-flex"><i class="i-correct mr-2"></i>50 XP</div>
                            </a>
                        </div>
                        <div class="lesson py-3 px-4">
                            <a href="#" class="d-flex justify-content-between">
                                <div class="pr-2">Интегралы</div>
                                <div class="d-flex"><i class="i-correct mr-2"></i>150 XP</div>
                            </a>
                        </div>
                        <div class="lesson py-3 px-4">
                            <a href="#" class="d-flex justify-content-between">
                                <div class="pr-2">Косинус, Синус</div>
                                <div class="d-flex"><i class="i-correct mr-2"></i>50 XP</div>
                            </a>
                        </div>
                        <div class="lesson py-3 px-4">
                            <a href="#" class="d-flex justify-content-between">
                                <div class="pr-2">Формулы</div>
                                <div class="d-flex"><i class="i-correct mr-2"></i>50 XP</div>
                            </a>
                        </div>
                    </div>
                </div>

            </div>
            <div class="modal-footer position-absolute w-100 bg-white p-3 d-flex justify-content-center">
                <a href="test.html" class="d-flex align-items-center font-weight-bold mr-5" data-dismiss="modal"><i class="i-left color-orange mr-2 rem-12"></i> Вернуться</a>
                <button type="button" class="d-flex align-items-center btn btn-link link font-weight-bold"><i class="i-reload font-weight-bold color-orange mr-2 rem-12"></i>Сбросить прогресс</button>
            </div>
        </div>
    </div>
</div>

<div id="feedback" class="modal modal-standart modal-lessons" tabindex="-1" role="dialog">
    <div class="modal-dialog p-4" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h3 class="modal-title">Напишите отзыв</h3>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <i class="i-close"></i>
                </button>
            </div>
            <div class="modal-body">
                <div class="form-group mt-4">
                    <label class="font-weight-bold" for="reason">Причина отзыва</label>
                    <select type="password" class="form-control" id="reason">
                        <option value=""></option>
                        <option value="video-broken">Видео не работает правильно</option>
                        <option value="typo-video">Не то видео</option>
                        <option value="other">Другая</option>
                    </select>
                </div>
                <div class="form-group mt-4">
                    <label class="font-weight-bold" for="message">Сообщение отзыва</label>
                    <textarea id="message" class="form-control"></textarea>
                </div>
            </div>
            <div class="modal-footer position-absolute w-100 bg-white p-3 d-flex justify-content-between">
                <button type="button" data-dismiss="modal" class="button blue font-weight-bold"> Отмена</button>
                <button type="submit" class="button orange font-weight-bold">Отправить</button>
            </div>
        </div>
    </div>
</div>


<?php $this->endBody() ?>

<script>
    $('.pop-left').popover({
        trigger: 'hover',
        template: '<div class="popover blue"><div class="arrow"></div><div class="popover-body"></div></div>'
    });
    $('.pop-bottom').popover({
        trigger: 'hover',
        template: '<div class="popover blue"><div class="arrow"></div><div class="popover-body"></div></div>'
    });
    $('.pop-top').popover({
        trigger: 'hover',
        template: '<div class="popover blue"><div class="arrow"></div><div class="popover-body"></div></div>'
    });
</script>
</body>
</html>
<?php $this->endPage() ?>

