<?php

use yii\helpers\Html;
use yii\web\View;

/**
 * @var View $this
 * @var string $content
 */

frontend\modules\html\assets\DashboardAsset::register($this);
$bundle = $this->getAssetManager()->getBundle(\frontend\modules\html\assets\DashboardAsset::class)->baseUrl;

?>
<?php $this->beginPage() ?>
<!DOCTYPE html>
<html lang="<?= Yii::$app->language ?>">
<head>
    <meta charset="<?= Yii::$app->charset ?>">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no">
    <link rel="icon"
          href=""
          type="image/png"/>
    <link rel="shortcut icon"
          href=""
          type="image/png"/>
    <?= Html::csrfMetaTags() ?>
    <title><?= Html::encode($this->title) ?></title>

    <?php $this->head() ?>
</head>
<body>
<?php $this->beginBody() ?>
<div class="main-block">
    <header class="d-flex flex-wrap justify-content-between p-3">
        <a href="/" class="d-flex align-items-center logo pb-2 order-1">
            <img src="<?= $bundle?>/images/logo.png" alt="ЕГЭ-ОНЛАЙН" title="ЕГЭ-ОНЛАЙН">
            <div class="px-2">ЕГЭ-ОНЛАЙН</div>
        </a>
        <div class="d-flex align-items-center order-2 order-sm-3 order-md-3 order-xl-3">
            <a href="#" data-toggle="popover" data-placement="left" data-content="Перейти к тесту" class="pop-left btn-top p-2 d-inline-flex align-items-center">
                <i class="i-exam color-grey"></i>
            </a>
            <a href="#" data-toggle="popover" data-placement="left" data-content="Показать видео" class="pop-left btn-top p-2 d-inline-flex align-items-center">
                <i class="i-film color-grey"></i>
            </a>
            <a href="#" data-toggle="modal" data-placement="left" data-target="#feedback" data-content="Напишите отзыв" class="pop-left btn-top p-2 d-inline-flex align-items-center">
                <i class="i-info color-grey"></i>
            </a>
        </div>

    </header>

    <?= $content ?>
</div>


<div id="feedback" class="modal modal-standart modal-lessons" tabindex="-1" role="dialog">
    <div class="modal-dialog p-4" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h3 class="modal-title">Напишите отзыв</h3>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <i class="i-close"></i>
                </button>
            </div>
            <div class="modal-body">
                <div class="form-group mt-4">
                    <label class="font-weight-bold" for="reason">Причина отзыва</label>
                    <select type="password" class="form-control" id="reason">
                        <option value=""></option>
                        <option value="video-broken">Видео не работает правильно</option>
                        <option value="typo-video">Не то видео</option>
                        <option value="other">Другая</option>
                    </select>
                </div>
                <div class="form-group mt-4">
                    <label class="font-weight-bold" for="message">Сообщение отзыва</label>
                    <textarea id="message" class="form-control"></textarea>
                </div>
            </div>
            <div class="modal-footer position-absolute w-100 bg-white p-3 d-flex justify-content-between">
                <button type="button" data-dismiss="modal" class="button blue font-weight-bold"> Отмена</button>
                <button type="submit" class="button orange font-weight-bold">Отправить</button>
            </div>
        </div>
    </div>
</div>


<?php $this->endBody() ?>
<script>
    $('.dashboard-course-name').on('click',function () {
        $(this).siblings('.lessons-list').toggleClass('open');
    })
</script>
</body>
</html>
<?php $this->endPage() ?>

