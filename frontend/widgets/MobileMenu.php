<?php

namespace frontend\widgets;

use common\dto\advanced\UserAdvancedDTO;
use common\interfaces\repositories\UserRepositoryInterface;
use common\models\User;
use common\services\CategoryTreeService;
use Yii;
use yii\base\Widget;

/**
 * Class MobileMenu
 * @package frontend\widgets
 */
class MobileMenu extends Widget
{
    /**
     * @var UserRepositoryInterface
     */
    private $_userRepository;
    /**
     * @var string
     */
    public $template = 'mobile-menu';

    /**
     * HeaderWidget constructor.
     * @param array $config
     * @param UserRepositoryInterface $userRepository
     */
    public function __construct(array $config = [], UserRepositoryInterface $userRepository)
    {
        parent::__construct($config);

        $this->_userRepository = $userRepository;
    }

    /**
     * @return string
     * @throws \yii\base\InvalidConfigException
     */
    public function run()
    {
        if (array_key_exists('profiles', Yii::$app->params['runtime'])) {
            $params = Yii::$app->params['runtime']['profiles'];
        } else {
            $categorySlugs = array_map(
                function ($var) {
                    return $var[Yii::$app->language] ?? null;
                },
                Yii::$app->cacheLayer->getCategoryAliasCache('ru-RU')
            );

            /* @var CategoryTreeService $categoryTreeService*/
            $categoryTreeService = Yii::$container->get(CategoryTreeService::class);
            $categoryTree = $categoryTreeService->getTree();

            $isGuest = Yii::$app->user->isGuest;
            $params = [
                'categorySlugs' => $categorySlugs,
                'categoryTree' => $categoryTree,
                'isGuest' => $isGuest,
            ];

            /** @var User $user */
            $user = Yii::$app->user->identity;
            if ($user !== null) {
                $userDTO = new UserAdvancedDTO($user);
                $userData = $userDTO->getData();
                $params['user'] = $userData;
            }

            Yii::$app->params['runtime']['profiles'] = $params;
        }

        return $this->render($this->template, $params);
    }
}