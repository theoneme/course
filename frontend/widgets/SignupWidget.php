<?php

namespace frontend\widgets;

use common\services\entities\UserService;
use frontend\models\auth\SignupForm;
use yii\base\Widget;

/**
 * Class SignupWidget
 * @package frontend\widgets
 */
class SignupWidget extends Widget
{
    /**
     * @var UserService
     */
    private $_userService;

    /**
     * SignupWidget constructor.
     * @param array $config
     * @param UserService $userService
     */
    public function __construct(UserService $userService, array $config = [])
    {
        parent::__construct($config);
        $this->_userService = $userService;
    }

    /**
     * @return string
     * @throws \common\exceptions\RepositoryException
     * @throws \yii\base\InvalidConfigException
     * @throws \yii\di\NotInstantiableException
     */
    public function run()
    {
        $signupForm = new SignupForm($this->_userService);

        return $this->render('signup-widget', [
            'signupForm' => $signupForm,
        ]);
    }
}