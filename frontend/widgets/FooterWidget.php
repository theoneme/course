<?php

namespace frontend\widgets;

use common\services\entities\UserService;
use yii\base\Widget;

/**
 * Class FooterWidget
 * @package frontend\widgets
 */
class FooterWidget extends Widget
{
    /**
     * @var UserService
     */
    private $_userService;
    /**
     * @var string
     */
    public $template = 'footer-widget';

    /**
     * FooterWidget constructor.
     * @param UserService $userService
     * @param array $config
     */
    public function __construct(UserService $userService, array $config = [])
    {
        parent::__construct($config);
        $this->_userService = $userService;
    }

    /**
     * @return string
     * @throws \common\exceptions\RepositoryException
     * @throws \yii\base\InvalidConfigException
     * @throws \yii\di\NotInstantiableException
     */
    public function run()
    {
        return $this->render($this->template);
    }
}