<?php
namespace frontend\widgets;

use common\helpers\UtilityHelper;
use Yii;
use common\widgets\Alert as BaseAlert;

/**
 * Class AlertModal
 * @package frontend\widgets
 */
class AlertModal extends BaseAlert
{
    /**
     * {@inheritdoc}
     * @throws \Exception
     */
    public function run()
    {
        $session = Yii::$app->session;
        $flashes = $session->getAllFlashes();
        $appendClass = isset($this->options['class']) ? ' ' . $this->options['class'] : '';
        $flashes = array_intersect_key($flashes, $this->alertTypes);

        foreach ($flashes as $type => $flash) {
            if (!is_array($flash) || !empty($flash['message'])) {
                $this->renderModal($type, $flash, 0, $appendClass);
            }
            else {
                foreach ((array) $flash as $i => $message) {
                    $this->renderModal($type, $message, $i, $appendClass);
                }
            }

            $session->removeFlash($type);
        }
    }

    private function renderModal($type, $message, $i, $appendClass){
        if (!is_array($message)){
            $message = ['message' => $message];
        }
        echo $this->render('alert-modal', [
            'type' => $type,
            'message' => $message['message'] ?? '',
            'subtitle' => $message['subtitle'] ?? '',
            'description' => $message['description'] ?? '',
            'id' => 'alert-modal-' . $type . '-' . $i,
            'class' => $this->alertTypes[$type] . $appendClass,
        ]);
    }
}
