<?php

namespace frontend\widgets;

use common\dto\UserDTO;
use common\helpers\UtilityHelper;
use common\interfaces\repositories\UserRepositoryInterface;
use frontend\forms\ContactForm;
use Yii;
use yii\base\Widget;

/**
 * Class HeaderWidget
 * @package frontend\widgets
 */
class HeaderWidget extends Widget
{
        /**
     * @var UserRepositoryInterface
     */
    private $_userRepository;

    /**
     * @var string
     */
    public $template = 'header-widget';

    /**
     * HeaderWidget constructor.
     * @param UserRepositoryInterface $userRepository
     * @param array $config
     */
    public function __construct(UserRepositoryInterface $userRepository, array $config = [])
    {
        parent::__construct($config);

        $this->_userRepository = $userRepository;
    }

    /**
     * @return string
     * @throws \common\exceptions\RepositoryException
     * @throws \yii\base\InvalidConfigException
     * @throws \yii\di\NotInstantiableException
     */
    public function run()
    {
        $params = [];

        $isGuest = Yii::$app->user->isGuest;
        $params['isGuest'] = $isGuest;
        $params['contactForm'] = new ContactForm();

        $user = UtilityHelper::user();
        if ($user !== null) {
            $userDTO = new UserDTO($user);
            $params['user'] = $userDTO->getData();
        }

        if (array_key_exists('course', Yii::$app->params['runtime'])) {
            $params['courseData'] = Yii::$app->params['runtime']['courseData'];
        }

        return $this->render($this->template, $params);
    }
}