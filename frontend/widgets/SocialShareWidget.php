<?php

namespace frontend\widgets;

use yii\base\Widget;

/**
 * Class SocialShareWidget
 * @package frontend\widgets
 */
class SocialShareWidget extends Widget
{
    /**
     * Url to be shared
     * @var string
     */
    public $url;
    /**
     * @var boolean
     */
    public $showLink = false;
    /**
     * @var string
     */
    public $template = 'social-share-widget';

    /**
     * @return string
     */
    public function run()
    {
        return $this->render($this->template, [
            'url' => $this->url,
            'showLink' => $this->showLink
        ]);
    }
}