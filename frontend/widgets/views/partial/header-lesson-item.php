<?php

use yii\helpers\Url;
use yii\web\View;

/**
 * @var View $this
 * @var array $lesson
 * @var boolean $done
 * @var string $index
 */

?>
<div class="lesson <?= $done ? 'done ' : ' '?> py-3 px-2 px-sm-4">
    <a href="<?= Url::to(['/lesson/view', 'id' => $lesson['id'], 'course_token' => Yii::$app->params['runtime']['course']['token']])?>" class="d-flex justify-content-between">
        <div class="pr-2 d-flex align-items-center">
            <div class="badge-course bg-blue color-white d-flex rounded-circle mr-2 align-items-center justify-content-center">
                <i class="i-film"></i>
            </div>
            <?= "{$index}. {$lesson['title']}" ?>
        </div>
        <div class="d-flex align-items-center text-nowrap">
            <i class="result-icon i-correct mr-2"></i>
            <?= Yii::t('app', '{xp} XP', ['xp' => $lesson['xp']])?>
        </div>
    </a>
</div>