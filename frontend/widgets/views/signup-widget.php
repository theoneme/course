<?php

use frontend\models\auth\SignupForm;
use yii\helpers\Html;
use yii\web\View;
use yii\widgets\ActiveForm;

/**
 * @var View $this
 * @var SignupForm $signupForm
 */

?>

<div id="registration" class="modal modal-standart" tabindex="-1" role="dialog">
    <div class="modal-dialog p-4" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h3 class="modal-title text-center"><?= Yii::t('app', 'Welcome')?></h3>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <i class="i-close"></i>
                </button>
            </div>
            <div class="modal-body">
                <div class="d-none register-buy-text">
                    <?= Yii::t('app', 'To purchase a course, you must register on our website.')?>
                </div>
                <br>
                <?php $form = ActiveForm::begin([
                    'id' => 'signup-form',
                    'enableAjaxValidation' => true,
                    'enableClientValidation' => false,
                    'action' => ['/auth/signup'],
                    'options' => [
                        'class' => 'needs-validation'
                    ],
                    'fieldConfig' => [
                        'template' => "{input}\n{error}",
                        'errorOptions' => [
                            'class' => 'help-block'
                        ],
                    ]
                ])?>
                    <div class="form-row">
                        <div class="col-md-12 mb-3">
                            <?= $form->field($signupForm, 'login')->textInput(['placeholder' => Yii::t('app', 'Enter Email or Phone')])?>
                        </div>
                        <div class="col-md-12 mb-3">
                            <?= $form->field($signupForm, 'password')->passwordInput(['placeholder' => Yii::t('app', 'Enter password')])?>
                        </div>
                    </div>
                    <div class="w-100 text-center py-4 px-5">
                        <?= Html::submitButton(Yii::t('app', 'Sign up'), ['class' => 'button w-100 orange font-weight-bold text-uppercase'])?>
                    </div>

                <?php ActiveForm::end(); ?>
                <p class="d-none">
                    Регистрируясь, Вы соглашаетесь с политикой конфиденциальности и с пользовательским соглашением
                </p>

                <div class="d-flex align-items-center py-4">
                    <div class="hr bg-light-blue w-100"></div>
                    <div class="px-2"><?= Yii::t('app', 'or')?></div>
                    <div class="hr bg-light-blue w-100"></div>
                </div>

                <div class="d-flex align-items-center justify-content-around pb-2">
                    <!--                    <a class="button fb text-center" href="#">-->
                    <!--                        <i class="i-facebook"></i>-->
                    <!--                    </a>-->
                    <!--                    <a class="button google text-center" href="#">-->
                    <!--                        <i class="i-google"></i>-->
                    <!--                    </a>-->
                    <?= Html::a('<i class="i-vk"></i>', ['/auth/social', 'authclient' => 'vk'], ['class' => 'button fb text-center'])?>
                </div>
            </div>

            <div class="modal-footer d-block px-4 text-center">
                <p>
                    <?= Yii::t('app', 'Already registered?')?>
                    <?= Html::a(Yii::t('app', 'Log in'), '#', [
                        'data' => [
                            'target' => '#login',
                            'toggle' => 'modal',
                            'dismiss' => 'modal',
                        ]
                    ])?>
                </p>
            </div>
        </div>
    </div>
</div>