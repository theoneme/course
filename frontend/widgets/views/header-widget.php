<?php

use common\models\Course;
use common\models\Translation;
use frontend\forms\ContactForm;
use frontend\widgets\LoginWidget;
use frontend\widgets\SignupWidget;
use yii\helpers\Html;
use yii\helpers\Url;
use yii\web\View;
use yii\widgets\ActiveForm;

/**
 * @var View $this
 * @var array $user
 * @var array|null $courseData
 * @var boolean $isGuest
 * @var ContactForm $contactForm
 */

?>

<header class="d-flex flex-wrap justify-content-between p-3">
    <a href="/" class="d-flex align-items-center logo pb-2 order-1">
        <img src="/images/logo.png" alt="ЕГЭ-ОНЛАЙН" title="ЕГЭ-ОНЛАЙН">
        <div class="px-2">ЕГЭ-ОНЛАЙН</div>
    </a>
    <?php if (!empty($courseData)) { ?>
        <nav class="nav-course d-flex justify-content-between bg-white border rounded p-0 order-3 order-sm-2 order-md-2 order-xl-2">
            <?php if(!empty($courseData['prev'])) {
                echo Html::a('<i class="i-back"></i>', $courseData['prev']['url'], [
                    'class' => 'pop-any pop-bottom h-100 d-flex align-items-center border-right p-3',
                    'data' => [
                        'toggle' => 'popover',
                        'placement' => 'bottom',
                        'content' => $courseData['prev']['title']
                    ],
                ]);
            } ?>
            <?= Html::a('<div class="w-100 text-center"><i class="i-menu color-blue pr-2"></i>' . Yii::t('app', 'Courses Outline') . '</div>', '#', [
                'class' => 'h-100 d-flex align-items-center w-100',
                'data' => [
                    'toggle' => 'modal',
                    'target' => '#lessons',
                ],
            ])?>
            <?php if(!empty($courseData['next'])) {
                echo Html::a('<i class="i-next"></i>', $courseData['next']['url'], [
                    'class' => 'pop-any pop-bottom h-100 d-flex align-items-center border-left p-3',
                    'data' => [
                        'toggle' => 'popover',
                        'placement' => 'bottom',
                        'content' => $courseData['next']['title']
                    ],
                ]);
            } ?>
        </nav>
    <?php } ?>
    <div class="d-flex align-items-center order-2 order-sm-3 order-md-3 order-xl-3">
        <?php
        if(Yii::$app->user->isGuest) {
            echo  Html::a(
                Yii::t('app', 'Log in'),
                '#',
                ['data' => ['target' => '#login', 'toggle' => 'modal']]
            );
        }
        else {
            echo  Html::a(Yii::t('app', 'Log out'), ['/auth/logout']);
        }
        ?>
        <?php if (!empty($courseData) && $courseData['type'] === Course::TYPE_DEFAULT) {
            echo Html::a(
                '<i class="i-film color-grey"></i>',
                ['/lesson/view', 'id' => $courseData['currentLesson']['id'], 'course_token' => Yii::$app->params['runtime']['course']['token']],
                [
                    'class' => 'pop-any pop-left btn-top p-2 d-inline-flex align-items-center',
                    'data' => [
                        'toggle' => 'popover',
                        'placement' => 'left',
                        'content' => Yii::t('app', 'Show video')
                    ],
                ]
            );
        } ?>
        <?= Html::a('<i class="i-info color-grey"></i>', null, [
            'class' => 'pop-any pop-left btn-top p-2 d-inline-flex align-items-center',
            'data' => [
                'toggle' => 'modal',
                'placement' => 'left',
                'target' => '#feedback',
                'content' => Yii::t('app', 'Write a review')
            ],
        ])?>
    </div>
</header>


<?php
if(Yii::$app->user->isGuest) {
    echo LoginWidget::widget();
    echo SignupWidget::widget();
}

if (!empty($courseData)) { ?>
    <div id="lessons" class="modal modal-standart modal-lessons" tabindex="-1" role="dialog">
        <div class="modal-dialog modal-lg" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <div class="d-flex justify-content-between w-100">
                        <div class="d-flex">
<!--                            <h4 class="font-weight-bold mx-2">--><?//= Yii::t('app', 'Progress')?><!--</h4>-->
                            <h3 class="modal-title"><?= Yii::$app->params['runtime']['course']['title']?></h3>
                        </div>
                        <div class="d-flex m-2">
                            <div class="progress custom-bar m-1">
                                <div class="progress-bar" role="progressbar" style="width: <?= $courseData['progressPercent']?>%" aria-valuenow="<?= $courseData['progressPercent']?>" aria-valuemin="0" aria-valuemax="100"></div>
                            </div>
                            <div class="mr-2 color-blue">
                                <?= $courseData['progressPercent']?>%
                            </div>
                        </div>
                    </div>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <i class="i-close"></i>
                    </button>
                </div>
                <div class="modal-body">
                    <div class="border border-light-blue">
                        <div class="p-3">
                            <p>
                                <?= Yii::$app->params['runtime']['course']['description']?>
                            </p>
                        </div>
                        <div>
                            <?php foreach ($courseData['lessons'] as $index => $lesson) {
                                if ($courseData['type'] === Course::TYPE_DEFAULT) {
                                    echo $this->render('partial/header-lesson-item', [
                                        'index' => $index + 1,
                                        'lesson' => $lesson,
                                        'done' => isset($courseData['lessonProgress'][$lesson['id']])
                                    ]);
                                }
                                if (isset($courseData['tasks'][$lesson['id']])) {
                                    $taskSuccess = $courseData['lessonTaskProgress'][$lesson['id']] ?? true;
                                    echo $this->render('partial/header-task-item', [
                                        'index' => $index + 1,
                                        'lesson' => $lesson,
                                        'done' => isset($courseData['lessonTaskProgress'][$lesson['id']]),
                                        'success' => $courseData['lessonTaskProgress'][$lesson['id']] ?? true,
                                        'points' => $courseData['tasks'][$lesson['id']]
                                    ]);
                                }
                            } ?>
                        </div>
                    </div>

                </div>
                <div class="modal-footer position-absolute w-100 bg-white p-3 d-flex justify-content-center">
                    <a href="#" class="d-flex align-items-center font-weight-bold mr-5" data-dismiss="modal"><i class="i-left color-orange mr-2 rem-12"></i> Вернуться</a>
                    <?= Html::a(
                        '<i class="i-reload font-weight-bold color-orange mr-2 rem-12"></i>' . Yii::t('app', 'Clear progress'),
                        ['/course/clear-progress', 'course_token' => Yii::$app->params['runtime']['course']['token']],
                        ['class' => 'd-flex align-items-center font-weight-bold', 'data-confirm' => Yii::t('app', 'Are you sure?')]
                    )?>
                </div>
            </div>
        </div>
    </div>
<?php } ?>

<div id="feedback" class="modal modal-standart modal-lessons" tabindex="-1" role="dialog">
    <div class="modal-dialog p-4" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h3 class="modal-title">Напишите отзыв</h3>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <i class="i-close"></i>
                </button>
            </div>
            <div class="modal-body">
                <?php $form = ActiveForm::begin([
                    'action' => Url::to(['/contact/send']),
                    'id' => 'contact-form',
                    'enableAjaxValidation' => true,
                    'enableClientValidation' => false,
                    'fieldConfig' => [
                        'options' => ['class' => 'form-group mt-4'],
                    ]
                ]); ?>
                    <?= $form->field($contactForm, 'subject')->textInput() ?>
                    <?= $form->field($contactForm, 'message')->textarea() ?>
                <?php ActiveForm::end() ?>
            </div>
            <div class="modal-footer position-absolute w-100 bg-white p-3 d-flex justify-content-between">
<!--                <button type="button" data-dismiss="modal" class="button blue font-weight-bold"> Отмена</button>-->
                <?= Html::button(Yii::t('app', 'Cancel'), ['class' => 'button blue font-weight-bold', 'data-dismiss' => 'modal'])?>
                <?= Html::submitButton(Yii::t('app', 'Submit'), ['class' => 'feedback-submit button orange font-weight-bold'])?>
            </div>
        </div>
    </div>
</div>

<?php

$script = <<<JS
    $('.pop-any').popover({
        trigger: 'hover',
        template: '<div class="popover blue"><div class="arrow"></div><div class="popover-body"></div></div>'
    });

    $(document).on('submit', '#contact-form', function(){
        let form = $(this);
        $.post(form.attr('action'), 
            form.serialize(), 
            function(response) {
                if (response && response.success) {
                    form.closest('.modal').modal('hide');
                }
                else {
                    // ???
                }
            }
        );
        return false;
    });
    $(document).on('click', '.feedback-submit', function(){
        $('#contact-form').submit();
        return false;
    });
    // $('.pop-bottom').popover({
    //     trigger: 'hover',
    //     template: '<div class="popover blue"><div class="arrow"></div><div class="popover-body"></div></div>'
    // });
    // $('.pop-top').popover({
    //     trigger: 'hover',
    //     template: '<div class="popover blue"><div class="arrow"></div><div class="popover-body"></div></div>'
    // });
JS;

$this->registerJs($script);
