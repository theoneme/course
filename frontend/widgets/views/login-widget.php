<?php

use frontend\models\auth\LoginForm;
use yii\helpers\Html;
use yii\web\View;
use yii\widgets\ActiveForm;

/**
 * @var View $this
 * @var LoginForm $loginForm
 */

?>

<div id="login" class="modal modal-standart" tabindex="-1" role="dialog">
    <div class="modal-dialog p-4" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h3 class="modal-title text-center"><?= Yii::t('app', 'Log in')?></h3>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <i class="i-close"></i>
                </button>
            </div>
            <div class="modal-body">
                <div class="d-none register-buy-text">
                    <?= Yii::t('app', 'To purchase a course, you must register on our website.')?>
                </div>
                <br>
                <?php $form = ActiveForm::begin([
                    'id' => 'login-form',
                    'enableAjaxValidation' => true,
                    'enableClientValidation' => false,
                    'action' => ['/auth/login-ajax'],
                    'options' => [
                        'class' => 'needs-validation'
                    ],
                    'fieldConfig' => [
                        'template' => "{input}\n{error}",
                        'errorOptions' => [
                            'class' => 'help-block'
                        ],
                    ]
                ])?>
                    <div class="form-row">
                        <div class="col-md-12 mb-3">
                            <?= $form->field($loginForm, 'login')->textInput(['placeholder' => Yii::t('app', 'Enter Email or Phone')])?>
                        </div>
                        <div class="col-md-12 mb-3">
                            <?= $form->field($loginForm, 'password')->passwordInput(['placeholder' => Yii::t('app', 'Enter password')])?>
                        </div>
                    </div>
                    <div class="w-100 text-center pt-2 pb-4 px-5">
                        <?= Html::submitButton(Yii::t('app', 'Log in'), ['class' => 'button w-100 orange font-weight-bold text-uppercase'])?>
                    </div>

                    <div class="d-flex justify-content-between align-items-end">
                        <div class="d-flex pr-2 align-items-center">
                            <?= $form->field($loginForm, 'rememberMe', [
                                'template' => '{input}',
                                'options' => ['tag' => false]
                            ])->checkbox(['id' => 'rememberMe'], false)?>
                            <label for="rememberMe">
                                <span class="cr-text"><?= Yii::t('app', 'Remember me') ?></span>
                            </label>
                        </div>
<!--                        <a href="#" class="">Забыли пароль?</a>-->
                    </div>
                <?php ActiveForm::end(); ?>

                <div class="d-flex align-items-center py-4">
                    <div class="hr bg-light-blue w-100"></div>
                    <div class="px-2"><?= Yii::t('app', 'or')?></div>
                    <div class="hr bg-light-blue w-100"></div>
                </div>

                <div class="d-flex align-items-center justify-content-around pb-2">
<!--                    <a class="button fb text-center" href="#">-->
<!--                        <i class="i-facebook"></i>-->
<!--                    </a>-->
<!--                    <a class="button google text-center" href="#">-->
<!--                        <i class="i-google"></i>-->
<!--                    </a>-->
                    <?= Html::a('<i class="i-vk"></i>', ['/auth/social', 'authclient' => 'vk'], ['class' => 'button fb text-center'])?>
                </div>
            </div>

            <div class="modal-footer d-block px-4">
                <p>
                    <?= Yii::t('app', 'Not registered yet?') ?>
                    <?= Html::a(Yii::t('app', 'Sign up now'), '#', [
                        'data' => [
                            'target' => '#registration',
                            'toggle' => 'modal',
                            'dismiss' => 'modal',
                        ]
                    ])?>
                    <?= Yii::t('app', '— it\'s easy and simple!') ?>
                </p>
            </div>
        </div>
    </div>
</div>