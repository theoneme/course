<?php

use yii\helpers\Html;
use yii\web\View;

/**
 * @var View $this
 * @var string $message
 * @var string $subtitle
 * @var string $description
 * @var string $class
 * @var integer $id
 * @var string $type
 */

?>

<div id="<?= $id?>" class="modal modal-standart" tabindex="-1" role="dialog">
    <div class="modal-dialog p-4" role="document">
        <div class="modal-content">
            <div class="modal-header border-0">
                <h3 class="w-100 modal-title text-center"><?= $message?></h3>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <i class="i-close"></i>
                </button>
            </div>
            <div class="modal-body">
                <div class="px-3 px-sm-5">
                    <?= Html::img("/images/$type.png", ['class' => 'w-100'])?>
                </div>
            </div>
            <div class="modal-footer border-0 flex-wrap bg-white p-3">
                <h5 class="w-100 text-center font-weight-bold"><?= $subtitle?></h5>
                <p class="w-100 text-center"><?= $description?></p>
            </div>
        </div>
    </div>
</div>

<?php
$this->registerJs("
    $('#$id').modal('show');
");