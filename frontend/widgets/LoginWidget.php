<?php

namespace frontend\widgets;

use common\interfaces\repositories\UserRepositoryInterface;
use frontend\models\auth\LoginForm;
use yii\base\Widget;

/**
 * Class LoginWidget
 * @package frontend\widgets
 */
class LoginWidget extends Widget
{
    /**
     * @var
     */
    private $_userRepository;

    /**
     * LoginWidget constructor.
     * @param array $config
     * @param UserRepositoryInterface $userRepository
     */
    public function __construct(UserRepositoryInterface $userRepository, array $config = [])
    {
        parent::__construct($config);
        $this->_userRepository = $userRepository;
    }

    /**
     * @return string
     * @throws \yii\base\InvalidConfigException
     */
    public function run()
    {
        $loginForm = new LoginForm($this->_userRepository);

        return $this->render('login-widget', ['loginForm' => $loginForm]);
    }
}