<?php

namespace frontend\services;

use common\interfaces\repositories\CategoryRepositoryInterface;
use common\models\Category;
use common\models\Translation;
use common\services\entities\AttributeValueService;
use Yii;

/**
 * Class IndexPageService
 * @package frontend\services
 */
class IndexPageService
{
    /**
     * @var AttributeValueService
     */
    private $_attributeValueService;
    /**
     * @var CategoryRepositoryInterface
     */
    private $_categoryRepository;

    /**
     * IndexPageService constructor.
     * @param AttributeValueService $attributeValueService
     * @param CategoryRepositoryInterface $categoryRepository
     * @param array $config
     */
    public function __construct(AttributeValueService $attributeValueService,
                                CategoryRepositoryInterface $categoryRepository,
                                array $config = [])
    {
        $this->_attributeValueService = $attributeValueService;
        $this->_categoryRepository = $categoryRepository;
    }

    /**
     * @return array
     * @throws \common\exceptions\RepositoryException
     * @throws \yii\base\InvalidConfigException
     * @throws \yii\di\NotInstantiableException
     */
    public function getData()
    {
        /* @var Category $category */
        $category = $this->_categoryRepository->select(['id', 'root', 'lft', 'rgt'], true)->findOneByCriteria(['id' => 2], true);

        $criteria = [];

        if ($category !== null) {
            $criteria = ['and', $criteria, ['category.root' => $category['root']], ['>=', 'category.lft', $category['lft']], ['<=', 'category.rgt', $category['rgt']]];
        }
        $criteria = ['and', $criteria, ['translation.key' => Translation::KEY_DESCRIPTION, 'translation.locale' => Yii::$app->language]];

//        $productIDs = $this->_productRepository->select(['id'], true)->joinWith(['category'], false)
//            ->limit(8)
//            ->orderBy(['id' => SORT_DESC])
//            ->findColumnByCriteria($criteria, 'id');
//
//        if (!empty($productIDs)) {
//            $products = $this->_productService->getMany(['id' => $productIDs]);
//        } else {
//            $products['items'] = [];
//        }

        return [
            'products' => [],
        ];
    }
}