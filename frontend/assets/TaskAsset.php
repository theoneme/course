<?php

namespace frontend\assets;

use yii\web\AssetBundle;

/**
 * Class TaskAsset
 * @package frontend\assets
 */
class TaskAsset extends AssetBundle
{
    public $basePath = '@webroot';
    public $baseUrl = '@web';

    public $js = [
    ];
    public $css = [
        'css/site/test.css',
    ];
    public $depends = [
        CommonAsset::class
    ];
}