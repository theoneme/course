<?php

namespace frontend\assets;

use frontend\assets\plugins\FontAwesomeAsset;
use yii\bootstrap\BootstrapAsset;
use yii\bootstrap\BootstrapPluginAsset;
use yii\web\AssetBundle;
use yii\web\YiiAsset;

class ErrorAsset extends AssetBundle
{
    public $basePath = '@webroot';
    public $baseUrl = '@web';
    public $css = [
        'css/error.css',
    ];
    public $depends = [
        BootstrapAsset::class,
//        FontAwesomeAsset::class,
        BootstrapPluginAsset::class,
        YiiAsset::class
    ];
}