<?php

namespace frontend\assets;

//use airani\bootstrap\BootstrapRtlAsset;
use frontend\assets\plugins\B4Asset;
use frontend\assets\plugins\B4PluginAsset;
use frontend\assets\plugins\FontAwesomeAsset;
use yii\web\AssetBundle;
use yii\web\YiiAsset;

/**
 * Class CommonAsset
 * @package frontend\assets
 */
class CommonAsset extends AssetBundle
{
    public $basePath = '@webroot';
    public $baseUrl = '@web';
    public $css = [
        'css/vendor/courses-icon.css'
    ];
    public $js = [

    ];
    public $depends = [
        YiiAsset::class,
        B4Asset::class,
        B4PluginAsset::class,
//        FontAwesomeAsset::class,
    ];
}
