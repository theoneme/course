<?php

namespace frontend\assets;

use yii\web\AssetBundle;

/**
 * Class LessonAsset
 * @package frontend\modules\html\assets
 */
class LessonAsset extends AssetBundle
{
    public $basePath = '@webroot';
    public $baseUrl = '@web';

    public $js = [
    ];
    public $css = [
        'css/site/lesson.css',
    ];
    public $depends = [
        CommonAsset::class
    ];
}