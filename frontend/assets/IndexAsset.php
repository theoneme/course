<?php

namespace frontend\assets;

use yii\web\AssetBundle;

/**
 * Class IndexAsset
 * @package frontend\assets
 */
class IndexAsset extends AssetBundle
{
    public $basePath = '@webroot';
    public $baseUrl = '@web';

    public $css = [
        'css/site/index.css'
    ];
    public $js = [
        'js/popper.min.js'
    ];
    public $depends = [
        CommonAsset::class
    ];
}