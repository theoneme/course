<?php

namespace frontend\assets;

use yii\web\AssetBundle;

/**
 * Class DashboardAsset
 * @package frontend\assets
 */
class DashboardAsset extends AssetBundle
{
    public $basePath = '@webroot';
    public $baseUrl = '@web';

    public $js = [
    ];
    public $css = [
        'css/site/dashboard.css',
    ];
    public $depends = [
        CommonAsset::class
    ];
}