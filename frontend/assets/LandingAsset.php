<?php

namespace frontend\assets;

use yii\web\AssetBundle;
use yii\web\YiiAsset;
use frontend\assets\plugins\B4Asset;
use frontend\assets\plugins\B4PluginAsset;

/**
 * Class LandingAsset
 * @package frontend\assets
 */
class LandingAsset extends AssetBundle
{
    public $basePath = '@webroot';
    public $baseUrl = '@web';

    public $css = [
        'css/site/landing.css'
    ];
    public $js = [
        'js/popper.min.js'
    ];
    public $depends = [
        CommonAsset::class,
    ];
}