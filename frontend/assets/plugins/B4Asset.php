<?php

namespace frontend\assets\plugins;

use yii\bootstrap\BootstrapAsset;

/**
 * Class BootstrapAsset
 * @package yii\bootstrap
 */
class B4Asset extends BootstrapAsset
{
    public $basePath = '@webroot';
    public $baseUrl = '@web';
    public $css = [
        'css/vendor/bootstrap4.min.css',
    ];
}
