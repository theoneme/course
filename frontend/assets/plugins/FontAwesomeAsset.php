<?php

namespace frontend\assets\plugins;

use yii\web\AssetBundle;

/**
 * Class FontAwesomeAsset
 * @package frontend\assets\plugins
 */
class FontAwesomeAsset extends AssetBundle
{
    public $basePath = '@webroot';
    public $baseUrl = '@web';
    public $css = [
        'css/vendor/desktop-icon.css'
    ];
}