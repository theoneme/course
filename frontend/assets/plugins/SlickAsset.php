<?php

namespace frontend\assets\plugins;

use yii\web\AssetBundle;
use yii\web\YiiAsset;

/**
 * Class SlickAsset
 * @package frontend\assets\plugins
 */
class SlickAsset extends AssetBundle
{
    public $basePath = '@webroot';
    public $baseUrl = '@web';
    public $css = [
        'css/vendor/slick.css',
        'css/vendor/slick-theme.css',
    ];
    public $js = [
        'js/vendor/slick.js',
    ];
    public $depends = [
        YiiAsset::class,
    ];
}