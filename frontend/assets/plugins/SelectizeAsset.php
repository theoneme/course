<?php

namespace frontend\assets\plugins;

use yii\web\AssetBundle;
use yii\web\JqueryAsset;

/**
 * Class SelectizeAsset
 * @package frontend\assets\plugins
 */
class SelectizeAsset extends AssetBundle
{
    public $sourcePath = '@bower/selectize/dist';
    public $css = [
        'css/selectize.legacy.css',
        '/css/custom-selectize.css'
    ];
    public $js = [
        'js/standalone/selectize.min.js'
    ];
    public $depends = [
//        CommonAsset::class,
        JqueryAsset::class,
    ];
}