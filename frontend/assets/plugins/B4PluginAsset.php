<?php

namespace frontend\assets\plugins;

use yii\bootstrap\BootstrapPluginAsset;

/**
 * Class B4PluginAsset
 * @package frontend\assets\plugins
 */
class B4PluginAsset extends BootstrapPluginAsset
{
    public $basePath = '@webroot';
    public $baseUrl = '@web';
    public $js = [
        'js/vendor/bootstrap4.min.js',
    ];

    public $depends = [
        PopperAsset::class,
    ];
}