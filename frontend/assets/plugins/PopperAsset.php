<?php

namespace frontend\assets\plugins;

use yii\web\AssetBundle;

/**
 * Class PopperAsset
 * @package frontend\assets\plugins
 */
class PopperAsset extends AssetBundle
{
    public $basePath = '@webroot';
    public $baseUrl = '@web';

    public $css = [

    ];
    public $js = [
        'js/popper.min.js'
    ];
}
