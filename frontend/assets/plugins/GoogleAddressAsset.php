<?php

namespace frontend\assets\plugins;

use common\assets\GoogleAsset;
use yii\web\AssetBundle;
use yii\web\JqueryAsset;

/**
 * Class GoogleAddressAsset
 * @package frontend\assets\plugins
 */
class GoogleAddressAsset extends AssetBundle
{
    public $js = [
        'js/classes/google-address-autocomplete.js',
    ];
    public $depends = [
        JqueryAsset::class,
        GoogleAsset::class
    ];
}
