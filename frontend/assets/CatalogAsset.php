<?php

namespace frontend\assets;

use yii\web\AssetBundle;

/**
 * Class IndexAsset
 * @package frontend\assets
 */
class CatalogAsset extends AssetBundle
{
    public $basePath = '@webroot';
    public $baseUrl = '@web';
    public $css = [
        'css/site/catalog.css'
    ];
    public $js = [

    ];
    public $depends = [
        CommonAsset::class
    ];
}