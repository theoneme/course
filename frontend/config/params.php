<?php
return [
    'adminEmail' => 'admin@example.com',
    'app_currency_code' => 'RUB',

    'priceChunks' => [
        [
            'range' => [
                'min' => 0,
                'max' => 500
            ],
            'step' => 100
        ],
        [
            'range' => [
                'min' => 1000,
                'max' => 5000
            ],
            'step' => 500
        ],
        [
            'range' => [
                'min' => 5000,
                'max' => 10000
            ],
            'step' => 1000
        ],
        [
            'range' => [
                'min' => 10000,
                'max' => 20000
            ],
            'step' => 2000
        ],
        [
            'range' => [
                'min' => 20000,
                'max' => 50000
            ],
            'step' => 5000
        ],
        [
            'range' => [
                'min' => 50000,
                'max' => 100000
            ],
            'step' => 10000
        ],
    ],

    'productCatalogFilters' => [
        'year',
        'type',
        'bottle',
        'country',
        'state',
        'region',
        'city',
    ],
];
