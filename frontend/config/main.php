<?php

use yii\authclient\clients\VKontakte;

$params = array_merge(
    require __DIR__ . '/../../common/config/params.php',
    require __DIR__ . '/../../common/config/params-local.php',
    require __DIR__ . '/params.php',
    require __DIR__ . '/params-local.php'
);

return [
    'id' => 'app-frontend',
    'basePath' => dirname(__DIR__),
    'bootstrap' => ['log', 'preview'],
    'controllerNamespace' => 'frontend\controllers',
    'components' => [
        'assetManager' => [
            'basePath' => '@webroot/assets',
            'baseUrl' => '@web/assets',
            'bundles' => [
                'yii\web\JqueryAsset' => [
                    'js' => [
                        YII_ENV_DEV ? 'jquery.js' : 'jquery.min.js',
                    ]
                ],
                'yii\bootstrap\BootstrapAsset' => [
                    'css' => [
                        YII_ENV_DEV ? 'css/bootstrap.css' : 'css/bootstrap.min.css',
                    ]
                ],
                'yii\bootstrap\BootstrapPluginAsset' => [
                    'js' => [
                        YII_ENV_DEV ? 'js/bootstrap.js' : 'js/bootstrap.min.js',
                    ]
                ]
            ],
        ],
        'request' => [
            'csrfParam' => '_csrf-frontend',
        ],
        'user' => [
            'identityClass' => 'common\models\User',
            'enableAutoLogin' => true,
            'identityCookie' => ['name' => '_identity-frontend', 'httpOnly' => true],
            'loginUrl' => ['auth/login'],
        ],
        'session' => [
            'name' => 'advanced-frontend',
            'cookieParams' => [
                'path' => '/',
            ]
        ],
        'log' => [
            'traceLevel' => YII_DEBUG ? 3 : 0,
            'targets' => [
                [
                    'class' => 'yii\log\FileTarget',
                    'levels' => ['error', 'warning'],
                    'except' => ['yii\web\HttpException:404', 'yii\i18n\PhpMessageSource::loadFallbackMessages'],
                ],
            ],
        ],
        'errorHandler' => [
//            'errorAction' => 'site/error',
            'errorView' => '@frontend/views/site/error.php',
        ],
        'urlManager' => [
//            'class' => CustomUrlManager::class,
            'ruleConfig' => [
                'class' => 'frontend\components\CustomUrlRule'
            ],
            'enablePrettyUrl' => true,
            'showScriptName' => false,
            'rules' => [
                'sitemap.xml' => 'sitemap/default/index',
                [
                    'pattern' => 'sitemap-<action(list|part)>-<entity:[\w-]+>',
                    'route' => 'sitemap/default/<action>',
                    'suffix' => '.xml',
                ],

                '/' => 'site/index',

                '<course_token:[\w-]+>/lesson/<id:\d+>/task' => 'lesson/task',
                '<course_token:[\w-]+>/lesson/<id:\d+>' => 'lesson/view',

                '<course_token:[\w-]+>/task/<action:[\w-]+>' => 'task/<action>',
                '<course_token:[\w-]+>/course/<action:[\w-]+>' => 'course/<action>',

                'lesson/<id:\d+>/task' => 'lesson/task',
                'lesson/<id:\d+>' => 'lesson/view',

                'html/<view:[\w-]+>' => 'html/site/index',

                '<slug:(privacy-policy|terms-of-service)>' => 'page/view',
                '<slug:[\w-]+>-<action:(product)>' => 'product/<action>/show',
                '<category:[\w-]+>-catalog' => 'product/catalog/index',

                '<controller:[\w-]+>/<action:[\w-]+>/<id:\d+>' => '<controller>/<action>',
                '<controller:[\w-]+>/<action:[\w-]+>' => '<controller>/<action>',

                '<module:\w+>/<controller:[\w-]+>/<action:[\w-]+>/<id:\d+>' => '<module>/<controller>/<action>',
                '<module:\w+>/<controller:[\w-]+>/<action:[\w-]+>' => '<module>/<controller>/<action>',
            ],
        ],
        'i18n' => [
            'translations' => [
                'main*' => [
                    'class' => 'yii\i18n\PhpMessageSource',
                    'basePath' => '@frontend/messages',
                    'fileMap' => [
                        'main' => 'main.php',
                    ],
                ],
                'index*' => [
                    'class' => 'yii\i18n\PhpMessageSource',
                    'basePath' => '@frontend/messages',
                    'fileMap' => [
                        'index' => 'index.php',
                    ],
                ],
                'model*' => [
                    'class' => 'yii\i18n\PhpMessageSource',
                    'basePath' => '@common/messages',
                    'fileMap' => [
                        'model' => 'model.php',
                    ],
                ],
                'catalog*' => [
                    'class' => 'yii\i18n\PhpMessageSource',
                    'basePath' => '@frontend/messages',
                    'fileMap' => [
                        'catalog' => 'catalog.php',
                    ],
                ],
                'labels*' => [
                    'class' => 'yii\i18n\PhpMessageSource',
                    'basePath' => '@common/messages',
                    'fileMap' => [
                        'labels' => 'labels.php',
                    ],
                ],
            ],
        ],
        'authClientCollection' => [
            'class' => 'yii\authclient\Collection',
            'clients' => [
                'vk' => [
                    'class' => VKontakte::class,
                    'clientId' => '7426961',
                    'clientSecret' => 'se0jFdmm0JlpTsbeWVUH',
                    'attributeNames' => [
                        'uid',
                        'first_name',
                        'last_name',
                        'nickname',
                        'screen_name',
                        'sex',
                        'bdate',
                        'city',
                        'country',
                        'timezone',
                        'photo',
                        'email'
                    ],
                ],
//                'google' => [
//                    'class' => 'yii\authclient\clients\Google',
//                    'clientId' => '211168917955-g3v64dqaff64ml86qogkh1nkg7jf0k8h.apps.googleusercontent.com',
//                    'clientSecret' => 'dn0Lf_DdD3wRUwwdwu-VTOWq',
//                ],
//                'facebook' => [
//                    'class' => 'yii\authclient\clients\Facebook',
//                    'clientId' => '202756367284434',
//                    'clientSecret' => 'fbe45c0ab7cc1523bc50de3116287c40',
//                    'attributeNames' => [
//                        'name',
//                        'id',
//                        'picture',
//                        'link',
//                        'email'
//                    ],
//                ],
            ],
        ],
    ],
    'modules' => [
        'html' => [
            'class' => 'frontend\modules\html\Module',
        ],
        'preview' => [
            'class' => 'frontend\modules\preview\Module',
        ],
    ],
    'params' => $params,
];
