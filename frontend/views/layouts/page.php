<?php
/**
 * Created by PhpStorm.
 * User: Алёнка
 * Date: 26.03.2020
 * Time: 16:24
 */

use frontend\assets\PageAsset;
use frontend\widgets\FooterWidget;
use yii\helpers\Html;

PageAsset::register($this);

?>
<?php $this->beginPage() ?>
<!DOCTYPE html>
<html lang="<?= Yii::$app->language ?>">
<head>
    <meta charset="<?= Yii::$app->charset ?>">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no">
    <link rel="icon" href="" type="image/png"/>
    <link rel="shortcut icon" href="" type="image/png"/>
    <?= Html::csrfMetaTags() ?>
    <title><?= Html::encode($this->title) ?></title>

    <?php $this->head() ?>
</head>
<body>
<?php $this->beginBody() ?>

<div class="main-block">

    <header class="d-flex flex-wrap justify-content-between p-3">
        <a href="/" class="d-flex align-items-center logo pb-2 order-1">
            <img src="/images/logo.png" alt="ЕГЭ-ОНЛАЙН" title="ЕГЭ-ОНЛАЙН">
            <div class="px-2">ЕГЭ-ОНЛАЙН</div>
        </a>
        <div class="d-flex align-items-center order-2 order-sm-3 order-md-3 order-xl-3">

            <?= Html::a('<i class="i-info color-grey"></i>', null, [
                'class' => 'pop-left btn-top p-2 d-inline-flex align-items-center',
                'data' => [
                    'toggle' => 'modal',
                    'placement' => 'left',
                    'target' => '#feedback',
                    'content' => Yii::t('app', 'Write a review')
                ],
            ])?>
        </div>
    </header>

    <div id="feedback" class="modal modal-standart modal-lessons" tabindex="-1" role="dialog">
        <div class="modal-dialog p-4" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h3 class="modal-title">Напишите отзыв</h3>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <i class="i-close"></i>
                    </button>
                </div>
                <div class="modal-body">
                    <div class="form-group mt-4">
                        <label class="font-weight-bold" for="reason">Причина отзыва</label>
                        <select type="password" class="form-control" id="reason">
                            <option value=""></option>
                            <option value="video-broken">Видео не работает правильно</option>
                            <option value="typo-video">Не то видео</option>
                            <option value="other">Другая</option>
                        </select>
                    </div>
                    <div class="form-group mt-4">
                        <label class="font-weight-bold" for="message">Сообщение отзыва</label>
                        <textarea id="message" class="form-control"></textarea>
                    </div>
                </div>
                <div class="modal-footer position-absolute w-100 bg-white p-3 d-flex justify-content-between">
                    <button type="button" data-dismiss="modal" class="button blue font-weight-bold"> Отмена</button>
                    <button type="submit" class="button orange font-weight-bold">Отправить</button>
                </div>
            </div>
        </div>
    </div>


   <div class="wrapper-page mw-100 p-3 mx-auto">
       <?= $content ?>
   </div>
</div>
<?= FooterWidget::widget()?>

<?php $this->endBody() ?>
<?php
$script = <<<JS

    $('.pop-left').popover({
        trigger: 'hover',
        template: '<div class="popover blue"><div class="arrow"></div><div class="popover-body"></div></div>'
    });
    $('.pop-bottom').popover({
        trigger: 'hover',
        template: '<div class="popover blue"><div class="arrow"></div><div class="popover-body"></div></div>'
    });
JS;

$this->registerJs($script);
?>
</body>
</html>
<?php $this->endPage() ?>
