<?php

use frontend\widgets\AlertModal;
use frontend\widgets\LoginWidget;
use frontend\widgets\SignupWidget;
use yii\helpers\Html;
use yii\web\View;

/**
 * @var View $this
 * @var string $content
 */

?>
<?php $this->beginPage() ?>
<!DOCTYPE html>
<html lang="<?= Yii::$app->language ?>">
<head>
    <meta charset="<?= Yii::$app->charset ?>">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no">
    <link rel="icon"
          href=""
          type="image/png"/>
    <link rel="shortcut icon"
          href=""
          type="image/png"/>
    <?= Html::csrfMetaTags() ?>
    <title><?= Html::encode($this->title) ?></title>

    <?php $this->head() ?>
</head>
<body>
<?php $this->beginBody() ?>

<div class="main-block landing">
    <header>
        <div class="wrapper d-flex flex-wrap align-items-center px-3 justify-content-between justify-content-xl-start">
            <a href="/" class="d-flex align-items-center logo mr-5">
                <img src="/images/logo.png" alt="ЕГЭ-ОНЛАЙН" title="ЕГЭ-ОНЛАЙН">
                <div class="px-2">ЕГЭ-ОНЛАЙН</div>
            </a>
            <nav class="navbar navbar-expand-xl">
                <a class="d-block text-right d-xl-none w-100" href="#" data-toggle="collapse" data-target="#top-menu">
                    <div class="d-inline-block gamb-button rounded-circle">
                        <div class="bars">
                            <span class="icon-bar"></span>
                            <span class="icon-bar"></span>
                            <span class="icon-bar"></span>
                        </div>
                    </div>
                </a>
                <div class="d-xl-flex justify-content-around collapse navbar-collapse" id="top-menu">
                    <ul class="no-list top-menu d-block mx-auto">
                        <li>
                            <a class="gohref" href="#we-offer">
                                <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 40 40">
                                    <path d="M0,20 L40,20 M33,13 L39,20 L33,27"></path>
                                </svg>
                                Наши преимущества
                            </a>
                        </li>
                        <li>
                            <a class="gohref" href="#about">
                                <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 40 40">
                                    <path d="M0,20 L40,20 M33,13 L39,20 L33,27"></path>
                                </svg>
                                О нас
                            </a>
                        </li>
                        <li>
                            <a class="gohref" href="#portfolio">
                                <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 40 40">
                                    <path d="M0,20 L40,20 M33,13 L39,20 L33,27"></path>
                                </svg>
                                Пример занятий
                            </a>
                        </li>
                        <li>
                            <a class="gohref" href="#tariff">
                                <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 40 40">
                                    <path d="M0,20 L40,20 M33,13 L39,20 L33,27"></path>
                                </svg>
                                Стоимость
                            </a>
                        </li>
                        <li>
                            <?= Html::a($this->render('/common/partial/arrow-left') . Yii::t('app', 'Courses'), ['/dashboard/index'])?>
                        </li>
                        <?php if(Yii::$app->user->isGuest) {?>
                            <li>
                                <?= Html::a(
                                $this->render('/common/partial/arrow-left') . Yii::t('app', 'Log in'),
                                '#',
                                    ['data' => ['target' => '#login', 'toggle' => 'modal']]
                                )?>
                            </li>
                        <?php }
                        else {?>
                            <li>
                                <?= Html::a($this->render('/common/partial/arrow-left') . Yii::t('app', 'Log out'), ['/auth/logout'])?>
                            </li>
                        <?php }?>
                    </ul>
                </div>
            </nav>
        </div>
    </header>

    <?php if(Yii::$app->user->isGuest) {
        echo LoginWidget::widget();
        echo SignupWidget::widget();
    }
    echo AlertModal::widget();
    ?>

    <?= $content ?>
    <div class="m-footer"></div>
</div>

<footer class="d-flex flex-wrap justify-content-center p-3 p-sm-5">

    <?= Html::a(Yii::t('app', 'Privacy policy'), ['/site/privacy-policy'], ['class' => 'm-3'])?>
    <?= Html::a(Yii::t('app', 'Terms of service'), ['/site/terms-of-service'], ['class' => 'm-3'])?>

    <!-- Yandex.Metrika counter -->
    <?php
    $script = <<<JS
        (function(m,e,t,r,i,k,a){m[i]=m[i]||function(){(m[i].a=m[i].a||[]).push(arguments)};
            m[i].l=1*new Date();k=e.createElement(t),a=e.getElementsByTagName(t)[0],k.async=1,k.src=r,a.parentNode.insertBefore(k,a)})
        (window, document, "script", "https://mc.yandex.ru/metrika/tag.js", "ym");
        
        ym(61400401, "init", {
            clickmap:true,
            trackLinks:true,
            accurateTrackBounce:true,
            webvisor:true
        });
JS;
     $this->registerJs($script);
    ?>
    <noscript>
        <div><img src="https://mc.yandex.ru/watch/61400401" style="position:absolute; left:-9999px;" alt="" /></div>
    </noscript>
    <!-- /Yandex.Metrika counter -->
</footer>

<?php $this->endBody() ?>
</body>
</html>
<?php $this->endPage() ?>

