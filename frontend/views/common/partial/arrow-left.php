<?php

use yii\web\View;

/**
 * @var View $this
 * @var string $class
 */

?>

<svg class="<?= $class ?? ''?>" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 40 40">
    <path d="M0,20 L40,20 M33,13 L39,20 L33,27"></path>
</svg>
