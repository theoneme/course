<?php

use frontend\assets\TaskAsset;
use yii\helpers\Html;
use yii\web\View;

/**
 * @var View $this
 * @var array $task
 */

//TaskAsset::register($this);

?>
<div class="p-4">
    <h5 class="font-weight-bold py-2"><?= Yii::t('app', 'Answer')?></h5>
    <?= Html::textInput('answer', null, [
        'class' => 'form-control',
        'placeholder' => Yii::t('app', 'Enter the answer'),
    ])?>
</div>