<?php

use frontend\assets\TaskAsset;
use yii\helpers\Html;
use yii\web\View;

/**
 * @var View $this
 * @var array $task
 */

//TaskAsset::register($this);

?>
<div class="p-4">
    <h5 class="font-weight-bold py-2"><?= Yii::t('app', 'Possible Answers')?></h5>
    <div>
        <?php foreach ($task['answers'] as $index => $answer) {?>
            <div class="d-flex justify-content-between p-2">
                <div class="d-flex pr-2 align-items-center">
                    <?= Html::checkbox('answer[]', false, ['id' => 'answer' . $index, 'value' => $answer['id']])?>
                    <?= Html::label('<span class="cr-text">' . $answer['title'] . '</span>', 'answer' . $index)?>
                </div>
                <div class="d-flex align-items-center">
                    <?= Yii::t('app', 'press')?>
                    <?= Html::label($index + 1, 'answer' . $index, ['class' => 'badge text-white bg-black p-2 ml-3 font-weight-normal tag-button'])?>
<!--                    <div class="badge text-white bg-black p-2 ml-3 font-weight-normal tag-button">--><?//= $index + 1?><!--</div>-->
                </div>
            </div>
        <?php }?>
    </div>
</div>