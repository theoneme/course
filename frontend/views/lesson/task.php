<?php

use common\models\Task;
use frontend\assets\TaskAsset;
use yii\helpers\Html;
use yii\helpers\Url;
use yii\web\View;

/**
 * @var View $this
 * @var array $task
 */

TaskAsset::register($this);
$progressPercent = Yii::$app->params['runtime']['courseData']['progressPercent'];

?>

<div class="p-3">
    <div class="d-flex align-items-center overflow-y-auto h-100 w-100">
        <section class="wrapper-test mx-auto w-100 border rounded bg-white py-4">
            <div class="p-4">
                <?php if (!$task['attachment']) {?>
                    <h1 class="h1-test font-weight-bold"><?= $task['title']?></h1>
                <?php } ?>
                <div>
                    <?= $task['attachment'] ? Html::img($task['attachment']) : $task['description']?>
                </div>
            </div>
            <div class="d-flex bg-grey justify-content-between bg-light-blue grey-box px-4 py-2">
                <div class="d-flex align-items-center">
                    <div class="mr-3">
                        <i class="i-verified font-weight-bold"></i>
                    </div>
                    <div>
                        <h5 class="font-weight-bold"><?= Yii::t('app', 'Solve the task')?></h5>
                    </div>
                </div>
                <div class="d-flex align-items-center">
                    <div class="badge badge-light bg-white py-2 px-3 rounded-pill font-weight-normal tag-xp letter-spacing-02">
                        <?= Yii::t('app', '{points, plural, one{# point} other{# points}}', ['points' => $task['points']])?>
                    </div>
                </div>
            </div>
            <div class="answer-container">
                <?php switch ($task['type']) {
                    case Task::TYPE_RADIO:
                        echo $this->render('partial/radio', ['task' => $task]);
                        break;
                    case Task::TYPE_CHECKBOX:
                        echo $this->render('partial/checkbox', ['task' => $task]);
                        break;
                    case Task::TYPE_TEXT:
                    default:
                        echo $this->render('partial/textbox', ['task' => $task]);
                        break;
                } ?>
            </div>
            <div class="d-flex flex-wrap justify-content-between pt-4 pl-4 pr-4">
                <div class="d-flex align-items-end take-hint font-weight-bold pr-3">
                    <a class="ml-2 p-2 hint" href="#hint" data-toggle="collapse" role="button" aria-expanded="false">
                        <i class="i-lamp-bulb p-2 color-blue font-weight-bold rounded-top"></i>
                        <span class="hint-text"><?= Yii::t('app', 'Show solution')?></span>
                    </a>
                </div>

                <div class="d-flex align-items-center ml-auto mb-2">
                    <?= Html::a(Yii::t('app', 'Submit Answer'), '#', ['class' => 'task-check-button button blue'])?>
                </div>
            </div>
            <div class="pb-3 pl-3 pr-3 collapse" id="hint">
                <div class="p-2 bg-light-blue p-3 collapse-hint">
                    <h6 class="font-weight-bold py-2"><?= Yii::t('app', 'Solution')?></h6>
                    <p class="">
                        <?= $task['answerAttachment'] ? Html::img($task['answerAttachment']) : $task['solution']?>
                    </p>
                </div>
            </div>
            <div class="d-flex flex-wrap justify-content-between pt-4 pl-4 pr-4">
                <div class="my-4 justify-content-end align-items-center">
                    <div class="d-flex mr-3">
                        <?= Yii::t('app', 'Time for task')?>:
                        <div id="timeStart">&nbsp;<?= Yii::t('app', '{minutes} m', ['minutes' => $task['minutes']])?></div>
                    </div>
                    <br>
                    <div class="d-flex mr-3">
                        <?= Yii::t('app', 'Time left')?>:
                        <div id="timeEnd"></div>
                    </div>
                </div>
            </div>
        </section>
    </div>
</div>

<div id="finish-test" class="modal modal-dark" tabindex="-1" role="dialog">
    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
        <i class="i-close"></i>
    </button>
    <div class="modal-dialog modal-dialog-centered" role="document">
        <div class="modal-content border-0">
            <div class="modal-header border-0">
                <h3 class="modal-title color-orange rem-12 w-100 text-center font-weight-bold check-success">
                    <span class="badge color-white bg-orange py-2 px-2 rounded-circle">
                        <i class="i-correct"></i>
                    </span>
                    + <?= Yii::t('app', '{points, plural, one{# point} other{# points}}', ['points' => $task['points']])?>
                </h3>
            </div>
            <div class="modal-body">
                <p class="text-center color-white check-message">

                </p>
            </div>
            <div class="modal-footer d-flex flex-wrap border-0">
                <div class="w-100 text-center text-uppercase color-white rem-12 pb-3"><?= Yii::t('app', 'Press enter to')?></div>
                <div class="w-100 text-center">
                    <?php
                    if (!empty(Yii::$app->params['runtime']['courseData']['next']['url'])) {
                        echo Html::a(
                            Yii::t('app', 'Continue'),
                            Yii::$app->params['runtime']['courseData']['next']['url'],
                            ['class' => 'button blue font-weight-bold d-inline-block check-success']
                        );
                     }
                    else {
                        echo Html::a(
                            Yii::t('app', 'Complete course'),
                            ['/dashboard/index'],
                            ['class' => 'button blue font-weight-bold d-inline-block check-success']
                        );
                    }
                    echo Html::a(
                        Yii::t('app', 'Retry'),
                        ['/lesson/task', 'id' => $task['lesson_id'], 'course_token' => Yii::$app->params['runtime']['course']['token']],
                        ['class' => 'button orange font-weight-bold d-none check-error']
                    );
                    ?>
                </div>
            </div>
        </div>
    </div>
</div>

<!--<div class="progress-bar-lesson position-absolute px-3 d-flex w-100 justify-content-around">-->
<!--    <div class="pop-any progress custom-bar mw-100 pop-top" data-toggle="popover" data-placement="top" data-content="--><?//= $progressPercent?><!--%">-->
<!--        <div class="progress-bar" role="progressbar" style="width: <?//= $progressPercent?>%;" aria-valuenow="<?//= $progressPercent?>" aria-valuemin="0" aria-valuemax="100"></div>-->
<!--    </div>-->
<!--</div>-->

<?php
$checkUrl = Url::to(['/task/check', 'id' => $task['id'], 'course_token' => Yii::$app->params['runtime']['course']['token']]);
$failUrl = Url::to(['/task/mark-failed', 'id' => $task['id'], 'course_token' => Yii::$app->params['runtime']['course']['token']]);
$minutes = $task['minutes'] ?? 0;
$script = <<<JS
    let countDown = $minutes * 60* 1000;
    let now = 0;
    let isPaused = false;

    let x = setInterval(function() {

        if(!isPaused) {
            let distance = countDown - now;

            // let hours = Math.floor((distance % (1000 * 60 * 60 * 24)) / (1000 * 60 * 60));
            let minutes = Math.floor((distance % (1000 * 60 * 60)) / (1000 * 60));
            let seconds = Math.floor((distance % (1000 * 60)) / 1000);
            // let hoursNow = Math.floor((now % (1000 * 60 * 60 * 24)) / (1000 * 60 * 60));
            // let minutesNow = Math.floor((now % (1000 * 60 * 60)) / (1000 * 60));
            // let secondsNow = Math.floor((now % (1000 * 60)) / 1000);

            document.getElementById("timeEnd").innerHTML = "&nbsp" + minutes + "м " + seconds + "с ";
            // document.getElementById("timeEnd").innerHTML = "&nbsp" + hours + "ч " + minutes + "м " + seconds + "с ";
            // document.getElementById("timeStart").innerHTML = "&nbsp" + hoursNow + "ч " + + minutesNow + "м " + secondsNow + "с ";

            now = now + 1000;

            if (distance < 0) {
                clearInterval(x);
                // document.getElementById("timeStart").innerHTML = "Время вышло";
                document.getElementById("timeEnd").innerHTML = "Время вышло";
                $.post('$failUrl', {}, function(response){});
            }
        }
    }, 1000);

    // $('.pause').on('click', function() {
    //     isPaused = true;
    // });
    //
    // $('.play').on('click', function() {
    //     isPaused = false;
    // });
    
    $(document).on('click', '.task-check-button', function(){
        // let inputs = $('input[name="answer"]:checked');
        // let answer = null;
        // if (inputs.length === 1) {
        //     answer = inputs.val();
        // }
        // else {
        //     answer = inputs.map(function(){return this.value;}).get();
        // }
        let answer = $('div.answer-container input').serialize();
        $.post('$checkUrl', answer, function(response){
            let modal = $('#finish-test');
            modal.find('.check-message').html(response.message);
            // alert(response.message);
            if (response.success) {
                modal.find('.check-success').removeClass('d-none').addClass('d-inline-block');
                modal.find('.check-error').removeClass('d-inline-block').addClass('d-none');
            } 
            else {
                modal.find('.check-success').removeClass('d-inline-block').addClass('d-none');
                modal.find('.check-error').removeClass('d-none').addClass('d-inline-block');
            }
            modal.modal('show');
        });
         return false;
    });
    
    $(document).on('keypress', function(event){
        let selector = '#answer' + (event.which - 49);
        if ($(selector).length) {
            $(selector).prop('checked', true);
        }
        let modal = $('#finish-test');
        if (event.which === 13 && modal.hasClass('show')) {
            window.location = modal.find('.modal-footer a.d-inline-block').attr('href');
        }
    });
    
    $(document).one('click', 'a.hint', function(){
        $.post('$failUrl', {}, function(response){});
         return false;
    });
JS;

$this->registerJs($script);
