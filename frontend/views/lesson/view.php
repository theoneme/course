<?php

use frontend\assets\LessonAsset;
use yii\helpers\Html;
use yii\web\View;

/**
 * @var View $this
 * @var array $lesson
 */

LessonAsset::register($this);
$progressPercent = Yii::$app->params['runtime']['courseData']['progressPercent'];
?>

<div class="center-block overflow-hidden">
    <div class="d-flex align-items-center overflow-y-auto p-3 h-100 w-100">
        <section class="wrapper-course mx-auto w-100">
            <div class="d-flex justify-content-between">
                <div>
                    <h1 class="h1-course"><?= $lesson['title']?></h1>
                </div>
                <div class="d-flex align-items-center">
                    <div class="badge bg-white color-black py-2 px-3 rounded-pill font-weight-normal tag-xp letter-spacing-02">
                        <?= Yii::t('app', '{xp} XP', ['xp' => $lesson['xp']])?>
                    </div>
                </div>
            </div>
            <div class="py-3 video">
                <video width="100%" height="auto" controls="controls">
                    <source src="<?= $lesson['attachment']?>">
                </video>
            </div>
            <div class="w-100 d-flex justify-content-end">
                <?php
                if (isset(Yii::$app->params['runtime']['courseData']['tasks'][$lesson['id']])) {
                    echo Html::a(
                        Yii::t('app', 'Got it!'),
                        ['/lesson/task', 'id' => $lesson['id'], 'course_token' => Yii::$app->params['runtime']['course']['token']],
                        ['class' => 'task-link button orange small']
                    );
                }
                else {
                    if (!empty(Yii::$app->params['runtime']['courseData']['next']['url'])) {
                        echo Html::a(
                            Yii::t('app', 'Got it!'),
                            Yii::$app->params['runtime']['courseData']['next']['url'],
                            ['class' => 'task-link button orange small']
                        );
                    }
                    else {
                        echo Html::a(
                            Yii::t('app', 'Complete course'),
                            ['/dashboard/index'],
                            ['class' => 'task-link button orange small']
                        );
                    }
                }
                ?>
            </div>
        </section>
    </div>
</div>

<div class="progress-bar-lesson position-absolute px-3 d-flex w-100 justify-content-around">
    <div class="pop-any progress custom-bar mw-100 pop-top" data-toggle="popover" data-placement="top" data-content="<?= $progressPercent?>%">
        <div class="progress-bar" role="progressbar" style="width: <?= $progressPercent?>%;" aria-valuenow="<?= $progressPercent?>" aria-valuemin="0" aria-valuemax="100"></div>
    </div>
</div>

<?php
$script = <<<JS
    $('video').on('ended', function(){
        window.location = $('.task-link').attr('href');
    });
JS;

$this->registerJs($script);
