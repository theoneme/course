<?php

use yii\helpers\Html;
use yii\web\View;

/**
 * @var View $this
 * @var array $lesson
 * @var integer $id
 * @var boolean $done
 * @var string $token
 * @var string $lessonUrl
 */

?>

<div class="<?= $done ? 'done' : ''?> lesson-progress position-relative font-weight-bold d-flex align-items-center">
    <div class="circle-progress position-relative bg-white rounded-circle d-flex align-items-center justify-content-around mr-3">
        <?= $done ? '<i class="i-correct"></i>' : $lesson['sort']?>
    </div>
    <?= Html::a($lesson['title'], [$lessonUrl, 'id' => $id, 'course_token' => $token])?>
</div>