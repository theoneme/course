<?php

use common\helpers\UtilityHelper;
use common\models\Course;
use common\models\User;
use common\models\UserCourse;
use frontend\assets\DashboardAsset;
use yii\helpers\Html;
use yii\web\View;

/**
 * @var View $this
 * @var array $courses
 * @var array $lessonProgress
 * @var UserCourse[] $userCourses
 */

DashboardAsset::register($this);
?>

<div class="wrapper-dashboard p-3 mx-auto">
    <section class="w-100 border rounded bg-white">
        <?php foreach ($courses as $course) {
            $token = $userCourses[$course['id']]['token'] ?? null;
            $lessonUrl = ((int)$course['type']) === Course::TYPE_DEFAULT ? '/lesson/view' : '/lesson/task';
            $nextLessonId = null;
        ?>
            <div class="course-block border-bottom">
                <div class="d-flex p-4">
                    <div class="img-course mr-3 d-none d-sm-block d-md-block d-lg-block">
                        <?= Html::img($course['image'], ['class' => 'w-100', 'alt' => $course['title'], 'title' => $course['title']])?>
                        <div class="m-3 text-right price">
                            <?= $course['priceFormatted']?>
                        </div>
                    </div>
                    <div class="">
                        <h3 class="font-weight-bold dashboard-course-name my-2">
                            <?php
                                $count = count($course['lessons']);
                                $showMoreText = $count > 3
                                    ? '. ' . Yii::t('app', 'Show all lessons ({count})', ['count' => $count]) . ' <i class="i-down-arrow d-inline-block ml-2"></i>'
                                    : '';
                                echo Html::a($course['title'] . $showMoreText, '#', ['class' => 'course-name']);
                            ?>
                        </h3>
                        <p>
                            <?= $course['description']?>
                        </p>
                        <div class="lessons-list">
                            <?php foreach ($course['lessons'] as $id => $lesson) {
                                $done = isset($lessonProgress[$id]);
                                echo $this->render('lesson-item', [
                                    'id' => $id,
                                    'lesson' => $lesson,
                                    'done' => $done,
                                    'token' => $token,
                                    'lessonUrl' => $lessonUrl
                                ]);
                                if (!isset($nextLessonId) && !$done) {
                                    $nextLessonId = $id;
                                }
                            } ?>
                        </div>
                    </div>
                </div>
                <?php if(!empty($course['lessons'])) {
                    if ($token) {
                        echo Html::a(
                            Yii::t('app', 'Continue studying'),
                            [$lessonUrl, 'id' => $nextLessonId ?? $id, 'course_token' => $token],
                            ['class' => 'button orange rounded-0 w-100 d-block text-center font-big']
                        );
                    }
                    else {
                        echo Html::a(
                            Yii::t('app', 'Try for free'),
                            ["/preview$lessonUrl", 'id' => $nextLessonId ?? $id, 'course_id' => $course['id']],
                            ['class' => 'button blue rounded-0 w-100 d-block text-center font-big']
                        );
                        echo Html::a(
                            Yii::t('app', 'Pay for the course'),
                            ['/payment/sberbank', 'course_id' => $course['id']],
                            ['class' => 'button orange rounded-0 w-100 d-block text-center font-big']
                        );
                    }
                } ?>
            </div>
        <?php } ?>
    </section>
</div>

<?php
$script = <<<JS
    $('.dashboard-course-name').on('click',function () {
        $(this).closest('.course-block').find('.lessons-list').toggleClass('open');
        $(this).find('i').toggleClass('i-down-arrow').toggleClass('i-up-arrow');
        return false;
    })
JS;
$this->registerJs($script);