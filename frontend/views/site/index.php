<?php

use frontend\assets\IndexAsset;
use frontend\assets\plugins\FancyBoxAsset;
use frontend\assets\plugins\FontAwesomeAsset;
use frontend\assets\plugins\SlickAsset;
use frontend\forms\ContactForm;
use yii\helpers\Html;
use yii\helpers\Url;
use yii\web\View;

/**
 * @var View $this
 * @var array $categories
 * @var array $products
 * @var ContactForm $contactForm
 */

SlickAsset::register($this);
//FontAwesomeAsset::register($this);
FancyBoxAsset::register($this);
IndexAsset::register($this);

?>

<section class="banner overflow-hidden" style="background: url(<?= Url::to(["/images/header_image.jpg"]) ?>);background-size: cover">
    <div class="wrapper">
        <div class="center-block text-center">
            <h1><?= Yii::t('app', 'Banner title') ?></h1>
            <h4><?= Yii::t('app', 'Banner subtitle') ?></h4>
        </div>
    </div>
</section>


<section class="explore light-grey-bg">
    <div class="wrapper">
        <div class="container-fluid">
            <h2 class="text-center"><?= Yii::t('app', 'Category block title') ?></h2>
<!--            <h5 class="text-center">-->
<!--                <i class="icon-location-pointer"></i>-->
<!--                --><?//= $address?>
<!--            </h5>-->
            <div class="row">
                <?php foreach ($categories as $slug => $category) {
                    echo $this->render('/site/partial/category', ['category' => $category]);
                } ?>
            </div>
        </div>
    </div>
</section>


<section class="about-us">
    <div class="wrapper">
        <div class="container-fluid">
            <div class="row">
                <div class="col-12 col-md-8">
                    <h2><?= Yii::t('app', 'The advantages of working with our Real Estate Agency {name}', ['name' => Yii::$app->name]) ?></h2>
                    <div class="row">
                        <div class="col-12 col-md-6">
                            <div class="about-block">
                                <h4>
                                    &#9900;
                                    <?= Yii::t('app', 'Local market knowledge') ?>
                                </h4>
                                <p>
                                    <?= Yii::t('app', 'We are engaged in real estate for 10 years. We are well versed in local legislation and will help you arrange a product.') ?>
                                </p>
                            </div>

                            <div class="about-block">
                                <h4>
                                    &#9900;
                                    <?= Yii::t('app', 'Experienced agents') ?>
                                </h4>
                                <p>
                                    <?= Yii::t('app', 'Our company employs 15 experienced agents who are licensed to work with real estate in Spain.') ?>
                                </p>
                            </div>
                        </div>
                        <div class="col-12 col-md-6">
                            <div class="about-block">
                                <h4>
                                    &#9900;
                                    <?= Yii::t('app', 'Legal support') ?>
                                </h4>
                                <p>
                                    <?= Yii::t('app', 'Our company employs lawyers who will help you arrange real estate in accordance with all local laws and will help verify product rights and protect against risks.') ?>
                                </p>
                            </div>
                            <div class="about-block">
                                <p>
                                    <?= Yii::t('app', 'We will help you choose and arrange a product. And do it with professionalism and love.') ?>
                                </p>
                            </div>
                        </div>
                    </div>
                </div>
                <?= Html::tag('div', '', [
                    'class' => 'd-none d-md-block col-md-4 about-us-img',
                    'style' => "background: url(" . (Url::to(["/images/about_us.jpg"])) . ");background-size: cover",
                ])?>
            </div>
            <div class="about-block">
                <?= Html::a(
                    Yii::t('app', 'Explore Products'),
                    ['/product/catalog/index', 'category' => 'wine'],
                    ['class' => "button big white-blue"]
                )?>
            </div>
        </div>
    </div>
</section>

<section class="light-grey-bg products">
    <div class="wrapper">
        <div class="container-fluid">
            <h2 class="text-center">
                <?= Yii::t('app', 'Our newest products') ?>
            </h2>
            <h5 class="text-center grey">
                <?= Yii::t('app', 'Every day new objects come to us which you can see in our catalog.') ?>
            </h5>
            <div class="row">
                <?php if (!empty($products)) {
                    foreach ($products as $product) { ?>
                        <div class="col-12 col-sm-6 col-md-3">
                            <?= $this->render('@frontend/views/common/partial/product-grid', ['product' => $product]); ?>
                        </div>
                    <?php } ?>
                <?php } ?>
            </div>

            <div class="text-center">
                <?= Html::a(
                    Yii::t('app', 'Explore Products'),
                    ['/product/catalog/index', 'category' => 'wine'],
                    ['class' => "button transparent big d-inline-block"]
                )?>
            </div>
        </div>
    </div>
</section>

<section class="light-grey-bg" id="contact-block">
    <div class="map" id="gmap">

    </div>
    <div class="container-fluid">
        <h2 class="text-center"><?= Yii::t('app', 'Contact title') ?></h2>
        <h6 class="text-center grey"><?= Yii::t('app', 'Contact subtitle') ?></h6>
        <?= $this->render('/common/contact-form', ['model' => $contactForm]) ?>
    </div>
</section>

<?php
$script = <<<JS
    $('[data-fancybox="gallery"]').fancybox({
        baseClass: 'fancybox-advanced',
        hash: false,
        buttons: [
            'close'
        ],
        infobar: false,
        thumbs: false,
        mobile: {
            infobar: false,
            caption: false,
            thumbs: false
        },
        btnTpl: {
            close: '<button data-fancybox-close class="fancybox-button fancybox-button--close" title="{{CLOSE}}"><svg viewBox="0 0 60 60"><path d="M0,0 L60,60 M60,0 L0,60"/></svg></button>',
            arrowLeft: '<a data-fancybox-prev class="fancybox-button fancybox-button--arrow_left" title="{{PREV}}" href="javascript:;"><svg viewBox="0 0 60 60"><path d="M60,0 L30,30 L60,60"></path></svg></a>',
            arrowRight: '<a data-fancybox-next class="fancybox-button fancybox-button--arrow_right" title="{{NEXT}}" href="javascript:;"><svg viewBox="0 0 60 60"><path d="M0,0 L30,30 L0,60"></path></svg></a>'
        }
    });
JS;

$this->registerJs($script);