<?php

use frontend\assets\ErrorAsset;
use yii\base\Action;
use yii\base\Controller;
use yii\helpers\Html;
use yii\web\Application;

/**
 * @var $this \yii\web\View
 * @var \yii\base\Exception $exception
 * @var $content string
 */

ErrorAsset::register($this);
$this->title = $exception->getMessage();
Yii::$app->controller = new Controller('site', Yii::$app);
Yii::$app->controller->action = new Action('index', Yii::$app->controller);
Yii::$app->trigger(Application::EVENT_BEFORE_ACTION);

$statusCode = $exception->statusCode ?? 500;
?>
<?php $this->beginPage() ?>
<!DOCTYPE html>
<html lang="<?= Yii::$app->language ?>">
    <head>
        <meta charset="<?= Yii::$app->charset ?>">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <link rel="shortcut icon" href="/images/favicon.png" type="image/x-icon"/>
        <?= Html::csrfMetaTags() ?>
        <title><?= Html::encode($this->title) ?></title>
        <?php $this->head() ?>
    </head>
    <body>
        <?php $this->beginBody() ?>
        <div class="mainblock">
            <div class="all-content">
                <h1 class="text-left"><?= Yii::t('app', 'Something is wrong'); ?></h1>
                <?= $exception->getMessage()?>
                <div class="mfooter"></div>
            </div>
        </div>

        <?php $this->endBody() ?>
    </body>
</html>
<?php $this->endPage() ?>
