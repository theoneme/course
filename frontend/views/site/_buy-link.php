<?php
use yii\helpers\Html;
use yii\web\View;

/**
 * @var View $this
 * @var string $class
 */

//if (Yii::$app->user->isGuest) {
//    echo Html::a(
//        '<span>' . Yii::t('app', 'View') . '</span>' . $this->render('/common/partial/arrow-left', ['class' => 'arrow-prev']) . $this->render('/common/partial/arrow-left', ['class' => 'arrow-next']),
//        '#',
//        ['class' => 'button with-arrow show-register-buy-text ' . $class, 'data' => ['target' => '#login', 'toggle' => 'modal']]
//    );
//}
//else {
    echo Html::a(
        '<span>' . Yii::t('app', 'View') . '</span>' . $this->render('/common/partial/arrow-left', ['class' => 'arrow-prev']) . $this->render('/common/partial/arrow-left', ['class' => 'arrow-next']),
        ['/dashboard/index'],
        ['class' => 'button with-arrow ' . $class]
    );
//}