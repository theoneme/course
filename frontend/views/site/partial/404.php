<?php

?>

<div class="container box-404">
    <div class="brick"></div>
    <div class="number text-right">
        <div class="four"></div>
        <div class="zero">
            <div class="nail"></div>
        </div>
        <div class="four"></div>
    </div>
    <div class="info text-left">
        <h1 class="text-left"><?= Yii::t('app', 'Something is wrong'); ?></h1>
        <p><?= Yii::t('app', 'The page you are looking for was moved, removed, renamed or might never existed.'); ?></p>
        <a href="/" class="btn btn-blue-white btn-big"><?= Yii::t('app', 'Go Home') ?></a>
    </div>
</div>
