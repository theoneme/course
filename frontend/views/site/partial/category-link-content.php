<?php

use yii\web\View;

/**
 * @var View $this
 * @var array $category
 */

?>

<div class='explore-info text-center'>
    <h3><?= $category['title']?></h3>
</div>