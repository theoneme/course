<?php

use yii\helpers\Html;
use yii\helpers\Url;
use yii\web\View;

/**
 * @var View $this
 * @var array $category
 * @var array $fields
 * @var integer $index
 */


$img = Url::to(["/images/{$category['slug']}.jpg"]);
$linkOptions = [
    'class' => 'explore-prop d-block',
    'style' => "background:url({$img});background-size: cover",
];
?>

<div class="col-12 col-md-4 explore-prop-block">
    <?= Html::a(
        $this->render('category-link-content', ['category' => $category]),
        ['/product/catalog/index', 'category' => $category['slug']],
        $linkOptions
    )?>
</div>
