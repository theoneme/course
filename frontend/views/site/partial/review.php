<blockquote class="review-cite">
    <cite class="grey text-center d-block">
        "Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum
        has been the industry's standard dummy text ever since the 1500s, when an unknown
        printer took a galley of type and scrambled"
    </cite>
    <div class="text-center">
        <div class="d-inline-flex team">
            <div class="img">
                <img src="../images/sara-1.png" alt="">
            </div>
            <div class="text text-left">
                <div class="name">
                    Sara Strawberry
                </div>
                <div class="company grey">
                    Someone from company
                </div>
            </div>
        </div>
    </div>
</blockquote>