<?php

?>

<div class="container box-404">
    <div class="brick"></div>
    <div class="number text-right">
        <div class="five">
            <div class="nail"></div>
            4
        </div>
        <div class="zero">
            <div class="nail"></div>
        </div>
        <div class="five">
            <div class="nail"></div>
            3
        </div>
    </div>
    <div class="info text-left">
        <h1 class="text-left"><?= Yii::t('app', 'Something is wrong'); ?></h1>
        <p><?= Yii::t('app', 'Access Denied. You don’t have permission to access.'); ?></p>
        <a href="/" class="btn btn-blue-white btn-big"><?= Yii::t('app', 'Go Home') ?></a>
    </div>
</div>