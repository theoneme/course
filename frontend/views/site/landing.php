<?php

use common\models\Task;
use frontend\assets\LandingAsset;
use frontend\assets\plugins\SlickAsset;
use yii\helpers\Html;
use yii\web\View;

/**
 * @var View $this
 * @var boolean $showLoginModal
 * @var array $task
 */

SlickAsset::register($this);
LandingAsset::register($this);

$bundle = $this->getAssetManager()->getBundle(LandingAsset::class);

?>

    <section class="wrapper mx-auto d-block">
        <div class="slider px-3 px-sm-5">
            <div class="main-slide with-highlight">
                <div class="row align-items-center">
                    <div class="col-12 col-lg-6 text-center text-lg-left pb-5 py-sm-5">
                        <h2 class="h1-title font-weight-bold">
                            Вся математика для
                            <br/>
                            подготовки к
                            <span class="highlight overflow-hidden" data-text="ЕГЭ на 85+ баллов">
                                <span>ЕГЭ на 85+ баллов</span>
                            </span>
                        </h2>
                        <p class="text-medium mb-3 mb-lg-5">
                            30 тем, 60 задач. Интерактивный онлайн курс подготовки к ЕГЭ.
                            Стоимость 990 рублей.
                        </p>
                        <a href="#" class="d-inline-block button with-arrow to-next mb-3 mr-3">
                            <span>Подробнее</span>
                            <svg class="arrow-prev" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 40 40">
                                <path d="M0,20 L40,20 M33,13 L39,20 L33,27"></path>
                            </svg>
                            <svg class="arrow-next" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 40 40">
                                <path d="M0,20 L40,20 M33,13 L39,20 L33,27"></path>
                            </svg>
                        </a>
                        <?= $this->render('_buy-course-link')?>
                    </div>
                    <div class="col-12 col-lg-6">
                        <div class="img text-center">
                            <?= Html::img("{$bundle->baseUrl}/images/slide1.png", [
                                'alt' => 'ЕГЭ 2020 Онлайн курс подготовки',
                                'title' => 'Курс подготовки к сдаче ЕГЭ 2020',
                                'class' => 'd-inline-block'
                            ]) ?>
                        </div>
                    </div>
                </div>
            </div>
            <div class="main-slide with-highlight">
                <div class="row align-items-center">

                    <div class="col-12 col-lg-6 text-center text-lg-left pb-5 py-sm-5">
                        <h2 class="font-weight-bold">
                            Теория и практические задания
                            <span class="highlight overflow-hidden" data-text="Проверка результата онлайн">
                                <span>Проверка результата онлайн</span>
                            </span>
                        </h2>
                        <p class="text-medium mb-3 mb-lg-5">
                            Онлайн тестирование помогает определить уровень знаний. И восполнить пробелы по актуальным
                            темам.
                        </p>
                        <a href="#" class="d-inline-block button with-arrow to-next mb-3 mr-3">
                            <span>Подробнее</span>
                            <svg class="arrow-prev" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 40 40">
                                <path d="M0,20 L40,20 M33,13 L39,20 L33,27"></path>
                            </svg>
                            <svg class="arrow-next" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 40 40">
                                <path d="M0,20 L40,20 M33,13 L39,20 L33,27"></path>
                            </svg>
                        </a>
                        <?= $this->render('_buy-course-link')?>
                    </div>
                    <div class="col-12 col-lg-6">
                        <div class="img text-center">
                            <?= Html::img("{$bundle->baseUrl}/images/slide3.png", [
                                'alt' => 'ЕГЭ 2020 Онлайн курс подготовки',
                                'title' => 'Курс подготовки к сдаче ЕГЭ 2020',
                                'class' => 'd-inline-block'
                            ]) ?>
                        </div>
                    </div>
                </div>
            </div>
            <div class="main-slide with-highlight">
                <div class="row align-items-center">
                    <div class="col-12 col-lg-6 text-center text-lg-left pb-5 py-sm-5">
                        <h2 class="h1-title font-weight-bold">
                            Вы сможете
                            <br/>
                            контролировать
                            <span class="highlight overflow-hidden" data-text="результаты вашего ребенка">
                                <span>результаты вашего ребенка</span>
                            </span>
                        </h2>
                        <p class="text-medium mb-3 mb-lg-5">
                            Интерактивная форма обучения позволяет в реальном времени следить за успехами по подготовке
                            к успешной сдаче ЕГЭ вашим ребенком.
                        </p>
                        <a href="#we-offer" class="d-inline-block button with-arrow mb-3 mr-3">
                            <span>Подробнее</span>
                            <svg class="arrow-prev" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 40 40">
                                <path d="M0,20 L40,20 M33,13 L39,20 L33,27"></path>
                            </svg>
                            <svg class="arrow-next" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 40 40">
                                <path d="M0,20 L40,20 M33,13 L39,20 L33,27"></path>
                            </svg>
                        </a>
                        <?= $this->render('_buy-course-link')?>
                    </div>
                    <div class="col-12 col-lg-6">
                        <div class="img text-center">
                            <?= Html::img("{$bundle->baseUrl}/images/slide2.png", [
                                'alt' => 'ЕГЭ 2020 Онлайн курс подготовки',
                                'title' => 'Курс подготовки к сдаче ЕГЭ 2020',
                                'class' => 'd-inline-block'
                            ]) ?>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <section class="we-offer with-highlight pb-5 wrapper mx-auto d-block" id="we-offer">
        <div>
            <svg class="w-100 bg-light-blue" xmlns="http://www.w3.org/2000/svg" x="0px" y="0px" viewBox="0 0 1680 72">
                <polygon fill="#fff" points="0,0 1680,72 1680,0"></polygon>
            </svg>
        </div>
        <div class="bg-light-blue">
            <div class="container-fluid">
                <h1 class="pt-5 pb-3 text-center font-weight-bold">
                    Онлайн курс подготовки к
                    <span class="highlight overflow-hidden" data-text="ЕГЭ 2020">
                        <span>ЕГЭ 2020</span>
                    </span>
                </h1>
                <p class="text-center wrapper-small d-block mx-auto text-medium">
                    Мы разработали интерактивный онлайн курс по подготовке к сдаче ЕГЭ. Курс позволяет:
                </p>
                <div class="row pt-5">
                    <div class="col-12 col-sm-6 col-lg-3 offer-item text-center px-4 pb-5">
                        <div class="img mb-4">
                            <?= Html::img("{$bundle->baseUrl}/images/offer1.png", [
                                'alt' => 'ЕГЭ 2020 Онлайн курс подготовки',
                                'title' => 'Курс подготовки к сдаче ЕГЭ 2020',
                                'class' => 'd-block mx-auto h-auto'
                            ]) ?>
                        </div>
                        <div class="title position-relative pb-2">
                            <h5 class="font-weight-bold">Определить уровень знаний</h5>
                            <p>
                                Система тестов определит реальный уровень подготовки вашего ребенка
                            </p>
                        </div>
                    </div>
                    <div class="col-12 col-sm-6 col-lg-3 offer-item text-center px-4 pb-5">
                        <div class="img mb-4">
                            <?= Html::img("{$bundle->baseUrl}/images/offer2.png", [
                                'alt' => 'ЕГЭ 2020 Онлайн курс подготовки',
                                'title' => 'Курс подготовки к сдаче ЕГЭ 2020',
                                'class' => 'd-block mx-auto h-auto'
                            ]) ?>
                        </div>
                        <div class="title position-relative pb-2">
                            <h5 class="font-weight-bold">Интенсивный курс восполнит пробелы</h5>
                            <p>
                                Курс поможет повторить теорию и закрепить знания через практические задания
                            </p>
                        </div>
                    </div>
                    <div class="col-12 col-sm-6 col-lg-3 offer-item text-center px-4 pb-5">
                        <div class="img mb-4">
                            <?= Html::img("{$bundle->baseUrl}/images/offer3.png", [
                                'alt' => 'ЕГЭ 2020 Онлайн курс подготовки',
                                'title' => 'Курс подготовки к сдаче ЕГЭ 2020',
                                'class' => 'd-block mx-auto h-auto'
                            ]) ?>
                        </div>
                        <div class="title position-relative pb-2">
                            <h5 class="font-weight-bold">Вы сможете видеть прогресс</h5>
                            <p>Изменение уровня подготовки вашим ребенком к сдаче ЕГЭ</p>
                        </div>
                    </div>
                    <div class="col-12 col-sm-6 col-lg-3 offer-item text-center px-4 pb-5">
                        <div class="img mb-4">
                            <?= Html::img("{$bundle->baseUrl}/images/offer4.png", [
                                'alt' => 'ЕГЭ 2020 Онлайн курс подготовки',
                                'title' => 'Курс подготовки к сдаче ЕГЭ 2020',
                                'class' => 'd-block mx-auto h-auto'
                            ]) ?>
                        </div>
                        <div class="title position-relative pb-2">
                            <h5 class="font-weight-bold">
                                Мы научим Тайм менеджменту
                            </h5>
                            <p>
                                Важное умение правильно распределить время на решение задач приводит к получению более
                                высоких баллов по ЕГЭ.
                            </p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <section id="about" class="with-highlight py-5 wrapper mx-auto d-block">
        <div class="container-fluid">
            <div class="row align-items-center">
                <div class="col-12 col-lg-6">
                    <?= Html::img("{$bundle->baseUrl}/images/slide4.png", [
                        'class' => 'd-block mx-auto mw-100',
                        'alt' => 'ЕГЭ 2020 Онлайн курс подготовки',
                        'title' => 'Курс подготовки к сдаче ЕГЭ 2020'
                    ]) ?>
                </div>
                <div class="col-12 col-lg-6">
                    <h3 class="font-weight-bold">
                        Уникальность нашего
                        <span class="highlight overflow-hidden" data-text="Интерактивного онлайн курса">
                            <span>Интерактивного онлайн курса</span>
                        </span>
                    </h3>

                    <p class="mb-5">
                        1. Интерактив и геймификация обучения дает более высокую степень вовлеченности. При такой форме обучения, материал усваивается на 65% эффективнее.
                        </br>
                        2. Выполнение практических заданий после каждого теоретического блока позволит лучше закрепить теоретический материал.
                        </br>
                        3. Тренировка решения заданий за определенное время, позволит правильно оценивать время и дает навык быстрого решения задач.
                    </p>
                    <?= $this->render('_buy-course-link')?>
                </div>
            </div>
        </div>
    </section>
    <section id="portfolio" class="py-5 wrapper mx-auto d-block with-highlight">
        <div class="container-fluid">
            <h2 class="text-center font-weight-bold">
                Пример
                <span class="highlight overflow-hidden" data-text="занятия">
                    <span>занятия</span>
                </span>
            </h2>
            <p class="text-center text-medium pb-5">
                Так выглядит типовое занятие в нашей онлайн школе.
            </p>
            <div class="row">
                <div class="col-md-12 lesson-container video mx-auto">
                    <video width="100%" height="auto" controls="controls">
                        <source src="/uploads/video/course.mp4">
                    </video>
                    <div class="w-100 d-flex justify-content-end mt-2">
                        <div class="d-flex align-items-center ml-auto mb-2">
                            <?= Html::a(Yii::t('app', 'Got it!'), '#', ['class' => 'task-link button orange small'])?>
                        </div>
                    </div>
                </div>
                <div class="col-md-12 lesson-container task mx-auto d-none">
                    <div class="p-3 mx-5">
                        <div class="d-flex align-items-center overflow-y-auto h-100 w-100">
                            <section class="wrapper-test mx-auto w-100 border rounded bg-white py-4">
                                <div class="p-4">
                                    <?php if (!$task['attachment']) {?>
                                        <h1 class="h1-test font-weight-bold"><?= $task['title']?></h1>
                                    <?php } ?>
                                    <div>
                                        <?= $task['attachment'] ? Html::img($task['attachment']) : $task['description']?>
                                    </div>
                                </div>
                                <div class="d-flex bg-grey justify-content-between bg-light-blue grey-box px-4 py-2">
                                    <div class="d-flex align-items-center">
                                        <div class="mr-3">
                                            <i class="i-verified font-weight-bold"></i>
                                        </div>
                                        <div>
                                            <h5 class="font-weight-bold"><?= Yii::t('app', 'Solve the task')?></h5>
                                        </div>
                                    </div>
                                    <div class="d-flex align-items-center">
                                        <div class="badge badge-light bg-white py-2 px-3 rounded-pill font-weight-normal tag-xp letter-spacing-02">
                                            <?= Yii::t('app', '{points, plural, one{# point} other{# points}}', ['points' => $task['points']])?>
                                        </div>
                                    </div>
                                </div>
                                <div class="answer-container">
                                    <?php switch ($task['type']) {
                                        case Task::TYPE_RADIO:
                                            echo $this->render('/lesson/partial/radio', ['task' => $task]);
                                            break;
                                        case Task::TYPE_CHECKBOX:
                                            echo $this->render('/lesson/partial/checkbox', ['task' => $task]);
                                            break;
                                        case Task::TYPE_TEXT:
                                        default:
                                            echo $this->render('/lesson/partial/textbox', ['task' => $task]);
                                            break;
                                    } ?>
                                </div>
                                <div class="d-flex flex-wrap justify-content-between pt-4 pl-4 pr-4">
                                    <div class="d-flex align-items-end take-hint font-weight-bold pr-3">
                                        <a class="ml-2 p-2 hint" href="#hint" data-toggle="collapse" role="button" aria-expanded="false">
                                            <i class="i-lamp-bulb p-2 color-blue font-weight-bold rounded-top"></i>
                                            <span class="hint-text"><?= Yii::t('app', 'Show solution')?></span>
                                        </a>
                                    </div>

                                    <div class="d-flex align-items-center ml-auto mb-2">
                                        <?= Html::a(Yii::t('app', 'Submit Answer'), '#', ['class' => 'task-check-button button blue'])?>
                                    </div>
                                </div>
                                <div class="pb-3 pl-3 pr-3 collapse" id="hint">
                                    <div class="p-2 bg-light-blue p-3 collapse-hint">
                                        <h6 class="font-weight-bold py-2"><?= Yii::t('app', 'Solution')?></h6>
                                        <p class="">
                                            <?= $task['answerAttachment'] ? Html::img($task['answerAttachment']) : $task['solution']?>
                                        </p>
                                    </div>
                                </div>
                                <div class="d-flex flex-wrap justify-content-between pt-4 pl-4 pr-4">
                                    <div class="my-4 justify-content-end align-items-center">
                                        <div class="d-flex mr-3">
                                            <?= Yii::t('app', 'Time for task')?>:
                                            <div id="timeStart">&nbsp;<?= Yii::t('app', '{minutes} m', ['minutes' => $task['minutes']])?></div>
                                        </div>
                                        <br>
                                        <div class="d-flex mr-3">
                                            <?= Yii::t('app', 'Time left')?>:
                                            <div id="timeEnd"></div>
                                        </div>
                                    </div>
                                </div>
                            </section>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>

    <section id="tariff" class="py-5 wrapper mx-auto d-block">
        <div>
            <svg class="w-100 bg-light-blue" xmlns="http://www.w3.org/2000/svg" x="0px" y="0px" viewBox="0 0 1680 72">
                <polygon fill="#fff" points="0,0 1680,72 1680,0"></polygon>
            </svg>
        </div>
        <div class="bg-light-blue py-5 pb-3">
            <div class="container-fluid px-xl-5">
                <div class="row">
                    <div class="col-12 col-sm-6 col-lg-4 py-5 text-center text-sm-left">
                        <div class="w-100 d-flex flex-wrap flex-sm-nowrap">
                            <div class="mx-auto mr-sm-4 d-block">
                                <div class="tariff-price font-weight-bold d-flex">
                                    <span>1</span>
                                    <span class="currency">й</span>
                                </div>
                            </div>

                            <div class="w-100">
                                <div class="tariff-title font-weight-bold mt-2 mb-4">599 рублей</div>
                                <p class="text-medium">Онлайн симуляция ЕГЭ по математике. Позволяет оценить уровень
                                    знаний</p>
                                <ul class="no-list check color-grey letter-spacing-02 mb-5">
                                    </br>
                                    <li>Онлайн тестирование по математике</li>
                                    <li>Рекомендации по подготовке к сдаче ЕГЭ</li>
                                </ul>
                                <?= $this->render('_buy-link', ['class' => 'blue'])?>
                            </div>
                        </div>
                    </div>
                    <div class="col-12 col-sm-6 col-lg-4 py-5 text-center text-sm-left">
                        <div class="w-100 d-flex flex-wrap flex-sm-nowrap">
                            <div class="mx-auto mr-sm-4 d-block">
                                <div class="tariff-price font-weight-bold d-flex">
                                    <span>2</span>
                                    <span class="currency">й</span>
                                </div>
                            </div>
                            <div class="w-100">
                                <div class="tariff-title font-weight-bold mt-2 mb-4">890 рублей</div>
                                <p class="text-medium">Онлайн курс по подготовке к ЕГЭ по математике (Базовый уровень)</p>
                                <ul class="no-list check color-grey letter-spacing-02 mb-5">
                                    <li>Интерактивный онлайн курс по 12 темам</li>
                                    <li>Онлайн тестирование по математике 12 заданий</li>
                                    <li>Рекомендации по подготовке к сдаче ЕГЭ</li>
                                </ul>
                                <?= $this->render('_buy-link', ['class' => 'blue'])?>
                            </div>
                        </div>
                    </div>
                    <div class="col-12 col-sm-6 col-lg-4 py-5 text-center text-sm-left">
                        <div class="w-100 d-flex flex-wrap flex-sm-nowrap">
                            <div class="mx-auto mr-sm-4 d-block">
                                <div class="tariff-price font-weight-bold d-flex">
                                    <span>3</span>
                                    <span class="currency">й</span>
                                </div>
                            </div>
                            <div class="w-100">
                                <div class="tariff-title font-weight-bold mt-2 mb-4">990 рублей</div>
                                <p class="text-medium">Полный онлайн курс по подготовке к ЕГЭ по математике (Профильный уровень)</p>
                                <ul class="no-list check color-grey letter-spacing-02 mb-5">
                                    <li>Интерактивный онлайн курс по 30 темам</li>
                                    <li>Онлайн тестирование по математике 19 заданий</li>
                                    <li>Рекомендации по подготовке к сдаче ЕГЭ</li>
                                </ul>
                                <?= $this->render('_buy-link', ['class' => 'orange'])?>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

        </div>
    </section>

    <!--<footer class="d-flex flex-wrap justify-content-center p-3 p-sm-5">-->
    <!--    <a href="/site/privacy-policy" class="m-3">Обработка персональных данных</a>-->
    <!--    <a href="/site/terms-of-service" class="m-3">Пользовательское соглашение </a>-->
    <!--</footer>-->

    <a href="#" id="go-top" class="go-top rounded-circle">
        <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 40 40">
            <path d="M20,40 L20,0 M13,7 L20,1 L26,7"></path>
        </svg>
        <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 40 40">
            <path d="M20,40 L20,0 M13,7 L20,1 L26,7"></path>
        </svg>
    </a>

<?php

if (Yii::$app->user->isGuest && $showLoginModal) {
    $this->registerJs("
        $('#registration').modal('show');
    ");
}

$minutes = $task['minutes'] ?? 0;

$script = <<<JS
    $('video').on('ended', function(){
        $('.task-link').trigger('click');
    });

    $(".slider").slick({
        slidesToShow: 1,
        slidesToScroll: 1,
        dots: true,
        centerMode: false,
        arrows: false,
        infinite: true
    });

    function appear(){
        $('.with-highlight:not(.appear)').each(function () {
            if ($(document).scrollTop() + $(window).height()*0.8 > $(this).offset().top) {
                $(this).addClass('appear');
            }
        });
    }

    function square() {
        $('.square-big').height($('.square-big').width());
        $('.square-small').height($('.square-small').width());
        $('.square-long').height($('.square-big').width());
    }

    $(document).ready(function () {
        appear();
        square();
    });

    $(window).scroll(function() {
       appear();
       let bTop = $('.go-top');
       if ($(document).scrollTop() > 200) {
           if (!bTop.hasClass('on')) {
               bTop.addClass('on');
           }
       } else {
           if (bTop.hasClass('on')) {
               bTop.removeClass('on');
           }
       }
    });

    $(window).resize(function () {
        square();
    });

    $(document.body).on('click', '#go-top', function() {
        $('html, body').animate({scrollTop: 0},1000);
        return false;
    });

    $('.slider').on('afterChange', function(){
        appear();
    });

    $('.slider').on('beforeChange', function(){
        $('.main-slide').removeClass('appear');
    });

    $(document.body).on('click' ,'.to-next', function () {
        $(".slider").slick('slickNext');
    });
    $(document.body).on('click' ,'.show-register-buy-text', function () {
        $(".register-buy-text").removeClass('d-none').addClass('d-block');
    });
    $(document.body).on('click' ,'.task-link', function () {
        $(".lesson-container").toggleClass('d-none');
        return false;
    });
    $(document.body).on('click', '.task-check-button', function(){
         return false;
    });
    
    $('#registration, #login').on('hidden.bs.modal', function () {
        $(".register-buy-text").removeClass('d-block').addClass('d-none');
    });

    $('.gohref').click(function() {
        let el = $(this).attr('href');
        if (el !== '#') {
            $('body,html').animate({
                scrollTop: $(el).offset().top
            }, 1000);
            return false;
        }
    });
    
    
    let countDown = $minutes * 60* 1000;
    let now = 0;
    let isPaused = false;

    let x = setInterval(function() {

        if(!isPaused) {
            let distance = countDown - now;

            let minutes = Math.floor((distance % (1000 * 60 * 60)) / (1000 * 60));
            let seconds = Math.floor((distance % (1000 * 60)) / 1000);

            document.getElementById("timeEnd").innerHTML = "&nbsp" + minutes + "м " + seconds + "с ";

            now = now + 1000;

            if (distance < 0) {
                clearInterval(x);
                document.getElementById("timeEnd").innerHTML = "Время вышло";
            }
        }
    }, 1000);

JS;

$this->registerJs($script);
